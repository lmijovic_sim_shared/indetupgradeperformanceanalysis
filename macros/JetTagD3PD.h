//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug  9 13:41:02 2011 by ROOT version 5.28/00e
// from TTree btagd3pd/btagd3pd
// found on file: root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.tcorneli.mc11_14TeV.105568.pythia_ttbar.JetTagD3PD.ibl02.test6.110706101606/user.tcorneli.000301.EXT0._00001.JetTagD3PD.root
//////////////////////////////////////////////////////////

#ifndef JetTagD3PD_h
#define JetTagD3PD_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

class JetTagD3PD {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           jet_antikt4truth_n;
   vector<float>   *jet_antikt4truth_E;
   vector<float>   *jet_antikt4truth_pt;
   vector<float>   *jet_antikt4truth_m;
   vector<float>   *jet_antikt4truth_eta;
   vector<float>   *jet_antikt4truth_phi;
   vector<float>   *jet_antikt4truth_flavor_weight_Comb;
   vector<float>   *jet_antikt4truth_flavor_weight_IP2D;
   vector<float>   *jet_antikt4truth_flavor_weight_IP3D;
   vector<float>   *jet_antikt4truth_flavor_weight_SV0;
   vector<float>   *jet_antikt4truth_flavor_weight_SV1;
   vector<float>   *jet_antikt4truth_flavor_weight_SV2;
   vector<float>   *jet_antikt4truth_flavor_weight_JetProb;
   vector<float>   *jet_antikt4truth_flavor_weight_TrackCounting2D;
   vector<float>   *jet_antikt4truth_flavor_weight_SoftMuonTag;
   vector<float>   *jet_antikt4truth_flavor_weight_SoftElectronTag;
   vector<float>   *jet_antikt4truth_flavor_weight_JetFitterTagNN;
   vector<float>   *jet_antikt4truth_flavor_weight_JetFitterCOMBNN;
   vector<int>     *jet_antikt4truth_flavor_truth_label;
   vector<float>   *jet_antikt4truth_flavor_truth_dRminToB;
   vector<float>   *jet_antikt4truth_flavor_truth_dRminToC;
   vector<float>   *jet_antikt4truth_flavor_truth_dRminToT;
   vector<int>     *jet_antikt4truth_flavor_truth_BHadronpdg;
   vector<float>   *jet_antikt4truth_flavor_truth_vx_x;
   vector<float>   *jet_antikt4truth_flavor_truth_vx_y;
   vector<float>   *jet_antikt4truth_flavor_truth_vx_z;
   vector<int>     *jet_antikt4truth_flavor_putruth_label;
   vector<float>   *jet_antikt4truth_flavor_putruth_dRminToB;
   vector<float>   *jet_antikt4truth_flavor_putruth_dRminToC;
   vector<float>   *jet_antikt4truth_flavor_putruth_dRminToT;
   vector<int>     *jet_antikt4truth_flavor_putruth_BHadronpdg;
   vector<float>   *jet_antikt4truth_flavor_putruth_vx_x;
   vector<float>   *jet_antikt4truth_flavor_putruth_vx_y;
   vector<float>   *jet_antikt4truth_flavor_putruth_vx_z;
   vector<int>     *jet_antikt4truth_flavor_component_assoctrk_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_assoctrk_index;
   vector<vector<int> > *jet_antikt4truth_flavor_component_assoctrk_signOfIP;
   vector<vector<int> > *jet_antikt4truth_flavor_component_assoctrk_signOfZIP;
   vector<vector<float> > *jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx;
   vector<vector<float> > *jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx;
   vector<vector<float> > *jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx;
   vector<vector<float> > *jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx;
   vector<int>     *jet_antikt4truth_flavor_component_gentruthlepton_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_gentruthlepton_index;
   vector<vector<short> > *jet_antikt4truth_flavor_component_gentruthlepton_origin;
   vector<vector<int> > *jet_antikt4truth_flavor_component_gentruthlepton_slbarcode;
   vector<float>   *jet_antikt4truth_flavor_component_ip2d_pu;
   vector<float>   *jet_antikt4truth_flavor_component_ip2d_pb;
   vector<int>     *jet_antikt4truth_flavor_component_ip2d_isValid;
   vector<int>     *jet_antikt4truth_flavor_component_ip2d_ntrk;
   vector<float>   *jet_antikt4truth_flavor_component_ip3d_pu;
   vector<float>   *jet_antikt4truth_flavor_component_ip3d_pb;
   vector<int>     *jet_antikt4truth_flavor_component_ip3d_isValid;
   vector<int>     *jet_antikt4truth_flavor_component_ip3d_ntrk;
   vector<float>   *jet_antikt4truth_flavor_component_sv1_pu;
   vector<float>   *jet_antikt4truth_flavor_component_sv1_pb;
   vector<int>     *jet_antikt4truth_flavor_component_sv1_isValid;
   vector<float>   *jet_antikt4truth_flavor_component_sv2_pu;
   vector<float>   *jet_antikt4truth_flavor_component_sv2_pb;
   vector<int>     *jet_antikt4truth_flavor_component_sv2_isValid;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_pu;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_pb;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_pc;
   vector<int>     *jet_antikt4truth_flavor_component_jfit_isValid;
   vector<float>   *jet_antikt4truth_flavor_component_jfitcomb_pu;
   vector<float>   *jet_antikt4truth_flavor_component_jfitcomb_pb;
   vector<float>   *jet_antikt4truth_flavor_component_jfitcomb_pc;
   vector<int>     *jet_antikt4truth_flavor_component_jfitcomb_isValid;
   vector<int>     *jet_antikt4truth_flavor_component_jfit_nvtx;
   vector<int>     *jet_antikt4truth_flavor_component_jfit_nvtx1t;
   vector<int>     *jet_antikt4truth_flavor_component_jfit_ntrkAtVx;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_efrc;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_mass;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_sig3d;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_deltaPhi;
   vector<float>   *jet_antikt4truth_flavor_component_jfit_deltaEta;
   vector<int>     *jet_antikt4truth_flavor_component_ipplus_trk_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_ipplus_trk_index;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_d0val;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_d0sig;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_z0val;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_z0sig;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_w2D;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_w3D;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_pJP;
   vector<vector<float> > *jet_antikt4truth_flavor_component_ipplus_trk_pJPneg;
   vector<vector<int> > *jet_antikt4truth_flavor_component_ipplus_trk_grade;
   vector<vector<int> > *jet_antikt4truth_flavor_component_ipplus_trk_isFromV0;
   vector<int>     *jet_antikt4truth_flavor_component_svp_isValid;
   vector<int>     *jet_antikt4truth_flavor_component_svp_ntrkv;
   vector<int>     *jet_antikt4truth_flavor_component_svp_ntrkj;
   vector<int>     *jet_antikt4truth_flavor_component_svp_n2t;
   vector<float>   *jet_antikt4truth_flavor_component_svp_mass;
   vector<float>   *jet_antikt4truth_flavor_component_svp_efrc;
   vector<float>   *jet_antikt4truth_flavor_component_svp_x;
   vector<float>   *jet_antikt4truth_flavor_component_svp_y;
   vector<float>   *jet_antikt4truth_flavor_component_svp_z;
   vector<float>   *jet_antikt4truth_flavor_component_svp_err_x;
   vector<float>   *jet_antikt4truth_flavor_component_svp_err_y;
   vector<float>   *jet_antikt4truth_flavor_component_svp_err_z;
   vector<float>   *jet_antikt4truth_flavor_component_svp_cov_xy;
   vector<float>   *jet_antikt4truth_flavor_component_svp_cov_xz;
   vector<float>   *jet_antikt4truth_flavor_component_svp_cov_yz;
   vector<float>   *jet_antikt4truth_flavor_component_svp_chi2;
   vector<int>     *jet_antikt4truth_flavor_component_svp_ndof;
   vector<int>     *jet_antikt4truth_flavor_component_svp_ntrk;
   vector<int>     *jet_antikt4truth_flavor_component_svp_trk_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_svp_trk_index;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_isValid;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_ntrkv;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_ntrkj;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_n2t;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_mass;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_efrc;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_x;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_y;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_z;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_err_x;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_err_y;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_err_z;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_cov_xy;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_cov_xz;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_cov_yz;
   vector<float>   *jet_antikt4truth_flavor_component_sv0p_chi2;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_ndof;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_ntrk;
   vector<int>     *jet_antikt4truth_flavor_component_sv0p_trk_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_sv0p_trk_index;
   vector<int>     *jet_antikt4truth_flavor_component_softmuoninfo_muon_n;
   vector<vector<float> > *jet_antikt4truth_flavor_component_softmuoninfo_muon_w;
   vector<vector<float> > *jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel;
   vector<int>     *jet_antikt4truth_flavor_component_softelectron_electron_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_softelectron_electron_isElectron;
   vector<vector<float> > *jet_antikt4truth_flavor_component_softelectron_electron_d0;
   vector<vector<float> > *jet_antikt4truth_flavor_component_softelectron_electron_pTRel;
   vector<int>     *jet_antikt4truth_flavor_component_VKalbadtrack_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_VKalbadtrack_index;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_theta;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_phi;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_errtheta;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_errphi;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_chi2;
   vector<float>   *jet_antikt4truth_flavor_component_jfitvx_ndof;
   vector<int>     *jet_antikt4truth_flavor_component_jfvxonjetaxis_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_jfvxonjetaxis_index;
   vector<int>     *jet_antikt4truth_flavor_component_jftwotrackvertex_n;
   vector<vector<int> > *jet_antikt4truth_flavor_component_jftwotrackvertex_index;
   vector<float>   *jet_antikt4truth_L1_dr;
   vector<int>     *jet_antikt4truth_L1_matched;
   vector<float>   *jet_antikt4truth_L2_dr;
   vector<int>     *jet_antikt4truth_L2_matched;
   vector<float>   *jet_antikt4truth_EF_dr;
   vector<int>     *jet_antikt4truth_EF_matched;
   Int_t           jfvxonjetaxis_n;
   vector<float>   *jfvxonjetaxis_vtxPos;
   vector<float>   *jfvxonjetaxis_vtxErr;
   vector<int>     *jfvxonjetaxis_trk_n;
   vector<vector<float> > *jfvxonjetaxis_trk_phiAtVx;
   vector<vector<float> > *jfvxonjetaxis_trk_thetaAtVx;
   vector<vector<float> > *jfvxonjetaxis_trk_ptAtVx;
   vector<vector<int> > *jfvxonjetaxis_trk_index;
   Int_t           jftwotrkvertex_n;
   vector<float>   *jftwotrkvertex_isNeutral;
   vector<float>   *jftwotrkvertex_chi2;
   vector<float>   *jftwotrkvertex_ndof;
   vector<float>   *jftwotrkvertex_x;
   vector<float>   *jftwotrkvertex_y;
   vector<float>   *jftwotrkvertex_z;
   vector<float>   *jftwotrkvertex_errx;
   vector<float>   *jftwotrkvertex_erry;
   vector<float>   *jftwotrkvertex_errz;
   vector<float>   *jftwotrkvertex_mass;
   vector<int>     *jftwotrkvertex_trk_n;
   vector<vector<int> > *jftwotrkvertex_trk_index;
   UInt_t          RunNumber;
   UInt_t          EventNumber;
   UInt_t          timestamp;
   UInt_t          timestamp_ns;
   UInt_t          lbn;
   UInt_t          bcid;
   UInt_t          detmask0;
   UInt_t          detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          pixelFlags;
   UInt_t          sctFlags;
   UInt_t          trtFlags;
   UInt_t          larFlags;
   UInt_t          tileFlags;
   UInt_t          muonFlags;
   UInt_t          fwdFlags;
   UInt_t          coreFlags;
   UInt_t          pixelError;
   UInt_t          sctError;
   UInt_t          trtError;
   UInt_t          larError;
   UInt_t          tileError;
   UInt_t          muonError;
   UInt_t          fwdError;
   UInt_t          coreError;
   Int_t           pileupinfo_n;
   vector<int>     *pileupinfo_time;
   vector<int>     *pileupinfo_index;
   vector<int>     *pileupinfo_type;
   vector<int>     *pileupinfo_runNumber;
   vector<int>     *pileupinfo_EventNumber;
   Int_t           trk_n;
   vector<float>   *trk_d0;
   vector<float>   *trk_z0;
   vector<float>   *trk_phi;
   vector<float>   *trk_theta;
   vector<float>   *trk_qoverp;
   vector<float>   *trk_pt;
   vector<float>   *trk_eta;
   vector<float>   *trk_err_d0;
   vector<float>   *trk_err_z0;
   vector<float>   *trk_err_phi;
   vector<float>   *trk_err_theta;
   vector<float>   *trk_err_qoverp;
   vector<float>   *trk_cov_d0_z0;
   vector<float>   *trk_cov_d0_phi;
   vector<float>   *trk_cov_d0_theta;
   vector<float>   *trk_cov_d0_qoverp;
   vector<float>   *trk_cov_z0_phi;
   vector<float>   *trk_cov_z0_theta;
   vector<float>   *trk_cov_z0_qoverp;
   vector<float>   *trk_cov_phi_theta;
   vector<float>   *trk_cov_phi_qoverp;
   vector<float>   *trk_cov_theta_qoverp;
   vector<float>   *trk_IPEstimate_d0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_z0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_d0_unbiased_wrtPV;
   vector<float>   *trk_IPEstimate_z0_unbiased_wrtPV;
   vector<float>   *trk_IPEstimate_err_d0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_err_z0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_err_d0_unbiased_wrtPV;
   vector<float>   *trk_IPEstimate_err_z0_unbiased_wrtPV;
   vector<float>   *trk_IPEstimate_errPV_d0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_errPV_z0_biased_wrtPV;
   vector<float>   *trk_IPEstimate_errPV_d0_unbiased_wrtPV;
   vector<float>   *trk_IPEstimate_errPV_z0_unbiased_wrtPV;
   vector<float>   *trk_d0_wrtPV;
   vector<float>   *trk_z0_wrtPV;
   vector<float>   *trk_phi_wrtPV;
   vector<float>   *trk_err_d0_wrtPV;
   vector<float>   *trk_err_z0_wrtPV;
   vector<float>   *trk_err_phi_wrtPV;
   vector<float>   *trk_err_theta_wrtPV;
   vector<float>   *trk_err_qoverp_wrtPV;
   vector<float>   *trk_cov_d0_z0_wrtPV;
   vector<float>   *trk_cov_d0_phi_wrtPV;
   vector<float>   *trk_cov_d0_theta_wrtPV;
   vector<float>   *trk_cov_d0_qoverp_wrtPV;
   vector<float>   *trk_cov_z0_phi_wrtPV;
   vector<float>   *trk_cov_z0_theta_wrtPV;
   vector<float>   *trk_cov_z0_qoverp_wrtPV;
   vector<float>   *trk_cov_phi_theta_wrtPV;
   vector<float>   *trk_cov_phi_qoverp_wrtPV;
   vector<float>   *trk_cov_theta_qoverp_wrtPV;
   vector<float>   *trk_d0_wrtBS;
   vector<float>   *trk_z0_wrtBS;
   vector<float>   *trk_phi_wrtBS;
   vector<float>   *trk_err_d0_wrtBS;
   vector<float>   *trk_err_z0_wrtBS;
   vector<float>   *trk_err_phi_wrtBS;
   vector<float>   *trk_err_theta_wrtBS;
   vector<float>   *trk_err_qoverp_wrtBS;
   vector<float>   *trk_chi2;
   vector<int>     *trk_ndof;
   vector<int>     *trk_nBLHits;
   vector<int>     *trk_nPixHits;
   vector<int>     *trk_nSCTHits;
   vector<int>     *trk_nTRTHits;
   vector<int>     *trk_nTRTHighTHits;
   vector<int>     *trk_nPixHoles;
   vector<int>     *trk_nSCTHoles;
   vector<int>     *trk_nTRTHoles;
   vector<int>     *trk_nBLSharedHits;
   vector<int>     *trk_nPixSharedHits;
   vector<int>     *trk_nSCTSharedHits;
   vector<int>     *trk_nBLayerOutliers;
   vector<int>     *trk_nPixelOutliers;
   vector<int>     *trk_nSCTOutliers;
   vector<int>     *trk_nTRTOutliers;
   vector<int>     *trk_nTRTHighTOutliers;
   vector<int>     *trk_nContribPixelLayers;
   vector<int>     *trk_nGangedPixels;
   vector<int>     *trk_nGangedFlaggedFakes;
   vector<int>     *trk_nPixelDeadSensors;
   vector<int>     *trk_nPixelSpoiltHits;
   vector<int>     *trk_nSCTDoubleHoles;
   vector<int>     *trk_nSCTDeadSensors;
   vector<int>     *trk_nSCTSpoiltHits;
   vector<int>     *trk_expectBLayerHit;
   vector<int>     *trk_hitPattern;
   vector<int>     *trk_nSiHits;
   vector<int>     *trk_fitter;
   vector<int>     *trk_patternReco1;
   vector<int>     *trk_patternReco2;
   vector<int>     *trk_seedFinder;
   vector<vector<float> > *trk_blayerPrediction_x;
   vector<vector<float> > *trk_blayerPrediction_y;
   vector<vector<float> > *trk_blayerPrediction_z;
   vector<vector<float> > *trk_blayerPrediction_locX;
   vector<vector<float> > *trk_blayerPrediction_locY;
   vector<vector<float> > *trk_blayerPrediction_err_locX;
   vector<vector<float> > *trk_blayerPrediction_err_locY;
   vector<vector<float> > *trk_blayerPrediction_etaDistToEdge;
   vector<vector<float> > *trk_blayerPrediction_phiDistToEdge;
   vector<vector<unsigned int> > *trk_blayerPrediction_detElementId;
   vector<vector<int> > *trk_blayerPrediction_row;
   vector<vector<int> > *trk_blayerPrediction_col;
   vector<vector<int> > *trk_blayerPrediction_type;
   vector<int>     *trk_BLayer_hit_n;
   vector<vector<unsigned int> > *trk_BLayer_hit_id;
   vector<vector<unsigned int> > *trk_BLayer_hit_detElementId;
   vector<vector<int> > *trk_BLayer_hit_bec;
   vector<vector<int> > *trk_BLayer_hit_layer;
   vector<vector<float> > *trk_BLayer_hit_charge;
   vector<vector<int> > *trk_BLayer_hit_sizePhi;
   vector<vector<int> > *trk_BLayer_hit_sizeZ;
   vector<vector<int> > *trk_BLayer_hit_size;
   vector<vector<int> > *trk_BLayer_hit_isFake;
   vector<vector<int> > *trk_BLayer_hit_isGanged;
   vector<vector<int> > *trk_BLayer_hit_isSplit;
   vector<vector<int> > *trk_BLayer_hit_splitProb1;
   vector<vector<int> > *trk_BLayer_hit_splitProb2;
   vector<vector<int> > *trk_BLayer_hit_isCompetingRIO;
   vector<vector<float> > *trk_BLayer_hit_locX;
   vector<vector<float> > *trk_BLayer_hit_locY;
   vector<vector<float> > *trk_BLayer_hit_incidencePhi;
   vector<vector<float> > *trk_BLayer_hit_incidenceTheta;
   vector<vector<float> > *trk_BLayer_hit_err_locX;
   vector<vector<float> > *trk_BLayer_hit_err_locY;
   vector<vector<float> > *trk_BLayer_hit_cov_locXY;
   vector<vector<float> > *trk_BLayer_hit_x;
   vector<vector<float> > *trk_BLayer_hit_y;
   vector<vector<float> > *trk_BLayer_hit_z;
   vector<vector<float> > *trk_BLayer_hit_trkLocX;
   vector<vector<float> > *trk_BLayer_hit_trkLocY;
   vector<vector<float> > *trk_BLayer_hit_err_trkLocX;
   vector<vector<float> > *trk_BLayer_hit_err_trkLocY;
   vector<vector<float> > *trk_BLayer_hit_cov_trkLocXY;
   vector<vector<float> > *trk_BLayer_hit_locBiasedResidualX;
   vector<vector<float> > *trk_BLayer_hit_locBiasedResidualY;
   vector<vector<float> > *trk_BLayer_hit_locBiasedPullX;
   vector<vector<float> > *trk_BLayer_hit_locBiasedPullY;
   vector<vector<float> > *trk_BLayer_hit_locUnbiasedResidualX;
   vector<vector<float> > *trk_BLayer_hit_locUnbiasedResidualY;
   vector<vector<float> > *trk_BLayer_hit_locUnbiasedPullX;
   vector<vector<float> > *trk_BLayer_hit_locUnbiasedPullY;
   vector<vector<float> > *trk_BLayer_hit_chi2;
   vector<vector<int> > *trk_BLayer_hit_ndof;
   vector<int>     *trk_Pixel_hit_n;
   vector<vector<unsigned int> > *trk_Pixel_hit_id;
   vector<vector<unsigned int> > *trk_Pixel_hit_detElementId;
   vector<vector<int> > *trk_Pixel_hit_bec;
   vector<vector<int> > *trk_Pixel_hit_layer;
   vector<vector<float> > *trk_Pixel_hit_charge;
   vector<vector<int> > *trk_Pixel_hit_sizePhi;
   vector<vector<int> > *trk_Pixel_hit_sizeZ;
   vector<vector<int> > *trk_Pixel_hit_size;
   vector<vector<int> > *trk_Pixel_hit_isFake;
   vector<vector<int> > *trk_Pixel_hit_isGanged;
   vector<vector<int> > *trk_Pixel_hit_isSplit;
   vector<vector<int> > *trk_Pixel_hit_splitProb1;
   vector<vector<int> > *trk_Pixel_hit_splitProb2;
   vector<vector<int> > *trk_Pixel_hit_isCompetingRIO;
   vector<vector<float> > *trk_Pixel_hit_locX;
   vector<vector<float> > *trk_Pixel_hit_locY;
   vector<vector<float> > *trk_Pixel_hit_incidencePhi;
   vector<vector<float> > *trk_Pixel_hit_incidenceTheta;
   vector<vector<float> > *trk_Pixel_hit_err_locX;
   vector<vector<float> > *trk_Pixel_hit_err_locY;
   vector<vector<float> > *trk_Pixel_hit_cov_locXY;
   vector<vector<float> > *trk_Pixel_hit_x;
   vector<vector<float> > *trk_Pixel_hit_y;
   vector<vector<float> > *trk_Pixel_hit_z;
   vector<vector<float> > *trk_Pixel_hit_trkLocX;
   vector<vector<float> > *trk_Pixel_hit_trkLocY;
   vector<vector<float> > *trk_Pixel_hit_err_trkLocX;
   vector<vector<float> > *trk_Pixel_hit_err_trkLocY;
   vector<vector<float> > *trk_Pixel_hit_cov_trkLocXY;
   vector<vector<float> > *trk_Pixel_hit_locBiasedResidualX;
   vector<vector<float> > *trk_Pixel_hit_locBiasedResidualY;
   vector<vector<float> > *trk_Pixel_hit_locBiasedPullX;
   vector<vector<float> > *trk_Pixel_hit_locBiasedPullY;
   vector<vector<float> > *trk_Pixel_hit_locUnbiasedResidualX;
   vector<vector<float> > *trk_Pixel_hit_locUnbiasedResidualY;
   vector<vector<float> > *trk_Pixel_hit_locUnbiasedPullX;
   vector<vector<float> > *trk_Pixel_hit_locUnbiasedPullY;
   vector<vector<float> > *trk_Pixel_hit_chi2;
   vector<vector<int> > *trk_Pixel_hit_ndof;
   vector<int>     *trk_SCT_hit_n;
   vector<vector<unsigned int> > *trk_SCT_hit_id;
   vector<vector<unsigned int> > *trk_SCT_hit_detElementId;
   vector<vector<int> > *trk_SCT_hit_bec;
   vector<vector<int> > *trk_SCT_hit_layer;
   vector<vector<int> > *trk_SCT_hit_sizePhi;
   vector<vector<int> > *trk_SCT_hit_isCompetingRIO;
   vector<vector<float> > *trk_SCT_hit_locX;
   vector<vector<float> > *trk_SCT_hit_locY;
   vector<vector<float> > *trk_SCT_hit_incidencePhi;
   vector<vector<float> > *trk_SCT_hit_incidenceTheta;
   vector<vector<float> > *trk_SCT_hit_err_locX;
   vector<vector<float> > *trk_SCT_hit_err_locY;
   vector<vector<float> > *trk_SCT_hit_cov_locXY;
   vector<vector<float> > *trk_SCT_hit_x;
   vector<vector<float> > *trk_SCT_hit_y;
   vector<vector<float> > *trk_SCT_hit_z;
   vector<vector<float> > *trk_SCT_hit_trkLocX;
   vector<vector<float> > *trk_SCT_hit_trkLocY;
   vector<vector<float> > *trk_SCT_hit_err_trkLocX;
   vector<vector<float> > *trk_SCT_hit_err_trkLocY;
   vector<vector<float> > *trk_SCT_hit_cov_trkLocXY;
   vector<vector<float> > *trk_SCT_hit_locBiasedResidualX;
   vector<vector<float> > *trk_SCT_hit_locBiasedResidualY;
   vector<vector<float> > *trk_SCT_hit_locBiasedPullX;
   vector<vector<float> > *trk_SCT_hit_locBiasedPullY;
   vector<vector<float> > *trk_SCT_hit_locUnbiasedResidualX;
   vector<vector<float> > *trk_SCT_hit_locUnbiasedResidualY;
   vector<vector<float> > *trk_SCT_hit_locUnbiasedPullX;
   vector<vector<float> > *trk_SCT_hit_locUnbiasedPullY;
   vector<vector<float> > *trk_SCT_hit_chi2;
   vector<vector<int> > *trk_SCT_hit_ndof;
   vector<int>     *trk_BLayer_outlier_n;
   vector<vector<unsigned int> > *trk_BLayer_outlier_id;
   vector<vector<unsigned int> > *trk_BLayer_outlier_detElementId;
   vector<vector<int> > *trk_BLayer_outlier_bec;
   vector<vector<int> > *trk_BLayer_outlier_layer;
   vector<vector<float> > *trk_BLayer_outlier_charge;
   vector<vector<int> > *trk_BLayer_outlier_sizePhi;
   vector<vector<int> > *trk_BLayer_outlier_sizeZ;
   vector<vector<int> > *trk_BLayer_outlier_size;
   vector<vector<int> > *trk_BLayer_outlier_isFake;
   vector<vector<int> > *trk_BLayer_outlier_isGanged;
   vector<vector<int> > *trk_BLayer_outlier_isSplit;
   vector<vector<int> > *trk_BLayer_outlier_splitProb1;
   vector<vector<int> > *trk_BLayer_outlier_splitProb2;
   vector<vector<int> > *trk_BLayer_outlier_isCompetingRIO;
   vector<vector<float> > *trk_BLayer_outlier_locX;
   vector<vector<float> > *trk_BLayer_outlier_locY;
   vector<vector<float> > *trk_BLayer_outlier_incidencePhi;
   vector<vector<float> > *trk_BLayer_outlier_incidenceTheta;
   vector<vector<float> > *trk_BLayer_outlier_err_locX;
   vector<vector<float> > *trk_BLayer_outlier_err_locY;
   vector<vector<float> > *trk_BLayer_outlier_cov_locXY;
   vector<vector<float> > *trk_BLayer_outlier_x;
   vector<vector<float> > *trk_BLayer_outlier_y;
   vector<vector<float> > *trk_BLayer_outlier_z;
   vector<vector<float> > *trk_BLayer_outlier_trkLocX;
   vector<vector<float> > *trk_BLayer_outlier_trkLocY;
   vector<vector<float> > *trk_BLayer_outlier_err_trkLocX;
   vector<vector<float> > *trk_BLayer_outlier_err_trkLocY;
   vector<vector<float> > *trk_BLayer_outlier_cov_trkLocXY;
   vector<vector<float> > *trk_BLayer_outlier_locBiasedResidualX;
   vector<vector<float> > *trk_BLayer_outlier_locBiasedResidualY;
   vector<vector<float> > *trk_BLayer_outlier_locBiasedPullX;
   vector<vector<float> > *trk_BLayer_outlier_locBiasedPullY;
   vector<vector<float> > *trk_BLayer_outlier_locUnbiasedResidualX;
   vector<vector<float> > *trk_BLayer_outlier_locUnbiasedResidualY;
   vector<vector<float> > *trk_BLayer_outlier_locUnbiasedPullX;
   vector<vector<float> > *trk_BLayer_outlier_locUnbiasedPullY;
   vector<vector<float> > *trk_BLayer_outlier_chi2;
   vector<vector<int> > *trk_BLayer_outlier_ndof;
   vector<int>     *trk_Pixel_outlier_n;
   vector<vector<unsigned int> > *trk_Pixel_outlier_id;
   vector<vector<unsigned int> > *trk_Pixel_outlier_detElementId;
   vector<vector<int> > *trk_Pixel_outlier_bec;
   vector<vector<int> > *trk_Pixel_outlier_layer;
   vector<vector<float> > *trk_Pixel_outlier_charge;
   vector<vector<int> > *trk_Pixel_outlier_sizePhi;
   vector<vector<int> > *trk_Pixel_outlier_sizeZ;
   vector<vector<int> > *trk_Pixel_outlier_size;
   vector<vector<int> > *trk_Pixel_outlier_isFake;
   vector<vector<int> > *trk_Pixel_outlier_isGanged;
   vector<vector<int> > *trk_Pixel_outlier_isSplit;
   vector<vector<int> > *trk_Pixel_outlier_splitProb1;
   vector<vector<int> > *trk_Pixel_outlier_splitProb2;
   vector<vector<int> > *trk_Pixel_outlier_isCompetingRIO;
   vector<vector<float> > *trk_Pixel_outlier_locX;
   vector<vector<float> > *trk_Pixel_outlier_locY;
   vector<vector<float> > *trk_Pixel_outlier_incidencePhi;
   vector<vector<float> > *trk_Pixel_outlier_incidenceTheta;
   vector<vector<float> > *trk_Pixel_outlier_err_locX;
   vector<vector<float> > *trk_Pixel_outlier_err_locY;
   vector<vector<float> > *trk_Pixel_outlier_cov_locXY;
   vector<vector<float> > *trk_Pixel_outlier_x;
   vector<vector<float> > *trk_Pixel_outlier_y;
   vector<vector<float> > *trk_Pixel_outlier_z;
   vector<vector<float> > *trk_Pixel_outlier_trkLocX;
   vector<vector<float> > *trk_Pixel_outlier_trkLocY;
   vector<vector<float> > *trk_Pixel_outlier_err_trkLocX;
   vector<vector<float> > *trk_Pixel_outlier_err_trkLocY;
   vector<vector<float> > *trk_Pixel_outlier_cov_trkLocXY;
   vector<vector<float> > *trk_Pixel_outlier_locBiasedResidualX;
   vector<vector<float> > *trk_Pixel_outlier_locBiasedResidualY;
   vector<vector<float> > *trk_Pixel_outlier_locBiasedPullX;
   vector<vector<float> > *trk_Pixel_outlier_locBiasedPullY;
   vector<vector<float> > *trk_Pixel_outlier_locUnbiasedResidualX;
   vector<vector<float> > *trk_Pixel_outlier_locUnbiasedResidualY;
   vector<vector<float> > *trk_Pixel_outlier_locUnbiasedPullX;
   vector<vector<float> > *trk_Pixel_outlier_locUnbiasedPullY;
   vector<vector<float> > *trk_Pixel_outlier_chi2;
   vector<vector<int> > *trk_Pixel_outlier_ndof;
   vector<int>     *trk_BLayer_hole_n;
   vector<vector<unsigned int> > *trk_BLayer_hole_detElementId;
   vector<vector<int> > *trk_BLayer_hole_bec;
   vector<vector<int> > *trk_BLayer_hole_layer;
   vector<vector<float> > *trk_BLayer_hole_trkLocX;
   vector<vector<float> > *trk_BLayer_hole_trkLocY;
   vector<vector<float> > *trk_BLayer_hole_err_trkLocX;
   vector<vector<float> > *trk_BLayer_hole_err_trkLocY;
   vector<vector<float> > *trk_BLayer_hole_cov_trkLocXY;
   vector<int>     *trk_Pixel_hole_n;
   vector<vector<unsigned int> > *trk_Pixel_hole_detElementId;
   vector<vector<int> > *trk_Pixel_hole_bec;
   vector<vector<int> > *trk_Pixel_hole_layer;
   vector<vector<float> > *trk_Pixel_hole_trkLocX;
   vector<vector<float> > *trk_Pixel_hole_trkLocY;
   vector<vector<float> > *trk_Pixel_hole_err_trkLocX;
   vector<vector<float> > *trk_Pixel_hole_err_trkLocY;
   vector<vector<float> > *trk_Pixel_hole_cov_trkLocXY;
   vector<float>   *trk_primvx_weight;
   vector<int>     *trk_primvx_index;
   vector<float>   *trk_mcpart_probability;
   vector<int>     *trk_mcpart_barcode;
   vector<int>     *trk_mcpart_index;
   vector<int>     *trk_detailed_mc_n;
   vector<vector<int> > *trk_detailed_mc_nCommonPixHits;
   vector<vector<int> > *trk_detailed_mc_nCommonSCTHits;
   vector<vector<int> > *trk_detailed_mc_nCommonTRTHits;
   vector<vector<int> > *trk_detailed_mc_nRecoPixHits;
   vector<vector<int> > *trk_detailed_mc_nRecoSCTHits;
   vector<vector<int> > *trk_detailed_mc_nRecoTRTHits;
   vector<vector<int> > *trk_detailed_mc_nTruthPixHits;
   vector<vector<int> > *trk_detailed_mc_nTruthSCTHits;
   vector<vector<int> > *trk_detailed_mc_nTruthTRTHits;
   vector<vector<int> > *trk_detailed_mc_begVtx_barcode;
   vector<vector<int> > *trk_detailed_mc_endVtx_barcode;
   vector<vector<int> > *trk_detailed_mc_barcode;
   vector<vector<int> > *trk_detailed_mc_index;
   Int_t           pixClus_n;
   vector<unsigned int> *pixClus_id;
   vector<char>    *pixClus_bec;
   vector<char>    *pixClus_layer;
   vector<unsigned int> *pixClus_detElementId;
   vector<char>    *pixClus_phi_module;
   vector<char>    *pixClus_eta_module;
   vector<short>   *pixClus_col;
   vector<short>   *pixClus_row;
   vector<float>   *pixClus_charge;
   vector<short>   *pixClus_LVL1A;
   vector<int>     *pixClus_sizePhi;
   vector<int>     *pixClus_sizeZ;
   vector<int>     *pixClus_size;
   vector<float>   *pixClus_locX;
   vector<float>   *pixClus_locY;
   vector<float>   *pixClus_x;
   vector<float>   *pixClus_y;
   vector<float>   *pixClus_z;
   vector<char>    *pixClus_isFake;
   vector<char>    *pixClus_isGanged;
   vector<int>     *pixClus_isSplit;
   vector<int>     *pixClus_splitProb1;
   vector<int>     *pixClus_splitProb2;
   vector<vector<int> > *pixClus_mc_barcode;
   Int_t           primvx_n;
   vector<float>   *primvx_x;
   vector<float>   *primvx_y;
   vector<float>   *primvx_z;
   vector<float>   *primvx_err_x;
   vector<float>   *primvx_err_y;
   vector<float>   *primvx_err_z;
   vector<float>   *primvx_cov_xy;
   vector<float>   *primvx_cov_xz;
   vector<float>   *primvx_cov_yz;
   vector<float>   *primvx_chi2;
   vector<int>     *primvx_ndof;
   vector<float>   *primvx_px;
   vector<float>   *primvx_py;
   vector<float>   *primvx_pz;
   vector<float>   *primvx_E;
   vector<float>   *primvx_m;
   vector<int>     *primvx_nTracks;
   vector<float>   *primvx_sumPt;
   vector<int>     *primvx_type;
   vector<int>     *primvx_trk_n;
   vector<vector<float> > *primvx_trk_weight;
   vector<vector<float> > *primvx_trk_unbiased_d0;
   vector<vector<float> > *primvx_trk_unbiased_z0;
   vector<vector<float> > *primvx_trk_err_unbiased_d0;
   vector<vector<float> > *primvx_trk_err_unbiased_z0;
   vector<vector<float> > *primvx_trk_chi2;
   vector<vector<float> > *primvx_trk_d0;
   vector<vector<float> > *primvx_trk_z0;
   vector<vector<float> > *primvx_trk_phi;
   vector<vector<float> > *primvx_trk_theta;
   vector<vector<int> > *primvx_trk_index;
   Int_t           mcevt_n;
   vector<int>     *mcevt_signal_process_id;
   vector<int>     *mcevt_event_number;
   vector<double>  *mcevt_event_scale;
   vector<double>  *mcevt_alphaQCD;
   vector<double>  *mcevt_alphaQED;
   vector<int>     *mcevt_pdf_id1;
   vector<int>     *mcevt_pdf_id2;
   vector<double>  *mcevt_pdf_x1;
   vector<double>  *mcevt_pdf_x2;
   vector<double>  *mcevt_pdf_scale;
   vector<double>  *mcevt_pdf1;
   vector<double>  *mcevt_pdf2;
   vector<vector<double> > *mcevt_weight;
   vector<int>     *mcevt_nparticle;
   vector<short>   *mcevt_pileUpType;
   Int_t           mcvtx_n;
   vector<float>   *mcvtx_x;
   vector<float>   *mcvtx_y;
   vector<float>   *mcvtx_z;
   Int_t           mcpart_n;
   vector<float>   *mcpart_pt;
   vector<float>   *mcpart_m;
   vector<float>   *mcpart_eta;
   vector<float>   *mcpart_phi;
   vector<int>     *mcpart_type;
   vector<int>     *mcpart_status;
   vector<int>     *mcpart_barcode;
   vector<int>     *mcpart_mothertype;
   vector<int>     *mcpart_motherbarcode;
   vector<int>     *mcpart_mcevt_index;
   vector<int>     *mcpart_mcprodvtx_index;
   vector<int>     *mcpart_mother_n;
   vector<vector<int> > *mcpart_mother_index;
   vector<int>     *mcpart_mcdecayvtx_index;
   vector<int>     *mcpart_child_n;
   vector<vector<int> > *mcpart_child_index;
   vector<int>     *mcpart_truthtracks_index;
   Int_t           truthtrack_n;
   vector<int>     *truthtrack_ok;
   vector<float>   *truthtrack_d0;
   vector<float>   *truthtrack_z0;
   vector<float>   *truthtrack_phi;
   vector<float>   *truthtrack_theta;
   vector<float>   *truthtrack_qoverp;
   vector<short>   *deadPixMod_idHash;
   Int_t           deadPixMod_nDead;

   // List of branches
   TBranch        *b_jet_antikt4truth_n;   //!
   TBranch        *b_jet_antikt4truth_E;   //!
   TBranch        *b_jet_antikt4truth_pt;   //!
   TBranch        *b_jet_antikt4truth_m;   //!
   TBranch        *b_jet_antikt4truth_eta;   //!
   TBranch        *b_jet_antikt4truth_phi;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_Comb;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_IP2D;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_IP3D;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_SV0;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_SV1;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_SV2;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_JetProb;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_TrackCounting2D;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_SoftMuonTag;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_SoftElectronTag;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_JetFitterTagNN;   //!
   TBranch        *b_jet_antikt4truth_flavor_weight_JetFitterCOMBNN;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_label;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_dRminToB;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_dRminToC;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_dRminToT;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_BHadronpdg;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_vx_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_vx_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_truth_vx_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_label;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_dRminToB;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_dRminToC;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_dRminToT;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_BHadronpdg;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_vx_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_vx_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_putruth_vx_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_signOfIP;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_signOfZIP;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_gentruthlepton_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_gentruthlepton_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_gentruthlepton_origin;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_gentruthlepton_slbarcode;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip2d_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip2d_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip2d_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip2d_ntrk;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip3d_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip3d_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip3d_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ip3d_ntrk;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv1_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv1_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv1_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv2_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv2_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv2_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_pc;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitcomb_pu;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitcomb_pb;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitcomb_pc;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitcomb_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_nvtx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_nvtx1t;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_ntrkAtVx;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_efrc;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_mass;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_sig3d;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_deltaPhi;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfit_deltaEta;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_d0val;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_d0sig;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_z0val;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_z0sig;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_w2D;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_w3D;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_pJP;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_pJPneg;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_grade;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_ipplus_trk_isFromV0;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_ntrkv;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_ntrkj;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_n2t;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_mass;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_efrc;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_err_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_err_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_err_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_cov_xy;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_cov_xz;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_cov_yz;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_chi2;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_ndof;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_ntrk;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_trk_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_svp_trk_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_isValid;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_ntrkv;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_ntrkj;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_n2t;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_mass;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_efrc;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_err_x;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_err_y;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_err_z;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_cov_xy;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_cov_xz;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_cov_yz;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_chi2;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_ndof;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_ntrk;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_trk_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_sv0p_trk_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softmuoninfo_muon_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softmuoninfo_muon_w;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softelectron_electron_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softelectron_electron_isElectron;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softelectron_electron_d0;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_softelectron_electron_pTRel;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_VKalbadtrack_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_VKalbadtrack_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_theta;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_phi;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_errtheta;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_errphi;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_chi2;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfitvx_ndof;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfvxonjetaxis_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jfvxonjetaxis_index;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jftwotrackvertex_n;   //!
   TBranch        *b_jet_antikt4truth_flavor_component_jftwotrackvertex_index;   //!
   TBranch        *b_jet_antikt4truth_L1_dr;   //!
   TBranch        *b_jet_antikt4truth_L1_matched;   //!
   TBranch        *b_jet_antikt4truth_L2_dr;   //!
   TBranch        *b_jet_antikt4truth_L2_matched;   //!
   TBranch        *b_jet_antikt4truth_EF_dr;   //!
   TBranch        *b_jet_antikt4truth_EF_matched;   //!
   TBranch        *b_jfvxonjetaxis_n;   //!
   TBranch        *b_jfvxonjetaxis_vtxPos;   //!
   TBranch        *b_jfvxonjetaxis_vtxErr;   //!
   TBranch        *b_jfvxonjetaxis_trk_n;   //!
   TBranch        *b_jfvxonjetaxis_trk_phiAtVx;   //!
   TBranch        *b_jfvxonjetaxis_trk_thetaAtVx;   //!
   TBranch        *b_jfvxonjetaxis_trk_ptAtVx;   //!
   TBranch        *b_jfvxonjetaxis_trk_index;   //!
   TBranch        *b_jftwotrkvertex_n;   //!
   TBranch        *b_jftwotrkvertex_isNeutral;   //!
   TBranch        *b_jftwotrkvertex_chi2;   //!
   TBranch        *b_jftwotrkvertex_ndof;   //!
   TBranch        *b_jftwotrkvertex_x;   //!
   TBranch        *b_jftwotrkvertex_y;   //!
   TBranch        *b_jftwotrkvertex_z;   //!
   TBranch        *b_jftwotrkvertex_errx;   //!
   TBranch        *b_jftwotrkvertex_erry;   //!
   TBranch        *b_jftwotrkvertex_errz;   //!
   TBranch        *b_jftwotrkvertex_mass;   //!
   TBranch        *b_jftwotrkvertex_trk_n;   //!
   TBranch        *b_jftwotrkvertex_trk_index;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_timestamp_ns;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_pileupinfo_n;   //!
   TBranch        *b_pileupinfo_time;   //!
   TBranch        *b_pileupinfo_index;   //!
   TBranch        *b_pileupinfo_type;   //!
   TBranch        *b_pileupinfo_runNumber;   //!
   TBranch        *b_pileupinfo_EventNumber;   //!
   TBranch        *b_trk_n;   //!
   TBranch        *b_trk_d0;   //!
   TBranch        *b_trk_z0;   //!
   TBranch        *b_trk_phi;   //!
   TBranch        *b_trk_theta;   //!
   TBranch        *b_trk_qoverp;   //!
   TBranch        *b_trk_pt;   //!
   TBranch        *b_trk_eta;   //!
   TBranch        *b_trk_err_d0;   //!
   TBranch        *b_trk_err_z0;   //!
   TBranch        *b_trk_err_phi;   //!
   TBranch        *b_trk_err_theta;   //!
   TBranch        *b_trk_err_qoverp;   //!
   TBranch        *b_trk_cov_d0_z0;   //!
   TBranch        *b_trk_cov_d0_phi;   //!
   TBranch        *b_trk_cov_d0_theta;   //!
   TBranch        *b_trk_cov_d0_qoverp;   //!
   TBranch        *b_trk_cov_z0_phi;   //!
   TBranch        *b_trk_cov_z0_theta;   //!
   TBranch        *b_trk_cov_z0_qoverp;   //!
   TBranch        *b_trk_cov_phi_theta;   //!
   TBranch        *b_trk_cov_phi_qoverp;   //!
   TBranch        *b_trk_cov_theta_qoverp;   //!
   TBranch        *b_trk_IPEstimate_d0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_z0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_d0_unbiased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_z0_unbiased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_err_d0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_err_z0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_err_d0_unbiased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_err_z0_unbiased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_errPV_d0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_errPV_z0_biased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_errPV_d0_unbiased_wrtPV;   //!
   TBranch        *b_trk_IPEstimate_errPV_z0_unbiased_wrtPV;   //!
   TBranch        *b_trk_d0_wrtPV;   //!
   TBranch        *b_trk_z0_wrtPV;   //!
   TBranch        *b_trk_phi_wrtPV;   //!
   TBranch        *b_trk_err_d0_wrtPV;   //!
   TBranch        *b_trk_err_z0_wrtPV;   //!
   TBranch        *b_trk_err_phi_wrtPV;   //!
   TBranch        *b_trk_err_theta_wrtPV;   //!
   TBranch        *b_trk_err_qoverp_wrtPV;   //!
   TBranch        *b_trk_cov_d0_z0_wrtPV;   //!
   TBranch        *b_trk_cov_d0_phi_wrtPV;   //!
   TBranch        *b_trk_cov_d0_theta_wrtPV;   //!
   TBranch        *b_trk_cov_d0_qoverp_wrtPV;   //!
   TBranch        *b_trk_cov_z0_phi_wrtPV;   //!
   TBranch        *b_trk_cov_z0_theta_wrtPV;   //!
   TBranch        *b_trk_cov_z0_qoverp_wrtPV;   //!
   TBranch        *b_trk_cov_phi_theta_wrtPV;   //!
   TBranch        *b_trk_cov_phi_qoverp_wrtPV;   //!
   TBranch        *b_trk_cov_theta_qoverp_wrtPV;   //!
   TBranch        *b_trk_d0_wrtBS;   //!
   TBranch        *b_trk_z0_wrtBS;   //!
   TBranch        *b_trk_phi_wrtBS;   //!
   TBranch        *b_trk_err_d0_wrtBS;   //!
   TBranch        *b_trk_err_z0_wrtBS;   //!
   TBranch        *b_trk_err_phi_wrtBS;   //!
   TBranch        *b_trk_err_theta_wrtBS;   //!
   TBranch        *b_trk_err_qoverp_wrtBS;   //!
   TBranch        *b_trk_chi2;   //!
   TBranch        *b_trk_ndof;   //!
   TBranch        *b_trk_nBLHits;   //!
   TBranch        *b_trk_nPixHits;   //!
   TBranch        *b_trk_nSCTHits;   //!
   TBranch        *b_trk_nTRTHits;   //!
   TBranch        *b_trk_nTRTHighTHits;   //!
   TBranch        *b_trk_nPixHoles;   //!
   TBranch        *b_trk_nSCTHoles;   //!
   TBranch        *b_trk_nTRTHoles;   //!
   TBranch        *b_trk_nBLSharedHits;   //!
   TBranch        *b_trk_nPixSharedHits;   //!
   TBranch        *b_trk_nSCTSharedHits;   //!
   TBranch        *b_trk_nBLayerOutliers;   //!
   TBranch        *b_trk_nPixelOutliers;   //!
   TBranch        *b_trk_nSCTOutliers;   //!
   TBranch        *b_trk_nTRTOutliers;   //!
   TBranch        *b_trk_nTRTHighTOutliers;   //!
   TBranch        *b_trk_nContribPixelLayers;   //!
   TBranch        *b_trk_nGangedPixels;   //!
   TBranch        *b_trk_nGangedFlaggedFakes;   //!
   TBranch        *b_trk_nPixelDeadSensors;   //!
   TBranch        *b_trk_nPixelSpoiltHits;   //!
   TBranch        *b_trk_nSCTDoubleHoles;   //!
   TBranch        *b_trk_nSCTDeadSensors;   //!
   TBranch        *b_trk_nSCTSpoiltHits;   //!
   TBranch        *b_trk_expectBLayerHit;   //!
   TBranch        *b_trk_hitPattern;   //!
   TBranch        *b_trk_nSiHits;   //!
   TBranch        *b_trk_fitter;   //!
   TBranch        *b_trk_patternReco1;   //!
   TBranch        *b_trk_patternReco2;   //!
   TBranch        *b_trk_seedFinder;   //!
   TBranch        *b_trk_blayerPrediction_x;   //!
   TBranch        *b_trk_blayerPrediction_y;   //!
   TBranch        *b_trk_blayerPrediction_z;   //!
   TBranch        *b_trk_blayerPrediction_locX;   //!
   TBranch        *b_trk_blayerPrediction_locY;   //!
   TBranch        *b_trk_blayerPrediction_err_locX;   //!
   TBranch        *b_trk_blayerPrediction_err_locY;   //!
   TBranch        *b_trk_blayerPrediction_etaDistToEdge;   //!
   TBranch        *b_trk_blayerPrediction_phiDistToEdge;   //!
   TBranch        *b_trk_blayerPrediction_detElementId;   //!
   TBranch        *b_trk_blayerPrediction_row;   //!
   TBranch        *b_trk_blayerPrediction_col;   //!
   TBranch        *b_trk_blayerPrediction_type;   //!
   TBranch        *b_trk_BLayer_hit_n;   //!
   TBranch        *b_trk_BLayer_hit_id;   //!
   TBranch        *b_trk_BLayer_hit_detElementId;   //!
   TBranch        *b_trk_BLayer_hit_bec;   //!
   TBranch        *b_trk_BLayer_hit_layer;   //!
   TBranch        *b_trk_BLayer_hit_charge;   //!
   TBranch        *b_trk_BLayer_hit_sizePhi;   //!
   TBranch        *b_trk_BLayer_hit_sizeZ;   //!
   TBranch        *b_trk_BLayer_hit_size;   //!
   TBranch        *b_trk_BLayer_hit_isFake;   //!
   TBranch        *b_trk_BLayer_hit_isGanged;   //!
   TBranch        *b_trk_BLayer_hit_isSplit;   //!
   TBranch        *b_trk_BLayer_hit_splitProb1;   //!
   TBranch        *b_trk_BLayer_hit_splitProb2;   //!
   TBranch        *b_trk_BLayer_hit_isCompetingRIO;   //!
   TBranch        *b_trk_BLayer_hit_locX;   //!
   TBranch        *b_trk_BLayer_hit_locY;   //!
   TBranch        *b_trk_BLayer_hit_incidencePhi;   //!
   TBranch        *b_trk_BLayer_hit_incidenceTheta;   //!
   TBranch        *b_trk_BLayer_hit_err_locX;   //!
   TBranch        *b_trk_BLayer_hit_err_locY;   //!
   TBranch        *b_trk_BLayer_hit_cov_locXY;   //!
   TBranch        *b_trk_BLayer_hit_x;   //!
   TBranch        *b_trk_BLayer_hit_y;   //!
   TBranch        *b_trk_BLayer_hit_z;   //!
   TBranch        *b_trk_BLayer_hit_trkLocX;   //!
   TBranch        *b_trk_BLayer_hit_trkLocY;   //!
   TBranch        *b_trk_BLayer_hit_err_trkLocX;   //!
   TBranch        *b_trk_BLayer_hit_err_trkLocY;   //!
   TBranch        *b_trk_BLayer_hit_cov_trkLocXY;   //!
   TBranch        *b_trk_BLayer_hit_locBiasedResidualX;   //!
   TBranch        *b_trk_BLayer_hit_locBiasedResidualY;   //!
   TBranch        *b_trk_BLayer_hit_locBiasedPullX;   //!
   TBranch        *b_trk_BLayer_hit_locBiasedPullY;   //!
   TBranch        *b_trk_BLayer_hit_locUnbiasedResidualX;   //!
   TBranch        *b_trk_BLayer_hit_locUnbiasedResidualY;   //!
   TBranch        *b_trk_BLayer_hit_locUnbiasedPullX;   //!
   TBranch        *b_trk_BLayer_hit_locUnbiasedPullY;   //!
   TBranch        *b_trk_BLayer_hit_chi2;   //!
   TBranch        *b_trk_BLayer_hit_ndof;   //!
   TBranch        *b_trk_Pixel_hit_n;   //!
   TBranch        *b_trk_Pixel_hit_id;   //!
   TBranch        *b_trk_Pixel_hit_detElementId;   //!
   TBranch        *b_trk_Pixel_hit_bec;   //!
   TBranch        *b_trk_Pixel_hit_layer;   //!
   TBranch        *b_trk_Pixel_hit_charge;   //!
   TBranch        *b_trk_Pixel_hit_sizePhi;   //!
   TBranch        *b_trk_Pixel_hit_sizeZ;   //!
   TBranch        *b_trk_Pixel_hit_size;   //!
   TBranch        *b_trk_Pixel_hit_isFake;   //!
   TBranch        *b_trk_Pixel_hit_isGanged;   //!
   TBranch        *b_trk_Pixel_hit_isSplit;   //!
   TBranch        *b_trk_Pixel_hit_splitProb1;   //!
   TBranch        *b_trk_Pixel_hit_splitProb2;   //!
   TBranch        *b_trk_Pixel_hit_isCompetingRIO;   //!
   TBranch        *b_trk_Pixel_hit_locX;   //!
   TBranch        *b_trk_Pixel_hit_locY;   //!
   TBranch        *b_trk_Pixel_hit_incidencePhi;   //!
   TBranch        *b_trk_Pixel_hit_incidenceTheta;   //!
   TBranch        *b_trk_Pixel_hit_err_locX;   //!
   TBranch        *b_trk_Pixel_hit_err_locY;   //!
   TBranch        *b_trk_Pixel_hit_cov_locXY;   //!
   TBranch        *b_trk_Pixel_hit_x;   //!
   TBranch        *b_trk_Pixel_hit_y;   //!
   TBranch        *b_trk_Pixel_hit_z;   //!
   TBranch        *b_trk_Pixel_hit_trkLocX;   //!
   TBranch        *b_trk_Pixel_hit_trkLocY;   //!
   TBranch        *b_trk_Pixel_hit_err_trkLocX;   //!
   TBranch        *b_trk_Pixel_hit_err_trkLocY;   //!
   TBranch        *b_trk_Pixel_hit_cov_trkLocXY;   //!
   TBranch        *b_trk_Pixel_hit_locBiasedResidualX;   //!
   TBranch        *b_trk_Pixel_hit_locBiasedResidualY;   //!
   TBranch        *b_trk_Pixel_hit_locBiasedPullX;   //!
   TBranch        *b_trk_Pixel_hit_locBiasedPullY;   //!
   TBranch        *b_trk_Pixel_hit_locUnbiasedResidualX;   //!
   TBranch        *b_trk_Pixel_hit_locUnbiasedResidualY;   //!
   TBranch        *b_trk_Pixel_hit_locUnbiasedPullX;   //!
   TBranch        *b_trk_Pixel_hit_locUnbiasedPullY;   //!
   TBranch        *b_trk_Pixel_hit_chi2;   //!
   TBranch        *b_trk_Pixel_hit_ndof;   //!
   TBranch        *b_trk_SCT_hit_n;   //!
   TBranch        *b_trk_SCT_hit_id;   //!
   TBranch        *b_trk_SCT_hit_detElementId;   //!
   TBranch        *b_trk_SCT_hit_bec;   //!
   TBranch        *b_trk_SCT_hit_layer;   //!
   TBranch        *b_trk_SCT_hit_sizePhi;   //!
   TBranch        *b_trk_SCT_hit_isCompetingRIO;   //!
   TBranch        *b_trk_SCT_hit_locX;   //!
   TBranch        *b_trk_SCT_hit_locY;   //!
   TBranch        *b_trk_SCT_hit_incidencePhi;   //!
   TBranch        *b_trk_SCT_hit_incidenceTheta;   //!
   TBranch        *b_trk_SCT_hit_err_locX;   //!
   TBranch        *b_trk_SCT_hit_err_locY;   //!
   TBranch        *b_trk_SCT_hit_cov_locXY;   //!
   TBranch        *b_trk_SCT_hit_x;   //!
   TBranch        *b_trk_SCT_hit_y;   //!
   TBranch        *b_trk_SCT_hit_z;   //!
   TBranch        *b_trk_SCT_hit_trkLocX;   //!
   TBranch        *b_trk_SCT_hit_trkLocY;   //!
   TBranch        *b_trk_SCT_hit_err_trkLocX;   //!
   TBranch        *b_trk_SCT_hit_err_trkLocY;   //!
   TBranch        *b_trk_SCT_hit_cov_trkLocXY;   //!
   TBranch        *b_trk_SCT_hit_locBiasedResidualX;   //!
   TBranch        *b_trk_SCT_hit_locBiasedResidualY;   //!
   TBranch        *b_trk_SCT_hit_locBiasedPullX;   //!
   TBranch        *b_trk_SCT_hit_locBiasedPullY;   //!
   TBranch        *b_trk_SCT_hit_locUnbiasedResidualX;   //!
   TBranch        *b_trk_SCT_hit_locUnbiasedResidualY;   //!
   TBranch        *b_trk_SCT_hit_locUnbiasedPullX;   //!
   TBranch        *b_trk_SCT_hit_locUnbiasedPullY;   //!
   TBranch        *b_trk_SCT_hit_chi2;   //!
   TBranch        *b_trk_SCT_hit_ndof;   //!
   TBranch        *b_trk_BLayer_outlier_n;   //!
   TBranch        *b_trk_BLayer_outlier_id;   //!
   TBranch        *b_trk_BLayer_outlier_detElementId;   //!
   TBranch        *b_trk_BLayer_outlier_bec;   //!
   TBranch        *b_trk_BLayer_outlier_layer;   //!
   TBranch        *b_trk_BLayer_outlier_charge;   //!
   TBranch        *b_trk_BLayer_outlier_sizePhi;   //!
   TBranch        *b_trk_BLayer_outlier_sizeZ;   //!
   TBranch        *b_trk_BLayer_outlier_size;   //!
   TBranch        *b_trk_BLayer_outlier_isFake;   //!
   TBranch        *b_trk_BLayer_outlier_isGanged;   //!
   TBranch        *b_trk_BLayer_outlier_isSplit;   //!
   TBranch        *b_trk_BLayer_outlier_splitProb1;   //!
   TBranch        *b_trk_BLayer_outlier_splitProb2;   //!
   TBranch        *b_trk_BLayer_outlier_isCompetingRIO;   //!
   TBranch        *b_trk_BLayer_outlier_locX;   //!
   TBranch        *b_trk_BLayer_outlier_locY;   //!
   TBranch        *b_trk_BLayer_outlier_incidencePhi;   //!
   TBranch        *b_trk_BLayer_outlier_incidenceTheta;   //!
   TBranch        *b_trk_BLayer_outlier_err_locX;   //!
   TBranch        *b_trk_BLayer_outlier_err_locY;   //!
   TBranch        *b_trk_BLayer_outlier_cov_locXY;   //!
   TBranch        *b_trk_BLayer_outlier_x;   //!
   TBranch        *b_trk_BLayer_outlier_y;   //!
   TBranch        *b_trk_BLayer_outlier_z;   //!
   TBranch        *b_trk_BLayer_outlier_trkLocX;   //!
   TBranch        *b_trk_BLayer_outlier_trkLocY;   //!
   TBranch        *b_trk_BLayer_outlier_err_trkLocX;   //!
   TBranch        *b_trk_BLayer_outlier_err_trkLocY;   //!
   TBranch        *b_trk_BLayer_outlier_cov_trkLocXY;   //!
   TBranch        *b_trk_BLayer_outlier_locBiasedResidualX;   //!
   TBranch        *b_trk_BLayer_outlier_locBiasedResidualY;   //!
   TBranch        *b_trk_BLayer_outlier_locBiasedPullX;   //!
   TBranch        *b_trk_BLayer_outlier_locBiasedPullY;   //!
   TBranch        *b_trk_BLayer_outlier_locUnbiasedResidualX;   //!
   TBranch        *b_trk_BLayer_outlier_locUnbiasedResidualY;   //!
   TBranch        *b_trk_BLayer_outlier_locUnbiasedPullX;   //!
   TBranch        *b_trk_BLayer_outlier_locUnbiasedPullY;   //!
   TBranch        *b_trk_BLayer_outlier_chi2;   //!
   TBranch        *b_trk_BLayer_outlier_ndof;   //!
   TBranch        *b_trk_Pixel_outlier_n;   //!
   TBranch        *b_trk_Pixel_outlier_id;   //!
   TBranch        *b_trk_Pixel_outlier_detElementId;   //!
   TBranch        *b_trk_Pixel_outlier_bec;   //!
   TBranch        *b_trk_Pixel_outlier_layer;   //!
   TBranch        *b_trk_Pixel_outlier_charge;   //!
   TBranch        *b_trk_Pixel_outlier_sizePhi;   //!
   TBranch        *b_trk_Pixel_outlier_sizeZ;   //!
   TBranch        *b_trk_Pixel_outlier_size;   //!
   TBranch        *b_trk_Pixel_outlier_isFake;   //!
   TBranch        *b_trk_Pixel_outlier_isGanged;   //!
   TBranch        *b_trk_Pixel_outlier_isSplit;   //!
   TBranch        *b_trk_Pixel_outlier_splitProb1;   //!
   TBranch        *b_trk_Pixel_outlier_splitProb2;   //!
   TBranch        *b_trk_Pixel_outlier_isCompetingRIO;   //!
   TBranch        *b_trk_Pixel_outlier_locX;   //!
   TBranch        *b_trk_Pixel_outlier_locY;   //!
   TBranch        *b_trk_Pixel_outlier_incidencePhi;   //!
   TBranch        *b_trk_Pixel_outlier_incidenceTheta;   //!
   TBranch        *b_trk_Pixel_outlier_err_locX;   //!
   TBranch        *b_trk_Pixel_outlier_err_locY;   //!
   TBranch        *b_trk_Pixel_outlier_cov_locXY;   //!
   TBranch        *b_trk_Pixel_outlier_x;   //!
   TBranch        *b_trk_Pixel_outlier_y;   //!
   TBranch        *b_trk_Pixel_outlier_z;   //!
   TBranch        *b_trk_Pixel_outlier_trkLocX;   //!
   TBranch        *b_trk_Pixel_outlier_trkLocY;   //!
   TBranch        *b_trk_Pixel_outlier_err_trkLocX;   //!
   TBranch        *b_trk_Pixel_outlier_err_trkLocY;   //!
   TBranch        *b_trk_Pixel_outlier_cov_trkLocXY;   //!
   TBranch        *b_trk_Pixel_outlier_locBiasedResidualX;   //!
   TBranch        *b_trk_Pixel_outlier_locBiasedResidualY;   //!
   TBranch        *b_trk_Pixel_outlier_locBiasedPullX;   //!
   TBranch        *b_trk_Pixel_outlier_locBiasedPullY;   //!
   TBranch        *b_trk_Pixel_outlier_locUnbiasedResidualX;   //!
   TBranch        *b_trk_Pixel_outlier_locUnbiasedResidualY;   //!
   TBranch        *b_trk_Pixel_outlier_locUnbiasedPullX;   //!
   TBranch        *b_trk_Pixel_outlier_locUnbiasedPullY;   //!
   TBranch        *b_trk_Pixel_outlier_chi2;   //!
   TBranch        *b_trk_Pixel_outlier_ndof;   //!
   TBranch        *b_trk_BLayer_hole_n;   //!
   TBranch        *b_trk_BLayer_hole_detElementId;   //!
   TBranch        *b_trk_BLayer_hole_bec;   //!
   TBranch        *b_trk_BLayer_hole_layer;   //!
   TBranch        *b_trk_BLayer_hole_trkLocX;   //!
   TBranch        *b_trk_BLayer_hole_trkLocY;   //!
   TBranch        *b_trk_BLayer_hole_err_trkLocX;   //!
   TBranch        *b_trk_BLayer_hole_err_trkLocY;   //!
   TBranch        *b_trk_BLayer_hole_cov_trkLocXY;   //!
   TBranch        *b_trk_Pixel_hole_n;   //!
   TBranch        *b_trk_Pixel_hole_detElementId;   //!
   TBranch        *b_trk_Pixel_hole_bec;   //!
   TBranch        *b_trk_Pixel_hole_layer;   //!
   TBranch        *b_trk_Pixel_hole_trkLocX;   //!
   TBranch        *b_trk_Pixel_hole_trkLocY;   //!
   TBranch        *b_trk_Pixel_hole_err_trkLocX;   //!
   TBranch        *b_trk_Pixel_hole_err_trkLocY;   //!
   TBranch        *b_trk_Pixel_hole_cov_trkLocXY;   //!
   TBranch        *b_trk_primvx_weight;   //!
   TBranch        *b_trk_primvx_index;   //!
   TBranch        *b_trk_mcpart_probability;   //!
   TBranch        *b_trk_mcpart_barcode;   //!
   TBranch        *b_trk_mcpart_index;   //!
   TBranch        *b_trk_detailed_mc_n;   //!
   TBranch        *b_trk_detailed_mc_nCommonPixHits;   //!
   TBranch        *b_trk_detailed_mc_nCommonSCTHits;   //!
   TBranch        *b_trk_detailed_mc_nCommonTRTHits;   //!
   TBranch        *b_trk_detailed_mc_nRecoPixHits;   //!
   TBranch        *b_trk_detailed_mc_nRecoSCTHits;   //!
   TBranch        *b_trk_detailed_mc_nRecoTRTHits;   //!
   TBranch        *b_trk_detailed_mc_nTruthPixHits;   //!
   TBranch        *b_trk_detailed_mc_nTruthSCTHits;   //!
   TBranch        *b_trk_detailed_mc_nTruthTRTHits;   //!
   TBranch        *b_trk_detailed_mc_begVtx_barcode;   //!
   TBranch        *b_trk_detailed_mc_endVtx_barcode;   //!
   TBranch        *b_trk_detailed_mc_barcode;   //!
   TBranch        *b_trk_detailed_mc_index;   //!
   TBranch        *b_pixClus_n;   //!
   TBranch        *b_pixClus_id;   //!
   TBranch        *b_pixClus_bec;   //!
   TBranch        *b_pixClus_layer;   //!
   TBranch        *b_pixClus_detElementId;   //!
   TBranch        *b_pixClus_phi_module;   //!
   TBranch        *b_pixClus_eta_module;   //!
   TBranch        *b_pixClus_col;   //!
   TBranch        *b_pixClus_row;   //!
   TBranch        *b_pixClus_charge;   //!
   TBranch        *b_pixClus_LVL1A;   //!
   TBranch        *b_pixClus_sizePhi;   //!
   TBranch        *b_pixClus_sizeZ;   //!
   TBranch        *b_pixClus_size;   //!
   TBranch        *b_pixClus_locX;   //!
   TBranch        *b_pixClus_locY;   //!
   TBranch        *b_pixClus_x;   //!
   TBranch        *b_pixClus_y;   //!
   TBranch        *b_pixClus_z;   //!
   TBranch        *b_pixClus_isFake;   //!
   TBranch        *b_pixClus_isGanged;   //!
   TBranch        *b_pixClus_isSplit;   //!
   TBranch        *b_pixClus_splitProb1;   //!
   TBranch        *b_pixClus_splitProb2;   //!
   TBranch        *b_pixClus_mc_barcode;   //!
   TBranch        *b_primvx_n;   //!
   TBranch        *b_primvx_x;   //!
   TBranch        *b_primvx_y;   //!
   TBranch        *b_primvx_z;   //!
   TBranch        *b_primvx_err_x;   //!
   TBranch        *b_primvx_err_y;   //!
   TBranch        *b_primvx_err_z;   //!
   TBranch        *b_primvx_cov_xy;   //!
   TBranch        *b_primvx_cov_xz;   //!
   TBranch        *b_primvx_cov_yz;   //!
   TBranch        *b_primvx_chi2;   //!
   TBranch        *b_primvx_ndof;   //!
   TBranch        *b_primvx_px;   //!
   TBranch        *b_primvx_py;   //!
   TBranch        *b_primvx_pz;   //!
   TBranch        *b_primvx_E;   //!
   TBranch        *b_primvx_m;   //!
   TBranch        *b_primvx_nTracks;   //!
   TBranch        *b_primvx_sumPt;   //!
   TBranch        *b_primvx_type;   //!
   TBranch        *b_primvx_trk_n;   //!
   TBranch        *b_primvx_trk_weight;   //!
   TBranch        *b_primvx_trk_unbiased_d0;   //!
   TBranch        *b_primvx_trk_unbiased_z0;   //!
   TBranch        *b_primvx_trk_err_unbiased_d0;   //!
   TBranch        *b_primvx_trk_err_unbiased_z0;   //!
   TBranch        *b_primvx_trk_chi2;   //!
   TBranch        *b_primvx_trk_d0;   //!
   TBranch        *b_primvx_trk_z0;   //!
   TBranch        *b_primvx_trk_phi;   //!
   TBranch        *b_primvx_trk_theta;   //!
   TBranch        *b_primvx_trk_index;   //!
   TBranch        *b_mcevt_n;   //!
   TBranch        *b_mcevt_signal_process_id;   //!
   TBranch        *b_mcevt_event_number;   //!
   TBranch        *b_mcevt_event_scale;   //!
   TBranch        *b_mcevt_alphaQCD;   //!
   TBranch        *b_mcevt_alphaQED;   //!
   TBranch        *b_mcevt_pdf_id1;   //!
   TBranch        *b_mcevt_pdf_id2;   //!
   TBranch        *b_mcevt_pdf_x1;   //!
   TBranch        *b_mcevt_pdf_x2;   //!
   TBranch        *b_mcevt_pdf_scale;   //!
   TBranch        *b_mcevt_pdf1;   //!
   TBranch        *b_mcevt_pdf2;   //!
   TBranch        *b_mcevt_weight;   //!
   TBranch        *b_mcevt_nparticle;   //!
   TBranch        *b_mcevt_pileUpType;   //!
   TBranch        *b_mcvtx_n;   //!
   TBranch        *b_mcvtx_x;   //!
   TBranch        *b_mcvtx_y;   //!
   TBranch        *b_mcvtx_z;   //!
   TBranch        *b_mcpart_n;   //!
   TBranch        *b_mcpart_pt;   //!
   TBranch        *b_mcpart_m;   //!
   TBranch        *b_mcpart_eta;   //!
   TBranch        *b_mcpart_phi;   //!
   TBranch        *b_mcpart_type;   //!
   TBranch        *b_mcpart_status;   //!
   TBranch        *b_mcpart_barcode;   //!
   TBranch        *b_mcpart_mothertype;   //!
   TBranch        *b_mcpart_motherbarcode;   //!
   TBranch        *b_mcpart_mcevt_index;   //!
   TBranch        *b_mcpart_mcprodvtx_index;   //!
   TBranch        *b_mcpart_mother_n;   //!
   TBranch        *b_mcpart_mother_index;   //!
   TBranch        *b_mcpart_mcdecayvtx_index;   //!
   TBranch        *b_mcpart_child_n;   //!
   TBranch        *b_mcpart_child_index;   //!
   TBranch        *b_mcpart_truthtracks_index;   //!
   TBranch        *b_truthtrack_n;   //!
   TBranch        *b_truthtrack_ok;   //!
   TBranch        *b_truthtrack_d0;   //!
   TBranch        *b_truthtrack_z0;   //!
   TBranch        *b_truthtrack_phi;   //!
   TBranch        *b_truthtrack_theta;   //!
   TBranch        *b_truthtrack_qoverp;   //!
   TBranch        *b_deadPixMod_idHash;   //!
   TBranch        *b_deadPixMod_nDead;   //!

   JetTagD3PD(TTree *tree=0);
   virtual ~JetTagD3PD();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef JetTagD3PD_cxx
JetTagD3PD::JetTagD3PD(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.tcorneli.mc11_14TeV.105568.pythia_ttbar.JetTagD3PD.ibl02.test6.110706101606/user.tcorneli.000301.EXT0._00001.JetTagD3PD.root");
      if (!f) {
         f = new TFile("root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.tcorneli.mc11_14TeV.105568.pythia_ttbar.JetTagD3PD.ibl02.test6.110706101606/user.tcorneli.000301.EXT0._00001.JetTagD3PD.root");
      }
      tree = (TTree*)gDirectory->Get("btagd3pd");

   }
   Init(tree);
}

JetTagD3PD::~JetTagD3PD()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t JetTagD3PD::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t JetTagD3PD::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void JetTagD3PD::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jet_antikt4truth_E = 0;
   jet_antikt4truth_pt = 0;
   jet_antikt4truth_m = 0;
   jet_antikt4truth_eta = 0;
   jet_antikt4truth_phi = 0;
   jet_antikt4truth_flavor_weight_Comb = 0;
   jet_antikt4truth_flavor_weight_IP2D = 0;
   jet_antikt4truth_flavor_weight_IP3D = 0;
   jet_antikt4truth_flavor_weight_SV0 = 0;
   jet_antikt4truth_flavor_weight_SV1 = 0;
   jet_antikt4truth_flavor_weight_SV2 = 0;
   jet_antikt4truth_flavor_weight_JetProb = 0;
   jet_antikt4truth_flavor_weight_TrackCounting2D = 0;
   jet_antikt4truth_flavor_weight_SoftMuonTag = 0;
   jet_antikt4truth_flavor_weight_SoftElectronTag = 0;
   jet_antikt4truth_flavor_weight_JetFitterTagNN = 0;
   jet_antikt4truth_flavor_weight_JetFitterCOMBNN = 0;
   jet_antikt4truth_flavor_truth_label = 0;
   jet_antikt4truth_flavor_truth_dRminToB = 0;
   jet_antikt4truth_flavor_truth_dRminToC = 0;
   jet_antikt4truth_flavor_truth_dRminToT = 0;
   jet_antikt4truth_flavor_truth_BHadronpdg = 0;
   jet_antikt4truth_flavor_truth_vx_x = 0;
   jet_antikt4truth_flavor_truth_vx_y = 0;
   jet_antikt4truth_flavor_truth_vx_z = 0;
   jet_antikt4truth_flavor_putruth_label = 0;
   jet_antikt4truth_flavor_putruth_dRminToB = 0;
   jet_antikt4truth_flavor_putruth_dRminToC = 0;
   jet_antikt4truth_flavor_putruth_dRminToT = 0;
   jet_antikt4truth_flavor_putruth_BHadronpdg = 0;
   jet_antikt4truth_flavor_putruth_vx_x = 0;
   jet_antikt4truth_flavor_putruth_vx_y = 0;
   jet_antikt4truth_flavor_putruth_vx_z = 0;
   jet_antikt4truth_flavor_component_assoctrk_n = 0;
   jet_antikt4truth_flavor_component_assoctrk_index = 0;
   jet_antikt4truth_flavor_component_assoctrk_signOfIP = 0;
   jet_antikt4truth_flavor_component_assoctrk_signOfZIP = 0;
   jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx = 0;
   jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx = 0;
   jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx = 0;
   jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx = 0;
   jet_antikt4truth_flavor_component_gentruthlepton_n = 0;
   jet_antikt4truth_flavor_component_gentruthlepton_index = 0;
   jet_antikt4truth_flavor_component_gentruthlepton_origin = 0;
   jet_antikt4truth_flavor_component_gentruthlepton_slbarcode = 0;
   jet_antikt4truth_flavor_component_ip2d_pu = 0;
   jet_antikt4truth_flavor_component_ip2d_pb = 0;
   jet_antikt4truth_flavor_component_ip2d_isValid = 0;
   jet_antikt4truth_flavor_component_ip2d_ntrk = 0;
   jet_antikt4truth_flavor_component_ip3d_pu = 0;
   jet_antikt4truth_flavor_component_ip3d_pb = 0;
   jet_antikt4truth_flavor_component_ip3d_isValid = 0;
   jet_antikt4truth_flavor_component_ip3d_ntrk = 0;
   jet_antikt4truth_flavor_component_sv1_pu = 0;
   jet_antikt4truth_flavor_component_sv1_pb = 0;
   jet_antikt4truth_flavor_component_sv1_isValid = 0;
   jet_antikt4truth_flavor_component_sv2_pu = 0;
   jet_antikt4truth_flavor_component_sv2_pb = 0;
   jet_antikt4truth_flavor_component_sv2_isValid = 0;
   jet_antikt4truth_flavor_component_jfit_pu = 0;
   jet_antikt4truth_flavor_component_jfit_pb = 0;
   jet_antikt4truth_flavor_component_jfit_pc = 0;
   jet_antikt4truth_flavor_component_jfit_isValid = 0;
   jet_antikt4truth_flavor_component_jfitcomb_pu = 0;
   jet_antikt4truth_flavor_component_jfitcomb_pb = 0;
   jet_antikt4truth_flavor_component_jfitcomb_pc = 0;
   jet_antikt4truth_flavor_component_jfitcomb_isValid = 0;
   jet_antikt4truth_flavor_component_jfit_nvtx = 0;
   jet_antikt4truth_flavor_component_jfit_nvtx1t = 0;
   jet_antikt4truth_flavor_component_jfit_ntrkAtVx = 0;
   jet_antikt4truth_flavor_component_jfit_efrc = 0;
   jet_antikt4truth_flavor_component_jfit_mass = 0;
   jet_antikt4truth_flavor_component_jfit_sig3d = 0;
   jet_antikt4truth_flavor_component_jfit_deltaPhi = 0;
   jet_antikt4truth_flavor_component_jfit_deltaEta = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_n = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_index = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_d0val = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_d0sig = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_z0val = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_z0sig = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_w2D = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_w3D = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_pJP = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_pJPneg = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_grade = 0;
   jet_antikt4truth_flavor_component_ipplus_trk_isFromV0 = 0;
   jet_antikt4truth_flavor_component_svp_isValid = 0;
   jet_antikt4truth_flavor_component_svp_ntrkv = 0;
   jet_antikt4truth_flavor_component_svp_ntrkj = 0;
   jet_antikt4truth_flavor_component_svp_n2t = 0;
   jet_antikt4truth_flavor_component_svp_mass = 0;
   jet_antikt4truth_flavor_component_svp_efrc = 0;
   jet_antikt4truth_flavor_component_svp_x = 0;
   jet_antikt4truth_flavor_component_svp_y = 0;
   jet_antikt4truth_flavor_component_svp_z = 0;
   jet_antikt4truth_flavor_component_svp_err_x = 0;
   jet_antikt4truth_flavor_component_svp_err_y = 0;
   jet_antikt4truth_flavor_component_svp_err_z = 0;
   jet_antikt4truth_flavor_component_svp_cov_xy = 0;
   jet_antikt4truth_flavor_component_svp_cov_xz = 0;
   jet_antikt4truth_flavor_component_svp_cov_yz = 0;
   jet_antikt4truth_flavor_component_svp_chi2 = 0;
   jet_antikt4truth_flavor_component_svp_ndof = 0;
   jet_antikt4truth_flavor_component_svp_ntrk = 0;
   jet_antikt4truth_flavor_component_svp_trk_n = 0;
   jet_antikt4truth_flavor_component_svp_trk_index = 0;
   jet_antikt4truth_flavor_component_sv0p_isValid = 0;
   jet_antikt4truth_flavor_component_sv0p_ntrkv = 0;
   jet_antikt4truth_flavor_component_sv0p_ntrkj = 0;
   jet_antikt4truth_flavor_component_sv0p_n2t = 0;
   jet_antikt4truth_flavor_component_sv0p_mass = 0;
   jet_antikt4truth_flavor_component_sv0p_efrc = 0;
   jet_antikt4truth_flavor_component_sv0p_x = 0;
   jet_antikt4truth_flavor_component_sv0p_y = 0;
   jet_antikt4truth_flavor_component_sv0p_z = 0;
   jet_antikt4truth_flavor_component_sv0p_err_x = 0;
   jet_antikt4truth_flavor_component_sv0p_err_y = 0;
   jet_antikt4truth_flavor_component_sv0p_err_z = 0;
   jet_antikt4truth_flavor_component_sv0p_cov_xy = 0;
   jet_antikt4truth_flavor_component_sv0p_cov_xz = 0;
   jet_antikt4truth_flavor_component_sv0p_cov_yz = 0;
   jet_antikt4truth_flavor_component_sv0p_chi2 = 0;
   jet_antikt4truth_flavor_component_sv0p_ndof = 0;
   jet_antikt4truth_flavor_component_sv0p_ntrk = 0;
   jet_antikt4truth_flavor_component_sv0p_trk_n = 0;
   jet_antikt4truth_flavor_component_sv0p_trk_index = 0;
   jet_antikt4truth_flavor_component_softmuoninfo_muon_n = 0;
   jet_antikt4truth_flavor_component_softmuoninfo_muon_w = 0;
   jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel = 0;
   jet_antikt4truth_flavor_component_softelectron_electron_n = 0;
   jet_antikt4truth_flavor_component_softelectron_electron_isElectron = 0;
   jet_antikt4truth_flavor_component_softelectron_electron_d0 = 0;
   jet_antikt4truth_flavor_component_softelectron_electron_pTRel = 0;
   jet_antikt4truth_flavor_component_VKalbadtrack_n = 0;
   jet_antikt4truth_flavor_component_VKalbadtrack_index = 0;
   jet_antikt4truth_flavor_component_jfitvx_theta = 0;
   jet_antikt4truth_flavor_component_jfitvx_phi = 0;
   jet_antikt4truth_flavor_component_jfitvx_errtheta = 0;
   jet_antikt4truth_flavor_component_jfitvx_errphi = 0;
   jet_antikt4truth_flavor_component_jfitvx_chi2 = 0;
   jet_antikt4truth_flavor_component_jfitvx_ndof = 0;
   jet_antikt4truth_flavor_component_jfvxonjetaxis_n = 0;
   jet_antikt4truth_flavor_component_jfvxonjetaxis_index = 0;
   jet_antikt4truth_flavor_component_jftwotrackvertex_n = 0;
   jet_antikt4truth_flavor_component_jftwotrackvertex_index = 0;
   jet_antikt4truth_L1_dr = 0;
   jet_antikt4truth_L1_matched = 0;
   jet_antikt4truth_L2_dr = 0;
   jet_antikt4truth_L2_matched = 0;
   jet_antikt4truth_EF_dr = 0;
   jet_antikt4truth_EF_matched = 0;
   jfvxonjetaxis_vtxPos = 0;
   jfvxonjetaxis_vtxErr = 0;
   jfvxonjetaxis_trk_n = 0;
   jfvxonjetaxis_trk_phiAtVx = 0;
   jfvxonjetaxis_trk_thetaAtVx = 0;
   jfvxonjetaxis_trk_ptAtVx = 0;
   jfvxonjetaxis_trk_index = 0;
   jftwotrkvertex_isNeutral = 0;
   jftwotrkvertex_chi2 = 0;
   jftwotrkvertex_ndof = 0;
   jftwotrkvertex_x = 0;
   jftwotrkvertex_y = 0;
   jftwotrkvertex_z = 0;
   jftwotrkvertex_errx = 0;
   jftwotrkvertex_erry = 0;
   jftwotrkvertex_errz = 0;
   jftwotrkvertex_mass = 0;
   jftwotrkvertex_trk_n = 0;
   jftwotrkvertex_trk_index = 0;
   pileupinfo_time = 0;
   pileupinfo_index = 0;
   pileupinfo_type = 0;
   pileupinfo_runNumber = 0;
   pileupinfo_EventNumber = 0;
   trk_d0 = 0;
   trk_z0 = 0;
   trk_phi = 0;
   trk_theta = 0;
   trk_qoverp = 0;
   trk_pt = 0;
   trk_eta = 0;
   trk_err_d0 = 0;
   trk_err_z0 = 0;
   trk_err_phi = 0;
   trk_err_theta = 0;
   trk_err_qoverp = 0;
   trk_cov_d0_z0 = 0;
   trk_cov_d0_phi = 0;
   trk_cov_d0_theta = 0;
   trk_cov_d0_qoverp = 0;
   trk_cov_z0_phi = 0;
   trk_cov_z0_theta = 0;
   trk_cov_z0_qoverp = 0;
   trk_cov_phi_theta = 0;
   trk_cov_phi_qoverp = 0;
   trk_cov_theta_qoverp = 0;
   trk_IPEstimate_d0_biased_wrtPV = 0;
   trk_IPEstimate_z0_biased_wrtPV = 0;
   trk_IPEstimate_d0_unbiased_wrtPV = 0;
   trk_IPEstimate_z0_unbiased_wrtPV = 0;
   trk_IPEstimate_err_d0_biased_wrtPV = 0;
   trk_IPEstimate_err_z0_biased_wrtPV = 0;
   trk_IPEstimate_err_d0_unbiased_wrtPV = 0;
   trk_IPEstimate_err_z0_unbiased_wrtPV = 0;
   trk_IPEstimate_errPV_d0_biased_wrtPV = 0;
   trk_IPEstimate_errPV_z0_biased_wrtPV = 0;
   trk_IPEstimate_errPV_d0_unbiased_wrtPV = 0;
   trk_IPEstimate_errPV_z0_unbiased_wrtPV = 0;
   trk_d0_wrtPV = 0;
   trk_z0_wrtPV = 0;
   trk_phi_wrtPV = 0;
   trk_err_d0_wrtPV = 0;
   trk_err_z0_wrtPV = 0;
   trk_err_phi_wrtPV = 0;
   trk_err_theta_wrtPV = 0;
   trk_err_qoverp_wrtPV = 0;
   trk_cov_d0_z0_wrtPV = 0;
   trk_cov_d0_phi_wrtPV = 0;
   trk_cov_d0_theta_wrtPV = 0;
   trk_cov_d0_qoverp_wrtPV = 0;
   trk_cov_z0_phi_wrtPV = 0;
   trk_cov_z0_theta_wrtPV = 0;
   trk_cov_z0_qoverp_wrtPV = 0;
   trk_cov_phi_theta_wrtPV = 0;
   trk_cov_phi_qoverp_wrtPV = 0;
   trk_cov_theta_qoverp_wrtPV = 0;
   trk_d0_wrtBS = 0;
   trk_z0_wrtBS = 0;
   trk_phi_wrtBS = 0;
   trk_err_d0_wrtBS = 0;
   trk_err_z0_wrtBS = 0;
   trk_err_phi_wrtBS = 0;
   trk_err_theta_wrtBS = 0;
   trk_err_qoverp_wrtBS = 0;
   trk_chi2 = 0;
   trk_ndof = 0;
   trk_nBLHits = 0;
   trk_nPixHits = 0;
   trk_nSCTHits = 0;
   trk_nTRTHits = 0;
   trk_nTRTHighTHits = 0;
   trk_nPixHoles = 0;
   trk_nSCTHoles = 0;
   trk_nTRTHoles = 0;
   trk_nBLSharedHits = 0;
   trk_nPixSharedHits = 0;
   trk_nSCTSharedHits = 0;
   trk_nBLayerOutliers = 0;
   trk_nPixelOutliers = 0;
   trk_nSCTOutliers = 0;
   trk_nTRTOutliers = 0;
   trk_nTRTHighTOutliers = 0;
   trk_nContribPixelLayers = 0;
   trk_nGangedPixels = 0;
   trk_nGangedFlaggedFakes = 0;
   trk_nPixelDeadSensors = 0;
   trk_nPixelSpoiltHits = 0;
   trk_nSCTDoubleHoles = 0;
   trk_nSCTDeadSensors = 0;
   trk_nSCTSpoiltHits = 0;
   trk_expectBLayerHit = 0;
   trk_hitPattern = 0;
   trk_nSiHits = 0;
   trk_fitter = 0;
   trk_patternReco1 = 0;
   trk_patternReco2 = 0;
   trk_seedFinder = 0;
   trk_blayerPrediction_x = 0;
   trk_blayerPrediction_y = 0;
   trk_blayerPrediction_z = 0;
   trk_blayerPrediction_locX = 0;
   trk_blayerPrediction_locY = 0;
   trk_blayerPrediction_err_locX = 0;
   trk_blayerPrediction_err_locY = 0;
   trk_blayerPrediction_etaDistToEdge = 0;
   trk_blayerPrediction_phiDistToEdge = 0;
   trk_blayerPrediction_detElementId = 0;
   trk_blayerPrediction_row = 0;
   trk_blayerPrediction_col = 0;
   trk_blayerPrediction_type = 0;
   trk_BLayer_hit_n = 0;
   trk_BLayer_hit_id = 0;
   trk_BLayer_hit_detElementId = 0;
   trk_BLayer_hit_bec = 0;
   trk_BLayer_hit_layer = 0;
   trk_BLayer_hit_charge = 0;
   trk_BLayer_hit_sizePhi = 0;
   trk_BLayer_hit_sizeZ = 0;
   trk_BLayer_hit_size = 0;
   trk_BLayer_hit_isFake = 0;
   trk_BLayer_hit_isGanged = 0;
   trk_BLayer_hit_isSplit = 0;
   trk_BLayer_hit_splitProb1 = 0;
   trk_BLayer_hit_splitProb2 = 0;
   trk_BLayer_hit_isCompetingRIO = 0;
   trk_BLayer_hit_locX = 0;
   trk_BLayer_hit_locY = 0;
   trk_BLayer_hit_incidencePhi = 0;
   trk_BLayer_hit_incidenceTheta = 0;
   trk_BLayer_hit_err_locX = 0;
   trk_BLayer_hit_err_locY = 0;
   trk_BLayer_hit_cov_locXY = 0;
   trk_BLayer_hit_x = 0;
   trk_BLayer_hit_y = 0;
   trk_BLayer_hit_z = 0;
   trk_BLayer_hit_trkLocX = 0;
   trk_BLayer_hit_trkLocY = 0;
   trk_BLayer_hit_err_trkLocX = 0;
   trk_BLayer_hit_err_trkLocY = 0;
   trk_BLayer_hit_cov_trkLocXY = 0;
   trk_BLayer_hit_locBiasedResidualX = 0;
   trk_BLayer_hit_locBiasedResidualY = 0;
   trk_BLayer_hit_locBiasedPullX = 0;
   trk_BLayer_hit_locBiasedPullY = 0;
   trk_BLayer_hit_locUnbiasedResidualX = 0;
   trk_BLayer_hit_locUnbiasedResidualY = 0;
   trk_BLayer_hit_locUnbiasedPullX = 0;
   trk_BLayer_hit_locUnbiasedPullY = 0;
   trk_BLayer_hit_chi2 = 0;
   trk_BLayer_hit_ndof = 0;
   trk_Pixel_hit_n = 0;
   trk_Pixel_hit_id = 0;
   trk_Pixel_hit_detElementId = 0;
   trk_Pixel_hit_bec = 0;
   trk_Pixel_hit_layer = 0;
   trk_Pixel_hit_charge = 0;
   trk_Pixel_hit_sizePhi = 0;
   trk_Pixel_hit_sizeZ = 0;
   trk_Pixel_hit_size = 0;
   trk_Pixel_hit_isFake = 0;
   trk_Pixel_hit_isGanged = 0;
   trk_Pixel_hit_isSplit = 0;
   trk_Pixel_hit_splitProb1 = 0;
   trk_Pixel_hit_splitProb2 = 0;
   trk_Pixel_hit_isCompetingRIO = 0;
   trk_Pixel_hit_locX = 0;
   trk_Pixel_hit_locY = 0;
   trk_Pixel_hit_incidencePhi = 0;
   trk_Pixel_hit_incidenceTheta = 0;
   trk_Pixel_hit_err_locX = 0;
   trk_Pixel_hit_err_locY = 0;
   trk_Pixel_hit_cov_locXY = 0;
   trk_Pixel_hit_x = 0;
   trk_Pixel_hit_y = 0;
   trk_Pixel_hit_z = 0;
   trk_Pixel_hit_trkLocX = 0;
   trk_Pixel_hit_trkLocY = 0;
   trk_Pixel_hit_err_trkLocX = 0;
   trk_Pixel_hit_err_trkLocY = 0;
   trk_Pixel_hit_cov_trkLocXY = 0;
   trk_Pixel_hit_locBiasedResidualX = 0;
   trk_Pixel_hit_locBiasedResidualY = 0;
   trk_Pixel_hit_locBiasedPullX = 0;
   trk_Pixel_hit_locBiasedPullY = 0;
   trk_Pixel_hit_locUnbiasedResidualX = 0;
   trk_Pixel_hit_locUnbiasedResidualY = 0;
   trk_Pixel_hit_locUnbiasedPullX = 0;
   trk_Pixel_hit_locUnbiasedPullY = 0;
   trk_Pixel_hit_chi2 = 0;
   trk_Pixel_hit_ndof = 0;
   trk_SCT_hit_n = 0;
   trk_SCT_hit_id = 0;
   trk_SCT_hit_detElementId = 0;
   trk_SCT_hit_bec = 0;
   trk_SCT_hit_layer = 0;
   trk_SCT_hit_sizePhi = 0;
   trk_SCT_hit_isCompetingRIO = 0;
   trk_SCT_hit_locX = 0;
   trk_SCT_hit_locY = 0;
   trk_SCT_hit_incidencePhi = 0;
   trk_SCT_hit_incidenceTheta = 0;
   trk_SCT_hit_err_locX = 0;
   trk_SCT_hit_err_locY = 0;
   trk_SCT_hit_cov_locXY = 0;
   trk_SCT_hit_x = 0;
   trk_SCT_hit_y = 0;
   trk_SCT_hit_z = 0;
   trk_SCT_hit_trkLocX = 0;
   trk_SCT_hit_trkLocY = 0;
   trk_SCT_hit_err_trkLocX = 0;
   trk_SCT_hit_err_trkLocY = 0;
   trk_SCT_hit_cov_trkLocXY = 0;
   trk_SCT_hit_locBiasedResidualX = 0;
   trk_SCT_hit_locBiasedResidualY = 0;
   trk_SCT_hit_locBiasedPullX = 0;
   trk_SCT_hit_locBiasedPullY = 0;
   trk_SCT_hit_locUnbiasedResidualX = 0;
   trk_SCT_hit_locUnbiasedResidualY = 0;
   trk_SCT_hit_locUnbiasedPullX = 0;
   trk_SCT_hit_locUnbiasedPullY = 0;
   trk_SCT_hit_chi2 = 0;
   trk_SCT_hit_ndof = 0;
   trk_BLayer_outlier_n = 0;
   trk_BLayer_outlier_id = 0;
   trk_BLayer_outlier_detElementId = 0;
   trk_BLayer_outlier_bec = 0;
   trk_BLayer_outlier_layer = 0;
   trk_BLayer_outlier_charge = 0;
   trk_BLayer_outlier_sizePhi = 0;
   trk_BLayer_outlier_sizeZ = 0;
   trk_BLayer_outlier_size = 0;
   trk_BLayer_outlier_isFake = 0;
   trk_BLayer_outlier_isGanged = 0;
   trk_BLayer_outlier_isSplit = 0;
   trk_BLayer_outlier_splitProb1 = 0;
   trk_BLayer_outlier_splitProb2 = 0;
   trk_BLayer_outlier_isCompetingRIO = 0;
   trk_BLayer_outlier_locX = 0;
   trk_BLayer_outlier_locY = 0;
   trk_BLayer_outlier_incidencePhi = 0;
   trk_BLayer_outlier_incidenceTheta = 0;
   trk_BLayer_outlier_err_locX = 0;
   trk_BLayer_outlier_err_locY = 0;
   trk_BLayer_outlier_cov_locXY = 0;
   trk_BLayer_outlier_x = 0;
   trk_BLayer_outlier_y = 0;
   trk_BLayer_outlier_z = 0;
   trk_BLayer_outlier_trkLocX = 0;
   trk_BLayer_outlier_trkLocY = 0;
   trk_BLayer_outlier_err_trkLocX = 0;
   trk_BLayer_outlier_err_trkLocY = 0;
   trk_BLayer_outlier_cov_trkLocXY = 0;
   trk_BLayer_outlier_locBiasedResidualX = 0;
   trk_BLayer_outlier_locBiasedResidualY = 0;
   trk_BLayer_outlier_locBiasedPullX = 0;
   trk_BLayer_outlier_locBiasedPullY = 0;
   trk_BLayer_outlier_locUnbiasedResidualX = 0;
   trk_BLayer_outlier_locUnbiasedResidualY = 0;
   trk_BLayer_outlier_locUnbiasedPullX = 0;
   trk_BLayer_outlier_locUnbiasedPullY = 0;
   trk_BLayer_outlier_chi2 = 0;
   trk_BLayer_outlier_ndof = 0;
   trk_Pixel_outlier_n = 0;
   trk_Pixel_outlier_id = 0;
   trk_Pixel_outlier_detElementId = 0;
   trk_Pixel_outlier_bec = 0;
   trk_Pixel_outlier_layer = 0;
   trk_Pixel_outlier_charge = 0;
   trk_Pixel_outlier_sizePhi = 0;
   trk_Pixel_outlier_sizeZ = 0;
   trk_Pixel_outlier_size = 0;
   trk_Pixel_outlier_isFake = 0;
   trk_Pixel_outlier_isGanged = 0;
   trk_Pixel_outlier_isSplit = 0;
   trk_Pixel_outlier_splitProb1 = 0;
   trk_Pixel_outlier_splitProb2 = 0;
   trk_Pixel_outlier_isCompetingRIO = 0;
   trk_Pixel_outlier_locX = 0;
   trk_Pixel_outlier_locY = 0;
   trk_Pixel_outlier_incidencePhi = 0;
   trk_Pixel_outlier_incidenceTheta = 0;
   trk_Pixel_outlier_err_locX = 0;
   trk_Pixel_outlier_err_locY = 0;
   trk_Pixel_outlier_cov_locXY = 0;
   trk_Pixel_outlier_x = 0;
   trk_Pixel_outlier_y = 0;
   trk_Pixel_outlier_z = 0;
   trk_Pixel_outlier_trkLocX = 0;
   trk_Pixel_outlier_trkLocY = 0;
   trk_Pixel_outlier_err_trkLocX = 0;
   trk_Pixel_outlier_err_trkLocY = 0;
   trk_Pixel_outlier_cov_trkLocXY = 0;
   trk_Pixel_outlier_locBiasedResidualX = 0;
   trk_Pixel_outlier_locBiasedResidualY = 0;
   trk_Pixel_outlier_locBiasedPullX = 0;
   trk_Pixel_outlier_locBiasedPullY = 0;
   trk_Pixel_outlier_locUnbiasedResidualX = 0;
   trk_Pixel_outlier_locUnbiasedResidualY = 0;
   trk_Pixel_outlier_locUnbiasedPullX = 0;
   trk_Pixel_outlier_locUnbiasedPullY = 0;
   trk_Pixel_outlier_chi2 = 0;
   trk_Pixel_outlier_ndof = 0;
   trk_BLayer_hole_n = 0;
   trk_BLayer_hole_detElementId = 0;
   trk_BLayer_hole_bec = 0;
   trk_BLayer_hole_layer = 0;
   trk_BLayer_hole_trkLocX = 0;
   trk_BLayer_hole_trkLocY = 0;
   trk_BLayer_hole_err_trkLocX = 0;
   trk_BLayer_hole_err_trkLocY = 0;
   trk_BLayer_hole_cov_trkLocXY = 0;
   trk_Pixel_hole_n = 0;
   trk_Pixel_hole_detElementId = 0;
   trk_Pixel_hole_bec = 0;
   trk_Pixel_hole_layer = 0;
   trk_Pixel_hole_trkLocX = 0;
   trk_Pixel_hole_trkLocY = 0;
   trk_Pixel_hole_err_trkLocX = 0;
   trk_Pixel_hole_err_trkLocY = 0;
   trk_Pixel_hole_cov_trkLocXY = 0;
   trk_primvx_weight = 0;
   trk_primvx_index = 0;
   trk_mcpart_probability = 0;
   trk_mcpart_barcode = 0;
   trk_mcpart_index = 0;
   trk_detailed_mc_n = 0;
   trk_detailed_mc_nCommonPixHits = 0;
   trk_detailed_mc_nCommonSCTHits = 0;
   trk_detailed_mc_nCommonTRTHits = 0;
   trk_detailed_mc_nRecoPixHits = 0;
   trk_detailed_mc_nRecoSCTHits = 0;
   trk_detailed_mc_nRecoTRTHits = 0;
   trk_detailed_mc_nTruthPixHits = 0;
   trk_detailed_mc_nTruthSCTHits = 0;
   trk_detailed_mc_nTruthTRTHits = 0;
   trk_detailed_mc_begVtx_barcode = 0;
   trk_detailed_mc_endVtx_barcode = 0;
   trk_detailed_mc_barcode = 0;
   trk_detailed_mc_index = 0;
   pixClus_id = 0;
   pixClus_bec = 0;
   pixClus_layer = 0;
   pixClus_detElementId = 0;
   pixClus_phi_module = 0;
   pixClus_eta_module = 0;
   pixClus_col = 0;
   pixClus_row = 0;
   pixClus_charge = 0;
   pixClus_LVL1A = 0;
   pixClus_sizePhi = 0;
   pixClus_sizeZ = 0;
   pixClus_size = 0;
   pixClus_locX = 0;
   pixClus_locY = 0;
   pixClus_x = 0;
   pixClus_y = 0;
   pixClus_z = 0;
   pixClus_isFake = 0;
   pixClus_isGanged = 0;
   pixClus_isSplit = 0;
   pixClus_splitProb1 = 0;
   pixClus_splitProb2 = 0;
   pixClus_mc_barcode = 0;
   primvx_x = 0;
   primvx_y = 0;
   primvx_z = 0;
   primvx_err_x = 0;
   primvx_err_y = 0;
   primvx_err_z = 0;
   primvx_cov_xy = 0;
   primvx_cov_xz = 0;
   primvx_cov_yz = 0;
   primvx_chi2 = 0;
   primvx_ndof = 0;
   primvx_px = 0;
   primvx_py = 0;
   primvx_pz = 0;
   primvx_E = 0;
   primvx_m = 0;
   primvx_nTracks = 0;
   primvx_sumPt = 0;
   primvx_type = 0;
   primvx_trk_n = 0;
   primvx_trk_weight = 0;
   primvx_trk_unbiased_d0 = 0;
   primvx_trk_unbiased_z0 = 0;
   primvx_trk_err_unbiased_d0 = 0;
   primvx_trk_err_unbiased_z0 = 0;
   primvx_trk_chi2 = 0;
   primvx_trk_d0 = 0;
   primvx_trk_z0 = 0;
   primvx_trk_phi = 0;
   primvx_trk_theta = 0;
   primvx_trk_index = 0;
   mcevt_signal_process_id = 0;
   mcevt_event_number = 0;
   mcevt_event_scale = 0;
   mcevt_alphaQCD = 0;
   mcevt_alphaQED = 0;
   mcevt_pdf_id1 = 0;
   mcevt_pdf_id2 = 0;
   mcevt_pdf_x1 = 0;
   mcevt_pdf_x2 = 0;
   mcevt_pdf_scale = 0;
   mcevt_pdf1 = 0;
   mcevt_pdf2 = 0;
   mcevt_weight = 0;
   mcevt_nparticle = 0;
   mcevt_pileUpType = 0;
   mcvtx_x = 0;
   mcvtx_y = 0;
   mcvtx_z = 0;
   mcpart_pt = 0;
   mcpart_m = 0;
   mcpart_eta = 0;
   mcpart_phi = 0;
   mcpart_type = 0;
   mcpart_status = 0;
   mcpart_barcode = 0;
   mcpart_mothertype = 0;
   mcpart_motherbarcode = 0;
   mcpart_mcevt_index = 0;
   mcpart_mcprodvtx_index = 0;
   mcpart_mother_n = 0;
   mcpart_mother_index = 0;
   mcpart_mcdecayvtx_index = 0;
   mcpart_child_n = 0;
   mcpart_child_index = 0;
   mcpart_truthtracks_index = 0;
   truthtrack_ok = 0;
   truthtrack_d0 = 0;
   truthtrack_z0 = 0;
   truthtrack_phi = 0;
   truthtrack_theta = 0;
   truthtrack_qoverp = 0;
   deadPixMod_idHash = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("jet_antikt4truth_n", &jet_antikt4truth_n, &b_jet_antikt4truth_n);
   fChain->SetBranchAddress("jet_antikt4truth_E", &jet_antikt4truth_E, &b_jet_antikt4truth_E);
   fChain->SetBranchAddress("jet_antikt4truth_pt", &jet_antikt4truth_pt, &b_jet_antikt4truth_pt);
   fChain->SetBranchAddress("jet_antikt4truth_m", &jet_antikt4truth_m, &b_jet_antikt4truth_m);
   fChain->SetBranchAddress("jet_antikt4truth_eta", &jet_antikt4truth_eta, &b_jet_antikt4truth_eta);
   fChain->SetBranchAddress("jet_antikt4truth_phi", &jet_antikt4truth_phi, &b_jet_antikt4truth_phi);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_Comb", &jet_antikt4truth_flavor_weight_Comb, &b_jet_antikt4truth_flavor_weight_Comb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_IP2D", &jet_antikt4truth_flavor_weight_IP2D, &b_jet_antikt4truth_flavor_weight_IP2D);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_IP3D", &jet_antikt4truth_flavor_weight_IP3D, &b_jet_antikt4truth_flavor_weight_IP3D);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_SV0", &jet_antikt4truth_flavor_weight_SV0, &b_jet_antikt4truth_flavor_weight_SV0);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_SV1", &jet_antikt4truth_flavor_weight_SV1, &b_jet_antikt4truth_flavor_weight_SV1);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_SV2", &jet_antikt4truth_flavor_weight_SV2, &b_jet_antikt4truth_flavor_weight_SV2);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_JetProb", &jet_antikt4truth_flavor_weight_JetProb, &b_jet_antikt4truth_flavor_weight_JetProb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_TrackCounting2D", &jet_antikt4truth_flavor_weight_TrackCounting2D, &b_jet_antikt4truth_flavor_weight_TrackCounting2D);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_SoftMuonTag", &jet_antikt4truth_flavor_weight_SoftMuonTag, &b_jet_antikt4truth_flavor_weight_SoftMuonTag);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_SoftElectronTag", &jet_antikt4truth_flavor_weight_SoftElectronTag, &b_jet_antikt4truth_flavor_weight_SoftElectronTag);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_JetFitterTagNN", &jet_antikt4truth_flavor_weight_JetFitterTagNN, &b_jet_antikt4truth_flavor_weight_JetFitterTagNN);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_weight_JetFitterCOMBNN", &jet_antikt4truth_flavor_weight_JetFitterCOMBNN, &b_jet_antikt4truth_flavor_weight_JetFitterCOMBNN);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_label", &jet_antikt4truth_flavor_truth_label, &b_jet_antikt4truth_flavor_truth_label);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_dRminToB", &jet_antikt4truth_flavor_truth_dRminToB, &b_jet_antikt4truth_flavor_truth_dRminToB);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_dRminToC", &jet_antikt4truth_flavor_truth_dRminToC, &b_jet_antikt4truth_flavor_truth_dRminToC);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_dRminToT", &jet_antikt4truth_flavor_truth_dRminToT, &b_jet_antikt4truth_flavor_truth_dRminToT);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_BHadronpdg", &jet_antikt4truth_flavor_truth_BHadronpdg, &b_jet_antikt4truth_flavor_truth_BHadronpdg);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_vx_x", &jet_antikt4truth_flavor_truth_vx_x, &b_jet_antikt4truth_flavor_truth_vx_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_vx_y", &jet_antikt4truth_flavor_truth_vx_y, &b_jet_antikt4truth_flavor_truth_vx_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_truth_vx_z", &jet_antikt4truth_flavor_truth_vx_z, &b_jet_antikt4truth_flavor_truth_vx_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_label", &jet_antikt4truth_flavor_putruth_label, &b_jet_antikt4truth_flavor_putruth_label);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_dRminToB", &jet_antikt4truth_flavor_putruth_dRminToB, &b_jet_antikt4truth_flavor_putruth_dRminToB);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_dRminToC", &jet_antikt4truth_flavor_putruth_dRminToC, &b_jet_antikt4truth_flavor_putruth_dRminToC);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_dRminToT", &jet_antikt4truth_flavor_putruth_dRminToT, &b_jet_antikt4truth_flavor_putruth_dRminToT);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_BHadronpdg", &jet_antikt4truth_flavor_putruth_BHadronpdg, &b_jet_antikt4truth_flavor_putruth_BHadronpdg);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_vx_x", &jet_antikt4truth_flavor_putruth_vx_x, &b_jet_antikt4truth_flavor_putruth_vx_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_vx_y", &jet_antikt4truth_flavor_putruth_vx_y, &b_jet_antikt4truth_flavor_putruth_vx_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_putruth_vx_z", &jet_antikt4truth_flavor_putruth_vx_z, &b_jet_antikt4truth_flavor_putruth_vx_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_n", &jet_antikt4truth_flavor_component_assoctrk_n, &b_jet_antikt4truth_flavor_component_assoctrk_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_index", &jet_antikt4truth_flavor_component_assoctrk_index, &b_jet_antikt4truth_flavor_component_assoctrk_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_signOfIP", &jet_antikt4truth_flavor_component_assoctrk_signOfIP, &b_jet_antikt4truth_flavor_component_assoctrk_signOfIP);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_signOfZIP", &jet_antikt4truth_flavor_component_assoctrk_signOfZIP, &b_jet_antikt4truth_flavor_component_assoctrk_signOfZIP);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx", &jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx, &b_jet_antikt4truth_flavor_component_assoctrk_ud0wrtPriVtx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx", &jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx, &b_jet_antikt4truth_flavor_component_assoctrk_ud0ErrwrtPriVtx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx", &jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx, &b_jet_antikt4truth_flavor_component_assoctrk_uz0wrtPriVtx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx", &jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx, &b_jet_antikt4truth_flavor_component_assoctrk_uz0ErrwrtPriVtx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_gentruthlepton_n", &jet_antikt4truth_flavor_component_gentruthlepton_n, &b_jet_antikt4truth_flavor_component_gentruthlepton_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_gentruthlepton_index", &jet_antikt4truth_flavor_component_gentruthlepton_index, &b_jet_antikt4truth_flavor_component_gentruthlepton_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_gentruthlepton_origin", &jet_antikt4truth_flavor_component_gentruthlepton_origin, &b_jet_antikt4truth_flavor_component_gentruthlepton_origin);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_gentruthlepton_slbarcode", &jet_antikt4truth_flavor_component_gentruthlepton_slbarcode, &b_jet_antikt4truth_flavor_component_gentruthlepton_slbarcode);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip2d_pu", &jet_antikt4truth_flavor_component_ip2d_pu, &b_jet_antikt4truth_flavor_component_ip2d_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip2d_pb", &jet_antikt4truth_flavor_component_ip2d_pb, &b_jet_antikt4truth_flavor_component_ip2d_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip2d_isValid", &jet_antikt4truth_flavor_component_ip2d_isValid, &b_jet_antikt4truth_flavor_component_ip2d_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip2d_ntrk", &jet_antikt4truth_flavor_component_ip2d_ntrk, &b_jet_antikt4truth_flavor_component_ip2d_ntrk);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip3d_pu", &jet_antikt4truth_flavor_component_ip3d_pu, &b_jet_antikt4truth_flavor_component_ip3d_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip3d_pb", &jet_antikt4truth_flavor_component_ip3d_pb, &b_jet_antikt4truth_flavor_component_ip3d_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip3d_isValid", &jet_antikt4truth_flavor_component_ip3d_isValid, &b_jet_antikt4truth_flavor_component_ip3d_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ip3d_ntrk", &jet_antikt4truth_flavor_component_ip3d_ntrk, &b_jet_antikt4truth_flavor_component_ip3d_ntrk);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv1_pu", &jet_antikt4truth_flavor_component_sv1_pu, &b_jet_antikt4truth_flavor_component_sv1_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv1_pb", &jet_antikt4truth_flavor_component_sv1_pb, &b_jet_antikt4truth_flavor_component_sv1_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv1_isValid", &jet_antikt4truth_flavor_component_sv1_isValid, &b_jet_antikt4truth_flavor_component_sv1_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv2_pu", &jet_antikt4truth_flavor_component_sv2_pu, &b_jet_antikt4truth_flavor_component_sv2_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv2_pb", &jet_antikt4truth_flavor_component_sv2_pb, &b_jet_antikt4truth_flavor_component_sv2_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv2_isValid", &jet_antikt4truth_flavor_component_sv2_isValid, &b_jet_antikt4truth_flavor_component_sv2_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_pu", &jet_antikt4truth_flavor_component_jfit_pu, &b_jet_antikt4truth_flavor_component_jfit_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_pb", &jet_antikt4truth_flavor_component_jfit_pb, &b_jet_antikt4truth_flavor_component_jfit_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_pc", &jet_antikt4truth_flavor_component_jfit_pc, &b_jet_antikt4truth_flavor_component_jfit_pc);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_isValid", &jet_antikt4truth_flavor_component_jfit_isValid, &b_jet_antikt4truth_flavor_component_jfit_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitcomb_pu", &jet_antikt4truth_flavor_component_jfitcomb_pu, &b_jet_antikt4truth_flavor_component_jfitcomb_pu);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitcomb_pb", &jet_antikt4truth_flavor_component_jfitcomb_pb, &b_jet_antikt4truth_flavor_component_jfitcomb_pb);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitcomb_pc", &jet_antikt4truth_flavor_component_jfitcomb_pc, &b_jet_antikt4truth_flavor_component_jfitcomb_pc);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitcomb_isValid", &jet_antikt4truth_flavor_component_jfitcomb_isValid, &b_jet_antikt4truth_flavor_component_jfitcomb_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_nvtx", &jet_antikt4truth_flavor_component_jfit_nvtx, &b_jet_antikt4truth_flavor_component_jfit_nvtx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_nvtx1t", &jet_antikt4truth_flavor_component_jfit_nvtx1t, &b_jet_antikt4truth_flavor_component_jfit_nvtx1t);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_ntrkAtVx", &jet_antikt4truth_flavor_component_jfit_ntrkAtVx, &b_jet_antikt4truth_flavor_component_jfit_ntrkAtVx);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_efrc", &jet_antikt4truth_flavor_component_jfit_efrc, &b_jet_antikt4truth_flavor_component_jfit_efrc);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_mass", &jet_antikt4truth_flavor_component_jfit_mass, &b_jet_antikt4truth_flavor_component_jfit_mass);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_sig3d", &jet_antikt4truth_flavor_component_jfit_sig3d, &b_jet_antikt4truth_flavor_component_jfit_sig3d);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_deltaPhi", &jet_antikt4truth_flavor_component_jfit_deltaPhi, &b_jet_antikt4truth_flavor_component_jfit_deltaPhi);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_deltaEta", &jet_antikt4truth_flavor_component_jfit_deltaEta, &b_jet_antikt4truth_flavor_component_jfit_deltaEta);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_n", &jet_antikt4truth_flavor_component_ipplus_trk_n, &b_jet_antikt4truth_flavor_component_ipplus_trk_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_index", &jet_antikt4truth_flavor_component_ipplus_trk_index, &b_jet_antikt4truth_flavor_component_ipplus_trk_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_d0val", &jet_antikt4truth_flavor_component_ipplus_trk_d0val, &b_jet_antikt4truth_flavor_component_ipplus_trk_d0val);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_d0sig", &jet_antikt4truth_flavor_component_ipplus_trk_d0sig, &b_jet_antikt4truth_flavor_component_ipplus_trk_d0sig);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_z0val", &jet_antikt4truth_flavor_component_ipplus_trk_z0val, &b_jet_antikt4truth_flavor_component_ipplus_trk_z0val);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_z0sig", &jet_antikt4truth_flavor_component_ipplus_trk_z0sig, &b_jet_antikt4truth_flavor_component_ipplus_trk_z0sig);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_w2D", &jet_antikt4truth_flavor_component_ipplus_trk_w2D, &b_jet_antikt4truth_flavor_component_ipplus_trk_w2D);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_w3D", &jet_antikt4truth_flavor_component_ipplus_trk_w3D, &b_jet_antikt4truth_flavor_component_ipplus_trk_w3D);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_pJP", &jet_antikt4truth_flavor_component_ipplus_trk_pJP, &b_jet_antikt4truth_flavor_component_ipplus_trk_pJP);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_pJPneg", &jet_antikt4truth_flavor_component_ipplus_trk_pJPneg, &b_jet_antikt4truth_flavor_component_ipplus_trk_pJPneg);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_grade", &jet_antikt4truth_flavor_component_ipplus_trk_grade, &b_jet_antikt4truth_flavor_component_ipplus_trk_grade);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_isFromV0", &jet_antikt4truth_flavor_component_ipplus_trk_isFromV0, &b_jet_antikt4truth_flavor_component_ipplus_trk_isFromV0);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_isValid", &jet_antikt4truth_flavor_component_svp_isValid, &b_jet_antikt4truth_flavor_component_svp_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_ntrkv", &jet_antikt4truth_flavor_component_svp_ntrkv, &b_jet_antikt4truth_flavor_component_svp_ntrkv);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_ntrkj", &jet_antikt4truth_flavor_component_svp_ntrkj, &b_jet_antikt4truth_flavor_component_svp_ntrkj);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_n2t", &jet_antikt4truth_flavor_component_svp_n2t, &b_jet_antikt4truth_flavor_component_svp_n2t);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_mass", &jet_antikt4truth_flavor_component_svp_mass, &b_jet_antikt4truth_flavor_component_svp_mass);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_efrc", &jet_antikt4truth_flavor_component_svp_efrc, &b_jet_antikt4truth_flavor_component_svp_efrc);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_x", &jet_antikt4truth_flavor_component_svp_x, &b_jet_antikt4truth_flavor_component_svp_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_y", &jet_antikt4truth_flavor_component_svp_y, &b_jet_antikt4truth_flavor_component_svp_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_z", &jet_antikt4truth_flavor_component_svp_z, &b_jet_antikt4truth_flavor_component_svp_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_err_x", &jet_antikt4truth_flavor_component_svp_err_x, &b_jet_antikt4truth_flavor_component_svp_err_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_err_y", &jet_antikt4truth_flavor_component_svp_err_y, &b_jet_antikt4truth_flavor_component_svp_err_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_err_z", &jet_antikt4truth_flavor_component_svp_err_z, &b_jet_antikt4truth_flavor_component_svp_err_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_cov_xy", &jet_antikt4truth_flavor_component_svp_cov_xy, &b_jet_antikt4truth_flavor_component_svp_cov_xy);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_cov_xz", &jet_antikt4truth_flavor_component_svp_cov_xz, &b_jet_antikt4truth_flavor_component_svp_cov_xz);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_cov_yz", &jet_antikt4truth_flavor_component_svp_cov_yz, &b_jet_antikt4truth_flavor_component_svp_cov_yz);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_chi2", &jet_antikt4truth_flavor_component_svp_chi2, &b_jet_antikt4truth_flavor_component_svp_chi2);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_ndof", &jet_antikt4truth_flavor_component_svp_ndof, &b_jet_antikt4truth_flavor_component_svp_ndof);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_ntrk", &jet_antikt4truth_flavor_component_svp_ntrk, &b_jet_antikt4truth_flavor_component_svp_ntrk);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_trk_n", &jet_antikt4truth_flavor_component_svp_trk_n, &b_jet_antikt4truth_flavor_component_svp_trk_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_svp_trk_index", &jet_antikt4truth_flavor_component_svp_trk_index, &b_jet_antikt4truth_flavor_component_svp_trk_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_isValid", &jet_antikt4truth_flavor_component_sv0p_isValid, &b_jet_antikt4truth_flavor_component_sv0p_isValid);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_ntrkv", &jet_antikt4truth_flavor_component_sv0p_ntrkv, &b_jet_antikt4truth_flavor_component_sv0p_ntrkv);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_ntrkj", &jet_antikt4truth_flavor_component_sv0p_ntrkj, &b_jet_antikt4truth_flavor_component_sv0p_ntrkj);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_n2t", &jet_antikt4truth_flavor_component_sv0p_n2t, &b_jet_antikt4truth_flavor_component_sv0p_n2t);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_mass", &jet_antikt4truth_flavor_component_sv0p_mass, &b_jet_antikt4truth_flavor_component_sv0p_mass);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_efrc", &jet_antikt4truth_flavor_component_sv0p_efrc, &b_jet_antikt4truth_flavor_component_sv0p_efrc);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_x", &jet_antikt4truth_flavor_component_sv0p_x, &b_jet_antikt4truth_flavor_component_sv0p_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_y", &jet_antikt4truth_flavor_component_sv0p_y, &b_jet_antikt4truth_flavor_component_sv0p_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_z", &jet_antikt4truth_flavor_component_sv0p_z, &b_jet_antikt4truth_flavor_component_sv0p_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_err_x", &jet_antikt4truth_flavor_component_sv0p_err_x, &b_jet_antikt4truth_flavor_component_sv0p_err_x);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_err_y", &jet_antikt4truth_flavor_component_sv0p_err_y, &b_jet_antikt4truth_flavor_component_sv0p_err_y);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_err_z", &jet_antikt4truth_flavor_component_sv0p_err_z, &b_jet_antikt4truth_flavor_component_sv0p_err_z);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_cov_xy", &jet_antikt4truth_flavor_component_sv0p_cov_xy, &b_jet_antikt4truth_flavor_component_sv0p_cov_xy);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_cov_xz", &jet_antikt4truth_flavor_component_sv0p_cov_xz, &b_jet_antikt4truth_flavor_component_sv0p_cov_xz);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_cov_yz", &jet_antikt4truth_flavor_component_sv0p_cov_yz, &b_jet_antikt4truth_flavor_component_sv0p_cov_yz);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_chi2", &jet_antikt4truth_flavor_component_sv0p_chi2, &b_jet_antikt4truth_flavor_component_sv0p_chi2);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_ndof", &jet_antikt4truth_flavor_component_sv0p_ndof, &b_jet_antikt4truth_flavor_component_sv0p_ndof);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_ntrk", &jet_antikt4truth_flavor_component_sv0p_ntrk, &b_jet_antikt4truth_flavor_component_sv0p_ntrk);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_trk_n", &jet_antikt4truth_flavor_component_sv0p_trk_n, &b_jet_antikt4truth_flavor_component_sv0p_trk_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_sv0p_trk_index", &jet_antikt4truth_flavor_component_sv0p_trk_index, &b_jet_antikt4truth_flavor_component_sv0p_trk_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softmuoninfo_muon_n", &jet_antikt4truth_flavor_component_softmuoninfo_muon_n, &b_jet_antikt4truth_flavor_component_softmuoninfo_muon_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softmuoninfo_muon_w", &jet_antikt4truth_flavor_component_softmuoninfo_muon_w, &b_jet_antikt4truth_flavor_component_softmuoninfo_muon_w);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel", &jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel, &b_jet_antikt4truth_flavor_component_softmuoninfo_muon_pTRel);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softelectron_electron_n", &jet_antikt4truth_flavor_component_softelectron_electron_n, &b_jet_antikt4truth_flavor_component_softelectron_electron_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softelectron_electron_isElectron", &jet_antikt4truth_flavor_component_softelectron_electron_isElectron, &b_jet_antikt4truth_flavor_component_softelectron_electron_isElectron);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softelectron_electron_d0", &jet_antikt4truth_flavor_component_softelectron_electron_d0, &b_jet_antikt4truth_flavor_component_softelectron_electron_d0);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_softelectron_electron_pTRel", &jet_antikt4truth_flavor_component_softelectron_electron_pTRel, &b_jet_antikt4truth_flavor_component_softelectron_electron_pTRel);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_VKalbadtrack_n", &jet_antikt4truth_flavor_component_VKalbadtrack_n, &b_jet_antikt4truth_flavor_component_VKalbadtrack_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_VKalbadtrack_index", &jet_antikt4truth_flavor_component_VKalbadtrack_index, &b_jet_antikt4truth_flavor_component_VKalbadtrack_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_theta", &jet_antikt4truth_flavor_component_jfitvx_theta, &b_jet_antikt4truth_flavor_component_jfitvx_theta);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_phi", &jet_antikt4truth_flavor_component_jfitvx_phi, &b_jet_antikt4truth_flavor_component_jfitvx_phi);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_errtheta", &jet_antikt4truth_flavor_component_jfitvx_errtheta, &b_jet_antikt4truth_flavor_component_jfitvx_errtheta);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_errphi", &jet_antikt4truth_flavor_component_jfitvx_errphi, &b_jet_antikt4truth_flavor_component_jfitvx_errphi);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_chi2", &jet_antikt4truth_flavor_component_jfitvx_chi2, &b_jet_antikt4truth_flavor_component_jfitvx_chi2);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfitvx_ndof", &jet_antikt4truth_flavor_component_jfitvx_ndof, &b_jet_antikt4truth_flavor_component_jfitvx_ndof);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfvxonjetaxis_n", &jet_antikt4truth_flavor_component_jfvxonjetaxis_n, &b_jet_antikt4truth_flavor_component_jfvxonjetaxis_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jfvxonjetaxis_index", &jet_antikt4truth_flavor_component_jfvxonjetaxis_index, &b_jet_antikt4truth_flavor_component_jfvxonjetaxis_index);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jftwotrackvertex_n", &jet_antikt4truth_flavor_component_jftwotrackvertex_n, &b_jet_antikt4truth_flavor_component_jftwotrackvertex_n);
   fChain->SetBranchAddress("jet_antikt4truth_flavor_component_jftwotrackvertex_index", &jet_antikt4truth_flavor_component_jftwotrackvertex_index, &b_jet_antikt4truth_flavor_component_jftwotrackvertex_index);
   fChain->SetBranchAddress("jet_antikt4truth_L1_dr", &jet_antikt4truth_L1_dr, &b_jet_antikt4truth_L1_dr);
   fChain->SetBranchAddress("jet_antikt4truth_L1_matched", &jet_antikt4truth_L1_matched, &b_jet_antikt4truth_L1_matched);
   fChain->SetBranchAddress("jet_antikt4truth_L2_dr", &jet_antikt4truth_L2_dr, &b_jet_antikt4truth_L2_dr);
   fChain->SetBranchAddress("jet_antikt4truth_L2_matched", &jet_antikt4truth_L2_matched, &b_jet_antikt4truth_L2_matched);
   fChain->SetBranchAddress("jet_antikt4truth_EF_dr", &jet_antikt4truth_EF_dr, &b_jet_antikt4truth_EF_dr);
   fChain->SetBranchAddress("jet_antikt4truth_EF_matched", &jet_antikt4truth_EF_matched, &b_jet_antikt4truth_EF_matched);
   fChain->SetBranchAddress("jfvxonjetaxis_n", &jfvxonjetaxis_n, &b_jfvxonjetaxis_n);
   fChain->SetBranchAddress("jfvxonjetaxis_vtxPos", &jfvxonjetaxis_vtxPos, &b_jfvxonjetaxis_vtxPos);
   fChain->SetBranchAddress("jfvxonjetaxis_vtxErr", &jfvxonjetaxis_vtxErr, &b_jfvxonjetaxis_vtxErr);
   fChain->SetBranchAddress("jfvxonjetaxis_trk_n", &jfvxonjetaxis_trk_n, &b_jfvxonjetaxis_trk_n);
   fChain->SetBranchAddress("jfvxonjetaxis_trk_phiAtVx", &jfvxonjetaxis_trk_phiAtVx, &b_jfvxonjetaxis_trk_phiAtVx);
   fChain->SetBranchAddress("jfvxonjetaxis_trk_thetaAtVx", &jfvxonjetaxis_trk_thetaAtVx, &b_jfvxonjetaxis_trk_thetaAtVx);
   fChain->SetBranchAddress("jfvxonjetaxis_trk_ptAtVx", &jfvxonjetaxis_trk_ptAtVx, &b_jfvxonjetaxis_trk_ptAtVx);
   fChain->SetBranchAddress("jfvxonjetaxis_trk_index", &jfvxonjetaxis_trk_index, &b_jfvxonjetaxis_trk_index);
   fChain->SetBranchAddress("jftwotrkvertex_n", &jftwotrkvertex_n, &b_jftwotrkvertex_n);
   fChain->SetBranchAddress("jftwotrkvertex_isNeutral", &jftwotrkvertex_isNeutral, &b_jftwotrkvertex_isNeutral);
   fChain->SetBranchAddress("jftwotrkvertex_chi2", &jftwotrkvertex_chi2, &b_jftwotrkvertex_chi2);
   fChain->SetBranchAddress("jftwotrkvertex_ndof", &jftwotrkvertex_ndof, &b_jftwotrkvertex_ndof);
   fChain->SetBranchAddress("jftwotrkvertex_x", &jftwotrkvertex_x, &b_jftwotrkvertex_x);
   fChain->SetBranchAddress("jftwotrkvertex_y", &jftwotrkvertex_y, &b_jftwotrkvertex_y);
   fChain->SetBranchAddress("jftwotrkvertex_z", &jftwotrkvertex_z, &b_jftwotrkvertex_z);
   fChain->SetBranchAddress("jftwotrkvertex_errx", &jftwotrkvertex_errx, &b_jftwotrkvertex_errx);
   fChain->SetBranchAddress("jftwotrkvertex_erry", &jftwotrkvertex_erry, &b_jftwotrkvertex_erry);
   fChain->SetBranchAddress("jftwotrkvertex_errz", &jftwotrkvertex_errz, &b_jftwotrkvertex_errz);
   fChain->SetBranchAddress("jftwotrkvertex_mass", &jftwotrkvertex_mass, &b_jftwotrkvertex_mass);
   fChain->SetBranchAddress("jftwotrkvertex_trk_n", &jftwotrkvertex_trk_n, &b_jftwotrkvertex_trk_n);
   fChain->SetBranchAddress("jftwotrkvertex_trk_index", &jftwotrkvertex_trk_index, &b_jftwotrkvertex_trk_index);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("pileupinfo_n", &pileupinfo_n, &b_pileupinfo_n);
   fChain->SetBranchAddress("pileupinfo_time", &pileupinfo_time, &b_pileupinfo_time);
   fChain->SetBranchAddress("pileupinfo_index", &pileupinfo_index, &b_pileupinfo_index);
   fChain->SetBranchAddress("pileupinfo_type", &pileupinfo_type, &b_pileupinfo_type);
   fChain->SetBranchAddress("pileupinfo_runNumber", &pileupinfo_runNumber, &b_pileupinfo_runNumber);
   fChain->SetBranchAddress("pileupinfo_EventNumber", &pileupinfo_EventNumber, &b_pileupinfo_EventNumber);
   fChain->SetBranchAddress("trk_n", &trk_n, &b_trk_n);
   fChain->SetBranchAddress("trk_d0", &trk_d0, &b_trk_d0);
   fChain->SetBranchAddress("trk_z0", &trk_z0, &b_trk_z0);
   fChain->SetBranchAddress("trk_phi", &trk_phi, &b_trk_phi);
   fChain->SetBranchAddress("trk_theta", &trk_theta, &b_trk_theta);
   fChain->SetBranchAddress("trk_qoverp", &trk_qoverp, &b_trk_qoverp);
   fChain->SetBranchAddress("trk_pt", &trk_pt, &b_trk_pt);
   fChain->SetBranchAddress("trk_eta", &trk_eta, &b_trk_eta);
   fChain->SetBranchAddress("trk_err_d0", &trk_err_d0, &b_trk_err_d0);
   fChain->SetBranchAddress("trk_err_z0", &trk_err_z0, &b_trk_err_z0);
   fChain->SetBranchAddress("trk_err_phi", &trk_err_phi, &b_trk_err_phi);
   fChain->SetBranchAddress("trk_err_theta", &trk_err_theta, &b_trk_err_theta);
   fChain->SetBranchAddress("trk_err_qoverp", &trk_err_qoverp, &b_trk_err_qoverp);
   fChain->SetBranchAddress("trk_cov_d0_z0", &trk_cov_d0_z0, &b_trk_cov_d0_z0);
   fChain->SetBranchAddress("trk_cov_d0_phi", &trk_cov_d0_phi, &b_trk_cov_d0_phi);
   fChain->SetBranchAddress("trk_cov_d0_theta", &trk_cov_d0_theta, &b_trk_cov_d0_theta);
   fChain->SetBranchAddress("trk_cov_d0_qoverp", &trk_cov_d0_qoverp, &b_trk_cov_d0_qoverp);
   fChain->SetBranchAddress("trk_cov_z0_phi", &trk_cov_z0_phi, &b_trk_cov_z0_phi);
   fChain->SetBranchAddress("trk_cov_z0_theta", &trk_cov_z0_theta, &b_trk_cov_z0_theta);
   fChain->SetBranchAddress("trk_cov_z0_qoverp", &trk_cov_z0_qoverp, &b_trk_cov_z0_qoverp);
   fChain->SetBranchAddress("trk_cov_phi_theta", &trk_cov_phi_theta, &b_trk_cov_phi_theta);
   fChain->SetBranchAddress("trk_cov_phi_qoverp", &trk_cov_phi_qoverp, &b_trk_cov_phi_qoverp);
   fChain->SetBranchAddress("trk_cov_theta_qoverp", &trk_cov_theta_qoverp, &b_trk_cov_theta_qoverp);
   fChain->SetBranchAddress("trk_IPEstimate_d0_biased_wrtPV", &trk_IPEstimate_d0_biased_wrtPV, &b_trk_IPEstimate_d0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_z0_biased_wrtPV", &trk_IPEstimate_z0_biased_wrtPV, &b_trk_IPEstimate_z0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_d0_unbiased_wrtPV", &trk_IPEstimate_d0_unbiased_wrtPV, &b_trk_IPEstimate_d0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_z0_unbiased_wrtPV", &trk_IPEstimate_z0_unbiased_wrtPV, &b_trk_IPEstimate_z0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_err_d0_biased_wrtPV", &trk_IPEstimate_err_d0_biased_wrtPV, &b_trk_IPEstimate_err_d0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_err_z0_biased_wrtPV", &trk_IPEstimate_err_z0_biased_wrtPV, &b_trk_IPEstimate_err_z0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_err_d0_unbiased_wrtPV", &trk_IPEstimate_err_d0_unbiased_wrtPV, &b_trk_IPEstimate_err_d0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_err_z0_unbiased_wrtPV", &trk_IPEstimate_err_z0_unbiased_wrtPV, &b_trk_IPEstimate_err_z0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_errPV_d0_biased_wrtPV", &trk_IPEstimate_errPV_d0_biased_wrtPV, &b_trk_IPEstimate_errPV_d0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_errPV_z0_biased_wrtPV", &trk_IPEstimate_errPV_z0_biased_wrtPV, &b_trk_IPEstimate_errPV_z0_biased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_errPV_d0_unbiased_wrtPV", &trk_IPEstimate_errPV_d0_unbiased_wrtPV, &b_trk_IPEstimate_errPV_d0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_IPEstimate_errPV_z0_unbiased_wrtPV", &trk_IPEstimate_errPV_z0_unbiased_wrtPV, &b_trk_IPEstimate_errPV_z0_unbiased_wrtPV);
   fChain->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV, &b_trk_d0_wrtPV);
   fChain->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV, &b_trk_z0_wrtPV);
   fChain->SetBranchAddress("trk_phi_wrtPV", &trk_phi_wrtPV, &b_trk_phi_wrtPV);
   fChain->SetBranchAddress("trk_err_d0_wrtPV", &trk_err_d0_wrtPV, &b_trk_err_d0_wrtPV);
   fChain->SetBranchAddress("trk_err_z0_wrtPV", &trk_err_z0_wrtPV, &b_trk_err_z0_wrtPV);
   fChain->SetBranchAddress("trk_err_phi_wrtPV", &trk_err_phi_wrtPV, &b_trk_err_phi_wrtPV);
   fChain->SetBranchAddress("trk_err_theta_wrtPV", &trk_err_theta_wrtPV, &b_trk_err_theta_wrtPV);
   fChain->SetBranchAddress("trk_err_qoverp_wrtPV", &trk_err_qoverp_wrtPV, &b_trk_err_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_cov_d0_z0_wrtPV", &trk_cov_d0_z0_wrtPV, &b_trk_cov_d0_z0_wrtPV);
   fChain->SetBranchAddress("trk_cov_d0_phi_wrtPV", &trk_cov_d0_phi_wrtPV, &b_trk_cov_d0_phi_wrtPV);
   fChain->SetBranchAddress("trk_cov_d0_theta_wrtPV", &trk_cov_d0_theta_wrtPV, &b_trk_cov_d0_theta_wrtPV);
   fChain->SetBranchAddress("trk_cov_d0_qoverp_wrtPV", &trk_cov_d0_qoverp_wrtPV, &b_trk_cov_d0_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_cov_z0_phi_wrtPV", &trk_cov_z0_phi_wrtPV, &b_trk_cov_z0_phi_wrtPV);
   fChain->SetBranchAddress("trk_cov_z0_theta_wrtPV", &trk_cov_z0_theta_wrtPV, &b_trk_cov_z0_theta_wrtPV);
   fChain->SetBranchAddress("trk_cov_z0_qoverp_wrtPV", &trk_cov_z0_qoverp_wrtPV, &b_trk_cov_z0_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_cov_phi_theta_wrtPV", &trk_cov_phi_theta_wrtPV, &b_trk_cov_phi_theta_wrtPV);
   fChain->SetBranchAddress("trk_cov_phi_qoverp_wrtPV", &trk_cov_phi_qoverp_wrtPV, &b_trk_cov_phi_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_cov_theta_qoverp_wrtPV", &trk_cov_theta_qoverp_wrtPV, &b_trk_cov_theta_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_d0_wrtBS", &trk_d0_wrtBS, &b_trk_d0_wrtBS);
   fChain->SetBranchAddress("trk_z0_wrtBS", &trk_z0_wrtBS, &b_trk_z0_wrtBS);
   fChain->SetBranchAddress("trk_phi_wrtBS", &trk_phi_wrtBS, &b_trk_phi_wrtBS);
   fChain->SetBranchAddress("trk_err_d0_wrtBS", &trk_err_d0_wrtBS, &b_trk_err_d0_wrtBS);
   fChain->SetBranchAddress("trk_err_z0_wrtBS", &trk_err_z0_wrtBS, &b_trk_err_z0_wrtBS);
   fChain->SetBranchAddress("trk_err_phi_wrtBS", &trk_err_phi_wrtBS, &b_trk_err_phi_wrtBS);
   fChain->SetBranchAddress("trk_err_theta_wrtBS", &trk_err_theta_wrtBS, &b_trk_err_theta_wrtBS);
   fChain->SetBranchAddress("trk_err_qoverp_wrtBS", &trk_err_qoverp_wrtBS, &b_trk_err_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_chi2", &trk_chi2, &b_trk_chi2);
   fChain->SetBranchAddress("trk_ndof", &trk_ndof, &b_trk_ndof);
   fChain->SetBranchAddress("trk_nBLHits", &trk_nBLHits, &b_trk_nBLHits);
   fChain->SetBranchAddress("trk_nPixHits", &trk_nPixHits, &b_trk_nPixHits);
   fChain->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits, &b_trk_nSCTHits);
   fChain->SetBranchAddress("trk_nTRTHits", &trk_nTRTHits, &b_trk_nTRTHits);
   fChain->SetBranchAddress("trk_nTRTHighTHits", &trk_nTRTHighTHits, &b_trk_nTRTHighTHits);
   fChain->SetBranchAddress("trk_nPixHoles", &trk_nPixHoles, &b_trk_nPixHoles);
   fChain->SetBranchAddress("trk_nSCTHoles", &trk_nSCTHoles, &b_trk_nSCTHoles);
   fChain->SetBranchAddress("trk_nTRTHoles", &trk_nTRTHoles, &b_trk_nTRTHoles);
   fChain->SetBranchAddress("trk_nBLSharedHits", &trk_nBLSharedHits, &b_trk_nBLSharedHits);
   fChain->SetBranchAddress("trk_nPixSharedHits", &trk_nPixSharedHits, &b_trk_nPixSharedHits);
   fChain->SetBranchAddress("trk_nSCTSharedHits", &trk_nSCTSharedHits, &b_trk_nSCTSharedHits);
   fChain->SetBranchAddress("trk_nBLayerOutliers", &trk_nBLayerOutliers, &b_trk_nBLayerOutliers);
   fChain->SetBranchAddress("trk_nPixelOutliers", &trk_nPixelOutliers, &b_trk_nPixelOutliers);
   fChain->SetBranchAddress("trk_nSCTOutliers", &trk_nSCTOutliers, &b_trk_nSCTOutliers);
   fChain->SetBranchAddress("trk_nTRTOutliers", &trk_nTRTOutliers, &b_trk_nTRTOutliers);
   fChain->SetBranchAddress("trk_nTRTHighTOutliers", &trk_nTRTHighTOutliers, &b_trk_nTRTHighTOutliers);
   fChain->SetBranchAddress("trk_nContribPixelLayers", &trk_nContribPixelLayers, &b_trk_nContribPixelLayers);
   fChain->SetBranchAddress("trk_nGangedPixels", &trk_nGangedPixels, &b_trk_nGangedPixels);
   fChain->SetBranchAddress("trk_nGangedFlaggedFakes", &trk_nGangedFlaggedFakes, &b_trk_nGangedFlaggedFakes);
   fChain->SetBranchAddress("trk_nPixelDeadSensors", &trk_nPixelDeadSensors, &b_trk_nPixelDeadSensors);
   fChain->SetBranchAddress("trk_nPixelSpoiltHits", &trk_nPixelSpoiltHits, &b_trk_nPixelSpoiltHits);
   fChain->SetBranchAddress("trk_nSCTDoubleHoles", &trk_nSCTDoubleHoles, &b_trk_nSCTDoubleHoles);
   fChain->SetBranchAddress("trk_nSCTDeadSensors", &trk_nSCTDeadSensors, &b_trk_nSCTDeadSensors);
   fChain->SetBranchAddress("trk_nSCTSpoiltHits", &trk_nSCTSpoiltHits, &b_trk_nSCTSpoiltHits);
   fChain->SetBranchAddress("trk_expectBLayerHit", &trk_expectBLayerHit, &b_trk_expectBLayerHit);
   fChain->SetBranchAddress("trk_hitPattern", &trk_hitPattern, &b_trk_hitPattern);
   fChain->SetBranchAddress("trk_nSiHits", &trk_nSiHits, &b_trk_nSiHits);
   fChain->SetBranchAddress("trk_fitter", &trk_fitter, &b_trk_fitter);
   fChain->SetBranchAddress("trk_patternReco1", &trk_patternReco1, &b_trk_patternReco1);
   fChain->SetBranchAddress("trk_patternReco2", &trk_patternReco2, &b_trk_patternReco2);
   fChain->SetBranchAddress("trk_seedFinder", &trk_seedFinder, &b_trk_seedFinder);
   fChain->SetBranchAddress("trk_blayerPrediction_x", &trk_blayerPrediction_x, &b_trk_blayerPrediction_x);
   fChain->SetBranchAddress("trk_blayerPrediction_y", &trk_blayerPrediction_y, &b_trk_blayerPrediction_y);
   fChain->SetBranchAddress("trk_blayerPrediction_z", &trk_blayerPrediction_z, &b_trk_blayerPrediction_z);
   fChain->SetBranchAddress("trk_blayerPrediction_locX", &trk_blayerPrediction_locX, &b_trk_blayerPrediction_locX);
   fChain->SetBranchAddress("trk_blayerPrediction_locY", &trk_blayerPrediction_locY, &b_trk_blayerPrediction_locY);
   fChain->SetBranchAddress("trk_blayerPrediction_err_locX", &trk_blayerPrediction_err_locX, &b_trk_blayerPrediction_err_locX);
   fChain->SetBranchAddress("trk_blayerPrediction_err_locY", &trk_blayerPrediction_err_locY, &b_trk_blayerPrediction_err_locY);
   fChain->SetBranchAddress("trk_blayerPrediction_etaDistToEdge", &trk_blayerPrediction_etaDistToEdge, &b_trk_blayerPrediction_etaDistToEdge);
   fChain->SetBranchAddress("trk_blayerPrediction_phiDistToEdge", &trk_blayerPrediction_phiDistToEdge, &b_trk_blayerPrediction_phiDistToEdge);
   fChain->SetBranchAddress("trk_blayerPrediction_detElementId", &trk_blayerPrediction_detElementId, &b_trk_blayerPrediction_detElementId);
   fChain->SetBranchAddress("trk_blayerPrediction_row", &trk_blayerPrediction_row, &b_trk_blayerPrediction_row);
   fChain->SetBranchAddress("trk_blayerPrediction_col", &trk_blayerPrediction_col, &b_trk_blayerPrediction_col);
   fChain->SetBranchAddress("trk_blayerPrediction_type", &trk_blayerPrediction_type, &b_trk_blayerPrediction_type);
   fChain->SetBranchAddress("trk_BLayer_hit_n", &trk_BLayer_hit_n, &b_trk_BLayer_hit_n);
   fChain->SetBranchAddress("trk_BLayer_hit_id", &trk_BLayer_hit_id, &b_trk_BLayer_hit_id);
   fChain->SetBranchAddress("trk_BLayer_hit_detElementId", &trk_BLayer_hit_detElementId, &b_trk_BLayer_hit_detElementId);
   fChain->SetBranchAddress("trk_BLayer_hit_bec", &trk_BLayer_hit_bec, &b_trk_BLayer_hit_bec);
   fChain->SetBranchAddress("trk_BLayer_hit_layer", &trk_BLayer_hit_layer, &b_trk_BLayer_hit_layer);
   fChain->SetBranchAddress("trk_BLayer_hit_charge", &trk_BLayer_hit_charge, &b_trk_BLayer_hit_charge);
   fChain->SetBranchAddress("trk_BLayer_hit_sizePhi", &trk_BLayer_hit_sizePhi, &b_trk_BLayer_hit_sizePhi);
   fChain->SetBranchAddress("trk_BLayer_hit_sizeZ", &trk_BLayer_hit_sizeZ, &b_trk_BLayer_hit_sizeZ);
   fChain->SetBranchAddress("trk_BLayer_hit_size", &trk_BLayer_hit_size, &b_trk_BLayer_hit_size);
   fChain->SetBranchAddress("trk_BLayer_hit_isFake", &trk_BLayer_hit_isFake, &b_trk_BLayer_hit_isFake);
   fChain->SetBranchAddress("trk_BLayer_hit_isGanged", &trk_BLayer_hit_isGanged, &b_trk_BLayer_hit_isGanged);
   fChain->SetBranchAddress("trk_BLayer_hit_isSplit", &trk_BLayer_hit_isSplit, &b_trk_BLayer_hit_isSplit);
   fChain->SetBranchAddress("trk_BLayer_hit_splitProb1", &trk_BLayer_hit_splitProb1, &b_trk_BLayer_hit_splitProb1);
   fChain->SetBranchAddress("trk_BLayer_hit_splitProb2", &trk_BLayer_hit_splitProb2, &b_trk_BLayer_hit_splitProb2);
   fChain->SetBranchAddress("trk_BLayer_hit_isCompetingRIO", &trk_BLayer_hit_isCompetingRIO, &b_trk_BLayer_hit_isCompetingRIO);
   fChain->SetBranchAddress("trk_BLayer_hit_locX", &trk_BLayer_hit_locX, &b_trk_BLayer_hit_locX);
   fChain->SetBranchAddress("trk_BLayer_hit_locY", &trk_BLayer_hit_locY, &b_trk_BLayer_hit_locY);
   fChain->SetBranchAddress("trk_BLayer_hit_incidencePhi", &trk_BLayer_hit_incidencePhi, &b_trk_BLayer_hit_incidencePhi);
   fChain->SetBranchAddress("trk_BLayer_hit_incidenceTheta", &trk_BLayer_hit_incidenceTheta, &b_trk_BLayer_hit_incidenceTheta);
   fChain->SetBranchAddress("trk_BLayer_hit_err_locX", &trk_BLayer_hit_err_locX, &b_trk_BLayer_hit_err_locX);
   fChain->SetBranchAddress("trk_BLayer_hit_err_locY", &trk_BLayer_hit_err_locY, &b_trk_BLayer_hit_err_locY);
   fChain->SetBranchAddress("trk_BLayer_hit_cov_locXY", &trk_BLayer_hit_cov_locXY, &b_trk_BLayer_hit_cov_locXY);
   fChain->SetBranchAddress("trk_BLayer_hit_x", &trk_BLayer_hit_x, &b_trk_BLayer_hit_x);
   fChain->SetBranchAddress("trk_BLayer_hit_y", &trk_BLayer_hit_y, &b_trk_BLayer_hit_y);
   fChain->SetBranchAddress("trk_BLayer_hit_z", &trk_BLayer_hit_z, &b_trk_BLayer_hit_z);
   fChain->SetBranchAddress("trk_BLayer_hit_trkLocX", &trk_BLayer_hit_trkLocX, &b_trk_BLayer_hit_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_hit_trkLocY", &trk_BLayer_hit_trkLocY, &b_trk_BLayer_hit_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_hit_err_trkLocX", &trk_BLayer_hit_err_trkLocX, &b_trk_BLayer_hit_err_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_hit_err_trkLocY", &trk_BLayer_hit_err_trkLocY, &b_trk_BLayer_hit_err_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_hit_cov_trkLocXY", &trk_BLayer_hit_cov_trkLocXY, &b_trk_BLayer_hit_cov_trkLocXY);
   fChain->SetBranchAddress("trk_BLayer_hit_locBiasedResidualX", &trk_BLayer_hit_locBiasedResidualX, &b_trk_BLayer_hit_locBiasedResidualX);
   fChain->SetBranchAddress("trk_BLayer_hit_locBiasedResidualY", &trk_BLayer_hit_locBiasedResidualY, &b_trk_BLayer_hit_locBiasedResidualY);
   fChain->SetBranchAddress("trk_BLayer_hit_locBiasedPullX", &trk_BLayer_hit_locBiasedPullX, &b_trk_BLayer_hit_locBiasedPullX);
   fChain->SetBranchAddress("trk_BLayer_hit_locBiasedPullY", &trk_BLayer_hit_locBiasedPullY, &b_trk_BLayer_hit_locBiasedPullY);
   fChain->SetBranchAddress("trk_BLayer_hit_locUnbiasedResidualX", &trk_BLayer_hit_locUnbiasedResidualX, &b_trk_BLayer_hit_locUnbiasedResidualX);
   fChain->SetBranchAddress("trk_BLayer_hit_locUnbiasedResidualY", &trk_BLayer_hit_locUnbiasedResidualY, &b_trk_BLayer_hit_locUnbiasedResidualY);
   fChain->SetBranchAddress("trk_BLayer_hit_locUnbiasedPullX", &trk_BLayer_hit_locUnbiasedPullX, &b_trk_BLayer_hit_locUnbiasedPullX);
   fChain->SetBranchAddress("trk_BLayer_hit_locUnbiasedPullY", &trk_BLayer_hit_locUnbiasedPullY, &b_trk_BLayer_hit_locUnbiasedPullY);
   fChain->SetBranchAddress("trk_BLayer_hit_chi2", &trk_BLayer_hit_chi2, &b_trk_BLayer_hit_chi2);
   fChain->SetBranchAddress("trk_BLayer_hit_ndof", &trk_BLayer_hit_ndof, &b_trk_BLayer_hit_ndof);
   fChain->SetBranchAddress("trk_Pixel_hit_n", &trk_Pixel_hit_n, &b_trk_Pixel_hit_n);
   fChain->SetBranchAddress("trk_Pixel_hit_id", &trk_Pixel_hit_id, &b_trk_Pixel_hit_id);
   fChain->SetBranchAddress("trk_Pixel_hit_detElementId", &trk_Pixel_hit_detElementId, &b_trk_Pixel_hit_detElementId);
   fChain->SetBranchAddress("trk_Pixel_hit_bec", &trk_Pixel_hit_bec, &b_trk_Pixel_hit_bec);
   fChain->SetBranchAddress("trk_Pixel_hit_layer", &trk_Pixel_hit_layer, &b_trk_Pixel_hit_layer);
   fChain->SetBranchAddress("trk_Pixel_hit_charge", &trk_Pixel_hit_charge, &b_trk_Pixel_hit_charge);
   fChain->SetBranchAddress("trk_Pixel_hit_sizePhi", &trk_Pixel_hit_sizePhi, &b_trk_Pixel_hit_sizePhi);
   fChain->SetBranchAddress("trk_Pixel_hit_sizeZ", &trk_Pixel_hit_sizeZ, &b_trk_Pixel_hit_sizeZ);
   fChain->SetBranchAddress("trk_Pixel_hit_size", &trk_Pixel_hit_size, &b_trk_Pixel_hit_size);
   fChain->SetBranchAddress("trk_Pixel_hit_isFake", &trk_Pixel_hit_isFake, &b_trk_Pixel_hit_isFake);
   fChain->SetBranchAddress("trk_Pixel_hit_isGanged", &trk_Pixel_hit_isGanged, &b_trk_Pixel_hit_isGanged);
   fChain->SetBranchAddress("trk_Pixel_hit_isSplit", &trk_Pixel_hit_isSplit, &b_trk_Pixel_hit_isSplit);
   fChain->SetBranchAddress("trk_Pixel_hit_splitProb1", &trk_Pixel_hit_splitProb1, &b_trk_Pixel_hit_splitProb1);
   fChain->SetBranchAddress("trk_Pixel_hit_splitProb2", &trk_Pixel_hit_splitProb2, &b_trk_Pixel_hit_splitProb2);
   fChain->SetBranchAddress("trk_Pixel_hit_isCompetingRIO", &trk_Pixel_hit_isCompetingRIO, &b_trk_Pixel_hit_isCompetingRIO);
   fChain->SetBranchAddress("trk_Pixel_hit_locX", &trk_Pixel_hit_locX, &b_trk_Pixel_hit_locX);
   fChain->SetBranchAddress("trk_Pixel_hit_locY", &trk_Pixel_hit_locY, &b_trk_Pixel_hit_locY);
   fChain->SetBranchAddress("trk_Pixel_hit_incidencePhi", &trk_Pixel_hit_incidencePhi, &b_trk_Pixel_hit_incidencePhi);
   fChain->SetBranchAddress("trk_Pixel_hit_incidenceTheta", &trk_Pixel_hit_incidenceTheta, &b_trk_Pixel_hit_incidenceTheta);
   fChain->SetBranchAddress("trk_Pixel_hit_err_locX", &trk_Pixel_hit_err_locX, &b_trk_Pixel_hit_err_locX);
   fChain->SetBranchAddress("trk_Pixel_hit_err_locY", &trk_Pixel_hit_err_locY, &b_trk_Pixel_hit_err_locY);
   fChain->SetBranchAddress("trk_Pixel_hit_cov_locXY", &trk_Pixel_hit_cov_locXY, &b_trk_Pixel_hit_cov_locXY);
   fChain->SetBranchAddress("trk_Pixel_hit_x", &trk_Pixel_hit_x, &b_trk_Pixel_hit_x);
   fChain->SetBranchAddress("trk_Pixel_hit_y", &trk_Pixel_hit_y, &b_trk_Pixel_hit_y);
   fChain->SetBranchAddress("trk_Pixel_hit_z", &trk_Pixel_hit_z, &b_trk_Pixel_hit_z);
   fChain->SetBranchAddress("trk_Pixel_hit_trkLocX", &trk_Pixel_hit_trkLocX, &b_trk_Pixel_hit_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_hit_trkLocY", &trk_Pixel_hit_trkLocY, &b_trk_Pixel_hit_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_hit_err_trkLocX", &trk_Pixel_hit_err_trkLocX, &b_trk_Pixel_hit_err_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_hit_err_trkLocY", &trk_Pixel_hit_err_trkLocY, &b_trk_Pixel_hit_err_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_hit_cov_trkLocXY", &trk_Pixel_hit_cov_trkLocXY, &b_trk_Pixel_hit_cov_trkLocXY);
   fChain->SetBranchAddress("trk_Pixel_hit_locBiasedResidualX", &trk_Pixel_hit_locBiasedResidualX, &b_trk_Pixel_hit_locBiasedResidualX);
   fChain->SetBranchAddress("trk_Pixel_hit_locBiasedResidualY", &trk_Pixel_hit_locBiasedResidualY, &b_trk_Pixel_hit_locBiasedResidualY);
   fChain->SetBranchAddress("trk_Pixel_hit_locBiasedPullX", &trk_Pixel_hit_locBiasedPullX, &b_trk_Pixel_hit_locBiasedPullX);
   fChain->SetBranchAddress("trk_Pixel_hit_locBiasedPullY", &trk_Pixel_hit_locBiasedPullY, &b_trk_Pixel_hit_locBiasedPullY);
   fChain->SetBranchAddress("trk_Pixel_hit_locUnbiasedResidualX", &trk_Pixel_hit_locUnbiasedResidualX, &b_trk_Pixel_hit_locUnbiasedResidualX);
   fChain->SetBranchAddress("trk_Pixel_hit_locUnbiasedResidualY", &trk_Pixel_hit_locUnbiasedResidualY, &b_trk_Pixel_hit_locUnbiasedResidualY);
   fChain->SetBranchAddress("trk_Pixel_hit_locUnbiasedPullX", &trk_Pixel_hit_locUnbiasedPullX, &b_trk_Pixel_hit_locUnbiasedPullX);
   fChain->SetBranchAddress("trk_Pixel_hit_locUnbiasedPullY", &trk_Pixel_hit_locUnbiasedPullY, &b_trk_Pixel_hit_locUnbiasedPullY);
   fChain->SetBranchAddress("trk_Pixel_hit_chi2", &trk_Pixel_hit_chi2, &b_trk_Pixel_hit_chi2);
   fChain->SetBranchAddress("trk_Pixel_hit_ndof", &trk_Pixel_hit_ndof, &b_trk_Pixel_hit_ndof);
   fChain->SetBranchAddress("trk_SCT_hit_n", &trk_SCT_hit_n, &b_trk_SCT_hit_n);
   fChain->SetBranchAddress("trk_SCT_hit_id", &trk_SCT_hit_id, &b_trk_SCT_hit_id);
   fChain->SetBranchAddress("trk_SCT_hit_detElementId", &trk_SCT_hit_detElementId, &b_trk_SCT_hit_detElementId);
   fChain->SetBranchAddress("trk_SCT_hit_bec", &trk_SCT_hit_bec, &b_trk_SCT_hit_bec);
   fChain->SetBranchAddress("trk_SCT_hit_layer", &trk_SCT_hit_layer, &b_trk_SCT_hit_layer);
   fChain->SetBranchAddress("trk_SCT_hit_sizePhi", &trk_SCT_hit_sizePhi, &b_trk_SCT_hit_sizePhi);
   fChain->SetBranchAddress("trk_SCT_hit_isCompetingRIO", &trk_SCT_hit_isCompetingRIO, &b_trk_SCT_hit_isCompetingRIO);
   fChain->SetBranchAddress("trk_SCT_hit_locX", &trk_SCT_hit_locX, &b_trk_SCT_hit_locX);
   fChain->SetBranchAddress("trk_SCT_hit_locY", &trk_SCT_hit_locY, &b_trk_SCT_hit_locY);
   fChain->SetBranchAddress("trk_SCT_hit_incidencePhi", &trk_SCT_hit_incidencePhi, &b_trk_SCT_hit_incidencePhi);
   fChain->SetBranchAddress("trk_SCT_hit_incidenceTheta", &trk_SCT_hit_incidenceTheta, &b_trk_SCT_hit_incidenceTheta);
   fChain->SetBranchAddress("trk_SCT_hit_err_locX", &trk_SCT_hit_err_locX, &b_trk_SCT_hit_err_locX);
   fChain->SetBranchAddress("trk_SCT_hit_err_locY", &trk_SCT_hit_err_locY, &b_trk_SCT_hit_err_locY);
   fChain->SetBranchAddress("trk_SCT_hit_cov_locXY", &trk_SCT_hit_cov_locXY, &b_trk_SCT_hit_cov_locXY);
   fChain->SetBranchAddress("trk_SCT_hit_x", &trk_SCT_hit_x, &b_trk_SCT_hit_x);
   fChain->SetBranchAddress("trk_SCT_hit_y", &trk_SCT_hit_y, &b_trk_SCT_hit_y);
   fChain->SetBranchAddress("trk_SCT_hit_z", &trk_SCT_hit_z, &b_trk_SCT_hit_z);
   fChain->SetBranchAddress("trk_SCT_hit_trkLocX", &trk_SCT_hit_trkLocX, &b_trk_SCT_hit_trkLocX);
   fChain->SetBranchAddress("trk_SCT_hit_trkLocY", &trk_SCT_hit_trkLocY, &b_trk_SCT_hit_trkLocY);
   fChain->SetBranchAddress("trk_SCT_hit_err_trkLocX", &trk_SCT_hit_err_trkLocX, &b_trk_SCT_hit_err_trkLocX);
   fChain->SetBranchAddress("trk_SCT_hit_err_trkLocY", &trk_SCT_hit_err_trkLocY, &b_trk_SCT_hit_err_trkLocY);
   fChain->SetBranchAddress("trk_SCT_hit_cov_trkLocXY", &trk_SCT_hit_cov_trkLocXY, &b_trk_SCT_hit_cov_trkLocXY);
   fChain->SetBranchAddress("trk_SCT_hit_locBiasedResidualX", &trk_SCT_hit_locBiasedResidualX, &b_trk_SCT_hit_locBiasedResidualX);
   fChain->SetBranchAddress("trk_SCT_hit_locBiasedResidualY", &trk_SCT_hit_locBiasedResidualY, &b_trk_SCT_hit_locBiasedResidualY);
   fChain->SetBranchAddress("trk_SCT_hit_locBiasedPullX", &trk_SCT_hit_locBiasedPullX, &b_trk_SCT_hit_locBiasedPullX);
   fChain->SetBranchAddress("trk_SCT_hit_locBiasedPullY", &trk_SCT_hit_locBiasedPullY, &b_trk_SCT_hit_locBiasedPullY);
   fChain->SetBranchAddress("trk_SCT_hit_locUnbiasedResidualX", &trk_SCT_hit_locUnbiasedResidualX, &b_trk_SCT_hit_locUnbiasedResidualX);
   fChain->SetBranchAddress("trk_SCT_hit_locUnbiasedResidualY", &trk_SCT_hit_locUnbiasedResidualY, &b_trk_SCT_hit_locUnbiasedResidualY);
   fChain->SetBranchAddress("trk_SCT_hit_locUnbiasedPullX", &trk_SCT_hit_locUnbiasedPullX, &b_trk_SCT_hit_locUnbiasedPullX);
   fChain->SetBranchAddress("trk_SCT_hit_locUnbiasedPullY", &trk_SCT_hit_locUnbiasedPullY, &b_trk_SCT_hit_locUnbiasedPullY);
   fChain->SetBranchAddress("trk_SCT_hit_chi2", &trk_SCT_hit_chi2, &b_trk_SCT_hit_chi2);
   fChain->SetBranchAddress("trk_SCT_hit_ndof", &trk_SCT_hit_ndof, &b_trk_SCT_hit_ndof);
   fChain->SetBranchAddress("trk_BLayer_outlier_n", &trk_BLayer_outlier_n, &b_trk_BLayer_outlier_n);
   fChain->SetBranchAddress("trk_BLayer_outlier_id", &trk_BLayer_outlier_id, &b_trk_BLayer_outlier_id);
   fChain->SetBranchAddress("trk_BLayer_outlier_detElementId", &trk_BLayer_outlier_detElementId, &b_trk_BLayer_outlier_detElementId);
   fChain->SetBranchAddress("trk_BLayer_outlier_bec", &trk_BLayer_outlier_bec, &b_trk_BLayer_outlier_bec);
   fChain->SetBranchAddress("trk_BLayer_outlier_layer", &trk_BLayer_outlier_layer, &b_trk_BLayer_outlier_layer);
   fChain->SetBranchAddress("trk_BLayer_outlier_charge", &trk_BLayer_outlier_charge, &b_trk_BLayer_outlier_charge);
   fChain->SetBranchAddress("trk_BLayer_outlier_sizePhi", &trk_BLayer_outlier_sizePhi, &b_trk_BLayer_outlier_sizePhi);
   fChain->SetBranchAddress("trk_BLayer_outlier_sizeZ", &trk_BLayer_outlier_sizeZ, &b_trk_BLayer_outlier_sizeZ);
   fChain->SetBranchAddress("trk_BLayer_outlier_size", &trk_BLayer_outlier_size, &b_trk_BLayer_outlier_size);
   fChain->SetBranchAddress("trk_BLayer_outlier_isFake", &trk_BLayer_outlier_isFake, &b_trk_BLayer_outlier_isFake);
   fChain->SetBranchAddress("trk_BLayer_outlier_isGanged", &trk_BLayer_outlier_isGanged, &b_trk_BLayer_outlier_isGanged);
   fChain->SetBranchAddress("trk_BLayer_outlier_isSplit", &trk_BLayer_outlier_isSplit, &b_trk_BLayer_outlier_isSplit);
   fChain->SetBranchAddress("trk_BLayer_outlier_splitProb1", &trk_BLayer_outlier_splitProb1, &b_trk_BLayer_outlier_splitProb1);
   fChain->SetBranchAddress("trk_BLayer_outlier_splitProb2", &trk_BLayer_outlier_splitProb2, &b_trk_BLayer_outlier_splitProb2);
   fChain->SetBranchAddress("trk_BLayer_outlier_isCompetingRIO", &trk_BLayer_outlier_isCompetingRIO, &b_trk_BLayer_outlier_isCompetingRIO);
   fChain->SetBranchAddress("trk_BLayer_outlier_locX", &trk_BLayer_outlier_locX, &b_trk_BLayer_outlier_locX);
   fChain->SetBranchAddress("trk_BLayer_outlier_locY", &trk_BLayer_outlier_locY, &b_trk_BLayer_outlier_locY);
   fChain->SetBranchAddress("trk_BLayer_outlier_incidencePhi", &trk_BLayer_outlier_incidencePhi, &b_trk_BLayer_outlier_incidencePhi);
   fChain->SetBranchAddress("trk_BLayer_outlier_incidenceTheta", &trk_BLayer_outlier_incidenceTheta, &b_trk_BLayer_outlier_incidenceTheta);
   fChain->SetBranchAddress("trk_BLayer_outlier_err_locX", &trk_BLayer_outlier_err_locX, &b_trk_BLayer_outlier_err_locX);
   fChain->SetBranchAddress("trk_BLayer_outlier_err_locY", &trk_BLayer_outlier_err_locY, &b_trk_BLayer_outlier_err_locY);
   fChain->SetBranchAddress("trk_BLayer_outlier_cov_locXY", &trk_BLayer_outlier_cov_locXY, &b_trk_BLayer_outlier_cov_locXY);
   fChain->SetBranchAddress("trk_BLayer_outlier_x", &trk_BLayer_outlier_x, &b_trk_BLayer_outlier_x);
   fChain->SetBranchAddress("trk_BLayer_outlier_y", &trk_BLayer_outlier_y, &b_trk_BLayer_outlier_y);
   fChain->SetBranchAddress("trk_BLayer_outlier_z", &trk_BLayer_outlier_z, &b_trk_BLayer_outlier_z);
   fChain->SetBranchAddress("trk_BLayer_outlier_trkLocX", &trk_BLayer_outlier_trkLocX, &b_trk_BLayer_outlier_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_outlier_trkLocY", &trk_BLayer_outlier_trkLocY, &b_trk_BLayer_outlier_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_outlier_err_trkLocX", &trk_BLayer_outlier_err_trkLocX, &b_trk_BLayer_outlier_err_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_outlier_err_trkLocY", &trk_BLayer_outlier_err_trkLocY, &b_trk_BLayer_outlier_err_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_outlier_cov_trkLocXY", &trk_BLayer_outlier_cov_trkLocXY, &b_trk_BLayer_outlier_cov_trkLocXY);
   fChain->SetBranchAddress("trk_BLayer_outlier_locBiasedResidualX", &trk_BLayer_outlier_locBiasedResidualX, &b_trk_BLayer_outlier_locBiasedResidualX);
   fChain->SetBranchAddress("trk_BLayer_outlier_locBiasedResidualY", &trk_BLayer_outlier_locBiasedResidualY, &b_trk_BLayer_outlier_locBiasedResidualY);
   fChain->SetBranchAddress("trk_BLayer_outlier_locBiasedPullX", &trk_BLayer_outlier_locBiasedPullX, &b_trk_BLayer_outlier_locBiasedPullX);
   fChain->SetBranchAddress("trk_BLayer_outlier_locBiasedPullY", &trk_BLayer_outlier_locBiasedPullY, &b_trk_BLayer_outlier_locBiasedPullY);
   fChain->SetBranchAddress("trk_BLayer_outlier_locUnbiasedResidualX", &trk_BLayer_outlier_locUnbiasedResidualX, &b_trk_BLayer_outlier_locUnbiasedResidualX);
   fChain->SetBranchAddress("trk_BLayer_outlier_locUnbiasedResidualY", &trk_BLayer_outlier_locUnbiasedResidualY, &b_trk_BLayer_outlier_locUnbiasedResidualY);
   fChain->SetBranchAddress("trk_BLayer_outlier_locUnbiasedPullX", &trk_BLayer_outlier_locUnbiasedPullX, &b_trk_BLayer_outlier_locUnbiasedPullX);
   fChain->SetBranchAddress("trk_BLayer_outlier_locUnbiasedPullY", &trk_BLayer_outlier_locUnbiasedPullY, &b_trk_BLayer_outlier_locUnbiasedPullY);
   fChain->SetBranchAddress("trk_BLayer_outlier_chi2", &trk_BLayer_outlier_chi2, &b_trk_BLayer_outlier_chi2);
   fChain->SetBranchAddress("trk_BLayer_outlier_ndof", &trk_BLayer_outlier_ndof, &b_trk_BLayer_outlier_ndof);
   fChain->SetBranchAddress("trk_Pixel_outlier_n", &trk_Pixel_outlier_n, &b_trk_Pixel_outlier_n);
   fChain->SetBranchAddress("trk_Pixel_outlier_id", &trk_Pixel_outlier_id, &b_trk_Pixel_outlier_id);
   fChain->SetBranchAddress("trk_Pixel_outlier_detElementId", &trk_Pixel_outlier_detElementId, &b_trk_Pixel_outlier_detElementId);
   fChain->SetBranchAddress("trk_Pixel_outlier_bec", &trk_Pixel_outlier_bec, &b_trk_Pixel_outlier_bec);
   fChain->SetBranchAddress("trk_Pixel_outlier_layer", &trk_Pixel_outlier_layer, &b_trk_Pixel_outlier_layer);
   fChain->SetBranchAddress("trk_Pixel_outlier_charge", &trk_Pixel_outlier_charge, &b_trk_Pixel_outlier_charge);
   fChain->SetBranchAddress("trk_Pixel_outlier_sizePhi", &trk_Pixel_outlier_sizePhi, &b_trk_Pixel_outlier_sizePhi);
   fChain->SetBranchAddress("trk_Pixel_outlier_sizeZ", &trk_Pixel_outlier_sizeZ, &b_trk_Pixel_outlier_sizeZ);
   fChain->SetBranchAddress("trk_Pixel_outlier_size", &trk_Pixel_outlier_size, &b_trk_Pixel_outlier_size);
   fChain->SetBranchAddress("trk_Pixel_outlier_isFake", &trk_Pixel_outlier_isFake, &b_trk_Pixel_outlier_isFake);
   fChain->SetBranchAddress("trk_Pixel_outlier_isGanged", &trk_Pixel_outlier_isGanged, &b_trk_Pixel_outlier_isGanged);
   fChain->SetBranchAddress("trk_Pixel_outlier_isSplit", &trk_Pixel_outlier_isSplit, &b_trk_Pixel_outlier_isSplit);
   fChain->SetBranchAddress("trk_Pixel_outlier_splitProb1", &trk_Pixel_outlier_splitProb1, &b_trk_Pixel_outlier_splitProb1);
   fChain->SetBranchAddress("trk_Pixel_outlier_splitProb2", &trk_Pixel_outlier_splitProb2, &b_trk_Pixel_outlier_splitProb2);
   fChain->SetBranchAddress("trk_Pixel_outlier_isCompetingRIO", &trk_Pixel_outlier_isCompetingRIO, &b_trk_Pixel_outlier_isCompetingRIO);
   fChain->SetBranchAddress("trk_Pixel_outlier_locX", &trk_Pixel_outlier_locX, &b_trk_Pixel_outlier_locX);
   fChain->SetBranchAddress("trk_Pixel_outlier_locY", &trk_Pixel_outlier_locY, &b_trk_Pixel_outlier_locY);
   fChain->SetBranchAddress("trk_Pixel_outlier_incidencePhi", &trk_Pixel_outlier_incidencePhi, &b_trk_Pixel_outlier_incidencePhi);
   fChain->SetBranchAddress("trk_Pixel_outlier_incidenceTheta", &trk_Pixel_outlier_incidenceTheta, &b_trk_Pixel_outlier_incidenceTheta);
   fChain->SetBranchAddress("trk_Pixel_outlier_err_locX", &trk_Pixel_outlier_err_locX, &b_trk_Pixel_outlier_err_locX);
   fChain->SetBranchAddress("trk_Pixel_outlier_err_locY", &trk_Pixel_outlier_err_locY, &b_trk_Pixel_outlier_err_locY);
   fChain->SetBranchAddress("trk_Pixel_outlier_cov_locXY", &trk_Pixel_outlier_cov_locXY, &b_trk_Pixel_outlier_cov_locXY);
   fChain->SetBranchAddress("trk_Pixel_outlier_x", &trk_Pixel_outlier_x, &b_trk_Pixel_outlier_x);
   fChain->SetBranchAddress("trk_Pixel_outlier_y", &trk_Pixel_outlier_y, &b_trk_Pixel_outlier_y);
   fChain->SetBranchAddress("trk_Pixel_outlier_z", &trk_Pixel_outlier_z, &b_trk_Pixel_outlier_z);
   fChain->SetBranchAddress("trk_Pixel_outlier_trkLocX", &trk_Pixel_outlier_trkLocX, &b_trk_Pixel_outlier_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_outlier_trkLocY", &trk_Pixel_outlier_trkLocY, &b_trk_Pixel_outlier_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_outlier_err_trkLocX", &trk_Pixel_outlier_err_trkLocX, &b_trk_Pixel_outlier_err_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_outlier_err_trkLocY", &trk_Pixel_outlier_err_trkLocY, &b_trk_Pixel_outlier_err_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_outlier_cov_trkLocXY", &trk_Pixel_outlier_cov_trkLocXY, &b_trk_Pixel_outlier_cov_trkLocXY);
   fChain->SetBranchAddress("trk_Pixel_outlier_locBiasedResidualX", &trk_Pixel_outlier_locBiasedResidualX, &b_trk_Pixel_outlier_locBiasedResidualX);
   fChain->SetBranchAddress("trk_Pixel_outlier_locBiasedResidualY", &trk_Pixel_outlier_locBiasedResidualY, &b_trk_Pixel_outlier_locBiasedResidualY);
   fChain->SetBranchAddress("trk_Pixel_outlier_locBiasedPullX", &trk_Pixel_outlier_locBiasedPullX, &b_trk_Pixel_outlier_locBiasedPullX);
   fChain->SetBranchAddress("trk_Pixel_outlier_locBiasedPullY", &trk_Pixel_outlier_locBiasedPullY, &b_trk_Pixel_outlier_locBiasedPullY);
   fChain->SetBranchAddress("trk_Pixel_outlier_locUnbiasedResidualX", &trk_Pixel_outlier_locUnbiasedResidualX, &b_trk_Pixel_outlier_locUnbiasedResidualX);
   fChain->SetBranchAddress("trk_Pixel_outlier_locUnbiasedResidualY", &trk_Pixel_outlier_locUnbiasedResidualY, &b_trk_Pixel_outlier_locUnbiasedResidualY);
   fChain->SetBranchAddress("trk_Pixel_outlier_locUnbiasedPullX", &trk_Pixel_outlier_locUnbiasedPullX, &b_trk_Pixel_outlier_locUnbiasedPullX);
   fChain->SetBranchAddress("trk_Pixel_outlier_locUnbiasedPullY", &trk_Pixel_outlier_locUnbiasedPullY, &b_trk_Pixel_outlier_locUnbiasedPullY);
   fChain->SetBranchAddress("trk_Pixel_outlier_chi2", &trk_Pixel_outlier_chi2, &b_trk_Pixel_outlier_chi2);
   fChain->SetBranchAddress("trk_Pixel_outlier_ndof", &trk_Pixel_outlier_ndof, &b_trk_Pixel_outlier_ndof);
   fChain->SetBranchAddress("trk_BLayer_hole_n", &trk_BLayer_hole_n, &b_trk_BLayer_hole_n);
   fChain->SetBranchAddress("trk_BLayer_hole_detElementId", &trk_BLayer_hole_detElementId, &b_trk_BLayer_hole_detElementId);
   fChain->SetBranchAddress("trk_BLayer_hole_bec", &trk_BLayer_hole_bec, &b_trk_BLayer_hole_bec);
   fChain->SetBranchAddress("trk_BLayer_hole_layer", &trk_BLayer_hole_layer, &b_trk_BLayer_hole_layer);
   fChain->SetBranchAddress("trk_BLayer_hole_trkLocX", &trk_BLayer_hole_trkLocX, &b_trk_BLayer_hole_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_hole_trkLocY", &trk_BLayer_hole_trkLocY, &b_trk_BLayer_hole_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_hole_err_trkLocX", &trk_BLayer_hole_err_trkLocX, &b_trk_BLayer_hole_err_trkLocX);
   fChain->SetBranchAddress("trk_BLayer_hole_err_trkLocY", &trk_BLayer_hole_err_trkLocY, &b_trk_BLayer_hole_err_trkLocY);
   fChain->SetBranchAddress("trk_BLayer_hole_cov_trkLocXY", &trk_BLayer_hole_cov_trkLocXY, &b_trk_BLayer_hole_cov_trkLocXY);
   fChain->SetBranchAddress("trk_Pixel_hole_n", &trk_Pixel_hole_n, &b_trk_Pixel_hole_n);
   fChain->SetBranchAddress("trk_Pixel_hole_detElementId", &trk_Pixel_hole_detElementId, &b_trk_Pixel_hole_detElementId);
   fChain->SetBranchAddress("trk_Pixel_hole_bec", &trk_Pixel_hole_bec, &b_trk_Pixel_hole_bec);
   fChain->SetBranchAddress("trk_Pixel_hole_layer", &trk_Pixel_hole_layer, &b_trk_Pixel_hole_layer);
   fChain->SetBranchAddress("trk_Pixel_hole_trkLocX", &trk_Pixel_hole_trkLocX, &b_trk_Pixel_hole_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_hole_trkLocY", &trk_Pixel_hole_trkLocY, &b_trk_Pixel_hole_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_hole_err_trkLocX", &trk_Pixel_hole_err_trkLocX, &b_trk_Pixel_hole_err_trkLocX);
   fChain->SetBranchAddress("trk_Pixel_hole_err_trkLocY", &trk_Pixel_hole_err_trkLocY, &b_trk_Pixel_hole_err_trkLocY);
   fChain->SetBranchAddress("trk_Pixel_hole_cov_trkLocXY", &trk_Pixel_hole_cov_trkLocXY, &b_trk_Pixel_hole_cov_trkLocXY);
   fChain->SetBranchAddress("trk_primvx_weight", &trk_primvx_weight, &b_trk_primvx_weight);
   fChain->SetBranchAddress("trk_primvx_index", &trk_primvx_index, &b_trk_primvx_index);
   fChain->SetBranchAddress("trk_mcpart_probability", &trk_mcpart_probability, &b_trk_mcpart_probability);
   fChain->SetBranchAddress("trk_mcpart_barcode", &trk_mcpart_barcode, &b_trk_mcpart_barcode);
   fChain->SetBranchAddress("trk_mcpart_index", &trk_mcpart_index, &b_trk_mcpart_index);
   fChain->SetBranchAddress("trk_detailed_mc_n", &trk_detailed_mc_n, &b_trk_detailed_mc_n);
   fChain->SetBranchAddress("trk_detailed_mc_nCommonPixHits", &trk_detailed_mc_nCommonPixHits, &b_trk_detailed_mc_nCommonPixHits);
   fChain->SetBranchAddress("trk_detailed_mc_nCommonSCTHits", &trk_detailed_mc_nCommonSCTHits, &b_trk_detailed_mc_nCommonSCTHits);
   fChain->SetBranchAddress("trk_detailed_mc_nCommonTRTHits", &trk_detailed_mc_nCommonTRTHits, &b_trk_detailed_mc_nCommonTRTHits);
   fChain->SetBranchAddress("trk_detailed_mc_nRecoPixHits", &trk_detailed_mc_nRecoPixHits, &b_trk_detailed_mc_nRecoPixHits);
   fChain->SetBranchAddress("trk_detailed_mc_nRecoSCTHits", &trk_detailed_mc_nRecoSCTHits, &b_trk_detailed_mc_nRecoSCTHits);
   fChain->SetBranchAddress("trk_detailed_mc_nRecoTRTHits", &trk_detailed_mc_nRecoTRTHits, &b_trk_detailed_mc_nRecoTRTHits);
   fChain->SetBranchAddress("trk_detailed_mc_nTruthPixHits", &trk_detailed_mc_nTruthPixHits, &b_trk_detailed_mc_nTruthPixHits);
   fChain->SetBranchAddress("trk_detailed_mc_nTruthSCTHits", &trk_detailed_mc_nTruthSCTHits, &b_trk_detailed_mc_nTruthSCTHits);
   fChain->SetBranchAddress("trk_detailed_mc_nTruthTRTHits", &trk_detailed_mc_nTruthTRTHits, &b_trk_detailed_mc_nTruthTRTHits);
   fChain->SetBranchAddress("trk_detailed_mc_begVtx_barcode", &trk_detailed_mc_begVtx_barcode, &b_trk_detailed_mc_begVtx_barcode);
   fChain->SetBranchAddress("trk_detailed_mc_endVtx_barcode", &trk_detailed_mc_endVtx_barcode, &b_trk_detailed_mc_endVtx_barcode);
   fChain->SetBranchAddress("trk_detailed_mc_barcode", &trk_detailed_mc_barcode, &b_trk_detailed_mc_barcode);
   fChain->SetBranchAddress("trk_detailed_mc_index", &trk_detailed_mc_index, &b_trk_detailed_mc_index);
   fChain->SetBranchAddress("pixClus_n", &pixClus_n, &b_pixClus_n);
   fChain->SetBranchAddress("pixClus_id", &pixClus_id, &b_pixClus_id);
   fChain->SetBranchAddress("pixClus_bec", &pixClus_bec, &b_pixClus_bec);
   fChain->SetBranchAddress("pixClus_layer", &pixClus_layer, &b_pixClus_layer);
   fChain->SetBranchAddress("pixClus_detElementId", &pixClus_detElementId, &b_pixClus_detElementId);
   fChain->SetBranchAddress("pixClus_phi_module", &pixClus_phi_module, &b_pixClus_phi_module);
   fChain->SetBranchAddress("pixClus_eta_module", &pixClus_eta_module, &b_pixClus_eta_module);
   fChain->SetBranchAddress("pixClus_col", &pixClus_col, &b_pixClus_col);
   fChain->SetBranchAddress("pixClus_row", &pixClus_row, &b_pixClus_row);
   fChain->SetBranchAddress("pixClus_charge", &pixClus_charge, &b_pixClus_charge);
   fChain->SetBranchAddress("pixClus_LVL1A", &pixClus_LVL1A, &b_pixClus_LVL1A);
   fChain->SetBranchAddress("pixClus_sizePhi", &pixClus_sizePhi, &b_pixClus_sizePhi);
   fChain->SetBranchAddress("pixClus_sizeZ", &pixClus_sizeZ, &b_pixClus_sizeZ);
   fChain->SetBranchAddress("pixClus_size", &pixClus_size, &b_pixClus_size);
   fChain->SetBranchAddress("pixClus_locX", &pixClus_locX, &b_pixClus_locX);
   fChain->SetBranchAddress("pixClus_locY", &pixClus_locY, &b_pixClus_locY);
   fChain->SetBranchAddress("pixClus_x", &pixClus_x, &b_pixClus_x);
   fChain->SetBranchAddress("pixClus_y", &pixClus_y, &b_pixClus_y);
   fChain->SetBranchAddress("pixClus_z", &pixClus_z, &b_pixClus_z);
   fChain->SetBranchAddress("pixClus_isFake", &pixClus_isFake, &b_pixClus_isFake);
   fChain->SetBranchAddress("pixClus_isGanged", &pixClus_isGanged, &b_pixClus_isGanged);
   fChain->SetBranchAddress("pixClus_isSplit", &pixClus_isSplit, &b_pixClus_isSplit);
   fChain->SetBranchAddress("pixClus_splitProb1", &pixClus_splitProb1, &b_pixClus_splitProb1);
   fChain->SetBranchAddress("pixClus_splitProb2", &pixClus_splitProb2, &b_pixClus_splitProb2);
   fChain->SetBranchAddress("pixClus_mc_barcode", &pixClus_mc_barcode, &b_pixClus_mc_barcode);
   fChain->SetBranchAddress("primvx_n", &primvx_n, &b_primvx_n);
   fChain->SetBranchAddress("primvx_x", &primvx_x, &b_primvx_x);
   fChain->SetBranchAddress("primvx_y", &primvx_y, &b_primvx_y);
   fChain->SetBranchAddress("primvx_z", &primvx_z, &b_primvx_z);
   fChain->SetBranchAddress("primvx_err_x", &primvx_err_x, &b_primvx_err_x);
   fChain->SetBranchAddress("primvx_err_y", &primvx_err_y, &b_primvx_err_y);
   fChain->SetBranchAddress("primvx_err_z", &primvx_err_z, &b_primvx_err_z);
   fChain->SetBranchAddress("primvx_cov_xy", &primvx_cov_xy, &b_primvx_cov_xy);
   fChain->SetBranchAddress("primvx_cov_xz", &primvx_cov_xz, &b_primvx_cov_xz);
   fChain->SetBranchAddress("primvx_cov_yz", &primvx_cov_yz, &b_primvx_cov_yz);
   fChain->SetBranchAddress("primvx_chi2", &primvx_chi2, &b_primvx_chi2);
   fChain->SetBranchAddress("primvx_ndof", &primvx_ndof, &b_primvx_ndof);
   fChain->SetBranchAddress("primvx_px", &primvx_px, &b_primvx_px);
   fChain->SetBranchAddress("primvx_py", &primvx_py, &b_primvx_py);
   fChain->SetBranchAddress("primvx_pz", &primvx_pz, &b_primvx_pz);
   fChain->SetBranchAddress("primvx_E", &primvx_E, &b_primvx_E);
   fChain->SetBranchAddress("primvx_m", &primvx_m, &b_primvx_m);
   fChain->SetBranchAddress("primvx_nTracks", &primvx_nTracks, &b_primvx_nTracks);
   fChain->SetBranchAddress("primvx_sumPt", &primvx_sumPt, &b_primvx_sumPt);
   fChain->SetBranchAddress("primvx_type", &primvx_type, &b_primvx_type);
   fChain->SetBranchAddress("primvx_trk_n", &primvx_trk_n, &b_primvx_trk_n);
   fChain->SetBranchAddress("primvx_trk_weight", &primvx_trk_weight, &b_primvx_trk_weight);
   fChain->SetBranchAddress("primvx_trk_unbiased_d0", &primvx_trk_unbiased_d0, &b_primvx_trk_unbiased_d0);
   fChain->SetBranchAddress("primvx_trk_unbiased_z0", &primvx_trk_unbiased_z0, &b_primvx_trk_unbiased_z0);
   fChain->SetBranchAddress("primvx_trk_err_unbiased_d0", &primvx_trk_err_unbiased_d0, &b_primvx_trk_err_unbiased_d0);
   fChain->SetBranchAddress("primvx_trk_err_unbiased_z0", &primvx_trk_err_unbiased_z0, &b_primvx_trk_err_unbiased_z0);
   fChain->SetBranchAddress("primvx_trk_chi2", &primvx_trk_chi2, &b_primvx_trk_chi2);
   fChain->SetBranchAddress("primvx_trk_d0", &primvx_trk_d0, &b_primvx_trk_d0);
   fChain->SetBranchAddress("primvx_trk_z0", &primvx_trk_z0, &b_primvx_trk_z0);
   fChain->SetBranchAddress("primvx_trk_phi", &primvx_trk_phi, &b_primvx_trk_phi);
   fChain->SetBranchAddress("primvx_trk_theta", &primvx_trk_theta, &b_primvx_trk_theta);
   fChain->SetBranchAddress("primvx_trk_index", &primvx_trk_index, &b_primvx_trk_index);
   fChain->SetBranchAddress("mcevt_n", &mcevt_n, &b_mcevt_n);
   fChain->SetBranchAddress("mcevt_signal_process_id", &mcevt_signal_process_id, &b_mcevt_signal_process_id);
   fChain->SetBranchAddress("mcevt_event_number", &mcevt_event_number, &b_mcevt_event_number);
   fChain->SetBranchAddress("mcevt_event_scale", &mcevt_event_scale, &b_mcevt_event_scale);
   fChain->SetBranchAddress("mcevt_alphaQCD", &mcevt_alphaQCD, &b_mcevt_alphaQCD);
   fChain->SetBranchAddress("mcevt_alphaQED", &mcevt_alphaQED, &b_mcevt_alphaQED);
   fChain->SetBranchAddress("mcevt_pdf_id1", &mcevt_pdf_id1, &b_mcevt_pdf_id1);
   fChain->SetBranchAddress("mcevt_pdf_id2", &mcevt_pdf_id2, &b_mcevt_pdf_id2);
   fChain->SetBranchAddress("mcevt_pdf_x1", &mcevt_pdf_x1, &b_mcevt_pdf_x1);
   fChain->SetBranchAddress("mcevt_pdf_x2", &mcevt_pdf_x2, &b_mcevt_pdf_x2);
   fChain->SetBranchAddress("mcevt_pdf_scale", &mcevt_pdf_scale, &b_mcevt_pdf_scale);
   fChain->SetBranchAddress("mcevt_pdf1", &mcevt_pdf1, &b_mcevt_pdf1);
   fChain->SetBranchAddress("mcevt_pdf2", &mcevt_pdf2, &b_mcevt_pdf2);
   fChain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
   fChain->SetBranchAddress("mcevt_nparticle", &mcevt_nparticle, &b_mcevt_nparticle);
   fChain->SetBranchAddress("mcevt_pileUpType", &mcevt_pileUpType, &b_mcevt_pileUpType);
   fChain->SetBranchAddress("mcvtx_n", &mcvtx_n, &b_mcvtx_n);
   fChain->SetBranchAddress("mcvtx_x", &mcvtx_x, &b_mcvtx_x);
   fChain->SetBranchAddress("mcvtx_y", &mcvtx_y, &b_mcvtx_y);
   fChain->SetBranchAddress("mcvtx_z", &mcvtx_z, &b_mcvtx_z);
   fChain->SetBranchAddress("mcpart_n", &mcpart_n, &b_mcpart_n);
   fChain->SetBranchAddress("mcpart_pt", &mcpart_pt, &b_mcpart_pt);
   fChain->SetBranchAddress("mcpart_m", &mcpart_m, &b_mcpart_m);
   fChain->SetBranchAddress("mcpart_eta", &mcpart_eta, &b_mcpart_eta);
   fChain->SetBranchAddress("mcpart_phi", &mcpart_phi, &b_mcpart_phi);
   fChain->SetBranchAddress("mcpart_type", &mcpart_type, &b_mcpart_type);
   fChain->SetBranchAddress("mcpart_status", &mcpart_status, &b_mcpart_status);
   fChain->SetBranchAddress("mcpart_barcode", &mcpart_barcode, &b_mcpart_barcode);
   fChain->SetBranchAddress("mcpart_mothertype", &mcpart_mothertype, &b_mcpart_mothertype);
   fChain->SetBranchAddress("mcpart_motherbarcode", &mcpart_motherbarcode, &b_mcpart_motherbarcode);
   fChain->SetBranchAddress("mcpart_mcevt_index", &mcpart_mcevt_index, &b_mcpart_mcevt_index);
   fChain->SetBranchAddress("mcpart_mcprodvtx_index", &mcpart_mcprodvtx_index, &b_mcpart_mcprodvtx_index);
   fChain->SetBranchAddress("mcpart_mother_n", &mcpart_mother_n, &b_mcpart_mother_n);
   fChain->SetBranchAddress("mcpart_mother_index", &mcpart_mother_index, &b_mcpart_mother_index);
   fChain->SetBranchAddress("mcpart_mcdecayvtx_index", &mcpart_mcdecayvtx_index, &b_mcpart_mcdecayvtx_index);
   fChain->SetBranchAddress("mcpart_child_n", &mcpart_child_n, &b_mcpart_child_n);
   fChain->SetBranchAddress("mcpart_child_index", &mcpart_child_index, &b_mcpart_child_index);
   fChain->SetBranchAddress("mcpart_truthtracks_index", &mcpart_truthtracks_index, &b_mcpart_truthtracks_index);
   fChain->SetBranchAddress("truthtrack_n", &truthtrack_n, &b_truthtrack_n);
   fChain->SetBranchAddress("truthtrack_ok", &truthtrack_ok, &b_truthtrack_ok);
   fChain->SetBranchAddress("truthtrack_d0", &truthtrack_d0, &b_truthtrack_d0);
   fChain->SetBranchAddress("truthtrack_z0", &truthtrack_z0, &b_truthtrack_z0);
   fChain->SetBranchAddress("truthtrack_phi", &truthtrack_phi, &b_truthtrack_phi);
   fChain->SetBranchAddress("truthtrack_theta", &truthtrack_theta, &b_truthtrack_theta);
   fChain->SetBranchAddress("truthtrack_qoverp", &truthtrack_qoverp, &b_truthtrack_qoverp);
   fChain->SetBranchAddress("deadPixMod_idHash", &deadPixMod_idHash, &b_deadPixMod_idHash);
   fChain->SetBranchAddress("deadPixMod_nDead", &deadPixMod_nDead, &b_deadPixMod_nDead);
   Notify();
}

Bool_t JetTagD3PD::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void JetTagD3PD::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t JetTagD3PD::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef JetTagD3PD_cxx
