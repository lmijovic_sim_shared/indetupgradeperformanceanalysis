#include <vector>
using namespace std;


void BTagging_Input_Weight()
{
  gInterpreter->GenerateDictionary("vector<vector<int> >;list<vector<int> >","list;vector");
  gInterpreter->GenerateDictionary("vector<vector<float> >;list<vector<float> >","list;vector");

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);

  TCanvas * c1 = new TCanvas("NJetsTracks","NJetsTracks",1000,500);
  c1->Divide(2,1);

  TCanvas * c2 = new TCanvas("Sig","Sig",750,750);
  c2->Divide(2,2);
  c2->cd(1)->SetLogy();
  c2->cd(2)->SetLogy();
  c2->cd(3)->SetLogy();
  c2->cd(4)->SetLogy();

  TCanvas * c3 = new TCanvas("TruthLabel","TruthLabel",500,500);

  TCanvas * c4 = new TCanvas("w_IP3D","w_IP3D",500,500);
  c4->SetLogy();

  TCanvas * c5 = new TCanvas("SV1","SV1",900,600);
  c5->Divide(3,2);
  c5->cd(2)->SetLogy();
  c5->cd(3)->SetLogy();
  c5->cd(4)->SetLogy();
  c5->cd(5)->SetLogy();
  c5->cd(6)->SetLogy();


  TH1F * h_Njets= new TH1F("h_Njets","h_Njets",25,0,25);
  TH1F * h_Ntracks_IP= new TH1F("h_Ntracks_IP","h_Ntracks_IP",30,0,30);
  TH1F * h_TruLab= new TH1F("TruLab","TruLab",7,0,7);

  TH1F * h_Sigd0_light = new TH1F("h_Sigd0_light","h_Sigd0_light",100,-50,50);
  TH1F * h_Sigz0_light = new TH1F("h_Sigz0_light","h_Sigz0_light",100,-40,40);
  TH1F * h_Vald0_light = new TH1F("h_Vald0_light","h_Vald0_light",100,-1,1);
  TH1F * h_Valz0_light = new TH1F("h_Valz0_light","h_Valz0_light",100,-1.5,1.5);

  TH1F * h_Sigd0_c = new TH1F("h_Sigd0_c","h_Sigd0_c",100,-50,50);
  TH1F * h_Sigz0_c = new TH1F("h_Sigz0_c","h_Sigz0_c",100,-40,40);
  TH1F * h_Vald0_c = new TH1F("h_Vald0_c","h_Vald0_c",100,-1,1);
  TH1F * h_Valz0_c = new TH1F("h_Valz0_c","h_Valz0_c",100,-1.5,1.5);

  TH1F * h_Sigd0_b = new TH1F("h_Sigd0_b","h_Sigd0_b",100,-50,50);
  TH1F * h_Sigz0_b = new TH1F("h_Sigz0_b","h_Sigz0_b",100,-40,40);
  TH1F * h_Vald0_b = new TH1F("h_Vald0_b","h_Vald0_b",100,-1,1);
  TH1F * h_Valz0_b = new TH1F("h_Valz0_b","h_Valz0_b",100,-1.5,1.5);

  TH1F * h_wIP3D_light = new TH1F("h_wIP3D_light","h_wIP3D_light",50,-20,30);
  TH1F * h_wIP3D_c = new TH1F("h_wIP3D_c","h_wIP3D_c",50,-20,30);
  TH1F * h_wIP3D_b = new TH1F("h_wIP3D_b","h_wIP3D_b",50,-20,30);

  TH1F * h_Ntracks_SV = new TH1F("h_Ntracks_SV","h_Ntracks_SV",10,0,10);

  TH1F * h_SV_mass_light = new TH1F("h_SV_mass_light","h_SV_mass_light",30,0,6);
  TH1F * h_SV_mass_c = new TH1F("h_SV_mass_c","h_SV_mass_c",30,0,6);
  TH1F * h_SV_mass_b = new TH1F("h_SV_mass_b","h_SV_mass_b",30,0,6);

  TH1F * h_SV_efrc_light = new TH1F("h_SV_efrc_light","h_SV_efrc_light",30,0,1);
  TH1F * h_SV_efrc_c = new TH1F("h_SV_efrc_c","h_SV_efrc_c",30,0,1);
  TH1F * h_SV_efrc_b = new TH1F("h_SV_efrc_b","h_SV_efrc_b",30,0,1);

  TH1F * h_SV_n2t_light = new TH1F("h_SV_n2t_light","h_SV_n2t_light",40,0,40);
  TH1F * h_SV_n2t_c = new TH1F("h_SV_n2t_c","h_SV_n2t_c",40,0,40);
  TH1F * h_SV_n2t_b = new TH1F("h_SV_n2t_b","h_SV_n2t_b",40,0,40);

  TH1F * h_wSV1_light = new TH1F("h_wSV1_light","h_wSV1_light",50,-20,30);
  TH1F * h_wSV1_c = new TH1F("h_wSV1_c","h_wSV1_c",50,-20,30);
  TH1F * h_wSV1_b = new TH1F("h_wSV1_b","h_wSV1_b",50,-20,30);

  TH1F * h_wIP3DSV1_light = new TH1F("h_wIP3DSV1_light","h_wIP3DSV1_light",50,-20,30);
  TH1F * h_wIP3DSV1_c = new TH1F("h_wIP3DSV1_c","h_wIP3DSV1_c",50,-20,30);
  TH1F * h_wIP3DSV1_b = new TH1F("h_wIP3DSV1_b","h_wIP3DSV1_b",50,-20,30);


  TChain BChain;

  for( Int_t n =1; n <= 250; n++ )
  //for( Int_t n =1; n <= 10; n++ )
  {
    char ntuple[300];
    if( n != 30 && n != 48 && n != 61 && n != 88 && n != 100 && n != 139 && n != 170 )
    {
      sprintf(ntuple,"root://eosatlas//eos/atlas/atlasgroupdisk/det-slhc/user/nstyles/group/user.nstyles.group.det-slhc.mc11_slhcid.MC11.105249.pythia_bb500.JetTagD3PD.110913192833_der1315990022/group.det-slhc.33364_000314.EXT0._%05u.JetTagD3PD.root",n);
      BChain.AddFile(ntuple,0,"btagd3pd");
    }
  }


  Int_t Njets;
  vector<int> * TruLab;
  vector<float> *jet_pt;
  vector<float>   *jet_eta;

  vector<float> * wIP3D;
  vector <int > * Ntracks_IP;
  std::vector<std::vector<float> > * d0sig; 
  std::vector<std::vector<float> > * z0sig;
  std::vector<std::vector<float> > * d0val;
  std::vector<std::vector<float> > * z0val;

  vector<float> * wSV1;
  vector<int> * Ntracks_SV;
  vector<int> * SV_n2t;
  vector<float> * SV_mass;
  vector<float> * SV_efrc;
 
  vector<float> * wIP3DSV1;

  
  TBranch * b_Njets;
  TBranch * b_TruLab;
  TBranch * b_jet_pt;
  TBranch * b_jet_eta;

  TBranch * b_wIP3D;
  TBranch * b_Ntracks_IP;
  TBranch * b_d0sig; 
  TBranch * b_z0sig;
  TBranch * b_d0val;
  TBranch * b_z0val;

  TBranch * b_wSV1;
  TBranch * b_Ntracks_SV;
  TBranch * b_SV_n2t;
  TBranch * b_SV_mass;
  TBranch * b_SV_efrc;

  TBranch * b_wIP3DSV1;


  BChain.SetBranchAddress("jet_antikt4truth_n",&Njets,&b_Njets);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_truth_label",&TruLab,&b_TruLab);
  BChain.SetBranchAddress("jet_antikt4truth_pt",&jet_pt,&b_jet_pt);
  BChain.SetBranchAddress("jet_antikt4truth_eta",&jet_eta,&b_jet_eta);

  BChain.SetBranchAddress("jet_antikt4truth_flavor_weight_IP3D",&wIP3D,&b_wIP3D);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_component_ipplus_trk_n",&Ntracks_IP,&b_Ntracks_IP);

  BChain.SetBranchAddress("jet_antikt4truth_flavor_weight_SV1",&wSV1,&b_wSV1);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_component_svp_ntrk",&Ntracks_SV,&b_Ntracks_SV);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_component_svp_n2t",&SV_n2t,&b_SV_n2t);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_component_svp_mass",&SV_mass,&b_SV_mass);
  BChain.SetBranchAddress("jet_antikt4truth_flavor_component_svp_efrc",&SV_efrc,&b_SV_efrc);

  BChain.SetBranchAddress("jet_antikt4truth_flavor_weight_Comb",&wIP3DSV1,&b_wIP3DSV1);


  Int_t count_light = 0;
  Int_t count_c = 0;
  Int_t count_b = 0;
  Int_t count_trk_light = 0;
  Int_t count_trk_c = 0;
  Int_t count_trk_b = 0;


  Long64_t nentries = (Long64_t)BChain.GetEntries();

  cout << "Number of entries: " << nentries << endl;

  for( Long64_t i=0; i<nentries; i++ )
  {
    Long64_t localEntry = BChain.LoadTree(i);

    TBranch * b_d0sig = BChain.GetBranch("jet_antikt4truth_flavor_component_ipplus_trk_d0sig");
    b_d0sig->SetAddress(&d0sig);

    TBranch * b_z0sig = BChain.GetBranch("jet_antikt4truth_flavor_component_ipplus_trk_z0sig");
    b_z0sig->SetAddress(&z0sig);

    TBranch * b_d0val = BChain.GetBranch("jet_antikt4truth_flavor_component_ipplus_trk_d0val");
    b_d0val->SetAddress(&d0val);

    TBranch * b_z0val = BChain.GetBranch("jet_antikt4truth_flavor_component_ipplus_trk_z0val");
    b_z0val->SetAddress(&z0val); 


    b_Njets->GetEntry(localEntry);
    b_TruLab->GetEntry(localEntry);
    b_jet_pt->GetEntry(localEntry);
    b_jet_eta->GetEntry(localEntry);

    b_wIP3D->GetEntry(localEntry);
    b_Ntracks_IP->GetEntry(localEntry);
    b_d0sig->GetEntry(localEntry);
    b_z0sig->GetEntry(localEntry);
    b_d0val->GetEntry(localEntry);
    b_z0val->GetEntry(localEntry);

    b_wSV1->GetEntry(localEntry);
    b_Ntracks_SV->GetEntry(localEntry);
    b_SV_n2t->GetEntry(localEntry);
    b_SV_mass->GetEntry(localEntry);
    b_SV_efrc->GetEntry(localEntry);

    b_wIP3DSV1->GetEntry(localEntry);

 
    h_Njets->Fill(Njets);
 
    for( Int_t j=0; j<Njets; j++)
    {
      // take only jets with pt > 15 GeV and eta < 2.5
      if( jet_pt->at(j) <= 15000. || fabs(jet_eta->at(j)) >=  2.5 ) continue;

      h_Njets->Fill(Njets);
      h_TruLab->Fill(TruLab->at(j));
      h_Ntracks_IP->Fill(Ntracks_IP->at(j));
      h_Ntracks_SV->Fill(Ntracks_SV->at(j));

      if( TruLab->at(j) == 0 ) // filling histos for light jets
      {
        h_wIP3D_light->Fill(wIP3D->at(j));
        h_wIP3DSV1_light->Fill(wIP3DSV1->at(j));
        h_SV_mass_light->Fill(SV_mass->at(j));
	h_SV_efrc_light->Fill(SV_efrc->at(j));
	h_SV_n2t_light->Fill(SV_n2t->at(j));
	h_wSV1_light->Fill(wSV1->at(j));
        count_light++; // needed to normalize the histos to #entries
      }
      else if( TruLab->at(j) == 4 ) // filling histos for c-jets
      {
	h_wIP3D_c->Fill(wIP3D->at(j));
        h_wIP3DSV1_c->Fill(wIP3DSV1->at(j));
	h_SV_mass_c->Fill(SV_mass->at(j));
	h_SV_efrc_c->Fill(SV_efrc->at(j));
	h_SV_n2t_c->Fill(SV_n2t->at(j));
	h_wSV1_c->Fill(wSV1->at(j));
	count_c++;
      }
      else if( TruLab->at(j) == 5 ) // filling histos for b-jets
      {
	h_wIP3D_b->Fill(wIP3D->at(j));
        h_wIP3DSV1_b->Fill(wIP3DSV1->at(j));
	h_SV_mass_b->Fill(SV_mass->at(j));
	h_SV_efrc_b->Fill(SV_efrc->at(j));
	h_SV_n2t_b->Fill(SV_n2t->at(j));
	h_wSV1_b->Fill(wSV1->at(j));
	count_b++;
      }
          
    
      for( Int_t k=0; k<Ntracks_IP->at(j); k++) // loop over all tracks associated to this jet in IP3D algorithm
      {
        if( TruLab->at(j) == 0 ) // filling histos for tracks in light jets
	{
          h_Sigd0_light->Fill(d0sig->at(j).at(k));
          h_Sigz0_light->Fill(z0sig->at(j).at(k));
          h_Vald0_light->Fill(d0val->at(j).at(k));
          h_Valz0_light->Fill(z0val->at(j).at(k));
          count_trk_light++; // needed to normalize the histos to #entries
        }

        if( TruLab->at(j) == 4 ) // filling histos for tracks in c-jets
	{
          h_Sigd0_c->Fill(d0sig->at(j).at(k));
          h_Sigz0_c->Fill(z0sig->at(j).at(k));
          h_Vald0_c->Fill(d0val->at(j).at(k));
          h_Valz0_c->Fill(z0val->at(j).at(k));
          count_trk_c++;
        }

        if( TruLab->at(j) == 5 ) // filling histos for tracks in b-jets
	{
          h_Sigd0_b->Fill(d0sig->at(j).at(k));
          h_Sigz0_b->Fill(z0sig->at(j).at(k));
          h_Vald0_b->Fill(d0val->at(j).at(k));
          h_Valz0_b->Fill(z0val->at(j).at(k));
          count_trk_b++;
        }

      }
    } //end of loop over jets

    if( (i+1)%1000 == 0 ) cout << "\r" << i+1 << " " << flush;

  } //end of loop over entries

 
// *** uncomment this to normalize histos to number of entries. Grid can be turned on in function plot2 ****************
 
  // h_wIP3D_light->Scale(1./count_light);
//   h_wIP3D_c->Scale(1./count_c);
//   h_wIP3D_b->Scale(1./count_b);

//   h_Sigd0_light->Scale(1./count_trk_light);
//   h_Sigz0_light->Scale(1./count_trk_light);
//   h_Vald0_light->Scale(1./count_trk_light);
//   h_Valz0_light->Scale(1./count_trk_light);

//   h_Sigd0_c->Scale(1./count_trk_c);
//   h_Sigz0_c->Scale(1./count_trk_c);
//   h_Vald0_c->Scale(1./count_trk_c);
//   h_Valz0_c->Scale(1./count_trk_c);

//   h_Sigd0_b->Scale(1./count_trk_b);
//   h_Sigz0_b->Scale(1./count_trk_b);
//   h_Vald0_b->Scale(1./count_trk_b);
//   h_Valz0_b->Scale(1./count_trk_b);

//   h_SV_mass_b->Scale(1./count_b);
//   h_SV_mass_c->Scale(1./count_c);
//   h_SV_mass_light->Scale(1./count_light);

//   h_SV_efrc_b->Scale(1./count_b);
//   h_SV_efrc_c->Scale(1./count_c);
//   h_SV_efrc_light->Scale(1./count_light); 

//   h_SV_n2t_b->Scale(1./count_b);
//   h_SV_n2t_c->Scale(1./count_c);
//   h_SV_n2t_light->Scale(1./count_light); 

//   h_wSV1_b->Scale(1./count_b);
//   h_wSV1_c->Scale(1./count_c);
//   h_wSV1_light->Scale(1./count_light); 

//   h_wIP3DSV1_b->Scale(1./count_b);
//   h_wIP3DSV1_c->Scale(1./count_c);
//   h_wIP3DSV1_light->Scale(1./count_light); 

// *********************************************************************************************************************


  c1->cd(1);
  plot(h_Njets,"Number of jets","");
  c1->cd(2);
  plot(h_Ntracks_IP,"Number of tracks per jet (IP3D)","Number of jets");

  c2->cd(1);
  Maxi( h_Vald0_light, h_Vald0_c, h_Vald0_b );  
  plot2(h_Vald0_light,"Signed transverse impact parameter","Number of tracks",kBlue);
  plotSame(h_Vald0_c,kGreen);
  plotSame(h_Vald0_b,kRed);
  c2->cd(2);
  Maxi( h_Valz0_light, h_Valz0_c, h_Valz0_b );  
  plot2(h_Valz0_light,"Signed longitudinal impact parameter","Number of tracks",kBlue);
  plotSame(h_Valz0_c,kGreen);
  plotSame(h_Valz0_b,kRed);
  c2->cd(3);
  Maxi( h_Sigd0_light, h_Sigd0_c, h_Sigd0_b );  
  plot2(h_Sigd0_light,"Signed transv. impact param. significance","Number of tracks",kBlue);
  plotSame(h_Sigd0_c,kGreen);
  plotSame(h_Sigd0_b,kRed);
  c2->cd(4);
  Maxi( h_Sigz0_light, h_Sigz0_c, h_Sigz0_b );
  plot2(h_Sigz0_light,"Signed long. impact param. significance","Number of tracks",kBlue);
  plotSame(h_Sigz0_c,kGreen);
  plotSame(h_Sigz0_b,kRed);
  c2->cd(1);
  TLegend * leg = new TLegend(0.19,0.67,0.49,0.87);
  leg->AddEntry(h_Sigd0_b,"tracks in b-jets");
  leg->AddEntry(h_Sigd0_c,"tracks in c-jets");
  leg->AddEntry(h_Sigd0_light,"tracks in light jets");
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->Draw("same");

  c3->cd();
  plot(h_TruLab,"Truth label","Number of jets");

  c4->cd();
  Maxi( h_wIP3D_light, h_wIP3D_c, h_wIP3D_b );
  plot2(h_wIP3D_light,"IP3D weight","Number of jets",kBlue);
  plotSame(h_wIP3D_c,kGreen);
  plotSame(h_wIP3D_b,kRed);
  TLegend * leg2 = new TLegend(0.65,0.7,0.87,0.85);
  leg2->AddEntry(h_wIP3D_b,"b-jets");
  leg2->AddEntry(h_wIP3D_c,"c-jets");
  leg2->AddEntry(h_wIP3D_light,"light jets");
  leg2->SetFillColor(0);
  leg2->SetBorderSize(0);
  leg2->Draw("same");

  c5->cd(1);
  plot(h_Ntracks_SV,"Number of tracks","Number of jets");
  c5->cd(2);
  Maxi( h_SV_mass_b, h_SV_mass_c, h_SV_mass_light ); 
  plot2(h_SV_mass_b,"SV mass","Number of jets",kRed);
  plotSame(h_SV_mass_c,kGreen);
  plotSame(h_SV_mass_light,kBlue);
  c5->cd(3);
  Maxi( h_SV_efrc_b, h_SV_efrc_c, h_SV_efrc_light );  
  plot2(h_SV_efrc_b,"SV energy fraction","Number of jets",kRed);
  plotSame(h_SV_efrc_c,kGreen);
  plotSame(h_SV_efrc_light,kBlue);
  c5->cd(4);
  Maxi( h_SV_n2t_b, h_SV_n2t_c, h_SV_n2t_light );
  plot2(h_SV_n2t_b,"Number of two-track vertices","Number of jets",kRed);
  plotSame(h_SV_n2t_c,kGreen);
  plotSame(h_SV_n2t_light,kBlue);
  c5->cd(5);
  Maxi( h_wSV1_b, h_wSV1_c, h_wSV1_light ); 
  plot2(h_wSV1_b,"SV1 weight","Number of jets",kRed);
  plotSame(h_wSV1_c,kGreen);
  plotSame(h_wSV1_light,kBlue);
  c5->cd(6);
  Maxi( h_wIP3DSV1_b, h_wIP3DSV1_c, h_wIP3DSV1_light ); 
  plot2(h_wIP3DSV1_b,"IP3D + SV1 weight","Number of jets",kRed);
  plotSame(h_wIP3DSV1_c,kGreen);
  plotSame(h_wIP3DSV1_light,kBlue);
  c5->cd(2);
  TLegend * leg3 = new TLegend(0.65,0.7,0.87,0.85);
  leg3->AddEntry(h_SV_mass_b,"b-jets");
  leg3->AddEntry(h_SV_mass_c,"c-jets");
  leg3->AddEntry(h_SV_mass_light,"light jets");
  leg3->SetFillColor(0);
  leg3->SetBorderSize(0);
  leg3->Draw("same");
   
}



void plot(TH1F* histo, TString xaxistitle, TString yaxistitle)
{
  gPad->SetBottomMargin(0.17);
  gPad->SetLeftMargin(0.2);

  histo->SetFillColor(kYellow-7);
  histo->GetXaxis()->SetTitle(xaxistitle);
  histo->GetYaxis()->SetTitle(yaxistitle);
  histo->GetXaxis()->SetTitleSize(0.05);
  histo->GetYaxis()->SetTitleSize(0.05);
  histo->GetXaxis()->SetLabelSize(0.05);
  histo->GetYaxis()->SetLabelSize(0.05); 
  histo->GetYaxis()->SetTitleOffset(1.6);
  histo->GetYaxis()->SetTitleOffset(2.1);
  histo->SetTitle("");
  histo->Draw();
}


void plot2(TH1F* histo, TString xaxistitle, TString yaxistitle, Color_t color)
{
  //gPad->SetGrid();
  gPad->SetTicks(1,1);
  gPad->SetBottomMargin(0.17);
  gPad->SetLeftMargin(0.17);

  histo->SetLineColor(color);
  histo->SetLineWidth(2);
  histo->SetFillColor(0);
  histo->GetXaxis()->SetTitle(xaxistitle);
  histo->GetYaxis()->SetTitle(yaxistitle);
  histo->GetXaxis()->SetTitleSize(0.05);
  histo->GetYaxis()->SetTitleSize(0.05);
  histo->GetXaxis()->SetLabelSize(0.05);
  histo->GetYaxis()->SetLabelSize(0.05); 
  histo->GetXaxis()->SetTitleOffset(1.2);
  histo->GetYaxis()->SetTitleOffset(1.7);
  histo->SetTitle("");
  histo->Draw();
}


void plotSame(TH1F* histo, Color_t color)
{
  histo->SetLineColor(color);
  histo->SetLineWidth(2);
  histo->SetFillColor(0);
  histo->SetTitle("");
  histo->GetXaxis()->DrawClone();
  histo->Draw("same");
}


void Maxi( TH1F * histo1, TH1F * histo2, TH1F * histo3 ) // histo1 needs to be the histo drawn first
{
  float max1 = histo1->GetMaximum();
  float max2 = histo2->GetMaximum();
  float max3 = histo3->GetMaximum();

  float max = max1>max2 ? max1 : max2;
  max = max>max3 ? max : max3;

  histo1->SetMaximum( max*2 );
}
