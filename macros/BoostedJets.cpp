#include "TChain.h"
// stl_loader.h
#include<vector>

/*#ifdef __CINT__
  #pragma link C++ class vector<vector<float> >;
  #pragma link C++ class vector<vector<int> > + ; 
  #pragma link C++ class vector<vector<float> > + ; 
  #pragma link C++ class vector<vector<double> > + ; 
  #else template class std::vector<std::vector<float> >;
  #endif*/
#include "TMath.h"
#include "TString.h"
#include "TFile.h"
#include "TCut.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TCanvas.h"
#include <iostream>
#include "TGraph.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "TApplication.h"
#include "TStyle.h"
#include "TLegend.h"
	 
using namespace std;
//gROOT->LoadMacro("AtlasStyle.h");
	
//	SetAtlasStyle();

void run(/*TChain& MyChain*/);	

int main(int argc, char **argv){

  TApplication app("app", &argc, argv);///leaves plot when I'm closing

  std::string commandLine = argv[0];
  /// cout << "executing command " << commandLine << endl;

  if(argc>1) cout << "FATAL!!" << endl;
  else cout << "executing command " << commandLine << endl;

  /// TChain chain;

  run(/*chain*/);
  app.Run();

  return 0;
}

void run(/*TChain& chain*/){
 

  /// Mychain = new TChain("btagd3pd","");
  ///  TChain * Mychain1 = new TChain("btagd3pd/trk_Pixel_hit_incidenceTheta","");

  TChain *Mychain = new TChain();

  /// Mychain = &chain;

  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00001.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00002.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00003.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00004.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00005.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00006.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00007.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00008.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00009.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00010.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00011.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00012.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00013.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00014.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00015.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00016.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00017.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00018.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00019.JetTagD3PD.root/btagd3pd");
Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105015.J6_pythia_jetjet.rec.JetTagD3PD.120312204704/group.det-slhc.65790_001086.EXT0._00020.JetTagD3PD.root/btagd3pd");

  /////////////////////////////////////////////////////////////////
  TString SaveTag = "mc";  
  int x=4;
  float L0R=37.;
  float L0Z=319.5;///324.; <-- old number by Karo, corrected using Nick's pdf
  int L0EtaModule=31; //Chiara: number of modules+1
  int L0PhiModule=16; //Chiara: number of staves
  float L1R=75.;
  float L1Z=553.8;///555.;<-- old number by Karo, corrected using Nick's pdf
  int L1EtaModule=53;
  int L1PhiModule=32;
  float L2R=156.139;//149.;<-- old number by Karo, corrected using Nick's pdf
  float L2Z=574;///555.;<-- old number by Karo, corrected using Nick's pdf
  int L2EtaModule=33;
  int L2PhiModule=32;
  float L3R=195.21;///209.;<-- old number by Karo, corrected using Nick's pdf
  float L3Z=574.;///555.;<-- old number by Karo, corrected using Nick's pdf
  int L3EtaModule=33;
  int L3PhiModule=40;
  
  int resolution=60; ///from R=0-3.0

  double const ptCutJets = 800000; //MeV

  //=============================//
  // Chiara: add SCT information //
  //=============================//

  float R_L0_SCT = 380.;
  float z_L0_SCT = 2357.;
  int stave_L0_SCT = 28;
  int mod_L0_SCT = 25;//it's 24+1, but for some reson, everything is considered+1..to be understood
  
  ////////////////////  //////////////////////////////////////////// 
  float EtaModuleMax=L0EtaModule;
  float PhiModuleMax=L0PhiModule;
  if(EtaModuleMax<L1EtaModule){
    EtaModuleMax=L1EtaModule;
  }
  if(EtaModuleMax<L2EtaModule){
    EtaModuleMax=L2EtaModule;
  }
  if(EtaModuleMax<L3EtaModule){
    EtaModuleMax=L3EtaModule;
  }
  
  if(PhiModuleMax<L1PhiModule){
    PhiModuleMax=L1PhiModule;
  }
  if(PhiModuleMax<L2PhiModule){
    PhiModuleMax=L2PhiModule;
  }
  if(PhiModuleMax<L3PhiModule){
    PhiModuleMax=L3PhiModule;
  }

  float L0Eta=log(tan(atan(L0R/L0Z)/2.))*(-1);
  float L1Eta=log(tan(atan(L1R/L1Z)/2.))*(-1);
  float L2Eta=log(tan(atan(L2R/L2Z)/2.))*(-1);
  float L3Eta=log(tan(atan(L3R/L3Z)/2.))*(-1);
  cout<<"L0Eta"<<L0Eta<<endl;
  cout<<"L0Eta"<<L1Eta<<endl;
  cout<<"L0Eta"<<L2Eta<<endl;
  cout<<"L0Eta"<<L3Eta<<endl;

  //==============================//
  // Chiara: SCT first layer info //
  //==============================//

  float eta_L0_SCT = (-1)*log(tan(atan(R_L0_SCT/z_L0_SCT)));
  cout << "eta_L0_SCT: " << eta_L0_SCT <<endl;
  
  float MaxDeltaetaL0=2*log(tan(atan(37/(324/30.))/2.))*(-1);
  float MaxDeltaetaL1=2*log(tan(atan(75/(555/52.))/2.))*(-1);
  float MaxDeltaphiL0=2*TMath::Pi()/16.;
  cout<<"MaxDeltaetaL0:"<< MaxDeltaetaL0<<endl;
  cout<<"MaxDeltaphiL0:"<< MaxDeltaphiL0<<endl;
  cout<<"MaxDeltaetaL1:"<< MaxDeltaetaL1<<endl;

  //Mychain->Add("./Andreas_OUT.JetTagD3PD.root/btagd3pd");
  ////Chiara: commented outMychain->Add("./OUT.JetTagD3PD6.root/btagd3pd");
  //Mychain->Add("./group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PDs.v1.120125150028/group.det-slhc.57542_000718.EXT0._00027.JetTagD3PD.root/btagd3pdy");
  //Drawing Histograms..

  float const xmax = 0.3;

  //TGraphErrors* PixDeltaRow1= new TGraphErrors();
  /*  TH1F *RadiusTrack= new TH1F("RadiusTrack","",resolution,0.,xmax);
      TH1F *RadiusTrack1= new TH1F("RadiusTrack1","",resolution,0.,xmax);
      TH1F *PixLayer0= new TH1F("PixLayer0","",resolution,0.,xmax);
      TH1F *PixLayer1= new TH1F("PixLayer1","",resolution,0.,xmax);
      TH1F *PixLayer2= new TH1F("PixLayer2","",resolution,0.,xmax);
      TH1F *PixLayer3= new TH1F("PixLayer3","",resolution,0.,xmax);
      TH1F *PixLayerEta0= new TH1F("PixLayerEta0","",resolution,0.,6.0);
      TH1F *PixLayerEta1= new TH1F("PixLayerEta1","",resolution,0.,6.0);
      TH1F *PixLayerEta2= new TH1F("PixLayerEta2","",resolution,0.,6.0);
      TH1F *PixLayerEta3= new TH1F("PixLayerEta3","",resolution,0.,6.0);
      TH1F *PixLayerPhi0= new TH1F("PixLayerPhi0","",40,0.,1.6);
      TH1F *PixLayerPhi1= new TH1F("PixLayerPhi1","",40,0.,1.6);
      TH1F *PixLayerPhi2= new TH1F("PixLayerPhi2","",40,0.,1.6);
      TH1F *PixLayerPhi3= new TH1F("PixLayerPhi3","",40,0.,1.6);
      TH1F *PixelLayer0= new TH1F("PixelLayer0","",resolution,0.,xmax);
      TH1F *PixelLayer1= new TH1F("PixelLayer1","",resolution,0.,xmax);
      TH1F *PixelLayer2= new TH1F("PixelLayer2","",resolution,0.,xmax);
      TH1F *PixelLayer3= new TH1F("PixelLayer3","",resolution,0.,xmax);*/
  TH1F *AllCluster0= new TH1F("AllCluster0","",resolution,0.,xmax);
  TH1F *AllCluster1= new TH1F("AllCluster1","",resolution,0.,xmax);
  TH1F *AllCluster2= new TH1F("AllCluster2","",resolution,0.,xmax);
  TH1F *AllCluster3= new TH1F("AllCluster3","",resolution,0.,xmax);
  TH1F *AllCluster4 = new TH1F("AllCluster4", "", resolution, 0, xmax);///SCT first layer
  TH1F *AllClusterDiv0= new TH1F("AllClusterDiv0","",resolution,0.,xmax);
  TH1F *AllClusterDiv1= new TH1F("AllClusterDiv1","",resolution,0.,xmax);
  TH1F *AllClusterDiv2= new TH1F("AllClusterDiv2","",resolution,0.,xmax);
  TH1F *AllClusterDiv3= new TH1F("AllClusterDiv3","",resolution,0.,xmax);
  TH1F *AllClusterDiv4 = new TH1F("AllClusterDiv4", "", resolution, 0., xmax);//SCT first layer
  TH1F *AmountSensorl0= new TH1F("AmountSensorl0","",resolution,0.,xmax);
  TH1F *AmountSensorl1= new TH1F("AmountSensorl1","",resolution,0.,xmax);
  TH1F *AmountSensorl2= new TH1F("AmountSensorl2","",resolution,0.,xmax);
  TH1F *AmountSensorl3= new TH1F("AmountSensorl3","",resolution,0.,xmax);
  TH1F *AmountSensorSCTl0 = new TH1F("AmountSensorSCTl0", "", resolution, 0., xmax);
  /*  TH1F *OccupancyOntrack0= new TH1F("OccupancyOntrack0","",resolution,0.,xmax);
      TH1F *OccupancyOntrack1= new TH1F("OccupancyOntrack1","",resolution,0.,xmax);
      TH1F *OccupancyOntrack2= new TH1F("OccupancyOntrack2","",resolution,0.,xmax);
      TH1F *OccupancyOntrack3= new TH1F("OccupancyOntrack3","",resolution,0.,xmax);*/
  TH1F *OccupancyAllC0= new TH1F("OccupancyAllC0","",resolution,0.,xmax);
  TH1F *OccupancyAllC1= new TH1F("OccupancyAllC1","",resolution,0.,xmax);
  TH1F *OccupancyAllC2= new TH1F("OccupancyAllC2","",resolution,0.,xmax);
  TH1F *OccupancyAllC3= new TH1F("OccupancyAllC3","",resolution,0.,xmax);
  TH1F *OccupancyAllC4 = new TH1F("OccupancyAllC4", "", resolution, 0., xmax);
  //TH1F *test= new TH1F("test","",6,0.,6.0);

  Int_t sctClus_n;
  vector<float> * sctClus_size = 0;
  vector<int> * sctClus_layer = 0;
  vector<int> * sctClus_bec = 0;
  vector<int>     *sctClus_phi_module = 0;
  vector<int>     *sctClus_eta_module = 0;
  vector<float>   *sctClus_x = 0;
  vector<float>   *sctClus_y = 0;
  vector<float>   *sctClus_z = 0;
  vector<float>   *pixClus_x = 0;
  vector<float>   *pixClus_y = 0;
  vector<float>   *pixClus_z = 0;
  Int_t vx_n = -100;
  Int_t EventNumber = -11;
  vector<char> * vx_nTracks = 0;
  vector<char> * pixClus_layer = 0;
  vector<char> * pixClus_layer1 = 0;
  vector<char> * pixClus_bec1 = 0;
  vector<char> * pixClus_phi_module = 0;
  vector<char> * pixClus_eta_module = 0;
  //int pixnum;
  vector<int> * PixTrkClusGroupsize = 0;
  vector<float> *jet_antikt4truth_eta = 0;
  vector<float> *jet_antikt4truth_phi = 0;
  /* Float_t *trk_Pixel_hit_incidenceTheta1;
     vector<vector<float> > *trk_Pixel_hit_incidenceTheta;
     vector<vector<float> > *trk_Pixel_hit_x;
     vector<vector<float> > *trk_Pixel_hit_y;
     vector<vector<float> > *trk_Pixel_hit_z;
     vector<vector<float> > *trk_Pixel_hit_incidencePhi;
     vector<vector<int> > *trk_Pixel_hit_bec;
     vector<vector<int> > *trk_Pixel_hit_layer;*/
  vector<float> *jet_antikt4truth_pt = 0;
  ///  vector<vector<int> > *trk_Pixel_hit_size;
  // Mychain->SetBranchAddress("vx_n",&vx_n);
  //Mychain->SetBranchAddress("vx_nTracks",&vx_nTracks);
  Mychain->SetBranchAddress("pixClus_size",&PixTrkClusGroupsize);
  Mychain->SetBranchAddress("pixClus_x",&pixClus_x);
  Mychain->SetBranchAddress("pixClus_y",&pixClus_y);
  Mychain->SetBranchAddress("pixClus_z",&pixClus_z);
  Mychain->SetBranchAddress("pixClus_phi_module",&pixClus_phi_module);
  Mychain->SetBranchAddress("pixClus_eta_module",&pixClus_eta_module);
  /// Mychain->SetBranchAddress("trk_Pixel_hit_size",&trk_Pixel_hit_size);
  Mychain->SetBranchAddress("pixClus_layer",&pixClus_layer1);
  Mychain->SetBranchAddress("pixClus_bec",&pixClus_bec1);
  Mychain->SetBranchAddress("EventNumber",&EventNumber);
  Mychain->SetBranchAddress("sctClus_layer",&sctClus_layer);
  Mychain->SetBranchAddress("sctClus_bec",&sctClus_bec);
  Mychain->SetBranchAddress("sctClus_size",&sctClus_size);
  Mychain->SetBranchAddress("sctClus_phi_module", &sctClus_phi_module);
  Mychain->SetBranchAddress("sctClus_eta_module", &sctClus_eta_module);
  Mychain->SetBranchAddress("sctClus_x", &sctClus_x);
  Mychain->SetBranchAddress("sctClus_y", &sctClus_y);
  Mychain->SetBranchAddress("sctClus_z", &sctClus_z);
  Mychain->SetBranchAddress("jet_antikt4truth_eta",&jet_antikt4truth_eta);
  Mychain->SetBranchAddress("jet_antikt4truth_pt",&jet_antikt4truth_pt);
  Mychain->SetBranchAddress("jet_antikt4truth_phi",&jet_antikt4truth_phi);
  /*  Mychain->SetBranchAddress("trk_Pixel_hit_incidenceTheta",&trk_Pixel_hit_incidenceTheta);
      Mychain->SetBranchAddress("trk_Pixel_hit_incidencePhi",&trk_Pixel_hit_incidencePhi);
      Mychain->SetBranchAddress("trk_Pixel_hit_x",&trk_Pixel_hit_x);
      Mychain->SetBranchAddress("trk_Pixel_hit_y",&trk_Pixel_hit_y);
      Mychain->SetBranchAddress("trk_Pixel_hit_z",&trk_Pixel_hit_z);
      Mychain->SetBranchAddress("trk_Pixel_hit_bec",&trk_Pixel_hit_bec);
      Mychain->SetBranchAddress("trk_Pixel_hit_layer",&trk_Pixel_hit_layer);*/
  int NMC = Mychain->GetEntries();
  int Number_Used_Jetsl0=0;
  int Number_Used_Jetsl1=0;
  int Number_Used_Jetsl2=0;
  int Number_Used_Jetsl3=0;
  int Number_Used_JetsSCTl0 = 0; ///SCT fistr layer
  cout<<"Events: "<<NMC<<endl;
  std::vector<std::vector<TH1F*> > histpxl0(L0EtaModule);
  std::vector<std::vector<TH1F*> > histpxl1(L1EtaModule);
  std::vector<std::vector<TH1F*> > histpxl2(L2EtaModule);
  std::vector<std::vector<TH1F*> > histpxl3(L3EtaModule);
  vector<vector<TH1F*> > histSCTl0_x(mod_L0_SCT);
  std::vector<std::vector<TH1F*> > histpyl0(L0EtaModule);
  std::vector<std::vector<TH1F*> > histpyl1(L1EtaModule);
  std::vector<std::vector<TH1F*> > histpyl2(L2EtaModule);
  std::vector<std::vector<TH1F*> > histpyl3(L3EtaModule);
  vector<vector<TH1F*> > histSCTl0_y(mod_L0_SCT);
  std::vector<std::vector<TH1F*> > histpzl0(L0EtaModule);
  std::vector<std::vector<TH1F*> > histpzl1(L1EtaModule);
  std::vector<std::vector<TH1F*> > histpzl2(L2EtaModule);
  std::vector<std::vector<TH1F*> > histpzl3(L3EtaModule);
  vector<vector<TH1F*> > histSCTl0_z(mod_L0_SCT);
  for (int j=0;j<EtaModuleMax;++j) {
    for (int i=0;i<PhiModuleMax;++i) {
      if(i<L0PhiModule && j<L0EtaModule){
        TH1F * histxl0 = new TH1F(Form("histpxl0x%d%d",i,j),"",1,0.,1.);
	TH1F * histyl0 = new TH1F(Form("histpxl0y%d%d",i,j),"",1,0.,1.);
	TH1F * histzl0 = new TH1F(Form("histpxl0z%d%d",i,j),"",1,0.,1.);
        histxl0->StatOverflows(1);
	histyl0->StatOverflows(1);
	histzl0->StatOverflows(1);
        histpxl0[j].push_back(histxl0);
	histpyl0[j].push_back(histyl0);
	histpzl0[j].push_back(histzl0);
      }
      if(i<L1PhiModule && j<L1EtaModule){
        TH1F * histxl1 = new TH1F(Form("histpxl1x%d%d",i,j),"",1,0.,1.);
	TH1F * histyl1 = new TH1F(Form("histpxl1y%d%d",i,j),"",1,0.,1.);
	TH1F * histzl1 = new TH1F(Form("histpxl1z%d%d",i,j),"",1,0.,1.);
        histxl1->StatOverflows(1);
	histyl1->StatOverflows(1);
	histzl1->StatOverflows(1);
        histpxl1[j].push_back(histxl1);
	histpyl1[j].push_back(histyl1);
	histpzl1[j].push_back(histzl1);
      }
      if(i<L2PhiModule && j<L2EtaModule){
        TH1F * histxl2 = new TH1F(Form("histpxl2x%d%d",i,j),"",1,0.,1.);
	TH1F * histyl2 = new TH1F(Form("histpxl2y%d%d",i,j),"",1,0.,1.);
	TH1F * histzl2 = new TH1F(Form("histpxl2z%d%d",i,j),"",1,0.,1.);
        histxl2->StatOverflows(1);
	histyl2->StatOverflows(1);
	histzl2->StatOverflows(1);
        histpxl2[j].push_back(histxl2);
	histpyl2[j].push_back(histyl2);
	histpzl2[j].push_back(histzl2);
      }
      if(i<L3PhiModule && j<L3EtaModule){
        TH1F * histxl3 = new TH1F(Form("histpxl3x%d%d",i,j),"",1,0.,1.);
	TH1F * histyl3 = new TH1F(Form("histpxl3y%d%d",i,j),"",1,0.,1.);
	TH1F * histzl3 = new TH1F(Form("histpxl3z%d%d",i,j),"",1,0.,1.);
        histxl3->StatOverflows(1);
	histyl3->StatOverflows(1);
	histzl3->StatOverflows(1);
        histpxl3[j].push_back(histxl3);
	histpyl3[j].push_back(histyl3);
	histpzl3[j].push_back(histzl3);
      }
    }
  }

  //=============================//
  // Chiara: do the same for SCT //
  //=============================//

  for(int iEtaMod=0; iEtaMod<mod_L0_SCT; ++iEtaMod){

    for(int iPhiMod=0; iPhiMod<stave_L0_SCT; ++iPhiMod){

      if((iEtaMod<mod_L0_SCT)&&(iPhiMod<stave_L0_SCT)){

	TH1F *histL0_x = new TH1F(Form("histL0_SCT_x%d%d", iPhiMod, iEtaMod), "", 1, 0., 1.);
	TH1F *histL0_y = new TH1F(Form("histL0_SCT_y%d%d", iPhiMod, iEtaMod), "", 1, 0., 1.);
	TH1F *histL0_z = new TH1F(Form("histL0_SCT_z%d%d", iPhiMod, iEtaMod), "", 1, 0., 1.);

	histL0_x->StatOverflows(1);
	histL0_y->StatOverflows(1);
	histL0_z->StatOverflows(1);

	histSCTl0_x[iEtaMod].push_back(histL0_x);
	histSCTl0_y[iEtaMod].push_back(histL0_y);
	histSCTl0_z[iEtaMod].push_back(histL0_z);
      }
    }
  }  

  for(int i=0;i<NMC;i++){
    //cout<<"First Event Loop"<<endl;
    if(i%1000==0) cout << "============loop - event N " << i << "============" << endl;
    ///Mychain->GetEvent(o1);
    /// cout << "n entries: " << Mychain->GetEntries() << endl;
    /// int ev = i+2;
    Mychain->GetEvent(i);
    /// cout << "DEBUG:: after get event" << endl;
    vector<char>::iterator ModuleEta=pixClus_eta_module->begin();
    vector<char>::iterator ModulePhi=pixClus_phi_module->begin();
    vector<char>::iterator PClayer=pixClus_layer1->begin();
    vector<char>::iterator PCbec=pixClus_bec1->begin();
    vector<float>::iterator PCx;
    vector<float>::iterator PCy=pixClus_y->begin();
    vector<float>::iterator PCz=pixClus_z->begin();
    vector<vector<float> > *trk_Pixel_hit_x;
    vector<vector<float> > *trk_Pixel_hit_y;
    vector<vector<float> > *trk_Pixel_hit_z;
    for(PCx=pixClus_x->begin(); PCx != pixClus_x->end(); ++PCx){
      /// cout << "DEBUG:: I'm in pixClus loop" << endl;
      int layerPix1 = (int)(*PClayer);
      int becPix1 = (int)(*PCbec);
      if(layerPix1==0 && becPix1==0){
        for(int il=0; il<L0EtaModule; ++il){
	  int ModEta = (int)(*ModuleEta);
	  int ModPhi = (int)(*ModulePhi);
	  int EtaMiddleOffset=(int)((L0EtaModule-1)/2.);
	  if(ModEta==(il-EtaMiddleOffset)){
	    for(int il1=0; il1<L0PhiModule; ++il1){
	      if(ModPhi==il1){
	        histpxl0[il][il1]->Fill(*PCx,1);
	        histpyl0[il][il1]->Fill(*PCy,1);
	        histpzl0[il][il1]->Fill(*PCz,1);
	      }
	    }
	  }
	} 
      }
      if(layerPix1==1 && becPix1==0){
        for(int il=0; il<L1EtaModule; ++il){
	  int ModEta = (int)(*ModuleEta);
	  int ModPhi = (int)(*ModulePhi);
	  int EtaMiddleOffset=(int)((L1EtaModule-1)/2.);
	  if(ModEta==(il-EtaMiddleOffset)){
	    for(int il1=0; il1<L1PhiModule; ++il1){
	      if(ModPhi==il1){
	        histpxl1[il][il1]->Fill(*PCx);
	        histpyl1[il][il1]->Fill(*PCy);
	        histpzl1[il][il1]->Fill(*PCz);
	      }
	    }
	  }
	}
      }
      if(layerPix1==2 && becPix1==0){
        for(int il=0; il<L2EtaModule; ++il){
	  int ModEta = (int)(*ModuleEta);
	  int ModPhi = (int)(*ModulePhi);
	  int EtaMiddleOffset=(int)((L2EtaModule-1)/2.);
	  if(ModEta==(il-EtaMiddleOffset)){
	    for(int il1=0; il1<L2PhiModule; ++il1){
	      if(ModPhi==il1){
	        histpxl2[il][il1]->Fill(*PCx);
	        histpyl2[il][il1]->Fill(*PCy);
	        histpzl2[il][il1]->Fill(*PCz);
	      }
	    }
	  }
	}
      } 
      if(layerPix1==3 && becPix1==0){
        for(int il=0; il<L3EtaModule; ++il){
	  int ModEta = (int)(*ModuleEta);
	  int ModPhi = (int)(*ModulePhi);
	  int EtaMiddleOffset=(int)((L3EtaModule-1)/2.);
	  if(ModEta==(il-EtaMiddleOffset)){
	    for(int il1=0; il1<L3PhiModule; ++il1){
	      if(ModPhi==il1){
	        histpxl3[il][il1]->Fill(*PCx);
	        histpyl3[il][il1]->Fill(*PCy);
	        histpzl3[il][il1]->Fill(*PCz);
	      }
	    }
	  }
	}  
      }
      
      ++PCy;
      ++PCz;
      ++PCbec;
      ++PClayer;
      ++ModulePhi;
      ++ModuleEta;
    
    }

    //======================//
    // Chiara: add SCT case //
    //======================//

    //loop over sct cluster x
    for(int iX=0; iX<sctClus_x->size(); iX++){

      if(((*sctClus_layer)[iX]==0)&&((*sctClus_bec)[iX]==0)){

	//loop over eta module
	for(int iEta=0; iEta<mod_L0_SCT; iEta++){

	  int etaMidOffset = (int)((mod_L0_SCT-1)/2.);
	  int etaSctMod = (int)((*sctClus_eta_module)[iX]);
	  if(etaSctMod==(iEta-etaMidOffset)){

	    //loop over phi stave
	    for(int iPhi=0; iPhi<stave_L0_SCT; iPhi++){

	      int phiSctMod = (int)((*sctClus_phi_module)[iX]);
	      if(phiSctMod==iPhi){

		histSCTl0_x[iEta][iPhi]->Fill((*sctClus_x)[iX]);
		histSCTl0_y[iEta][iPhi]->Fill((*sctClus_y)[iX]);
		histSCTl0_z[iEta][iPhi]->Fill((*sctClus_z)[iX]);
	      }
	    }
	  }
	}
      }
    }
  }
 
  for(int o=0;o<NMC;o++){
    //cout<<"Events:"<<NMC<<endl;
    Mychain->GetEvent(o);
    if(o%1000==0) cout << ">>>>>loop 2 - event N: " << o << "<<<<<" << endl;
    vector<float>::iterator itr13;
    vector<float>::iterator itr14=jet_antikt4truth_phi->begin();
    vector<int>::iterator itr18;
    vector<float>::iterator itr16=jet_antikt4truth_pt->begin();
    vector<float>::iterator itr15;
    vector<float>::iterator PCx;
    vector<float>::iterator PCy;
    vector<float>::iterator PCz;
    vector<char>::iterator PClayer;
    vector<char>::iterator PCbec;
    vector<int>::iterator PCsize;
    Int_t Counti=0;
    /*  vector<vector<float> > *TheVectorOfVectorsx;
	vector<float> VectorOfFloatsx;
	vector<float> Trackx;
	TheVectorOfVectorsx = trk_Pixel_hit_x;
	if(TheVectorOfVectorsx->size() > 0){
	for(int j=0;j<TheVectorOfVectorsx->size();++j){
        VectorOfFloatsx = TheVectorOfVectorsx->at(j);
        if(VectorOfFloatsx.size() > 0){
	int end=VectorOfFloatsx.size();
	for(int i=0;i<end;++i){
	float aFloatx = VectorOfFloatsx.at(i);
	Trackx.push_back(aFloatx);
	}
	}
	}
	}
	vector<vector<float> > *TheVectorOfVectorsy;
	vector<float> VectorOfFloatsy;
	vector<float> Tracky;
	TheVectorOfVectorsy = trk_Pixel_hit_y;
	if(TheVectorOfVectorsy->size() > 0){
	for(int j=0;j<TheVectorOfVectorsy->size();++j){
        VectorOfFloatsy = TheVectorOfVectorsy->at(j);
        if(VectorOfFloatsy.size() > 0){
	int end=VectorOfFloatsy.size();
	for(int i=0;i<end;++i){
	float aFloaty = VectorOfFloatsy.at(i);
	Tracky.push_back(aFloaty);
	}
	}
	}
	}
	vector<vector<float> > *TheVectorOfVectorsz;
	vector<float> VectorOfFloatsz;
	vector<float> Trackz;
	TheVectorOfVectorsz = trk_Pixel_hit_z;
	if(TheVectorOfVectorsz->size() > 0){
	for(int j=0;j<TheVectorOfVectorsz->size();++j){
        VectorOfFloatsz = TheVectorOfVectorsz->at(j);
        if(VectorOfFloatsz.size() > 0){
	int end=VectorOfFloatsz.size();
	for(int i=0;i<end;++i){
	float aFloatz = VectorOfFloatsz.at(i);
	Trackz.push_back(aFloatz);
	}
	}
	}
	}
	vector<vector<float> > *TheVectorOfVectors;
	vector<float> VectorOfFloats;
	vector<float> incidenceTheta;
	TheVectorOfVectors = trk_Pixel_hit_incidenceTheta;
	if(TheVectorOfVectors->size() > 0){
	for(int j=0;j<TheVectorOfVectors->size();++j){
        VectorOfFloats = TheVectorOfVectors->at(j);
        if(VectorOfFloats.size() > 0){
	int end=VectorOfFloats.size();
	for(int i=0;i<end;++i){
	float aFloat = VectorOfFloats.at(i);
	incidenceTheta.push_back(aFloat);
	}
	}
	}
	}
    
	vector<vector<float> > *TheVectorOfVectors1;
	vector<float> VectorOfFloats1;
	vector<float> incidencePhi;
	TheVectorOfVectors1 = trk_Pixel_hit_incidencePhi;
	if(TheVectorOfVectors1->size() > 0){
	for(int j=0;j<TheVectorOfVectors1->size();++j){
        VectorOfFloats1 = TheVectorOfVectors1->at(j);
        if(VectorOfFloats1.size() > 0){
	int end=VectorOfFloats1.size();
	for(int i=0;i<end;++i){
	float aFloat1 = VectorOfFloats1.at(i);
	incidencePhi.push_back(aFloat1);
	}
	}
	}
	} 
    
	vector<vector<int> > *TheVectorOfVectors2;
	vector<int> VectorOfFloats2;
	vector<int> PixBec;
	TheVectorOfVectors2 = trk_Pixel_hit_bec;
	if(TheVectorOfVectors2->size() > 0){
	for(int j=0;j<TheVectorOfVectors2->size();++j){
        VectorOfFloats2 = TheVectorOfVectors2->at(j);
        if(VectorOfFloats2.size() > 0){
	int end=VectorOfFloats2.size();
	for(int i=0;i<end;++i){
	int aFloat2 = VectorOfFloats2.at(i);
	PixBec.push_back(aFloat2);
	}
	}
	}
	} 
    
	vector<vector<int> > *TheVectorOfVectors3;
	vector<int> VectorOfFloats3;
	vector<int> PixLayer;
	TheVectorOfVectors3 = trk_Pixel_hit_layer;
	if(TheVectorOfVectors3->size() > 0){
	for(int j=0;j<TheVectorOfVectors3->size();++j){
        VectorOfFloats3 = TheVectorOfVectors3->at(j);
        if(VectorOfFloats3.size() > 0){
	int end=VectorOfFloats3.size();
	for(int i=0;i<end;++i){
	int aFloat3 = VectorOfFloats3.at(i);
	PixLayer.push_back(aFloat3);
	//cout<<"aFloat3"<<aFloat3<<endl;
	//test->Fill(aFloat3);
	}
	}
	}
	}
	vector<vector<int> > *TheVectorOfVectors4;
	vector<int> VectorOfFloats4;
	vector<int> PixelSizeOntrack;
	TheVectorOfVectors4 = trk_Pixel_hit_size;
	if(TheVectorOfVectors4->size() > 0){
	for(int j=0;j<TheVectorOfVectors4->size();++j){
        VectorOfFloats4 = TheVectorOfVectors4->at(j);
        if(VectorOfFloats4.size() > 0){
	int end=VectorOfFloats4.size();
	for(int i=0;i<end;++i){
	int aInt = VectorOfFloats4.at(i);
	PixelSizeOntrack.push_back(aInt);
	}
	}
	}
	}*/
    for(itr13=jet_antikt4truth_eta->begin(); itr13 != jet_antikt4truth_eta->end(); ++itr13){
    
      if(*itr16>ptCutJets){
	if(fabs(*itr13)<L0Eta){
	  Number_Used_Jetsl0=Number_Used_Jetsl0+1;
	}
	if(fabs(*itr13)<L1Eta){
	  Number_Used_Jetsl1=Number_Used_Jetsl1+1;
	}
	if(fabs(*itr13)<L2Eta){
	  Number_Used_Jetsl2=Number_Used_Jetsl2+1;
	}
	if(fabs(*itr13)<L3Eta){
	  Number_Used_Jetsl3=Number_Used_Jetsl3+1;
	}
	//itr18=trk_Pixel_hit_size->begin();
	/*	for(int i1=0;i1<incidencePhi.size();++i1){
	//cout<<"Looking at clusters on track: "<<endl;
	if(PixBec[i1]==0){//fabs(incidenceTheta[i1])<TMath::Pi()/2.){
	float PCR=sqrt((Trackx[i1])**2+(Tracky[i1])**2);
	float PCPhi=atan(Tracky[i1]/(Trackx[i1]));
	float PCTheta=atan(PCR/(Trackz[i1]));
	float PCEta=(log(fabs(tan(PCTheta/2))))*(-1);
	float DeltaPhi=fabs(PCPhi-*itr14);
	if(DeltaPhi>TMath::Pi()){
	DeltaPhi=2*TMath::Pi()-DeltaPhi;
	}
	if(PixLayer[i1]==0 && fabs(*itr13)<L0Eta){
	PixLayer0->Fill(fabs(sqrt((PCEta-*itr13)**2+(DeltaPhi)**2)),PixelSizeOntrack[i1]);
	PixLayerEta0->Fill(fabs(PCEta-*itr13),PixelSizeOntrack[i1]);
	PixLayerPhi0->Fill(DeltaPhi,PixelSizeOntrack[i1]);
	}
	if(PixLayer[i1]==1 && fabs(*itr13)<L1Eta){
	PixLayer1->Fill(fabs(sqrt((PCEta-*itr13)**2+(DeltaPhi)**2)),PixelSizeOntrack[i1]);
	PixLayerEta1->Fill(fabs(PCEta-*itr13),PixelSizeOntrack[i1]);
	PixLayerPhi1->Fill(DeltaPhi,PixelSizeOntrack[i1]);
	}
	if(PixLayer[i1]==2 && fabs(*itr13)<L2Eta){
	//cout<<"CLuster on-track layer 2"<<endl;
	PixLayer2->Fill(fabs(sqrt((PCEta-*itr13)**2+(DeltaPhi)**2)),PixelSizeOntrack[i1]);
	PixLayerEta2->Fill(fabs(PCEta-*itr13),PixelSizeOntrack[i1]);
	PixLayerPhi2->Fill(DeltaPhi,PixelSizeOntrack[i1]);
	}
	if(PixLayer[i1]==3 && fabs(*itr13)<L3Eta){
	PixLayer3->Fill(fabs(sqrt((PCEta-*itr13)**2+(DeltaPhi)**2)),PixelSizeOntrack[i1]);
	PixLayerEta3->Fill(fabs(PCEta-*itr13),PixelSizeOntrack[i1]);
	PixLayerPhi3->Fill(DeltaPhi,PixelSizeOntrack[i1]);
	}
	// RadiusTrack->Fill(fabs(sqrt((incidenceEta-*itr13)**2+(incidencePhi[i1]-*itr14)**2)),PixelSizeOntrack[i1]);
	}
	}*/
	   
	PCbec=pixClus_bec1->begin();
	PCy=pixClus_y->begin();
	PCz=pixClus_z->begin();
	PClayer=pixClus_layer1->begin();
	PCsize=PixTrkClusGroupsize->begin();
	for(PCx=pixClus_x->begin(); PCx != pixClus_x->end(); ++PCx){
	  //cout<<"Looking at all clusters: "<<endl;
	  int layerPix = (int)(*PClayer);
	  int becPix = (int)(*PCbec);//static_cast<int>(*PCbec);
	  if(becPix==0){
	    float PCR=sqrt((*PCx)*(*PCx)+(*PCy)*(*PCy));
	    float PCPhi=atan(*PCy/(*PCx));
	    float PCTheta=atan(PCR/(*PCz));
	    float PCEta=(log(fabs(tan(PCTheta/2))))*(-1);
	    float PCDeltaPhi=fabs(PCPhi-*itr14);
	    if(PCDeltaPhi>TMath::Pi()){
	      PCDeltaPhi=2*TMath::Pi()-PCDeltaPhi;
	    }
	    if(layerPix==0 && fabs(*itr13)<L0Eta){
	      AllCluster0->Fill(fabs(sqrt((PCEta-(*itr13))*(PCEta-(*itr13))+(PCDeltaPhi)*(PCDeltaPhi))),(*PCsize));
	    }
	    if(layerPix==1 && fabs(*itr13)<L1Eta){
	      AllCluster1->Fill(fabs(sqrt((PCEta-*itr13)*(PCEta-*itr13)+(PCDeltaPhi)*(PCDeltaPhi))),*PCsize);
	    }
	    if(layerPix==2 && fabs(*itr13)<L2Eta){
	      AllCluster2->Fill(fabs(sqrt((PCEta-*itr13)*(PCEta-*itr13)+(PCDeltaPhi)*(PCDeltaPhi))),*PCsize);
	    }
	    if(layerPix==3 && fabs(*itr13)<L3Eta){
	      AllCluster3->Fill(fabs(sqrt((PCEta-*itr13)*(PCEta-*itr13)+(PCDeltaPhi)*(PCDeltaPhi))),*PCsize);
	    }

	    //AllCluster->Fill(fabs(sqrt((PCEta-*itr13)**2+(PCPhi-*itr14)**2)),*PCsize);
	    //AllCluster->Fill(*PCx,1);
		
	  }
	  ++PCy;
	  ++PCz;
	  ++PCbec;
	  ++PCsize;
	  ++PClayer;
	      
	}

      
	for(int il=0; il<31; ++il){ //Chiara: these are defined at the beginning, again number of modules and staves <-- correct???
	  //cout<<"Looking at number of sensors: "<<endl;
	  for(int il1=0; il1<16; ++il1){
	    if(il!=15 && fabs(*itr13)<L0Eta){ //15 == ((n mod-1)/2)+1
	      float Histx=histpxl0[il][il1]->GetMean();
	      float Histy=histpyl0[il][il1]->GetMean();
	      float Histz=histpzl0[il][il1]->GetMean();
	      float HistR=sqrt((Histx)*Histx+(Histy)*Histy);
	      float HistPhi=atan(Histy/(Histx));
	      float HistTheta=atan(HistR/(Histz));
	      float HistEta=(log(fabs(tan(HistTheta/2))))*(-1);
	      float HistDeltaPhi=fabs(HistPhi-*itr14);
	      if(HistDeltaPhi>TMath::Pi()){
		HistDeltaPhi=2*TMath::Pi()-HistDeltaPhi;
	      }
	      AmountSensorl0->Fill(fabs(sqrt((HistEta-*itr13)*(HistEta-*itr13)+(HistDeltaPhi)*(HistDeltaPhi))),26880.);//336*80
	    }
	  }
	}
	for(int il=0; il<53; ++il){
	  for(int il1=0; il1<32; ++il1){
	    if(il!=26 && fabs(*itr13)<L1Eta){
	      float Histx=histpxl1[il][il1]->GetMean();
	      float Histy=histpyl1[il][il1]->GetMean();
	      float Histz=histpzl1[il][il1]->GetMean();
	      float HistR=sqrt((Histx)*Histx+(Histy)*Histy);
	      float HistPhi=atan(Histy/(Histx));
	      float HistTheta=atan(HistR/(Histz));
	      float HistEta=(log(fabs(tan(HistTheta/2))))*(-1);
	      float HistDeltaPhi=fabs(HistPhi-*itr14);
	      if(HistDeltaPhi>TMath::Pi()){
		HistDeltaPhi=2*TMath::Pi()-HistDeltaPhi;
	      }
	      AmountSensorl1->Fill(fabs(sqrt((HistEta-*itr13)*(HistEta-*itr13)+(HistDeltaPhi)*HistDeltaPhi)),26880.);//336*80
	    }
	  }
	}
	for(int il=0; il<33; ++il){
	  for(int il1=0; il1<32; ++il1){
	    if(il!=16 && fabs(*itr13)<L2Eta){
	      float Histx=histpxl2[il][il1]->GetMean();
	      float Histy=histpyl2[il][il1]->GetMean();
	      float Histz=histpzl2[il][il1]->GetMean();
	      float HistR=sqrt((Histx)*Histx+(Histy)*Histy);
	      float HistPhi=atan(Histy/(Histx));
	      float HistTheta=atan(HistR/(Histz));
	      float HistEta=(log(fabs(tan(HistTheta/2))))*(-1);
	      float HistDeltaPhi=fabs(HistPhi-*itr14);
	      if(HistDeltaPhi>TMath::Pi()){
		HistDeltaPhi=2*TMath::Pi()-HistDeltaPhi;
	      }
	      AmountSensorl2->Fill(fabs(sqrt((HistEta-*itr13)*(HistEta-*itr13)+(HistDeltaPhi)*(HistDeltaPhi))),107520.);//336*80*2*2
	    }
	  }
	}
	for(int il=0; il<33; ++il){
	  for(int il1=0; il1<40; ++il1 && fabs(*itr13)<L0Eta){
	    if(il!=16){
	      float Histx=histpxl3[il][il1]->GetMean();
	      float Histy=histpyl3[il][il1]->GetMean();
	      float Histz=histpzl3[il][il1]->GetMean();
	      float HistR=sqrt((Histx)*Histx+(Histy)*Histy);
	      float HistPhi=atan(Histy/(Histx));
	      float HistTheta=atan(HistR/(Histz));
	      float HistEta=(log(fabs(tan(HistTheta/2))))*(-1);
	      float HistDeltaPhi=fabs(HistPhi-*itr14);
	      if(HistDeltaPhi>TMath::Pi()){
		HistDeltaPhi=2*TMath::Pi()-HistDeltaPhi;
	      }
	      AmountSensorl3->Fill(fabs(sqrt((HistEta-*itr13)*(HistEta-*itr13)+(HistDeltaPhi)*HistDeltaPhi)),107520.);//336*80*2*2
	    }
	  }
	}
	      
      }
      ++Counti;
      ++itr14;
      ++itr18;
      ++itr16;
    }
  }

  //===============================================================//
  // Chiara: SCT loop, will define first layer only - all clusters //
  //===============================================================//

  for(int iEvent=0; iEvent<NMC; iEvent++){

    Mychain->GetEvent(iEvent);

    if(iEvent%1000==0) cout << "--->SCT loop - event N: " << iEvent << "<---" << endl;

    //loop over jet eta
    for(int iJet=0; iJet<jet_antikt4truth_eta->size(); iJet++){

      //select only jets with Pt>50GeV
      ///   int const ptCutJetsJets = 500000;///MeV
      if((*jet_antikt4truth_pt)[iJet]>ptCutJets){

	//loop over x coord of the SCT cluster
	for(int iCl=0; iCl<sctClus_x->size(); iCl++){

	  ///cout << "DEBUG::looking into SCT clusters" << endl;
	
	  //looking only at SCT barrel
	  int becSct = (int)((*sctClus_bec)[iCl]);
	  if(becSct==0){

	    float SCT_R = sqrt((((*sctClus_x)[iCl])*((*sctClus_x)[iCl]))+(((*sctClus_y)[iCl])*((*sctClus_y)[iCl])));
	    float SCT_phi = atan(((*sctClus_y)[iCl])/((*sctClus_x)[iCl]));
	    float SCT_theta = atan(SCT_R/((*sctClus_z)[iCl]));
	    float SCT_eta = (-1)*log(fabs(tan(SCT_theta/2)));
	    float SCT_deltaPhi = fabs(SCT_phi-((*jet_antikt4truth_phi)[iJet]));
	    if(SCT_deltaPhi>TMath::Pi()){

	      SCT_deltaPhi = 2*TMath::Pi()-SCT_deltaPhi;
	    }

	    //consider only first SCT layer
	    int layerSct = (int)((*sctClus_layer)[iCl]);
	    if((layerSct==0)&&(fabs((*jet_antikt4truth_eta)[iJet])<eta_L0_SCT)){

	      AllCluster4->Fill(fabs(sqrt(((SCT_eta-(*jet_antikt4truth_eta)[iJet])*(SCT_eta-(*jet_antikt4truth_eta)[iJet]))+(SCT_deltaPhi*SCT_deltaPhi))), (*sctClus_size)[iCl]);
	    }
	  }
	}

	if(fabs((*jet_antikt4truth_eta)[iJet])<eta_L0_SCT) Number_Used_JetsSCTl0++;

	//filling sensors - looping on n modules and n staves
	for(int iMod=0; iMod<mod_L0_SCT; iMod++){

	  for(int iSt=0; iSt<stave_L0_SCT; iSt++){

	    int halfEta = ((mod_L0_SCT-1)/2)+1;
	    if((iMod!=halfEta)&&(fabs((*jet_antikt4truth_eta)[iJet])<eta_L0_SCT)){

	      float hist_x = histSCTl0_x[iMod][iSt]->GetMean();
	      float hist_y = histSCTl0_y[iMod][iSt]->GetMean();
	      float hist_z = histSCTl0_z[iMod][iSt]->GetMean();
	      float hist_R = sqrt((hist_x*hist_x)+(hist_y*hist_y));
	      float hist_phi = atan(hist_y/hist_x);
	      float hist_theta = atan(hist_R/hist_z);
	      float hist_eta = (-1)*log(fabs(tan(hist_theta/2)));
	      float hist_deltaPhi = fabs(hist_phi-(*jet_antikt4truth_phi)[iJet]);
	      if(hist_deltaPhi>TMath::Pi()){

		hist_deltaPhi = 2*TMath::Pi()-hist_deltaPhi;
	      }

	      double const nStrips = 1280*4;
	      AmountSensorSCTl0->Fill(fabs(sqrt(((hist_eta-(*jet_antikt4truth_eta)[iJet])*(hist_eta-(*jet_antikt4truth_eta)[iJet]))+(hist_deltaPhi*hist_deltaPhi))), nStrips);
	    }
	  }
	}
      }
    }
  }
  
  for(int i=1;i<=resolution;++i){
    /* float L0=PixLayer0->GetBinContent(i);
       float L1=PixLayer1->GetBinContent(i);
       float L2=PixLayer2->GetBinContent(i);
       float L3=PixLayer3->GetBinContent(i);*/
    float AllClusterContent0=AllCluster0->GetBinContent(i);
    float AllClusterContent1=AllCluster1->GetBinContent(i);
    float AllClusterContent2=AllCluster2->GetBinContent(i);
    float AllClusterContent3=AllCluster3->GetBinContent(i);

    //====================//
    // Chiara: adding SCT //
    //====================//

    float AllClusterContent4 = AllCluster4->GetBinContent(i);

    float i3=(float)i;
    float denominator=(i+0.5)*(xmax/(float)resolution);/////(i/100.)+0.005;///perche'??
    /* PixelLayer0->SetBinContent(i,L0/((i/100.)+0.005));
       PixelLayer1->SetBinContent(i,L1/((i/100.)+0.005));
       PixelLayer2->SetBinContent(i,L2/((i/100.)+0.005));
       PixelLayer3->SetBinContent(i,L3/((i/100.)+0.005));*/
    AllClusterDiv0->SetBinContent(i,AllClusterContent0/denominator);
    AllClusterDiv1->SetBinContent(i,AllClusterContent1/denominator);
    AllClusterDiv2->SetBinContent(i,AllClusterContent2/denominator);
    AllClusterDiv3->SetBinContent(i,AllClusterContent3/denominator);
    AllClusterDiv4->SetBinContent(i, AllClusterContent4/denominator);
    float LayoutNumberl0=(AmountSensorl0->GetBinContent(i))/Number_Used_Jetsl0;
    float LayoutNumberl1=(AmountSensorl1->GetBinContent(i))/Number_Used_Jetsl1;
    float LayoutNumberl2=(AmountSensorl2->GetBinContent(i))/Number_Used_Jetsl2;
    float LayoutNumberl3=(AmountSensorl3->GetBinContent(i))/Number_Used_Jetsl3;
    float layoutNumL0_SCT = (AmountSensorSCTl0->GetBinContent(i))/Number_Used_JetsSCTl0;
    //cout<<"LayoutNumberl0"<<LayoutNumberl0<<endl;
    //cout<<"LayoutNumberl1"<<LayoutNumberl1<<endl;
    //cout<<"LayoutNumberl2"<<LayoutNumberl2<<endl;
    //cout<<"LayoutNumberl3"<<LayoutNumberl3<<endl;
    float NUJ0=(float)Number_Used_Jetsl0;
    float NUJ1=(float)Number_Used_Jetsl1;
    float NUJ2=(float)Number_Used_Jetsl2;
    float NUJ3=(float)Number_Used_Jetsl3;
    /* OccupancyOntrack0->SetBinContent(i,0);
       OccupancyOntrack1->SetBinContent(i,0);
       OccupancyOntrack2->SetBinContent(i,0);
       OccupancyOntrack3->SetBinContent(i,0);*/
    OccupancyAllC0->SetBinContent(i,0);
    OccupancyAllC1->SetBinContent(i,0);
    OccupancyAllC2->SetBinContent(i,0);
    OccupancyAllC3->SetBinContent(i,0);
    OccupancyAllC4->SetBinContent(i,0);
    if(LayoutNumberl0!=0){
      ///  OccupancyOntrack0->SetBinContent(i,(L0/NUJ0)/LayoutNumberl0);
      OccupancyAllC0->SetBinContent(i,(AllClusterContent0/NUJ0)/LayoutNumberl0);
    }
    if(LayoutNumberl1!=0){
      ///   OccupancyOntrack1->SetBinContent(i,(L1/NUJ1)/LayoutNumberl1);
      OccupancyAllC1->SetBinContent(i,(AllClusterContent1/NUJ1)/LayoutNumberl1);
    }
    if(LayoutNumberl2!=0){
      ///  OccupancyOntrack2->SetBinContent(i,(L2/NUJ2)/LayoutNumberl2);
      OccupancyAllC2->SetBinContent(i,(AllClusterContent2/NUJ2)/LayoutNumberl2);
    }
    if(LayoutNumberl3!=0){
      ///  OccupancyOntrack3->SetBinContent(i,(L3/NUJ3)/LayoutNumberl3);
      OccupancyAllC3->SetBinContent(i,(AllClusterContent3/NUJ3)/LayoutNumberl3);
    }
    if(layoutNumL0_SCT!=0){

      OccupancyAllC4->SetBinContent(i, (AllClusterContent4/((float)Number_Used_JetsSCTl0))/layoutNumL0_SCT);
    }
  }

  cout << "Num used jets Pix layer 0: " << Number_Used_Jetsl0 << endl;
  cout << "Num used jets Pix layer 1: " << Number_Used_Jetsl1 << endl;
  cout << "Num used jets Pix layer 2: " << Number_Used_Jetsl2 << endl;
  cout << "Num used jets Pix layer 3: " << Number_Used_Jetsl3 << endl;
  cout << "Num used jets SCT layer 0: " << Number_Used_JetsSCTl0 << endl;


  //c2->Divide(3,3);

  cout << "DEBUG:: I haven't set the canvas yet!" << endl;

  /* TCanvas *c2 = new TCanvas("c2","square",200,10,1200,1200);
     c2->RangeAxis(0.,0.00005,35.,0.1);
     c2->SetFillColor(10);

     cout << "DEBUG:: not divided canvas yet.." << endl;

     c2->Divide(2,2);

     cout << "DEBUG:: divided canvas" << endl;

     gStyle->SetOptStat(kFALSE);*/

  //PixDeltaRow1->SetTitle("Jets in #phi vs. #eta");
  /*RadiusTrack->SetTitle("Number of pixels hit vs R");
    RadiusTrack->GetXaxis()->SetTitle("R");
    RadiusTrack->GetYaxis()->SetTitle("Number of pixels hit");
    RadiusTrack->GetYaxis()->SetTitleOffset(1.4);
    RadiusTrack->Draw("");
    gStyle->SetOptStat(0);
    c2->Update();
    c2->cd(2);
    RadiusTrack1->SetTitle("(Number of pixels hit per jet)/R vs R");
    RadiusTrack1->GetXaxis()->SetTitle("R");
    RadiusTrack1->GetYaxis()->SetTitle("Number of pixels hit per jet divided by R");
    RadiusTrack1->GetYaxis()->SetTitleOffset(1.4);
    RadiusTrack1->Scale(1./Number_Used_Jets);
    RadiusTrack1->Draw("");
    gStyle->SetOptStat(0);
    //PixDeltaRow1->Draw("AP");*/


  /// c2->cd(1);
  /* PixLayer0->SetLineColor(2);
     PixLayer1->SetLineColor(3);
     PixLayer2->SetLineColor(4);
     PixLayer3->SetLineColor(6);
     PixLayer0->SetTitle("For clusters on-track");
     PixLayer0->GetXaxis()->SetTitle("R");
     PixLayer0->GetYaxis()->SetTitle("Number of pixels hit");
     PixLayer0->GetYaxis()->SetTitleOffset(1.4);
     PixLayer0->Draw("");
     PixLayer1->Draw("same");
     PixLayer2->Draw("same");
     PixLayer3->Draw("same");
     leg = new TLegend(0.1,0.7,0.38,0.9);
     leg->SetHeader("Pixel Layers");
     leg->AddEntry(PixLayer0,"Layer 0","l");
     leg->AddEntry(PixLayer1,"Layer 1","l");
     leg->AddEntry(PixLayer2,"Layer 2","l");
     leg->AddEntry(PixLayer3,"Layer 3","l");
     leg->SetFillColor(10);
     gStyle->SetOptStat(0);
     leg->Draw("same");
     c2->Update();

     c2->cd(2);
     PixelLayer0->SetLineColor(2);
     PixelLayer1->SetLineColor(3);
     PixelLayer2->SetLineColor(4);
     PixelLayer3->SetLineColor(6);
     PixelLayer2->SetTitle("For clusters on-track");
     PixelLayer2->GetXaxis()->SetTitle("R");
     PixelLayer2->GetYaxis()->SetTitle("Number of pixels hit per jet divided by R");
     PixelLayer2->GetYaxis()->SetTitleOffset(1.4);
     PixelLayer0->Scale(1./Number_Used_Jetsl0);
     PixelLayer1->Scale(1./Number_Used_Jetsl1);
     PixelLayer2->Scale(1./Number_Used_Jetsl2);
     PixelLayer3->Scale(1./Number_Used_Jetsl3);
     PixelLayer2->Draw("");
     PixelLayer1->Draw("same");
     PixelLayer3->Draw("same");
     PixelLayer0->Draw("same");
     gStyle->SetOptStat(0);
     leg1 = new TLegend(0.6,0.7,0.9,0.9);
     leg1->SetHeader("Pixel Layers");
     leg1->AddEntry(PixelLayer0,"Layer 0","l");
     leg1->AddEntry(PixelLayer1,"Layer 1","l");
     leg1->AddEntry(PixelLayer2,"Layer 2","l");
     leg1->AddEntry(PixelLayer3,"Layer 3","l");
     leg1->SetFillColor(10);
     leg1->Draw("same");
     c2->Update();

     c2->cd(3);*/
  /* AllCluster0->SetLineColor(2);
     AllCluster1->SetLineColor(3);
     AllCluster2->SetLineColor(4);
     AllCluster3->SetLineColor(6);
     AllCluster4->SetLineColor(kOrange+1);
     AllCluster0->SetTitle("All Clusters");
     AllCluster0->GetXaxis()->SetTitle("#Delta#eta");
     AllCluster0->GetYaxis()->SetTitle("Number of pixels/strips hit per jet");*/
  AllCluster0->Scale(1./Number_Used_Jetsl0);
  //// AllCluster0->Draw("");  
  AllCluster1->Scale(1./Number_Used_Jetsl1);
  /// AllCluster1->Draw("same");
  AllCluster2->Scale(1./Number_Used_Jetsl2);
  /// AllCluster2->Draw("same");
  AllCluster4->Scale(1./Number_Used_JetsSCTl0);
  //// AllCluster4->Draw("same");
  AllCluster3->Scale(1./Number_Used_Jetsl3);
  /* AllCluster3->Draw("same");
     TLegend *legSCT = new TLegend(0.2, 0.55, 0.5, 0.85); 
     legSCT->SetHeader("Pixel layers+1st SCT layer");
     legSCT->AddEntry(AllCluster0, "Pixel layer 0");
     legSCT->AddEntry(AllCluster1, "Pixel layer 1");
     legSCT->AddEntry(AllCluster2, "Pixel layer 2");
     legSCT->AddEntry(AllCluster3, "Pixel layer 3");
     legSCT->AddEntry(AllCluster4, "SCT layer 0");
     legSCT->SetFillColor(10);
     legSCT->Draw("same");
     c2->Update();

     c2->cd(2); ////(4);
     AllClusterDiv0->SetLineColor(2);
     AllClusterDiv1->SetLineColor(3);
     AllClusterDiv2->SetLineColor(4);
     AllClusterDiv3->SetLineColor(6);
     AllClusterDiv4->SetLineColor(kOrange+1);
     AllClusterDiv3->SetTitle("All Clusters");
     AllClusterDiv3->GetXaxis()->SetTitle("#Delta#eta");
     AllClusterDiv3->GetYaxis()->SetTitle("Number of pixels/strips hit per jet divided by #Delta#eta");*/
  AllClusterDiv0->Scale(1./Number_Used_Jetsl0);
  AllClusterDiv1->Scale(1./Number_Used_Jetsl1);
  AllClusterDiv2->Scale(1./Number_Used_Jetsl2);
  AllClusterDiv3->Scale(1./Number_Used_Jetsl3);
  AllClusterDiv4->Scale(1./Number_Used_JetsSCTl0);
  /*AllClusterDiv3->Draw("");
    AllClusterDiv1->Draw("same");
    AllClusterDiv0->Draw("same");
    AllClusterDiv2->Draw("same");
    AllClusterDiv4->Draw("same");
    TLegend *legSCT_b = new TLegend(0.6, 0.6, 0.9, 0.9); 
    legSCT_b->SetHeader("Pixel layers+1st SCT layer");
    legSCT_b->AddEntry(AllClusterDiv0, "Pixel layer 0");
    legSCT_b->AddEntry(AllClusterDiv1, "Pixel layer 1");
    legSCT_b->AddEntry(AllClusterDiv2, "Pixel layer 2");
    legSCT_b->AddEntry(AllClusterDiv3, "Pixel layer 3");
    legSCT_b->AddEntry(AllClusterDiv4, "SCT layer 0");
    legSCT_b->SetFillColor(10);
    legSCT_b->Draw("same");
    c2->Update();

    c2->cd(3); ///(5);
    /// c2->SetLogy();
    /// c2_3->SetLogy();
    AmountSensorl0->SetLineColor(2);
    AmountSensorl1->SetLineColor(3);
    AmountSensorl2->SetLineColor(4);
    AmountSensorl3->SetLineColor(6);
    AmountSensorSCTl0->SetLineColor(kOrange+1);
    AmountSensorl3->SetTitle("Number of pixels/strips per module per jet");
    AmountSensorl3->GetXaxis()->SetTitle("#Delta#eta");
    AmountSensorl3->GetYaxis()->SetTitle("Number of pixels/strips per module per jet");*/
  AmountSensorl3->Scale(1./Number_Used_Jetsl3);
  /// AmountSensorl3->Draw("");
  AmountSensorl1->Scale(1./Number_Used_Jetsl1);
  /// AmountSensorl1->Draw("same");
  AmountSensorl2->Scale(1./Number_Used_Jetsl2);
  /// AmountSensorl2->Draw("same");
  AmountSensorl0->Scale(1./Number_Used_Jetsl0);
  /// AmountSensorl0->Draw("same");
  AmountSensorSCTl0->Scale(1./Number_Used_JetsSCTl0);
  /*AmountSensorSCTl0->Draw("same");
    TLegend *legSCT_c = new TLegend(0.1, 0.6, 0.4, 0.9); 
    legSCT_c->SetHeader("Pixel layers+1st SCT layer");
    legSCT_c->AddEntry(AmountSensorl0, "Pixel layer 0");
    legSCT_c->AddEntry(AmountSensorl1, "Pixel layer 1");
    legSCT_c->AddEntry(AmountSensorl2, "Pixel layer 2");
    legSCT_c->AddEntry(AmountSensorl3, "Pixel layer 3");
    legSCT_c->AddEntry(AmountSensorSCTl0, "SCT layer 0");
    legSCT_c->SetFillColor(10);
    legSCT_c->Draw("same");  
    /// c2->SetLogy();
    c2->Update();*/

  /*c2->cd(6);
    PixLayerEta0->SetLineColor(2);
    PixLayerEta1->SetLineColor(3);
    PixLayerEta2->SetLineColor(4);
    PixLayerEta3->SetLineColor(6);
    PixLayerEta0->SetTitle("Clusters on-track");
    PixLayerEta0->GetXaxis()->SetTitle("Delta eta");
    PixLayerEta0->GetYaxis()->SetTitle("Number of pixels hit per jet");
    PixLayerEta0->Scale(1./Number_Used_Jetsl0);
    PixLayerEta0->Draw("");
    PixLayerEta1->Scale(1./Number_Used_Jetsl1);
    PixLayerEta1->Draw("same");
    PixLayerEta2->Scale(1./Number_Used_Jetsl2);
    PixLayerEta2->Draw("same");
    PixLayerEta3->Scale(1./Number_Used_Jetsl3);
    PixLayerEta3->Draw("same");
    leg1->Draw("same");
    c2->Update();
    c2->cd(7);
    c2->RangeAxis(0.,10.,1.8.,10000000.);
    PixLayerPhi0->SetLineColor(2);
    PixLayerPhi1->SetLineColor(3);
    PixLayerPhi2->SetLineColor(4);
    PixLayerPhi3->SetLineColor(6);
    PixLayerPhi0->SetTitle("Clusters on-track");
    PixLayerPhi0->GetXaxis()->SetTitle("Delta phi");
    PixLayerPhi0->GetYaxis()->SetTitle("Number of pixels hit per jet");
    PixLayerPhi0->Scale(1./Number_Used_Jetsl0);
    //PixLayerPhi0->SetMinimum(17.);
    //PixLayerPhi0->SetMaximum(41.);
    PixLayerPhi0->Draw("");
    PixLayerPhi1->Scale(1./Number_Used_Jetsl1);
    PixLayerPhi1->Draw("same");
    PixLayerPhi2->Scale(1./Number_Used_Jetsl2);
    PixLayerPhi2->Draw("same");
    PixLayerPhi3->Scale(1./Number_Used_Jetsl3);
    PixLayerPhi3->Draw("same");
    leg1->Draw("same");
    c2->Update();
    c2->cd(8);
    c2_8->SetLogy();
    OccupancyOntrack0->SetLineColor(2);
    OccupancyOntrack1->SetLineColor(3);
    OccupancyOntrack2->SetLineColor(4);
    OccupancyOntrack3->SetLineColor(6);
    OccupancyOntrack0->SetTitle("Clusters on-track");
    OccupancyOntrack0->GetXaxis()->SetTitle("R");
    OccupancyOntrack0->GetYaxis()->SetTitle("Occupancy");
    OccupancyOntrack0->SetMinimum(0.000008);
    OccupancyOntrack0->SetMaximum(0.001);
    OccupancyOntrack0->Draw("");
    OccupancyOntrack1->Draw("same");
    OccupancyOntrack2->Draw("same");
    OccupancyOntrack3->Draw("same");
    leg1->Draw("same");
    c2->SetLogy();
    c2->Update();

    c2->cd(9);*/
  /*  cout << "DEBUG:: 1" << endl;
      c2->cd(4);
      /// c2->SetLogy();
      ///c2_9->SetLogy();
      ////c2_4->SetLogy();
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC0->SetLineColor(2);
      OccupancyAllC1->SetLineColor(3);
      OccupancyAllC2->SetLineColor(4);
      OccupancyAllC3->SetLineColor(6);
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC4->SetLineColor(kOrange+1);
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC4->SetTitle("All Clusters");
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC4->GetXaxis()->SetTitle("#Delta#eta");
      OccupancyAllC4->GetYaxis()->SetTitle("Occupancy");
      /// OccupancyAllC4->SetMinimum(0);
      /// OccupancyAllC0->SetMaximum(0.012);
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC4->GetYaxis()->SetRange(0.,0.05);
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC4->Draw("");
      cout << "DEBUG:: 1" << endl;
      OccupancyAllC1->Draw("same");
      OccupancyAllC0->Draw("same");
      OccupancyAllC2->Draw("same");
      OccupancyAllC3->Draw("same");
      cout << "DEBUG:: 1" << endl;
      legSCT_b->Draw("same");
      /// c2->SetLogy();
      cout << "DEBUG:: 1" << endl;
      c2->Update();
      cout << "DEBUG:: 1" << endl;*/

  TFile file("20120324/plotsOccupancyJ6.root", "recreate");
  AllCluster0->Write();
  AllCluster1->Write();
  AllCluster2->Write();
  AllCluster3->Write();
  AllCluster4->Write();
  AllClusterDiv0->Write();
  AllClusterDiv1->Write();
  AllClusterDiv2->Write();
  AllClusterDiv3->Write();
  AllClusterDiv4->Write();
  AmountSensorl0->Write();
  AmountSensorl1->Write();
  AmountSensorl2->Write();
  AmountSensorl3->Write();
  AmountSensorSCTl0->Write();
  OccupancyAllC0->Write();
  OccupancyAllC1->Write();
  OccupancyAllC2->Write();
  OccupancyAllC3->Write();
  OccupancyAllC4->Write();
  file.Close();

  return;
}
