# Cut definitions
from InDetUpgradePerformanceAnalysis  import AllCuts, MinCut, RangeCut, MaxCut, AbsSelectorWithIndex, RangeCutWithIndex, AbsSelector

# Define the basic track cuts. These are extended by TrackResolutionCuts
class BasicTrackCuts(AllCuts):
    def __init__(self):
        AllCuts.__init__(self)
        self.Add(MinCut("trk_pt",1000))  # MeV
        self.Add(RangeCut("trk_d0_wrtPV",-1.0,1.0)) # mm
        self.Add(RangeCut("trk_z0_wrtPV",-150.0,150.0)) # mm
        #self.Add(MinimumHitsCut(11,"trk_nPixHits","trk_nSCTHits"))
        self.Add(MinCut("trk_nAllHits",11))
        self.Add(MaxCut("trk_nPixHoles",2))

# Cuts applied to tracks entering the resolution plots
class TrackResolutionCuts(AllCuts):
    def __init__(self, pdg, pt=None, prob=0.9, trackPtCut=True, minPt=None):
        AllCuts.__init__(self)
        # Apply all the basic track cuts 
        self.Add(BasicTrackCuts())
        # Cut on track pT
        if pt and trackPtCut: self.Add(RangeCut("trk_pt",pt-(pt*0.2),pt+(pt*0.2)))
        # Must be matched to an MC truth particle 
        self.Add(MinCut("trk_mc_index",0))
        # Match MC truth particle must be of type "pdg"
        self.Add(AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdg))
        # Cut on matched MC truth particle pT
        if pt: self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",pt-(pt*0.01),pt+(pt*0.01)))
        if minPt: self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",minPt, 99999999))
        # Cut on track match probability
        self.Add(MinCut("trk_mc_probability", prob))

# Define the basic track cuts for hits plots
class TrackCutsForHitsPlots(AllCuts):
    def __init__(self,  pdg, pt=None, prob=0.9, minPt=None):
        AllCuts.__init__(self)
        self.Add(MinCut("trk_pt",1000))  # MeV
        # Cut on track pT
        if pt: self.Add(RangeCut("trk_pt",pt-(pt*0.2),pt+(pt*0.2)))
        self.Add(RangeCut("trk_d0_wrtPV",-1.0,1.0)) # mm
        self.Add(RangeCut("trk_z0_wrtPV",-150.0,150.0)) # mm
        # Must be matched to an MC truth particle 
        self.Add(MinCut("trk_mc_index",0))
        # Match MC truth particle must be of type "pdg"
        self.Add(AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdg))
        # Cut on matched MC truth particle pT
        if pt: self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",pt-(pt*0.01),pt+(pt*0.01)))
        if minPt: self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",minPt, 99999999))
        # Cut on track match probability
        self.Add(MinCut("trk_mc_probability", prob))

# Cuts applied to truth particles considered for tracking efficiency plots
class EfficiencyTruthParticleCuts(AllCuts):
    def __init__(self, pdg, pt=None, extended_eta=False, minPt=None):
        AllCuts.__init__(self)
        self.Add(AbsSelector("mc_gen_type",pdg))
        self.Add(RangeCut("mc_gen_barcode",1,200000))
        if pt: self.Add(RangeCut("mc_gen_pt", pt-(pt*0.01), pt+(pt*0.01)))
        if minPt: self.Add(MinCut("mc_gen_pt", minPt))
        if extended_eta:
            self.Add(RangeCut("mc_gen_eta",-4.0,4.0))
        else:
            self.Add(RangeCut("mc_gen_eta",-2.7,2.7))
        self.Add(RangeCut("mc_perigee_d0",-1.0,1.0)) # mm
        self.Add(RangeCut("mc_perigee_z0",-150.0,150.0)) # mm
        

# Cuts applied to tracks considered in the mis-reconstructed track fraction plots ("FakeRate")
class FakeRakeTrackSelection(AllCuts):
    def __init__(self, pdg, pt=None):
        AllCuts.__init__(self)
        # Apply all the basic track cuts 
        self.Add(BasicTrackCuts())

