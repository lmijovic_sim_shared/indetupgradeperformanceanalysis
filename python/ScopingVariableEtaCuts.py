eta_Bronze = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Bronze_1holes_fr0p05 = [8, 8, 10, 11, 7]
eta_cuts_Bronze_1holes_fr0p02 = [8, 9, 11, 12, 8]
eta_cuts_Bronze_1holes_fr0p01 = [8, 9, 11, 12, 8]

eta_Bronze_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Bronze_m10_1holes_fr0p05 = [8, 8, 10, 10, 8]
eta_cuts_Bronze_m10_1holes_fr0p02 = [8, 9, 11, 11, 8]
eta_cuts_Bronze_m10_1holes_fr0p01 = [8, 9, 11, 12, 9]

eta_Silver = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Silver_1holes_fr0p05 = [11, 10, 11, 11, 7]
eta_cuts_Silver_1holes_fr0p02 = [11, 10, 11, 11, 8]
eta_cuts_Silver_1holes_fr0p01 = [11, 10, 11, 11, 8]

eta_VF_Silver = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Silver_1holes_fr0p01 = [11, 10, 11, 11, 8, 10, 9]

eta_Silver_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Silver_m10_1holes_fr0p05 = [8, 8, 10, 10, 8]
eta_cuts_Silver_m10_1holes_fr0p02 = [8, 9, 11, 11, 8]
eta_cuts_Silver_m10_1holes_fr0p01 = [9, 9, 11, 12, 9]

eta_VF_Silver_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Silver_m10_1holes_fr0p01 = [9, 9, 11, 12, 9, 8, 6]

eta_VF_Gold = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Gold_1holes_fr0p05 = [11, 11, 11, 13, 10, 10, 9]
eta_cuts_VF_Gold_1holes_fr0p02 = [11, 11, 11, 13, 10, 10, 9]
eta_cuts_VF_Gold_1holes_fr0p01 = [11, 11, 11, 13, 10, 10, 9]

eta_VF_Gold_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Gold_m10_1holes_fr0p05 = [8, 9, 9, 11, 8, 8, 6]
eta_cuts_VF_Gold_m10_1holes_fr0p02 = [8, 9, 10, 11, 8, 8, 6]
eta_cuts_VF_Gold_m10_1holes_fr0p01 = [9, 9, 11, 11, 8, 8, 6]

eta_Bronze = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Bronze_0holes_fr0p05 = [8, 8, 9, 11, 6]
eta_cuts_Bronze_0holes_fr0p02 = [8, 8, 10, 11, 7]
eta_cuts_Bronze_0holes_fr0p01 = [8, 8, 11, 12, 7]

eta_Bronze_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Bronze_m10_0holes_fr0p05 = [7, 8, 10, 9, 5]
eta_cuts_Bronze_m10_0holes_fr0p02 = [8, 8, 10, 10, 7]
eta_cuts_Bronze_m10_0holes_fr0p01 = [8, 9, 11, 11, 7]

eta_Silver = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Silver_0holes_fr0p05 = [11, 10, 11, 11, 6]
eta_cuts_Silver_0holes_fr0p02 = [11, 10, 11, 11, 7]
eta_cuts_Silver_0holes_fr0p01 = [11, 10, 11, 12, 7]

eta_Silver_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 4.0] 
eta_cuts_Silver_m10_0holes_fr0p05 = [8, 8, 10, 9, 5]
eta_cuts_Silver_m10_0holes_fr0p02 = [9, 9, 10, 10, 6]
eta_cuts_Silver_m10_0holes_fr0p01 = [9, 9, 11, 11, 7]

eta_VF_Gold = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Gold_0holes_fr0p05 = [11, 11, 11, 13, 10, 10, 9]
eta_cuts_VF_Gold_0holes_fr0p02 = [11, 11, 11, 13, 10, 10, 9]
eta_cuts_VF_Gold_0holes_fr0p01 = [11, 11, 11, 13, 10, 10, 9]

eta_VF_Gold_m10 = [0.0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] 
eta_cuts_VF_Gold_m10_0holes_fr0p05 = [8, 9, 9, 11, 8, 8, 6]
eta_cuts_VF_Gold_m10_0holes_fr0p02 = [9, 9, 10, 11, 8, 8, 6]
eta_cuts_VF_Gold_m10_0holes_fr0p01 = [9, 9, 10, 11, 8, 8, 6]
