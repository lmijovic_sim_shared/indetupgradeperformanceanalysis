import InDetUpgradePerformanceAnalysis.CustomBinning as CustomBinning
from InDetUpgradePerformanceAnalysis.StandardCuts import TrackCutsForHitsPlots
from InDetUpgradePerformanceAnalysis.StandardObservableDefinitions import DiffObservableFitted, DiffObservable, PullObservable, QOverPtObs
from InDetUpgradePerformanceAnalysis import SingleAnalysis, TrackRatio, ObservableGroup, AllCuts, AbsSelector, RangeCut, MinCut, MaxCut, Efficiency, FakeRate, SingleObservable, AbsSelectorWithIndex, RangeCutWithIndex, Distribution, RangeDependentMinCut, vector_float, ScopingVariableEtaCuts

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#               Helper Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Helper function to get correct binning range
def get_binning(mc=False):
    if "VF_Gold" in options.layout:
        if mc: return CustomBinning.SingleSidedEtaBinningMCNew_Gold()
        else: return CustomBinning.SingleSidedEtaBinningNew_Gold()
    elif "VF_Silver"==options.layout:
        if mc: return CustomBinning.SingleSidedEtaBinningMCNew_Silver()
        else: return CustomBinning.SingleSidedEtaBinningNew_Silver()
    else:
        if mc: return CustomBinning.SingleSidedEtaBinningMCNew_Bronze()
        else: return CustomBinning.SingleSidedEtaBinningNew_Bronze()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#               Cuts definitions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Cuts applied to truth particles considered for tracking efficiency plots
class EfficiencyTruthParticleCuts(AllCuts):
    def __init__(self, pdg, pt=None, maxPt=None, minPt=None):
        AllCuts.__init__(self)
        self.Add(AbsSelector("mc_gen_type",pdg))
        self.Add(RangeCut("mc_gen_barcode",1,200000))
        if pt: self.Add(RangeCut("mc_gen_pt", pt-(pt*0.01), pt+(pt*0.01)))
        if minPt: self.Add(MinCut("mc_gen_pt", minPt))
        if maxPt: self.Add(MaxCut("mc_gen_pt", maxPt))
        if "VF_Silver" == options.layout:
            self.Add(RangeCut("mc_gen_eta",-3.2,3.2))
        elif "VF_Gold" in options.layout:
            self.Add(RangeCut("mc_gen_eta",-4.0,4.0))
        else:
            self.Add(RangeCut("mc_gen_eta",-2.7,2.7))
        self.Add(RangeCut("mc_perigee_d0",-1.0,1.0)) # mm
        self.Add(RangeCut("mc_perigee_z0",-150.0,150.0)) # mm

# Cuts applied to truth particles considered for inclusive fake rate plots
# Select all primary charged particles within acceptance
class InclusiveFakeTruthParticleCuts(AllCuts):
    def __init__(self):
        AllCuts.__init__(self)
        self.Add(AbsSelector("mc_charge",1))
        self.Add(RangeCut("mc_gen_barcode",1,200000))
        self.Add(MinCut("mc_gen_pt", 1000))
        if "VF_Silver" == options.layout:
            self.Add(RangeCut("mc_gen_eta",-3.2,3.2))
        elif "VF_Gold" in options.layout:
            self.Add(RangeCut("mc_gen_eta",-4.0,4.0))
        else:
            self.Add(RangeCut("mc_gen_eta",-2.7,2.7))
        self.Add(RangeCut("mc_perigee_d0",-1.0,1.0)) # mm
        self.Add(RangeCut("mc_perigee_z0",-150.0,150.0)) # mm

# Cuts applied to tracks entering the resolution plots
class TrackResolutionCuts(AllCuts):
    def __init__(self,  prob, track_cuts, truthMinPt=None, truthMaxPt=None, truthMaxEta=None, pdgId=None):
        AllCuts.__init__(self)
        # Apply all the basic track cuts 
        self.Add(track_cuts)
        # Must be matched to an MC truth particle 
        self.Add(MinCut("trk_mc_index",0))
        # Cut on track match probability
        self.Add(MinCut("trk_mc_probability", prob))
        # Add pdgCut if needed
        if pdgId: self.Add(AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdgId))
        # Cut on matched MC truth particle pT
        if truthMinPt and not truthMaxPt: self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",truthMinPt, 99999999))
        if truthMaxEta: self.Add(RangeCutWithIndex("mc_gen_eta","trk_mc_index", -1*truthMaxEta, truthMaxEta))

# Hits cuts as a function of eta
class VariableEtaTrackCuts(AllCuts):
    def __init__(self, maxHoles=1, trk_eta_cuts=True, noIP=False):
        AllCuts.__init__(self)
        if trk_eta_cuts:
            if "VF_Silver" == options.layout:
                self.Add(RangeCut("trk_eta",-3.2,3.2))
            elif "VF_Gold" in options.layout:
                self.Add(RangeCut("trk_eta",-4.0,4.0))
            else:
                self.Add(RangeCut("trk_eta",-2.7,2.7))
        self.Add(MinCut("trk_pt", 1000))  # MeV
        if not noIP: self.Add(RangeCut("trk_d0_wrtPV",-1.0,1.0)) # mm
        if not noIP: self.Add(RangeCut("trk_z0_wrtPV",-150.0,150.0)) # mm
        self.Add(MaxCut("trk_nPixHoles", maxHoles+1))
        eta_bins, cuts = self.get_cuts(maxHoles)
        hits_cut = RangeDependentMinCut(float, float)(
            "trk_nAllHits", "trk_eta", 
            vector_float(eta_bins), vector_float(cuts)
        )
        print cuts
        self.Add(hits_cut)

    def get_cuts(self, maxHoles, thresh="0p01"):
        layout = options.layout
        if layout=="Gold": layout="VF_Gold"
        if layout=="Gold_m10": layout="VF_Gold_m10"
        eta = getattr(ScopingVariableEtaCuts, "eta_"+layout)
        cuts_name = "eta_cuts_{0}_{1}holes_fr{2}".format(layout, maxHoles, thresh) 
        print "Using cut set ",cuts_name
        cuts = getattr(ScopingVariableEtaCuts, cuts_name)
        print "HitCuts ", eta
        print "HitCuts ", cuts
        return (eta, cuts)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#           Observable Definitions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Definition of a set of fake rate plots
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class FakeRateObservables(ObservableGroup):
    def __init__(self, path, title, track_cuts, add_prob=False):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(track_cuts)
        binnings = [get_binning(mc=False), CustomBinning.PtBinning(), CustomBinning.GenVtxBinning(), CustomBinning.DoubleSidedEtaBinningNew_Gold(), CustomBinning.VariablePtBinning(), CustomBinning.VariablePtBinningPion(), CustomBinning.SingleSidedVariableEtaBinning()]
        for b in binnings:
            fr = FakeRate(b)
            self.Add(fr)
            if add_prob: self.Add(SingleObservable("trk_mc_probability", b))
        if add_prob: self.Add(Distribution("trk_mc_probability", 101, 0, 1.01))

# Matching probability plots
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class MatchProbObservables(ObservableGroup):
    def __init__(self, path, title, track_cuts):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(track_cuts)
        binnings = [get_binning(mc=False), CustomBinning.VariablePtBinning(), CustomBinning.SingleSidedVariableEtaBinning()]
        for b in binnings:
            self.Add(SingleObservable("trk_mc_probability", b))
        self.Add(Distribution("trk_mc_probability", 101, 0, 1.01))

# "Inclusive" fake rate observables (nTracks vs nVtx etc)
class InclusiveFakeRateObservables(TrackRatio):
    def __init__(self, path, trk_cuts):
        TrackRatio.__init__(self, InclusiveFakeTruthParticleCuts(), trk_cuts)
        self.SetPath(path)

# Efficiency Observables
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class EfficiencyObservables(ObservableGroup):
    def __init__(self, pdg, pt, path, title, trk_cuts=None, maxPt=None, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCuts(pdg,pt, maxPt=maxPt, minPt=minPt))
        binnings = [get_binning(mc=True), CustomBinning.GenVtxBinning(), CustomBinning.SingleSidedVariableEtaBinningMC() ]
        #CustomBinning.DoubleSidedEtaBinningMCNew_Gold(), 
        for b in binnings:
            eff=Efficiency( b )
            #eff=Efficiency( binning )
            # Only count tracks passing quality cuts: d0/z0/pt/nHits   
            if trk_cuts: eff.SetCuts(trk_cuts)
            self.Add(eff)

# Efficiency measurements as a function of pT
# Needs to be separate from above so can specify pT cuts separately
class EfficiencyObservablesPt(ObservableGroup):
    def __init__(self, pdg, pt, path, title, trk_cuts, binnings, maxPt=None, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCuts(pdg,pt, maxPt=maxPt, minPt=minPt))
        for b in binnings:
            eff=Efficiency( b )
            #eff=Efficiency( binning )
            # Only count tracks passing quality cuts: d0/z0/pt/nHits   
            if trk_cuts: eff.SetCuts(trk_cuts)
            self.Add(eff)

# Hit "resolution" obeservables - used for calculating the means and widths of the nHits distributions in bins
# Needs to be sepearate as dont apply track hits cuts
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class HitsResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title,charge=None, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        binning = CustomBinning.SingleSidedEtaBinningNew_Gold()
        self.SetCuts(TrackCutsForHitsPlots(pdg,pt,prob,minPt))
        ## Hits
        self.Add(SingleObservable( "trk_nAllHits", binning))
        self.Add(SingleObservable( "trk_nAllHoles", binning))
        self.Add(SingleObservable( "trk_nSCTHits", binning))
        self.Add(SingleObservable( "trk_nSCTHoles", binning))
        self.Add(SingleObservable( "trk_nSCTDeadSensors", binning))
        self.Add(SingleObservable( "trk_nAllDeadSensors", binning))
        self.Add(SingleObservable( "trk_nSCTHitsHoles", binning))
        self.Add(SingleObservable( "trk_nSCTHitsHolesDead", binning))
        self.Add(SingleObservable( "trk_nPixHitsHoles", binning))
        self.Add(SingleObservable( "trk_nPixHitsHolesDead", binning))
        self.Add(SingleObservable( "trk_nAllHitsHoles", binning))
        self.Add(SingleObservable( "trk_nAllHitsHolesDead", binning))
        self.Add(SingleObservable( "trk_nPixHits", binning))
        self.Add(SingleObservable( "trk_nPixelDeadSensors", binning))
        self.Add(SingleObservable( "trk_nPixHoles", binning))

# Definition of a set of resolution observables, but with Gaussian fitting
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class ResolutionObservables(ObservableGroup):
    def __init__(self, path, title, prob, binnings, track_cuts, charge=None,fit=True, extended_eta=False, pdgId=None, truthMinPt=None, truthMaxPt=None, truthMaxEta=None ):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Use either a gaussian fit or the RMS to get the resolutions
        if fit:
            DiffObs = DiffObservableFitted
            PullObs = PullObservableFitted
        else:
            DiffObs = DiffObservable
        # Set cuts
        # Apply the TrackResolutionCuts defined above
        self.SetCuts(TrackResolutionCuts(prob, track_cuts, pdgId=pdgId, truthMinPt=truthMinPt, truthMaxPt=truthMaxPt, truthMaxEta=truthMaxEta))
        self.Add(Distribution('trk_pt', 100, 0, 100e3))
        for binning in binnings:
            # Resolutions
            self.Add(DiffObs( "trk_d0","mc_perigee_d0", binning))
            self.Add(DiffObs( "trk_z0","mc_perigee_z0", binning))
            self.Add(DiffObs( "trk_phi","mc_perigee_phi", binning))
            #self.Add(DiffObs( "trk_pt","mc_gen_pt", binning))
            self.Add(DiffObs( "trk_qoverp","mc_perigee_qoverp", binning))
            self.Add(DiffObs( "trk_pt","mc_gen_pt", binning, True))
            self.Add(QOverPtObs( "trk_qoverp","mc_perigee_qoverp", "mc_perigee_theta", binning))
            self.Add(DiffObs( "trk_theta","mc_perigee_theta", binning))


