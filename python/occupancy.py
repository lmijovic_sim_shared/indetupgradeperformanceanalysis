
import math
from ROOT import Observable

from ROOT import TFile
from ROOT import TCanvas, TH1, TH1D, TH2D, TGraph, TGraphErrors,TMultiGraph
from ROOT import TProfile2D, TF1, TLegend
from ROOT import gROOT
from ROOT import kRed,kBlue,kGreen,kCyan,kMagenta
from ROOT import  TGaxis
from ROOT import  TStyle

class MyObservable(Observable):
    
    def __init__(self,name,xmin,ymin,xmax,ymax):
        Observable.__init__(self,name)
        
        self.name=name
        c= TCanvas()
        frame=c.DrawFrame(xmin,ymin,xmax,ymax,name)
        self.canvas= [c]
        self.frame = frame
        self.sensor = None
        self.graphs=[]
        self.histos=[]
        self.nmax=-1
        self.ana=None

    def AddSensor(self,sensor):
        Observable.AddSensor(sensor)
        self.sensor = sensor

    def Initialize(self):
        pass

    def AnalyzeEvent(self,step):
        pass

    def Analyze(self,step,i):
        pass

    def Finalize(self):
        pass

    def StorePlots(self):
        pass

    def update(self):
        if self.canvas != None :
            for c in self.canvas:
                c.Modified()
                c.Update()

    def Print(self):
        if self.canvas != None :
            for c in self.canvas:
                c.Print()

    def Save(self):
        pass



class ClusterMap(MyObservable):

    def __init__(self):
        #prepare canvas
        Observable.__init__(self,'ClusterMap',-1500,0,1500,160)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['pixClus_x','pixClus_y','pixClus_z']

    def draw(self,trackTree,int):
        self.canvas[0].cd()
        # use symmetry to increase statistics
        trackTree.Draw("TMath::Sqrt(pixClus_y*pixClus_y+pixClus_x*pixClus_x):pixClus_z","","same")
        trackTree.Draw("TMath::Sqrt(pixClus_y*pixClus_y+pixClus_x*pixClus_x):-pixClus_z","","same")

class Counters(object):

    def __init__(self, tot_pix=0):
        self.sum_evnt=0
        self.sum_x=0
        self.sum_y=0
        self.sum_z=0
        self.sum_r=0
        self.sum_clus=0
        self.sum_clussize=0
        self.sum_clussize2=0
        self.tot_pix=tot_pix
        self.detElementIds={}

    def add(self,x,y,z,clussize,detElementId):
        self.sum_x+=x
        self.sum_y+=y
        self.sum_r+=math.sqrt(x*x+y*y)
        self.sum_z+=z

        self.sum_clus+=1
        self.sum_clussize+=clussize
        self.sum_clussize2+=clussize*clussize
        self.detElementIds[detElementId]=1

    def r(self):
        return self.sum_r/self.sum_clus

    def x(self):
        return self.sum_x/self.sum_clus

    def y(self):
        return self.sum_y/self.sum_clus

    def z(self):
        return self.sum_z/self.sum_clus


    def increaseEvnt(self):
        self.sum_evnt+=1

    def __repr__(self):
        return '%d/%d/%d' % ( self.sum_clussize, self.tot_pix, self.sum_evnt)


    def occupancy(self):
        return float(self.sum_clussize)/float(self.tot_pix * self.sum_evnt)

    def occupancyError(self):
        n=float(self.sum_clus)
        if n>2:
            var=n**2/(n-1) * (self.sum_clussize2 - 1/n *self.sum_clussize**2)
            return math.sqrt(var/n)/float(self.tot_pix * self.sum_evnt)
        else:
            return self.occupancy()


class Occupancy2D(MyObservable):

    def __init__(self):
        self.name='Occupancy2D'
        Observable.__init__(self,'Occupancy2D',-3100.,0,3100.,1100.)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['trk_n','trk_SCT_hit_n',
                             'trk_SCT_hit_x','trk_SCT_hit_y','trk_SCT_hit_z',
                             'trk_SCT_hit_detElementId',
                             'pixClus_x','pixClus_y','pixClus_z',
                             'sctClus_x','sctClus_y','sctClus_z',
                             'trk_SCT_hit_bec', 'trk_SCT_hit_layer']

        c= TCanvas()
        frame=c.DrawFrame(-3100.,0,3100.,1100.)
        frame.GetXaxis().SetTitle('z / mm')
        frame.GetZaxis().SetTitle('r / mm')
        self.canvas.append(c)

        c= TCanvas()
        frame=c.DrawFrame(-3100.,0,3100.,1100.)
        frame.GetXaxis().SetTitle('z / mm')
        frame.GetZaxis().SetTitle('r / mm')
        self.canvas.append(c)

        c=TCanvas()
        self.canvas.append(c)

    def draw(self,trackTree,int,factor=200):  # 200 pileup

        # get other occupancy observables and create an occupancy map
        occ={}
        #subobs=['SCTBarrel','SCTDisk']
        subobs=['SCTBarrel']
        for name in subobs:
            obs=self.ana[name]
            if obs is not None:
                for d in obs:
                    o=d.occupancy()
                    for k in d.detElementIds:
                        occ[k]=o
        self.occ=occ
        
        #p=TProfile2D('sct_occ_prof','SCT occupancy',100,-3100.,3100.,100,0.,1000.)
        #self.histos.append(p)

        self.canvas[0].cd()
        h=TH2D('sct_clus_map2d','Occupancy2D',100,-3100.,3100.,100,0.,1000.)
        self.histos.append(h)

        nEvt = trackTree.GetEntries()
        if self.nmax!=-1 and nEvt>self.nmax:
            nEvt=self.nmax
        print 'read',nEvt,'events from', trackTree.GetEntries()

        for j in range(nEvt): # event loop
            trackTree.GetEntry(j)
            for k in range(trackTree.trk_n): # track loop
                for i in range(trackTree.trk_SCT_hit_n[k]): # hit loop
                    x=trackTree.trk_SCT_hit_x[k][i]
                    y=trackTree.trk_SCT_hit_y[k][i]
                    z=trackTree.trk_SCT_hit_z[k][i]
                    r=math.sqrt(x*x+y*y)
                    h.Fill(z,r)
                    print '#AS1'
                    print  trackTree.trk_SCT_hit_bec[k][i]
                    print '#AS2'
                    if trackTree.trk_SCT_hit_bec[k][i]==0: # barrel only for now
                        #o=occ[trackTree.trk_SCT_hit_detElementId[k][i]]
                        #p.Fill(z,r,o)
                        print '#AS3 ',
                        #trackTree.trk_SCT_hit_detElementId[k][i]
                        

        h.Draw("same colz")
        

        self.canvas[1].cd()
        trackTree.Draw("TMath::Sqrt(trk_SCT_hit_x*trk_SCT_hit_x+trk_SCT_hit_y*trk_SCT_hit_y):trk_SCT_hit_z","","same")

        trackTree.Draw("TMath::Sqrt(pixClus_y*pixClus_y+pixClus_x*pixClus_x):pixClus_z","","same",50)


        self.canvas[2].cd()
        trackTree.Draw("TMath::Sqrt(sctClus_y*sctClus_y+sctClus_x*sctClus_x):sctClus_z","","same",50)


            
class SCTDisks(MyObservable):

    def __init__(self):
        #prepare canvas
        self.name='SCTDiskOccupancy'
        Observable.__init__(self,'SCTDisk',-100.5,0,100.5,1.e3)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['sctClus_x','sctClus_y','sctClus_z',
                             'sctClus_n','sctClus_size','sctClus_layer',
                             'sctClus_bec','sctClus_eta_module','sctClus_detElementId']


        c= TCanvas()
        self.canvas.append(c)

        self.canvas[0].cd()

    def Initialize(self):
        # preparation
        self.factor = 200 * 100   # pileup * percent
        sensor=self.sensor
        self.nDisks = len(sensor.sctdisk)
        data=[]

        for i,pd in enumerate(sensor.sctdisk):
            d=[]
            for j,ring in enumerate(pd.rings):
                d.append(Counters(tot_pix=ring.total))
            data.append(d)
        for i,pd in enumerate(sensor.sctdisk):
            d=[]
            for j,ring in enumerate(pd.rings):
                d.append(Counters(tot_pix=ring.total))
            data.append(d)

        self.data=data


    def analyseOneEvent(self):
        trackTree=self.ana.trackTree
        
        for i in range(self.nDisks*2):
            for d in self.data[i]:
                d.increaseEvnt()

        for k in range(trackTree.sctClus_n):  # number of clusters (in a event)
            bec=trackTree.sctClus_bec[k]  # 0, 2, -2
            if bec!=0:  # discs only
                x=trackTree.sctClus_eta_module[k]   # calculate moduleIndex "x"
                kLayer=trackTree.sctClus_layer[k]
                if bec==-2:
                    kLayer+=self.nDisks
                self.data[kLayer][x].add(trackTree.sctClus_x[k],trackTree.sctClus_y[k],
                                         trackTree.sctClus_z[k],
                                         trackTree.sctClus_size[k],
                                         trackTree.sctClus_detElementId[k])


    def draw(self,trackTree,int,factor=200): 
        pass
    
    #depricated
    def _draw(self,trackTree,int,factor=200):  # 200 pileup
        self.factor=factor*100 # results in percent
        
            
        # main loop over given number of events
        self.canvas[0].cd()

        nEvt = trackTree.GetEntries()
        print 'read',nEvt,'events'
        if self.nmax!=-1 and nEvt>self.nmax:
            nEvt=self.nmax
        for j in range(nEvt): 
            trackTree.GetEntry(j)
            self.analyseOneEvent()


    def finish(self):
        # finilize
        self.canvas[1].cd()

        # create occupancy graphs
        mg = TMultiGraph()
        for i in range(self.nDisks*2):
            d=self.data[i]
            # debug output
            print d
            g=TGraphErrors(len(d))
            g.SetMarkerStyle(20)
            #g.SetLineWidth(3)
            g.SetMarkerColor(1+i)
            g.SetLineColor(1+i)

            for k in range(len(d)):
                y=self.factor*d[k].occupancy()
                yerr=self.factor*d[k].occupancyError()
                g.SetPoint(k,k,y)
                g.SetPointError(k,0,yerr)
                print k,y,yerr
            mg.Add(g,'lp')
            self.graphs.append(g)

        mg.Draw("a")
        self.graphs.append(mg)

        self.canvas[1].Modified()
        self.canvas[1].Update()

class SCTBarrel(MyObservable):
    class Iterator:
        def __init__(self,barrel):
            self.barrel=barrel
            self.layer=0
            self.elm=0
            self.dd=barrel.data[self.layer].values()
            self.nLayer=barrel.nLayer

        def __iter__(self):
            return self

        def next(self):
            if self.elm>=len(self.dd):
                self.elm=0
                self.layer+=1
                if self.layer>=self.nLayer:
                    raise StopIteration()
                self.dd=self.barrel.data[self.layer].values()
            d=self.dd[self.elm]
            self.elm+=1
            return d
            
        
    def __init__(self):
        #prepare canvas
        self.name='SCTBarrelOccupancy'
        Observable.__init__(self,'SCTBarrel',-100.5,0,100.5,1.e3)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['sctClus_x','sctClus_y','sctClus_z',
                             'sctClus_n','sctClus_size','sctClus_layer',
                             'sctClus_bec','sctClus_eta_module','sctClus_detElementId']


        c= TCanvas()
        self.canvas.append(c)

        self.canvas[0].cd()

    def __iter__(self):
        return SCTBarrel.Iterator(self)

    def Initialize(self):
        self.factor=200*100   # pileup * percent
        # initialise data containers
        sensor=self.sensor
        self.nLayer = len(sensor.sctlayer)
        self.data=[]
        self.histos=[]
        for i in range(self.nLayer):
            nModule = sensor.sctlayer[i].nmodule*sensor.sctlayer[i].nsegment
            nMax=nModule/2
            #if nMax<4:
            #nMax=104;
            nMin=-nMax
            hasZero=(nModule%2!=0)
            d={}
            for m in range(nMin,nMax+1):
                if m!=0 or hasZero:
                    d[m]=Counters(tot_pix=sensor.sctlayer[i].perModuleEta)
                
            self.data.append(d)
            self.histos.append(TH1D('sctlay%d'%i,'eta_modules sctlayer %d'%i,201,-100.5,100.5))

    def analyseOneEvent(self):
        for i in range(self.nLayer):
            for d in self.data[i].values():
                d.increaseEvnt()

        trackTree=self.ana.trackTree
        for k in range(trackTree.sctClus_n):  # number of clusters (in a event)
            if trackTree.sctClus_bec[k]==0:  # barrel only
                x=trackTree.sctClus_eta_module[k]   #  moduleIndex "x"
                if x>128:
                    x-=256

                kLayer=trackTree.sctClus_layer[k]
                self.histos[kLayer].Fill(x)

                self.data[kLayer][x].add(trackTree.sctClus_x[k],trackTree.sctClus_y[k],
                                    trackTree.sctClus_z[k],
                                    trackTree.sctClus_size[k],trackTree.sctClus_detElementId[k])
        
                
    def finish(self):
        self.canvas[1].cd()

        # create occupancy graphs
        mg = TMultiGraph()
        for i in range(self.nLayer):
            d=self.data[i]
            print d
            g=TGraphErrors(len(d))
            g.SetMarkerStyle(20)
            #g.SetLineWidth(3)
            g.SetMarkerColor(1+i)
            g.SetLineColor(1+i)

            keys=d.keys()
            keys.sort()
            for j,k in enumerate(keys):
                y=self.factor*d[k].occupancy()
                yerr=self.factor*d[k].occupancyError()
                g.SetPoint(j,k,y)
                g.SetPointError(j,0,yerr)
                print k,y,yerr
            mg.Add(g,'lp')
            self.graphs.append(g)

        mg.Draw("a")
        self.graphs.append(mg)

        self.canvas[1].Modified()
        self.canvas[1].Update()


        #def makeLegend(self):
        # make a legend
	#leg1=TLegend(0.1,0.15,0.3,0.45) # buttom left
	#leg1=TLegend(0.7,0.60,0.9,0.90) # top right
        leg1=TLegend(0.75,0.60,0.9,0.90) # top right
        leg1.SetTextSize(0.03)
        leg1.SetFillStyle(0)
        leg1.SetBorderSize(0)
        leg1.SetHeader("SCT layers")

        graphs=mg.GetListOfGraphs()
        for i,g in enumerate(graphs):
            leg1.AddEntry(g,'Layer %d'%i,'lp')
            

        frame=mg.GetHistogram()
        frame.GetXaxis().SetTitle('module eta index')
        frame.GetYaxis().SetTitle('Occupancy in percent')
        frame.SetTitle('Pileup 200')
        
        leg1.Draw('same')
        self.graphs.append(leg1)

        self.canvas[1].Modified()
        self.canvas[1].Update()

            
    def draw(self,trackTree,int,factor=200):
        self.canvas[0].cd()

        for h in self.histos:
            h.Draw('same')
            #self.graphs.append(h)

    def _draw(self,trackTree,int,factor=200):  # 200 pileup
        self.factor=factor*100 # results in percent
        
        self.canvas[0].cd()



        # main loop over number of events
        # faster HACK *AS*:
        nEvt = trackTree.GetEntries()
        print 'read',nEvt,'events'
        if self.nmax!=-1 and nEvt>self.nmax:
            nEvt=self.nmax
        for j in range(nEvt): 
            trackTree.GetEntry(j)
            self.analyseOneEvent()


        # debug output
        for i in range(self.nLayer):
            print self.data[i]

        # store histograms
        for h in histos:
            h.Draw('same')
            self.graphs.append(h)

        self.canvas[1].cd()

        # create occupancy graphs
        self.finish()
        



class PixelDisks(MyObservable):
    def __init__(self):
        #prepare canvas
        self.name='PixelDiskOccupancy'
        Observable.__init__(self,'PixelDisk',-100.5,0,100.5,1.e3)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['pixClus_x','pixClus_y','pixClus_z',
                             'pixClus_eta_module','pixClus_size','pixClus_n',
                             'pixClus_layer','pixClus_bec']

        c= TCanvas()
        self.canvas.append(c)

        self.canvas[0].cd()

    def draw(self,trackTree,int,factor=200):  # 200 pileup
        self.factor=factor*100 # results in percent
        
        self.canvas[0].cd()
        # use symmetry to increase statistics

        nEvt = trackTree.GetEntries()
        print 'read',nEvt,'events'

        # initialise data containers
        sensor=self.sensor
        nDisks = len(sensor.pixdisk)
        data=[]
        histos=[]

        for i,pd in enumerate(sensor.pixdisk):
            d=[]
            for j,ring in enumerate(pd.rings):
                d.append(Counters(tot_pix=ring.total))
            data.append(d)
        for i,pd in enumerate(sensor.pixdisk):
            d=[]
            for j,ring in enumerate(pd.rings):
                d.append(Counters(tot_pix=ring.total))
            data.append(d)

        self.data=data
            
        # main loop over number of events
        # faster HACK *AS*:
        if self.nmax!=-1 and nEvt>self.nmax:
            nEvt=self.nmax
        for j in range(nEvt): 
            trackTree.GetEntry(j)
            for i in range(nDisks*2):
                for d in data[i]:
                    d.increaseEvnt()

            for k in range(trackTree.pixClus_n):  # number of clusters (in a event)
                bec=ord(trackTree.pixClus_bec[k])  # 0, 2, 254
                if bec!=0:  # discs only
                    x=ord(trackTree.pixClus_eta_module[k])   # calculate moduleIndex "x"
                    kLayer=ord(trackTree.pixClus_layer[k])
                    if bec==254:
                        kLayer+=nDisks
                    data[kLayer][x].add(trackTree.pixClus_x[k],trackTree.pixClus_y[k],
                                        trackTree.pixClus_z[k],
                                        trackTree.pixClus_size[k])

        self.canvas[1].cd()

        # create occupancy graphs
        mg = TMultiGraph()
        for i in range(nDisks*2):
            d=data[i]
            # debug output
            print d
            g=TGraphErrors(len(d))
            g.SetMarkerStyle(20)
            #g.SetLineWidth(3)
            g.SetMarkerColor(1+i)
            g.SetLineColor(1+i)

            for k in range(len(d)):
                y=self.factor*d[k].occupancy()
                yerr=self.factor*d[k].occupancyError()
                g.SetPoint(k,k,y)
                g.SetPointError(k,0,yerr)
                print k,y,yerr
            mg.Add(g,'lp')
            self.graphs.append(g)

        mg.Draw("a")
        self.graphs.append(mg)

        self.canvas[1].Modified()
        self.canvas[1].Update()


        # TODO: subdivide in columns to increase eta binning!

class PixelBarrel(MyObservable):

    def __init__(self):
        #prepare canvas
        self.name='PixelBarrelOccupancy'
        Observable.__init__(self,'PixelBarrel',-100.5,0,100.5,1.e3)
        self.frame.GetXaxis().SetTitle('z / mm')
        self.frame.GetZaxis().SetTitle('r / mm')
        self.neededBranches=['pixClus_x','pixClus_y','pixClus_z',
                             'pixClus_eta_module','pixClus_size','pixClus_n',
                             'pixClus_layer','pixClus_bec']

        c= TCanvas()
        self.canvas.append(c)

        self.canvas[0].cd()

    def draw(self,trackTree,int,factor=200):  # 200 pileup
        self.factor=factor*100 # results in percent
        
        self.canvas[0].cd()
        # use symmetry to increase statistics

        nEvt = trackTree.GetEntries()
        print 'read',nEvt,'events'

        # initialise data containers
        sensor=self.sensor
        nLayer = len(sensor.layer)
        data=[]
        histos=[]
        for i in range(nLayer):
            nModule = sensor.layer[i].nmodule
            nMax=nModule/2
            nMin=-nMax
            hasZero=(nModule%2!=0)
            d={}
            for m in range(nMin,nMax+1):
                if m!=0 or hasZero:
                    d[m]=Counters(tot_pix=sensor.layer[i].perModuleEta)
                
            data.append(d)
            histos.append(TH1D('lay%d'%i,'eta_modules layer %d'%i,201,-100.5,100.5))

        # main loop over number of events
        # faster HACK *AS*:
        if self.nmax!=-1 and nEvt>self.nmax:
            nEvt=self.nmax
        for j in range(nEvt): 
            trackTree.GetEntry(j)
            for i in range(nLayer):
                for d in data[i].values():
                    d.increaseEvnt()

            for k in range(trackTree.pixClus_n):  # number of clusters (in a event)
                if ord(trackTree.pixClus_bec[k])==0:  # barrel only
                    x=ord(trackTree.pixClus_eta_module[k])   # calculate moduleIndex "x"
                    if x>128:
                        x-=256
                        # should be in [-nModule/2, ..., nModule/2]
                        
                    kLayer=ord(trackTree.pixClus_layer[k])                    
                    histos[kLayer].Fill(x)
                    #print j,k,kLayer,x

                    data[kLayer][x].add(trackTree.pixClus_x[k],trackTree.pixClus_y[k],
                                        trackTree.pixClus_z[k],
                                        trackTree.pixClus_size[k])

        self.data=data

        # debug output
        for i in range(nLayer):
            print data[i]

        # store histograms
        for h in histos:
            h.Draw('same')
            self.graphs.append(h)

        self.canvas[1].cd()

        # create occupancy graphs
        mg = TMultiGraph()
        for i in range(nLayer):
            d=data[i]
            print d
            g=TGraphErrors(len(d))
            g.SetMarkerStyle(20)
            #g.SetLineWidth(3)
            g.SetMarkerColor(1+i)
            g.SetLineColor(1+i)

            keys=d.keys()
            keys.sort()
            for j,k in enumerate(keys):
                y=self.factor*d[k].occupancy()
                yerr=self.factor*d[k].occupancyError()
                g.SetPoint(j,k,y)
                g.SetPointError(j,0,yerr)
                print k,y,yerr
            mg.Add(g,'lp')
            self.graphs.append(g)

        mg.Draw("a")
        self.graphs.append(mg)

        self.canvas[1].Modified()
        self.canvas[1].Update()


        #def makeLegend(self):
        # make a legend
	#leg1=TLegend(0.1,0.15,0.3,0.45) # buttom left
	#leg1=TLegend(0.7,0.60,0.9,0.90) # top right
        leg1=TLegend(0.75,0.60,0.9,0.90) # top right
        leg1.SetTextSize(0.03)
        leg1.SetFillStyle(0)
        leg1.SetBorderSize(0)
        leg1.SetHeader("Pixel layers")

        graphs=mg.GetListOfGraphs()
        for i,g in enumerate(graphs):
            leg1.AddEntry(g,'Layer %d'%i,'lp')
            

        frame=mg.GetHistogram()
        frame.GetXaxis().SetTitle('module eta index')
        frame.GetYaxis().SetTitle('Occupancy in percent')
        frame.SetTitle('Pileup 200')
        
        leg1.Draw('same')
        self.graphs.append(leg1)

        self.canvas[1].Modified()
        self.canvas[1].Update()


