#!/usr/bin/env python
import os

from xml.dom import minidom as dom
from xml.dom import getDOMImplementation

class AtlasDetector:
    """ This class creates a single XML tree containing information for the entire Inner Tracker."""
    def __init__(self,version='ATLAS-IBL-01-00-00'):

        self.nodelist = {}
        self.pixelLayout       = ""
        self.pixelEndcapLayout = ""
        self.sctLayout         = ""
        
        # import right version of Reader
        if version=='ATLAS-P2-ITK-05':
            self.pixelLayout       = "ExtBrl32Ref"
            self.pixelEndcapLayout = "ECRing32Ref"
            self.sctLayout         = "FourLayersNoStub_23-25-dev0"
        elif version=='ATLAS-P2-ITK-06':
            self.pixelLayout       = "ExtBrl4Ref"
            self.pixelEndcapLayout = "ECRing4Ref"
            self.sctLayout         = "FourLayersNoStub_23-25-dev0"
        elif version=='ATLAS-P2-ITK-07':
            self.pixelLayout       = "IExtBrl4Ref"
            self.pixelEndcapLayout = "ECRing4Ref"
            self.sctLayout         = "FourLayersNoStub_23-25-dev0"
        elif version=='ATLAS-P2-ITK-08':
            self.pixelLayout       = "InclBrl4Ref"
            self.pixelEndcapLayout = "ECRing4Ref"
            self.sctLayout         = "FourLayersNoStub_23-25-dev0"

            
        PIXMODULEFILE = "ITK_PixelModules.xml"
        PIXSTAVEFILE  = self.pixelLayout + "_PixelStave.xml"
        PIXBARRELFILE = self.pixelLayout + "_PixelBarrel.xml"
        PIXENDCAPFILE = self.pixelEndcapLayout + "_PixelEndcap.xml"
        
        SCTMODULEFILE = "ITK_SCTModules.xml"
        SCTSTAVEFILE  = self.sctLayout + "_SCTStave.xml"
        SCTBARRELFILE = self.sctLayout + "_SCTBarrel.xml"
        SCTENDCAPFILE = self.sctLayout + "_SCTEndcap.xml"

        from PyJobTransformsCore.envutil import find_file_env

        ###### Setup XML filesfor pixels ######
        xmlFiles = {}
        xmlFiles['PixelModules']= find_file_env(str(PIXMODULEFILE),'DATAPATH')
        xmlFiles['PixelStaves'] = find_file_env(str(PIXSTAVEFILE),'DATAPATH')
        xmlFiles['PixelBarrelLayers'] = find_file_env(str(PIXBARRELFILE),'DATAPATH')
        xmlFiles['PixelEndcapLayers'] = find_file_env(str(PIXENDCAPFILE),'DATAPATH')

        ###### Setup XML files for SCT ######
        xmlFiles['SCTModules']= find_file_env(str(SCTMODULEFILE),'DATAPATH')
        xmlFiles['SCTStaves'] = find_file_env(str(SCTSTAVEFILE),'DATAPATH')
        xmlFiles['SCTBarrelLayers'] = find_file_env(str(SCTBARRELFILE),'DATAPATH')
        xmlFiles['SCTEndcapLayers'] = find_file_env(str(SCTENDCAPFILE),'DATAPATH')

        self.xmlTrees = {}
        for key in xmlFiles:
            self.xmlTrees[key]=AtlasXmlTree(xmlFiles[key])
            print "xml for ",key,": ",self.xmlTrees[key]
            
        #impl = getDOMImplementation()
        #xmlDoc = impl.createDocument(None, "ATLAS", None)
        
        #nodes={}
        #for key in xmlFiles:
        #    nodes[key] = dom.parse(str(xmlFiles[key]))

        #print "pixelmodules first child: ",nodes["PixelModules"].firstChild
        
        #xmlDoc.firstChild.appendChild(nodes["PixelModules"].firstChild)
        #xmlDoc.firstChild.appendChild(nodes["PixelStaves"].firstChild)
        #xmlDoc.firstChild.appendChild(nodes["PixelBarrelLayers"].firstChild)
        #xmlDoc.firstChild.appendChild(nodes["PixelEndcapLayers"].firstChild)
        #xmlDoc.firstChild.appendChild(nodes["SCTBarrel"].firstChild)
        #xmlDoc.firstChild.appendChild(nodes["SCTEndcap"].firstChild)

        #print "xmlDoc: "
        #print xmlDoc.toprettyxml()

        #print "calling AtlasDetNode.root"
        #self.atlas = AtlasDetNode.root(self,version)
        #print "done calling AtlasDetNode.root"
        
    
    def __getitem__(self,name):
        
        if self.xmlTrees.has_key(name):
            print "returning from nodelist"
            return self.xmlTrees[name]

        else:
            print "key doesn't exist"
            return None

class AtlasXmlTree:

    def __init__(self,xmlFile):
        self.xmlTree = dom.parse(str(xmlFile))
        self.data = {} 
        
    def getAllEntries(self,name):
        #print "in AtlasXmlTree::getAllEntries for ",name
        
        if self.data.has_key(name):
            #print "returning data for key: ",name
            return self.data[name]
        else:
            #print "getting elements"
            elements = self.xmlTree.getElementsByTagName(name)
            #print "elements: ",elements
            
            self.data[name]=[]
            for element in elements:
                print "element: ",element
                entries=self.getEntriesForChildren(element.childNodes)
                self.data[name].append(entries)
                print "data: ",self.data[name]
            return self.data[name]

    def getEntriesForChildren(self,childNodes):
        entries={}
        for child in childNodes:
            #print "child: ",child
                    
            if child.localName==None:
                continue

            # get tag name
            tag=str(child.localName)
            if tag=="name": tag="Name"

            # get value
            val=child.firstChild.nodeValue                
            vals=str(val).strip().split() # strip whitespace and split into an array
            #print "vals: ",vals
                
            if len(vals)==0:                
                childEntries=self.getEntriesForChildren(child.childNodes)
                entries[tag]=childEntries                
            else:
                if len(vals)==1:
                    entries[tag]=self.convertXmlString(vals[0])
                else:
                    newvals=[]
                    for val in vals:
                        newval=self.convertXmlString(val)
                        #print "appending ",newval
                        newvals.append(newval)
                    #print "newvals: ",newvals
                    entries[tag]=newvals
            #print "entries[",tag,"]: ",entries[tag]
        return entries
    
    def convertXmlString(self,val):

        try:
            val=int(val)
        except:
            try:
                val=float(val)
            except:
                try:
                    val=str(val).strip()
                except:
                    print "val isn't string, int or float"
        return val

    def select(self,name,**kwargs):
        print "in AtlasXmlTree::select with name: ",name,", kwargs: ",kwargs
        
        entries = self.getAllEntries(name)
        print "entries: ",entries
        
        for key in kwargs:
            print "key: ",key
            for entry in entries:
                print "entry: ",entry
                if (entry[key]==kwargs[key]):
                    print "selected entry: ",entry
                    return entry
                else:
                    print "doesn't match"
        return None
    
    def __getitem__(self,name):
        if not self.data.has_key(name):
            print "getting entries for ",name
            self.getAllEntries(name)
        return self.data[name]
        
class AtlasDetNode:
    @classmethod
    def root(cls,atlas,version='ATLAS-IBL-01-00-00',fetchChildren=False):

        #self.atlas = atlas
        
        print 'found', version
        obj = cls(atlas, 'ATLAS', None, isBranch=True, fetchChildren=False)
        
        obj.fetchChildren()
        return obj    


    def __init__(self,node,name,comment=None,isBranch=False,fetchChildren=False):
        print "in AtlasDetNode init with node: ",node,", name: ",", comment: ",comment,", fetchChildren: ",fetchChildren
        
        self.node=node
        self.name=name
        self.isBranch=isBranch
        print "node: ",self.node
        if self.node:
            print "parent: ",self.node.parentNode
            if self.node.parentNode:
                print "parent parent: ",self.node.parentNode.parentNode

        # check if parent is ATLAS
        if not isBranch:
        #    parent=self.node.parentNode
        #    if parent and parent.localName=='ATLAS':
        #        self.isBranch=True
        #        print "parent's parent is atlas, setting true"
        #    else:
            self.isBranch=node.hasChildNodes# and node.previousSibling==None and node.nextSibling==None
                       
        print "isBranch: ",self.isBranch
        
        self.comment=comment
        self.children=[]
        self.data=None
        if fetchChildren:
            self.fetchChildren(True)
    
    def fetchChildren(self,recursive=False):
        print "in fetchChildren for ",self.name,", branch : ",self.isBranch
        if self.isBranch:
            print "getting elements with name: ",self.name
            elements = self.atlas.getElementsByTagName(str(self.name))
            print "elements: ",elements
            self.children=[]
            for child in elements[0].childNodes:
                if child.localName:
                    print "child: ",child
                    print "name: ",child.localName
                    self.children.append(AtlasDetNode(child,child.localName))

            for c in self.children:
                print "calling fetchChildren for child: ",c
                c.fetchChildren(recursive)

        else:
            print "getting data for ",self.name
            self.data=AtlasDetData(self.node)
    
    def fetchData(self):
        print "in AtlasDetNode::fetchData for ",self.name
        self.data.fetchData()
        return self.data
            
    def __getitem__(self,key):
        return self.data[key]

    def select(self,**kwarg):
        print "in AtlasDetNode::select for ",self.name," with kwarg: ",kwarg
        if self.data:
            return self.data.select(**kwarg)
        else:
            for child in self.children:
                child.fetchData()
                selected=child.select(**kwarg)
                if selected:
                    print "selected: ",selected
                    return selected
                else:
                    print "not selected"
                    
    def __str__(self):
        return str("AtlasDetNode: ")+self.name


class AtlasDetData:
    def __init__(self,node):
        print "in AtlasDetData with node: ",node
        self.node=node
        self.sdata=None
        self.name=node.localName
        self.desc=[]
        
    def fetchData(self):
        print "in AtlasDetData::fetchData"
        self.data=[]
        print "data: ",self.data    
        for child in self.node.childNodes:
            print "child: ",child
            if child.localName:
                print "name: ",child.localName,", val: ",child.firstChild.nodeValue
                self.desc.append(child.localName)#=child.firstChild.nodeValue
                #self.data.append(child.localName)
        
    def __str__(self):
        s='##'+self.name+'\n'
        print "self.data: ",self.data
        
        for row in self.data:
            print "row: ",row,", self.desc: ",self.desc
            s+= '*** NEXT ***\n'
            for d,v in zip(self.desc,row):
                s+= d+'='+str(v)+'\n'
        return s

    def select(self,**kwarg):
        print "in AtlasDetData::select for ",self.name,", kwarg: ",kwarg
        self.sdata=self.get(**kwarg)
        return self

    def __getitem__(self,key):
        if self.sdata !=None:
            try:
                return self.sdata[key.upper()]
            except:
                print 'value',key,'not found'
        return self.get(key)

    def get(self,*args,**kwargs):
        print "in AtlasDetData::get for ",self.name,", args: ",args,", kwargs: ",kwargs
        #print 'args',len(args)
        for key in args:
            if len(self.data)==1:
                d=dict(zip(self.desc, self.data[0]))
                try:
                    return d[key]
                except KeyError:
                    print key,'not found'
                    return None

        #print 'kwargs',len(kwargs)
        rows=[]
        values=[]

        for key,value in kwargs.items():
            print "key: ",key,", value: ",value
            try:
                print "self.desc: ",self.desc
                i=self.desc.index(key)
                print "i: ",i
                rows.append(zip(*self.data)[i])
                print "rows: ",rows
                values.append(value)
                print "values: ",values
            except (ValueError,AttributeError):
                print key,'=',value,' not found'
                return {}

            j=list(zip(*rows)).index(tuple(values))
            if j<0:
                values = [ v.upper() for v in values ]
                j=list(zip(*rows)).index(tuple(values))

        if j>=0 and j<len(self.data):    
            return dict(zip(self.desc, self.data[j]))
        return {}


def simpleTest():
    cur.execute('select * from HVS_NODE where PARENT_ID=0 order by NODE_ID')
    # cur.description
    # NODE_ID NODE_NAME PARENT_ID BRANCH_FLAG NODE_COMMENT

    res = cur.fetchall()
    for r in res:
        print AtlasDetNode(*r)

    cur.execute("select * from HVS_TAG2NODE where TAG_NAME like 'ATLAS-IBL%' ")
    res = cur.fetchall()
    for r in res:
        print r
    

    # close connection
    cnx.commit()

#simpleTest()

def newTest():
    global atlas, pixRO
    atlas=AtlasDetNode.root('ATLAS-IBL-01-00-00')
    atlas.fetchChildren()
    pixRO=atlas.children[0].children[0].children[15]
    pixRO.getData()


if __name__=='__main__':
    atlas=AtlasDetector('ATLAS-IBL-01-00-00')

    pixModule=atlas['PixelModule']
    pixRO=atlas['PixelReadout']


    oldRO=AtlasDetNode.root('PixelReadout-02')

    g=atlas['pixellayer']
    print g.select(layer=2)['moduletype']
    print g.select(layer=0)['moduletype']
    print g['moduletype']


#cur.close()

