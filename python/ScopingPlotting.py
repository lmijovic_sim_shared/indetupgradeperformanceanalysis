import ROOT as R

layout_markerstyle = { 
                "Gold": 20, "Gold_m10": 24,
                "VF_Gold": 21, "VF_Gold_m10": 25,
                "VF_Silver": 22, "VF_Silver_m10": 26,
                "Silver": 22, "Silver_m10": 26,
                "Bronze": 23, "Bronze_m10": 27,
              }

layout_color = { "VF_Gold": R.kBlack,  "VF_Gold_m10": R.kBlack,
                "Gold": R.kBlack, "Gold_m10": R.kBlack,
                "VF_Silver": R.kBlue, "VF_Silver_m10": R.kBlue,
                "Silver": R.kBlue, "Silver_m10": R.kBlue,
                "Bronze": R.kRed, "Bronze_m10": R.kRed,
              }
#layout_markerstyle = { 
#                "Gold": 20, "Gold_m10": 24,
#                "VF_Gold": 21, "VF_Gold_m10": 25,
#                "VF_Silver": 22, "VF_Silver_m10": 26,
#                "Silver": 22, "Silver_m10": 26,
#                "Bronze": 23, "Bronze_m10": 27,
#              }
#
#layout_color = { "VF_Gold": R.kOrange-3,  "VF_Gold_m10": R.kOrange-3,
#                "Gold": R.kOrange, "Gold_m10": R.kOrange,
#                "VF_Silver": R.kGray, "VF_Silver_m10": R.kGray,
#                "Silver": R.kGray, "Silver_m10": R.kGray,
#                "Bronze": R.kOrange+3, "Bronze_m10": R.kOrange+3,
#              }

layout_linestyle = { "VF_Gold": 1,  "VF_Gold_m10": 2,
                "Gold": 1, "Gold_m10": 2,
                "VF_Silver": 1, "VF_Silver_m10": 2,
                "Silver": 1, "Silver_m10": 2,
                "Bronze": 1,
                "Bronze_m10": 2,
              }

hits_style = { 
                4: 26, 5: 27, 6: 28,
                7: 20, 8: 24,
                9: 21, 10: 25,
                11: 22,
                12: 26
              }

layout_ranges = { "VF_Gold": (0.75,1.05),  "VF_Gold_m10": (0.5,1.1),
                "Gold": (0.6,1.08), "Gold_m10": (0.6,1.08),
                "VF_Silver": (0.5,1.11), "VF_Silver_m10": (0.,1.2),
                "Silver": (0.,1.2), "Silver_m10": (0.,1.2),
                "Bronze": (0,1.2),
                "Bronze_m10": (0,1.2),
              }

hits_color = { 4:1, 5:1, 6:1, 7: 1, 8:2, 9:3, 10:4, 11:6, 12: R.kOrange+1 }

hits_linestyle = { 4:1, 5:1, 6:1, 7: 1, 8:1, 9:1, 10:1, 11:1, 12:1}

pdgId_yrange = {11: (0.7, 1.09), 13: (0., 1.25), 211: (0.7, 1.09) }

mu_style = {0: 20, 80: 21, 140: 22, 200:23}
mu_color = {0: R.kBlack, 80:R.kGreen, 140: R.kBlue, 200: R.kRed}

layout_label = { 
    "VF_Gold": "Reference",
    "VF_Gold_m10": "Reference -10%",
    "Gold": "Reference",
    "Gold_m10": "Reference -10%",
    "VF_Silver": "Middle",
    "VF_Silver_m10": "Middle -10%",
    "Silver": "Middle",
    "Silver_m10": "Middle -10%",
    "Bronze": "Low",
    "Bronze_m10": "Low -10%",
              }

def get_dir(pt):
    dir = "pt%i" % (pt,)
    if options.extended_eta: 
        dir += "_extendedEta"
    return dir

def get_filename(layout, tag):
    filename = "InDetUpPerfPlots_%s_%s.root" % (tag, layout)
    return filename

def get_legend_pu(pt, mu):
    return 'p_{T}=%i GeV, <#mu>=%i' % (pt, mu)

def get_legend( layout ):
    return layout_label[layout]
