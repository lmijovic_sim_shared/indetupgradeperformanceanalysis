# Custom binning definitions
# See scripts/example_run.py for more information

from InDetUpgradePerformanceAnalysis  import Binning, vector_float

# Bin as a function of eta, single sided, for MC particles
# Bin width is 0.25
class SingleSidedEtaBinningMC(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 15,0,2.7, "etaSingleSidedMC", "#eta", True)

# Bin as a function of eta, single sided
# Bin width is 0.18
class SingleSidedEtaBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta",15,0,2.7, "etaSingleSided", "#eta", True)

eta_bins = vector_float( [0, 1.0, 1.2, 1.8, 2.2, 2.7, 3.2, 4.0] )
# Bin as a function of eta, single sided, for MC particles
# Bin width is 0.25
class SingleSidedVariableEtaBinningMC(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", eta_bins, "varEtaSingleSidedMC", "#eta", True)

# Bin as a function of eta, single sided
# Bin width is 0.18
class SingleSidedVariableEtaBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta", eta_bins, "varEtaSingleSided", "#eta", True)

# Bin as a function of eta, single sided, for MC particles, extended to eta=4
# Bin width is 0.182
class SingleSidedEtaBinningMCExtended(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 22,0,4., "etaSingleSidedMC", "#eta", True)

# Bin as a function of eta, single sided, extended to eta=4
# Bin width is 0.182
class SingleSidedEtaBinningExtended(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta",22,0,4., "etaSingleSided", "#eta", True)

# Bin as a function of eta, single sided, for MC particles, extended to eta=4
# Bin width is 0.182
class SingleSidedEtaBinningMCNew_Gold(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 25,0,4., "etaSingleSidedMC", "#eta", True)

class SingleSidedEtaBinningMCNew_Silver(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 20,0,3.2, "etaSingleSidedMC", "#eta", True)

class SingleSidedEtaBinningMCNew_Bronze(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 17,0,2.72, "etaSingleSidedMC", "#eta", True)

class SingleSidedEtaBinningNew_Gold(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta", 25,0,4., "etaSingleSided", "#eta", True)

class SingleSidedEtaBinningNew_Silver(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta", 20,0,3.2, "etaSingleSided", "#eta", True)

class SingleSidedEtaBinningNew_Bronze(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta", 17,0,2.72, "etaSingleSided", "#eta", True)

# Bin as a function of eta, single sided, for MC particles, extended to eta=4
# Bin width is 0.182
class DoubleSidedEtaBinningMCNew_Gold(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_eta", 50,-4,4., "etaDoubleSidedMC", "#eta", False)

class DoubleSidedEtaBinningNew_Gold(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta", 50,-4,4., "etaDoubleSided", "#eta", False)


# Bin as a function of eta, double sided
class DoubleSidedEtaBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_eta",30,-2.7,2.7, "etaDoubleSided", "|#eta|", False)

# Bin as a function of ph
class PhiBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_phi",16,-3.2,3.2, "phi", "#phi", False)

# Bin as a function of ph
class GenVtxBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "mcVx_n",150,0,300, "genVtx", "No. Generated Vertices", False)

# Bin as a function of ph
class PtBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_pt",40,0,200e3, "trkPt", "Track Pt", False)

# Bin as a function of ph
class TruthPtBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_pt",40,0,200e3, "truthPt", "Truth Pt", False)
        
pt_bins = vector_float( [x*1e3 for x in [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 100]])
# Bin as a function of ph
class VariablePtBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_pt", pt_bins, "varTrkPt", "Track Pt", False)

pt_bins2 = vector_float( [x*1e3 for x in [1, 4, 10, 20, 30, 40, 50, 70, 100]])
# Bin as a function of ph
class VariablePtBinning2(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_pt", pt_bins2, "varTrkPt", "Track Pt", False)

# Bin as a function of ph
class VariableTruthPtBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_pt", pt_bins, "varTruthPt", "Truth Pt", False)
        
pt_bins_pi = vector_float( [x*1e3 for x in [1, 2.5, 5, 7.5, 10, 15, 20, 30, 50, 100 ]] )
# Bin as a function of ph
class VariablePtBinningPion(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_pt", pt_bins_pi, "varTrkPtPion", "Track Pt", False)

# Bin as a function of ph
class VariableTruthPtBinningPion(Binning):
    def __init__(self):
        Binning.__init__(self, "mc_gen_pt", pt_bins_pi, "varTruthPtPion", "Truth Pt", False)

# Bin as a function of ph
class nHitsBinning(Binning):
    def __init__(self):
        Binning.__init__(self, "trk_nAllHits", 9,11,20, "nHits", "No. Hits", False)

