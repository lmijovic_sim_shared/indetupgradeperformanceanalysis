print 'InDetUpgradePerformanceAnalysis loading ...'

import sys
import os

athena_version = os.getenv('AtlasVersion')
if athena_version:
    major_version = int(athena_version.split(".")[0])
    minor_version = int(athena_version.split(".")[1])
else:
    major_version = 0
    minor_version = 0
# Determine if using athena with root6 to decide
# whether to use Reflex
isRoot6 = False
if athena_version:
    if major_version>20 or (major_version>19 and minor_version>=6):
        print "Assuming root6 - no REFLEX"
        isRoot6=True
    else:
        print "Assuming root5 - will use REFLEX"

# Must import PyCintex (if using) BEFORE importing ROOT
if not isRoot6 and athena_version:
    import PyCintex
    PyCintex.Cintex.Enable()

import ROOT
ROOT.gROOT.SetBatch()

try:
    from ROOT import InDetUpgradeAnalysis
except:
    print 'failed to find reflex bindings'
    print 'load rootcint bindings'
    from ROOT import gSystem
    import os
    path=__file__.rsplit('/',3)[0]
    print 'LM .... looking for libs in path : ', path
    if os.path.exists(path+'/InDetUpgradePerformanceAnalysis.so'):
        gSystem.Load(path+'/InDetUpgradePerformanceAnalysis.so')
        print "Loaded from "+path++'/InDetUpgradePerformanceAnalysis.so'
    else:
        # Try loading from RootCore
        print "Loading RootCore packages"
        from ROOT import gROOT
        gROOT.ProcessLine (".x $ROOTCOREDIR/scripts/load_packages.C")

from ROOT import TreeHouse,SingleAnalysis, Observable
from ROOT import Cuts, AllCuts, MinCut, MaxCut, AbsSelectorWithIndex, RangeCut, RangeCutWithIndex, MinimumHitsCut, RangeProductCut, RangeDependentMinCut
from ROOT import DumpSensorInfo
from ROOT import Efficiency, FakeRate, ObservableGroup, Binning, TrackRatio
from ROOT import SCTDiskOccupancy, SCTBarrelOccupancy, PixelBarrelOccupancy,  PixelDiskOccupancy, Occupancy2D, PixelOccupancy2D, SCTBarrelPerEventOccupancy, PixelBarrelPerEventOccupancy, PixelDiskPerEventOccupancy

from helper import *

# C++ enums don't seem to work in (Py)ROOT6
#CONTROL=ROOT.UpgPerfAna.CONTROL
#EVENT=ROOT.UpgPerfAna.EVENT
CONTROL=2
EVENT=5

import PyStandardCuts
