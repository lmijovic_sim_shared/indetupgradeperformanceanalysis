from geometry import AtlasDetector


from ROOT import SensorInfoBase
import math

class SiliconObjectBase(SensorInfoBase):
    def __init__(self):
        SensorInfoBase.__init__(self)
    
    def getDict(self):
        ll={}
        for k in self.res:
            ll[k]=getattr(self,k)
        return ll

    def __repr__(self):
        return self.name+str(self.getDict())

class SiliconObject(SiliconObjectBase):
    def __init__(self,name,idname,**kwargs):
        SiliconObjectBase.__init__(self)
        self.name=name
        print "in SiliconObject c'tor for ",name
        
        if kwargs.has_key('atlas') and kwargs.has_key(idname) and kwargs.has_key('tag'):
            eargs={'tag' : kwargs['tag'], idname : kwargs[idname], 'atlas' : kwargs['atlas'] }
            if kwargs.has_key('stripDoubleLength'):
                eargs.update({'stripDoubleLength' : kwargs['stripDoubleLength']})
            dd=self.getDimensions(**eargs)
            print "finished getting dimensions for SiliconObject:"
            print dd
            
        else:
            print 'WARNING something missing in SiliconObject',name
            for k in ['atlas',idname,'tag']:
                if not kwags.has_key(k):
                    print 'argument',k,'missing'
            dd=kwargs

        for k,v in dd.iteritems():
            setattr(self,k,v)
            #print 'add',k,v
            if isinstance(v,SensorInfoBase):
                print 'SensorInfoFound'
            elif isinstance(v,list):
                #print 'found list'
                for i,e in enumerate(v):
                    import re
                    print "i: ",i,", e: ",e
                    m=re.search('(\w+)\[([0-9]+)\]',e.tag)
                    if m is not None:
                        key=m.group(1)
                        val=int(m.group(2))
                        SensorInfoBase.addSensor(self,key,e,val)
            elif isinstance(v,int):
                SensorInfoBase.add(self,k,v)
            elif isinstance(v,float):
                SensorInfoBase.addFloat(self,k,v)

class Layer(SiliconObject):
    def __init__(self,**kwargs):
        super(Layer,self).__init__('Layer','layer_id',**kwargs)

        self.res=['tag','phi','eta','row','col','sector','nmodule',
                  'total','perModule','perModuleEta','chiplength','chipwidth','area']
            
        self.total=self._total()
        self.perModule=self._perModule()
        self.perModuleEta=self._perModuleEta()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)

    def _perModule(self):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self):
        return self._perModule()*self.sector

    def _total(self):
        return self._perModuleEta()*self.nmodule

    def getDimensions(self,tag='layer1',layer_id=1,atlas=None):

        print "in getDimensions"
        
        dim={}

        dim['tag']=tag

        # stave info
        print "getting pixelstaves"
        pixStaves=atlas['PixelStaves']
        print "pixStaves: ",pixStaves
        stave=pixStaves.select('PixelStave',Layer=layer_id)
        print "stave: ",stave
        dim['nmodule']=stave['NBarrelModules']
        staveLength=stave['StaveSupportLength']
        staveType=stave['Type']
        moduleType=stave['BarrelModuleType']
        moduleGap=stave['BarrelModuleGap']
        #moduleAngle=stave['BarrelModuleAngle'] <== commented this out ... 
        #moduleAngle=stave['MountainModuleAngle'] #<=== ?
        print '.... ok done with stave'
        
        # layer info
        pixLayers=atlas['PixelBarrelLayers']
        print "getting layer"
        layer=pixLayers.select('PixelBarrelLayer',Number=layer_id)
        print "layer: ",layer
        staveType=layer['StaveType']
        dim['sector']=layer['NStaves']

        print "mType: ",staveType,", sector: ",dim['sector']
        
        #staveTilt=layer['StaveTilt']
        #radius=layer['LayerRadius']

        # module and chip info
        modules=atlas['PixelModules']
        module=modules.select('Module',moduleName=moduleType)
        lengthInChips=module['lengthInChips']
        widthMaxInChips=module['widthMaxInChips']
        #sensorThickness=module['sensorThickness']
        #chipThickness=module['chipThickness']
        #hybridThickness=module['hybridThickness']
        chipName=module['chipName']

        # chip info
        chip=modules.select('FrontEndChip',Name=chipName)
        self.chiplength=chip['chipLength']
        self.chipwidth=chip['chipWidth']
        
        print "chip length/width: ",self.chiplength,", ",self.chipwidth

        self.area=self.chiplength*self.chipwidth
        print "area: ",self.area

        dim['row']=chip['rows']
        dim['phi']=lengthInChips
        dim['col']=chip['columns']
        dim['eta']=widthMaxInChips

        print "row/phi/col/eta: ",dim['row'],",",dim['phi'],",",dim['col'],",",dim['eta']
        return dim

#http://atlas.web.cern.ch/Atlas/GROUPS/OPERATIONS/dataBases/DDDB/tag_hierarchy_browser.php


class DiskRing(SiliconObjectBase):
    """ sensor info of a ring of a pixel disk"""
    def __init__(self,tag,disk_id,ring_id,atlas):
        SiliconObjectBase.__init__(self)
        self.name="Ring"
        self.res=['tag','phi','eta','row','col','ring_id','ringType',
                  'total','perModule','nmodule','chiplength','chipwidth','area']

        self.tag=tag
        self.disk_id=disk_id
        self.ring_id=ring_id
        self.atlas=atlas

        print "DiskRing"
        
        pixEndcap=atlas['PixelEndcapLayers']
        pixRings=pixEndcap.select('PixelEndcapRing',LayerNumber=disk_id)
        self.ringType=pixRings['RingModuleType']
        radiusType=pixRings['RadiusType'] #Inner
        ringModuleType=pixRings['RingModuleType'] #FourChip_RD53
        self.nmodule=pixRings['NumberOfSectors'] #36
        
        # modules
        modules=atlas['PixelModules']

        # module info
        module=modules.select('Module',moduleName=ringModuleType)
        self.phi=module['lengthInChips']
        self.eta=module['widthMaxInChips']
        chipName=module['chipName']
        
        # chip info
        chip=modules.select('FrontEndChip',Name=chipName)
        self.chiplength=chip['chipLength']
        self.chipwidth=chip['chipWidth']
        self.area=self.chiplength*self.chipwidth
        
        self.row=chip['rows']
        self.col=chip['columns']

        self.total=self._total()
        self.perModule=self._perModule()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)



    def nmodule_phi_row(self):
        return self.nmodule*self.phi*self.row

    def _perModule(self):
        return self.phi*self.eta*self.col*self.row        

    def _total(self):
        return self._perModule()*self.nmodule        


class Disk(SiliconObject):
    """Pixel Disk sensor infos"""
    def __init__(self,**kwargs):
        self.rings=[]
        self.nring=0
        self.sector=0
        super(Disk,self).__init__('Disk','disk_id',**kwargs)
        self.res=['tag','disk_id','rings','nring', 'sector', 'total']
        
        self.total=self._total()

    def _perModule(self,ring_id):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self,ring_id):
        return self._perModule(ring_id)*self.sector

    def _total(self):
        return sum([ r.total for r in self.rings])

    def getDimensions(self,tag='disk1',disk_id=1,atlas=None):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag
        dim['disk_id']=disk_id
        
        pixEndcap=atlas['PixelEndcapLayers']
        print "getting PixelEndcapLayer ",disk_id
        disk=pixEndcap.select('PixelEndcapRing',LayerNumber=disk_id)
        print "disk: ",disk

        dim['nring']=len(pixEndcap['RingPositions'])
        dim['sector']=disk['NumberOfSectors']
        print "sector: ",dim['sector']
        
        dim['rings']=[ DiskRing('ring[%d]'%i,disk_id,i,atlas) for i in range(dim['nring']) ]
        return dim


class SctLayer(SiliconObject):
    def __init__(self,**kwargs):
        super(SctLayer,self).__init__('SctLayer','layer_id',**kwargs)

        self.res=['tag','nstrip','nsegment','nwafer','nski','nmodule',
                  'total','perModule','perModuleEta','doubleSided','area','striplength','width']
            
        self.total=self._total()
        self.perModule=self._perModule()
        self.perModuleEta=self._perModuleEta()

        print "total: ",self.total
        print "perModule: ",self.perModule
        print "perModuleEta: ",self.perModuleEta
        
        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)


    def _perModule(self):
        return self.nstrip*self.nwafer

    def _perModuleEta(self):
        fac=1
        if self.doubleSided==1:
            #print "Double Sided changed factor"
            fac=2
        return fac*self._perModule()*self.nski

    def _total(self):
        return self._perModuleEta()*self.nmodule*self.nsegment

    @classmethod 
    def getDimensions(cls,tag='layer1',layer_id=1,atlas=None,stripDoubleLength=False):
        dim={}

        dim['tag']=tag

        # stave info
        print "getting sct staves"
        sctStaves=atlas['SCTStaves']
        stave=sctStaves.select('SCTStave',Layer=layer_id)
        print "stave: ",stave
        dim['doubleSided']=stave['DoubleSided']
        dim['nmodule']=stave['NBarrelModules']
        lType=stave['BarrelModuleType']
    
        # barrel info
        print "getting sct barrel layers"
        sctLayers=atlas['SCTBarrelLayers']
        layer=sctLayers.select('SCTBarrelLayer',Number=layer_id)
        print "layer: ",layer
        dim['nski']=layer['NStaves']

        # modules
        modules=atlas['SCTModules']

        # module info
        module=modules.select('Module',moduleName=lType)
        dim['nsegment']=module['lengthInChips']
        mType=module['chipName']
        
        # chip info
        chip=modules.select('FrontEndChip',Name=mType)
        dim['striplength']=chip['chipLength']
        dim['width']=chip['chipWidth']
        dim['area']=dim['striplength']*dim['width']
        dim['nwafer']=chip['rows']
        dim['nstrip']=chip['columns']
        if dim['nstrip']==-99: dim['nstrip']=1280

        return dim

class SctDiskRing(SiliconObjectBase):
    def __init__(self,tag,disk_id,ring_id,atlas):
        SiliconObjectBase.__init__(self)
        self.name="Ring"
        self.res=['nstrip','ring_id','ringType',
                  'total','perModule','nmodule','doubleSided',
                  'wmin','wmax','rmin','rmax','rrmin','rrmax','length','area']

        self.ring_id=ring_id
        self.tag=tag

        sctEndcap=atlas['SCTEndcapLayers']
        disk=sctEndcap.select('SCTEndcapDisc',DiscNumber=disk_id)

        self.ringType=disk['RingModuleType'][ring_id] #StripEC0_dev0
        self.nmodule=disk['NumberOfSectors'][ring_id]

        self.doubleSided=disk['DoubleSided']

        radius=disk['Radius'][ring_id]
        radiusType=disk['RadiusType']

        # modules
        modules=atlas['SCTModules']
        module=modules.select('Module',moduleName=self.ringType)
        lengthInChips=module['lengthInChips']
        chipName=module['chipName']
        widthMinInChips=module['widthMinInChips']
        widthMaxInChips=module['widthMaxInChips']
        
        # chip info
        chip=modules.select('FrontEndChip',Name=self.ringType)
        chipWidth=chip['chipWidth']
        columns=chip['columns']
        edge=chip['Edges']
        edgel=edge['inlength']
        edgew=edge['wide']
        
        moduleLength=lengthInChips*chip['chipLength']
        edgeLength=edgel
        length=moduleLength + 2.*edgeLength
        
        if widthMinInChips==0:
            widthMinInChips=widthMaxInChips
            
        widthmin=widthMinInChips*chipWidth + edgew
        widthmax=widthmin
        
        if chipName=="RD53": # copied this from XMLReaderSvc
            edgeLength=float(lengthInChips-1)*edgel
            length=moduleLength + float(lengthInChips-1)*2*edgel
            widthmax=widthmax + float(widthMaxInChips-1)*2.*edgew
            widthmin=widthmax
        else:
            widthmin=widthmin+edgew
            widthmax=widthmax+edgew
        self.wmin=widthmin
        self.wmax=widthmax
        
        if radiusType=="Inner":
            self.rrmin=radius
            self.rrmax=radius+moduleLength
        else:
            self.rrmax=radius
            self.rrmin=radius-moduleLength

        self.rmin=self.rrmin-edgeLength
        self.rmax=self.rrmax+edgeLength
        self.length=length
        
        self.area=math.atan2(self.wmax/2.,self.rmax)*(self.rmax**2-self.rmin**2)

        self.nstrip=columns
        if columns==-99:
            self.nstrip=1280 # I made this up
            
        self.perModule=self._perModule()
        
        self.total=self._total()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)

    def _perModule(self):
        return self.nstrip

    def _total(self):
        fac=1
        if self.doubleSided==1:
            fac=2
            #print "Doublesided changed factor"
        return fac*self._perModule()*self.nmodule   

class SctDisk(SiliconObject):
    def __init__(self,**kwargs):
        self.rings=[]
        self.nring=0
        self.sector=0
        super(SctDisk,self).__init__('SctDisk','disk_id',**kwargs)
        # calling getDimension
        
    def _perModule(self,ring_id):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self,ring_id):
        return self._perModule(ring_id)*self.sector

    def _total(self):
        return sum([ r.total for r in self.rings])

    def getDimensions(self,tag='disk1',disk_id=1,atlas=None):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag
        dim['disk_id']=disk_id

        print "getting SCT endcap"
        sctEndcap=atlas['SCTEndcapLayers']
        disk=sctEndcap.select('SCTEndcapDisc',DiscNumber=disk_id)

        ringPositions=disk['RingPositions']
        
        dim['nring']=len(ringPositions)

        dim['rings']=[ SctDiskRing('ring[%d]'%i,disk_id,i,atlas) for i in range(dim['nring']) ]

        self.res=['tag','disk_id','rings','nring', 'sector', 'total']
                            
        self.total=self._total()

        dim['total']=self.total
        return dim


class SensorInfo(SensorInfoBase):
    def __init__(self):
        SensorInfoBase.__init__(self)
        self.collection={}

    def add(self,layer,value=None):
        if not isinstance(layer,SensorInfoBase):
            if value is None:
                print 'WARNING got key',layer,'but no value'
            if type(value)==int :
                SensorInfoBase.add(self,layer,value)
            elif type(value)==float :
                SensorInfoBase.addFloat(self,layer,value)
            else :
                print 'WARNING got key',layer,'but value neither int or float'
            return

        import re
        m=re.search('(\w+)\[([0-9]+)\]',layer.tag)
        if m is not None:
            self._addArray(layer,m)
        else:
            self._add(layer)

    def _addArray(self,layer,m):
        #print 'addSensor',(self,layer.tag,layer)
        # decode
        key=m.group(1)
        val=int(m.group(2))
        SensorInfoBase.addSensor(self,key,layer,val)
        # create
        if not self.collection.has_key(key):
            a=[]
            self.collection[key]=a
            setattr(self,key,a)
        a=self.collection[key]
        # check length
        if val>=len(a):
            for i in range(len(a),val+1):
                a.append(None)
        # check exists
        if a[val] is None:
            a[val]=layer
        else:
            print 'ERROR',layer.tag,'already registered'
             
    def _add(self,layer):
        SensorInfoBase.addSensor(self,key,layer,0)
        if self.collection.has_key(layer.tag):
            print 'ERROR',layer.tag,'already registered'
        self.collection[layer.tag]=layer
        setattr(self,layer.tag,layer)
        #print 'adding ..'
        #print layer

def getSensorInfo(layout, stripDoubleLength=False):

    sensor=SensorInfo()

    # **************
    #  Pixel Barrel
    # **************
    print "getting PixelBarrel layout"
    pixBarrel=layout['PixelBarrelLayers']
    nLayer = len(pixBarrel.getAllEntries('PixelBarrelLayer'))
    print "nLayer=",nLayer

    sensor.add('npixlayer',nLayer)

    print "pix BARREL summary"
    for i in range(nLayer):
        l=Layer(tag='pixlayer[%d]'%i,layer_id=i,atlas=layout)
        print l
        sensor.add(l )

    # *************
    #  Pixel Disks
    # *************
    print "getting PixelEndcapLayers"
    pixEndcap=layout['PixelEndcapLayers']
    print "pixEndcap: ",pixEndcap
    
    nDisk= len(pixEndcap.getAllEntries('PixelEndcapRing'))
    print 'nDisk',nDisk

    disks=[]
    sensor.add('npixdisk',nDisk)
    for i in range(nDisk):
        print "getting disk for ",i
        d=Disk(tag='pixdisk[%d]'%i,disk_id=i,atlas=layout)
        print "have disk"
        sensor.add(d )
        print "added disk to sensor"
        disks.append(d)
        print "added disk to disks"

    print "pix DISKS summary"
    for d in disks:
        for r in d.rings:
            print r


    # **************
    #  SCT Barrel
    # **************
    print "getting SCTBarrel layout"
    sctBarrel=layout['SCTBarrelLayers']
    nLayer = len(sctBarrel.getAllEntries('SCTBarrelLayer'))
    print 'nLayer',nLayer
    sensor.add('nsctlayer',nLayer) #rdh used by SCTBarrelOccupancy

    print "sct BARREL summary"
    for i in range(nLayer):
        l=SctLayer(tag='sctlayer[%d]'%i,layer_id=i,atlas=layout, stripDoubleLength=stripDoubleLength)
        print l
        sensor.add(l)

    # **************
    #  SCT Disks
    # **************

    print "getting SCTDisks layout"
    sctEndcap=layout['SCTEndcapLayers']
    nDisk = len(sctEndcap.getAllEntries('SCTEndcapDisc'))
    print 'nDisk',nDisk
    sensor.add('nsctdisk',nDisk)

    disks=[]
    for i in range(nDisk):
        d=SctDisk(tag='sctdisk[%d]'%i,disk_id=i,atlas=layout)
        print d
        sensor.add(d )
        disks.append(d)

    print "DISKS summary"
    #for d in disks:
    for r in disks[0].rings:
        print r

    return sensor

