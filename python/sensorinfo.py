from geometry import AtlasDetector


from ROOT import SensorInfoBase
import math

class SiliconObjectBase(SensorInfoBase):
    def __init__(self):
        SensorInfoBase.__init__(self)
    
    def getDict(self):
        ll={}
        for k in self.res:
            ll[k]=getattr(self,k)
        return ll

    def __repr__(self):
        return self.name+str(self.getDict())

class SiliconObject(SiliconObjectBase):
    def __init__(self,name,idname,**kwargs):
        SiliconObjectBase.__init__(self)
        self.name=name
        if kwargs.has_key('atlas') and kwargs.has_key(idname) and kwargs.has_key('tag'):
            eargs={'tag' : kwargs['tag'], idname : kwargs[idname], 'atlas' : kwargs['atlas'] }
            if kwargs.has_key('stripDoubleLength'):
                eargs.update({'stripDoubleLength' : kwargs['stripDoubleLength']})
            dd=self.getDimensions(**eargs)
        else:
            print 'WARNING something missing in SiliconObject',name
            for k in ['atlas',idname,'tag']:
                if not kwags.has_key(k):
                    print 'argument',k,'missing'
            dd=kwargs
        for k,v in dd.iteritems():
            setattr(self,k,v)
            #print 'add',k,v
            if isinstance(v,SensorInfoBase):
                print 'SensorInfoFound'
            elif isinstance(v,list):
                #print 'found list'
                for i,e in enumerate(v):
                    import re
                    m=re.search('(\w+)\[([0-9]+)\]',e.tag)
                    if m is not None:
                        key=m.group(1)
                        val=int(m.group(2))
                        SensorInfoBase.addSensor(self,key,e,val)
            elif isinstance(v,int):
                SensorInfoBase.add(self,k,v)
            elif isinstance(v,float):
                SensorInfoBase.addFloat(self,k,v)

class Layer(SiliconObject):
    def __init__(self,**kwargs):
        super(Layer,self).__init__('Layer','layer_id',**kwargs)

        self.res=['tag','phi','eta','row','col','sector','nmodule',
                  'total','perModule','perModuleEta','chiplength','chipwidth','area']
            
        self.total=self._total()
        self.perModule=self._perModule()
        self.perModuleEta=self._perModuleEta()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)

    def _perModule(self):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self):
        return self._perModule()*self.sector

    def _total(self):
        return self._perModuleEta()*self.nmodule

    def getDimensions(self,tag='layer1',layer_id=1,atlas=None):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag

        pixLayer=atlas['PixelLayer']
        pixLayer.select(layer=layer_id)
        mType =pixLayer['MODULETYPE']  # 0,2,1,1 !!!
        idStave = pixLayer['STAVEINDEX']
        dim['sector']=pixLayer['nsectors']
        
        pixModule=atlas['PixelModule']
        pixModule.select(DESIGNTYPE=mType)
        # The length (in X/phi) and width (in Y/eta) of the whole module
        # corresponds to two or four ASICs
        self.chiplength=pixModule['CHIPLENGTH']
        self.chipwidth=pixModule['CHIPWIDTH']
        self.area=self.chiplength*self.chipwidth


        pixStave=atlas['PixelStave']
        #pixStave.select(PIXELSTAVE_DATA_ID=7+idStave)  # *AS* ???  (utopia 7,8,9 ; cartigny 20,21,22,23 )  STAVEINDEX
        pixStave.select(ID=idStave)
        sLayout=pixStave['LAYOUT']  # 3,3,3,3
        dim['nmodule']=pixStave['nmodule']

        #pixDisk=atlas['PixelDisk']

        pixRO=atlas['PixelReadout']
        #print pixRO.fetchData()
        #pixRO.select(PIXELREADOUT_DATA_ID=5+mType)  # *AS* ???     (utopia 5,6 ; cartigny 8,9,10,11)
        pixRO.select(ID=mType)
        dim['row']=pixRO['ROWSPERCHIP']
        dim['phi']=pixRO['NCHIPSPHI']
        dim['col']=pixRO['COLSPERCHIP']
        dim['eta']=pixRO['NCHIPSETA']

        return dim

#http://atlas.web.cern.ch/Atlas/GROUPS/OPERATIONS/dataBases/DDDB/tag_hierarchy_browser.php


class DiskRing(SiliconObjectBase):
    """ sensor info of a ring of a pixel disk"""
    def __init__(self,tag,disk_id,ring_id,atlas):
        SiliconObjectBase.__init__(self)
        self.name="Ring"
        self.res=['tag','phi','eta','row','col','ring_id','ringType',
                  'total','perModule','nmodule','chiplength','chipwidth','area']

        self.tag=tag
        self.disk_id=disk_id
        self.ring_id=ring_id
        self.atlas=atlas

        pixDiskRing=atlas['PixelDiskRing']
        pixDiskRing.select(disk=disk_id,ring=ring_id)
        self.ringType=pixDiskRing['ringtype']
        
        pixRing=atlas['PixelRing']
        #pixRing.select(PIXELRING_DATA_ID=self.ringType)
        pixRing.select(ID=self.ringType)
        self.nmodule=pixRing['NMODULE']
        mType=pixRing['MODULETYPE']
        
        pixModule=atlas['PixelModule']
        pixModule.select(DESIGNTYPE=mType)
        self.chiplength=pixModule['CHIPLENGTH']
        self.chipwidth=pixModule['CHIPWIDTH']
        self.area=self.chiplength*self.chipwidth
        
        #pixModule.select(MODULETYPE=mType)

        pixRO=atlas['PixelReadout']
        #pixRO.select(PIXELREADOUT_DATA_ID=5+mType)  # *AS* ???     (utopia 5,6 ; cartigny 8,9,10,11)
        pixRO.select(ID=mType)
        self.row=pixRO['ROWSPERCHIP']
        self.phi=pixRO['NCHIPSPHI']
        self.col=pixRO['COLSPERCHIP']
        self.eta=pixRO['NCHIPSETA']

        self.total=self._total()
        self.perModule=self._perModule()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)



    def nmodule_phi_row(self):
        return self.nmodule*self.phi*self.row

    def _perModule(self):
        return self.phi*self.eta*self.col*self.row        

    def _total(self):
        return self._perModule()*self.nmodule        


class Disk(SiliconObject):
    """Pixel Disk sensor infos"""
    def __init__(self,**kwargs):
        self.rings=[]
        self.nring=0
        self.sector=0
        super(Disk,self).__init__('Disk','disk_id',**kwargs)
        self.res=['tag','disk_id','rings','nring', 'sector', 'total']
        
        self.total=self._total()

    def _perModule(self,ring_id):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self,ring_id):
        return self._perModule(ring_id)*self.sector

    def _total(self):
        return sum([ r.total for r in self.rings])

    def getDimensions(self,tag='disk1',disk_id=1,atlas=None):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag
        dim['disk_id']=disk_id

        pixDisk=atlas['PixelDisk']
        pixDisk.select(disk=disk_id)
        dim['nring']=pixDisk['NRINGS']
        dim['sector']=pixDisk['NSECTORS']  # *AS* needed ?

        dim['rings']=[ DiskRing('ring[%d]'%i,disk_id,i,atlas) for i in range(dim['nring']) ]
        return dim


class SctLayer(SiliconObject):
    def __init__(self,**kwargs):
        super(SctLayer,self).__init__('SctLayer','layer_id',**kwargs)

        self.res=['tag','nstrip','nsegment','nwafer','nski','nmodule',
                  'total','perModule','perModuleEta','doubleSided','area','striplength','width']
            
        self.total=self._total()
        self.perModule=self._perModule()
        self.perModuleEta=self._perModuleEta()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)


    def _perModule(self):
        return self.nstrip*self.nwafer

    def _perModuleEta(self):
        fac=1
        if self.doubleSided==1:
            #print "Double Sided changed factor"
            fac=2
        return fac*self._perModule()*self.nski

    def _total(self):
        return self._perModuleEta()*self.nmodule*self.nsegment

    @classmethod 
    def getDimensions(cls,tag='layer1',layer_id=1,atlas=None,stripDoubleLength=False):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag
        sctLayer=atlas['SSctBrlLayer']
        #sctLayer.select(SSCTBRLLAYER_DATA_ID=layer_id)  # *AS* (utopia 0,..,4)  cartigny 5,...,10
        sctLayer.select(ID=layer_id)
        
        lType=sctLayer['LADDERTYPE']  # 0,1
        dim['nski']=sctLayer['SKISPERLAYER']
        dim['doubleSided']=sctLayer['DOUBLESIDED']

        sctLadder=atlas['SSctBrlLadder']
        #sctLadder.select(SSCTBRLLADDER_DATA_ID=lType)   # *AS* (utopia 0,1)
        sctLadder.select(ID=lType) 
        mType=sctLadder['MODTYPE']
        dim['nmodule']=sctLadder['NUMPERLADDER']

        sctModule=atlas['SSctBrlModule']
        #sctModule.select(SSCTBRLMODULE_DATA_ID=mType)  # *AS* (utopia 0,1)
        sctModule.select(ID=mType)  

        sctSensor=atlas['SSctBrlSensor']
        #sctSensor.select(SSCTBRLSENSOR_DATA_ID=mType)  # *AS* (utopia 0,1)
        sctSensor.select(ID=mType)  
        dim['nwafer']=sctSensor['NUMBEROFWAFERS']
        dim['nstrip']=sctSensor['NUMREADOUTSTRIPS']
        dim['nsegment']=sctSensor['NUMSEGMENTS']

        dim['striplength']=sctSensor['STRIPLENGTH']
        dim['width']=sctSensor['WIDTH']
        dim['area']=dim['striplength']*dim['width']
        
 
        return dim

class SctDiskRing(SiliconObjectBase):
    def __init__(self,tag,disk_id,ring_id,atlas):
        SiliconObjectBase.__init__(self)
        self.name="Ring"
        self.res=['nstrip','ring_id','ringType',
                  'total','perModule','nmodule','doubleSided',
                  'wmin','wmax','rmin','rmax','rrmin','rrmax','length','area']

        self.ring_id=ring_id
        self.tag=tag

        sctRingMap=atlas['SSctFwdWheelRingMap']
        sctRingMap.select(WHEELNUM=disk_id,RINGNUM=ring_id)
        self.ringType=sctRingMap['RINGTYPE']

        sctRing=atlas['SSctFwdRing']
        #sctRing.select(SSCTFWDRING_DATA_ID=self.ringType)   # *AS* utopia 0,..,15
        sctRing.select(ID=self.ringType)

        self.nmodule=sctRing['NUMMODULES'] 
        mType= sctRing['MODULETYPE']                # *AS* utopia 0,..,15
        self.doubleSided= sctRing['DOUBLESIDED']
        self.rrmin=sctRing['INNERRADIUS']
        self.rrmax=sctRing['OUTERRADIUS']

        
        

        sctModule=atlas['SSctFwdModule']
        #sctModule.select(SSCTFWDMODULE_DATA_ID=mType)  # *AS* utopia 0,..,15
        sctModule.select(ID=mType)  

        sctSensor=atlas['SSctFwdSensor']
        #sctSensor.select(SSCTFWDSENSOR_DATA_ID=mType)
        sctSensor.select(ID=mType)
        self.nstrip=sctSensor['NUMREADOUTSTRIPS']
        self.rmin=sctSensor['INNERRADIUS']
        self.rmax=sctSensor['OUTERRADIUS']
        self.wmin=sctSensor['INNERWIDTH']
        self.wmax=sctSensor['OUTERWIDTH']
        self.length=sctSensor['LENGTH']

        self.area=math.atan2(self.wmax/2.,self.rmax)*(self.rmax**2-self.rmin**2)

        self.perModule=self._perModule()
        
        self.total=self._total()

        for key in self.res:
            val=getattr(self,key)
            if isinstance(val,int):
                SensorInfoBase.add(self,key,val)
            if isinstance(val,float):
                SensorInfoBase.addFloat(self,key,val)

    def _perModule(self):
        return self.nstrip

    def _total(self):
        fac=1
        if self.doubleSided==1:
            fac=2
            #print "Doublesided changed factor"
        return fac*self._perModule()*self.nmodule   

class SctDisk(SiliconObject):
    def __init__(self,**kwargs):
        self.rings=[]
        self.nring=0
        self.sector=0
        super(SctDisk,self).__init__('SctDisk','disk_id',**kwargs)
        # calling getDimension
        
    def _perModule(self,ring_id):
        return self.phi*self.eta*self.col*self.row

    def _perModuleEta(self,ring_id):
        return self._perModule(ring_id)*self.sector

    def _total(self):
        return sum([ r.total for r in self.rings])

    def getDimensions(self,tag='disk1',disk_id=1,atlas=None):
        #AtlasDetector('ATLAS-SLHC-01-02-01') ):
        dim={}

        dim['tag']=tag
        dim['disk_id']=disk_id

        sctDisk=atlas['SSctFwdWheel']
        #sctDisk.select(SSCTFWDWHEEL_DATA_ID=disk_id)   # *AS* utopia 0,..,4
        sctDisk.select(ID=disk_id)   
        dim['nring']=sctDisk['NUMRINGS']

        dim['rings']=[ SctDiskRing('ring[%d]'%i,disk_id,i,atlas) for i in range(dim['nring']) ]

        self.res=['tag','disk_id','rings','nring', 'sector', 'total']
                            
        self.total=self._total()

        dim['total']=self.total
        return dim


class SensorInfo(SensorInfoBase):
    def __init__(self):
        SensorInfoBase.__init__(self)
        self.collection={}

    def add(self,layer,value=None):
        if not isinstance(layer,SensorInfoBase):
            if value is None:
                print 'WARNING got key',layer,'but no value'
            if type(value)==int :
                SensorInfoBase.add(self,layer,value)
            elif type(value)==float :
                SensorInfoBase.addFloat(self,layer,value)
            else :
                print 'WARNING got key',layer,'but value neither int or float'
            return

        import re
        m=re.search('(\w+)\[([0-9]+)\]',layer.tag)
        if m is not None:
            self._addArray(layer,m)
        else:
            self._add(layer)

    def _addArray(self,layer,m):
        #print 'addSensor',(self,layer.tag,layer)
        # decode
        key=m.group(1)
        val=int(m.group(2))
        SensorInfoBase.addSensor(self,key,layer,val)
        # create
        if not self.collection.has_key(key):
            a=[]
            self.collection[key]=a
            setattr(self,key,a)
        a=self.collection[key]
        # check length
        if val>=len(a):
            for i in range(len(a),val+1):
                a.append(None)
        # check exists
        if a[val] is None:
            a[val]=layer
        else:
            print 'ERROR',layer.tag,'already registered'
             
    def _add(self,layer):
        SensorInfoBase.addSensor(self,key,layer,0)
        if self.collection.has_key(layer.tag):
            print 'ERROR',layer.tag,'already registered'
        self.collection[layer.tag]=layer
        setattr(self,layer.tag,layer)
        #print 'adding ..'
        #print layer

def getSensorInfo(layout, stripDoubleLength=False):

    sensor=SensorInfo()

    # **************
    #  Pixel Barrel
    # **************
    pixBarrel=layout['PixelBarrelGeneral']
    nLayer = pixBarrel['NLAYER']
    sensor.add('npixlayer',nLayer)
    print 'nLayer',nLayer

    print "pix BARREL summary"
    for i in range(nLayer):
        l=Layer(tag='pixlayer[%d]'%i,layer_id=i,atlas=layout)
        print l
        sensor.add(l )

    # *************
    #  Pixel Disks
    # *************
    pixDisks=layout['PixelEndcapGeneral']
    nDisk= pixDisks['NDISK']
    print 'nDisk',nDisk

    disks=[]
    sensor.add('npixdisk',nDisk)
    for i in range(nDisk):
        d=Disk(tag='pixdisk[%d]'%i,disk_id=i,atlas=layout)
        sensor.add(d )
        disks.append(d)

    print "pix DISKS summary"
    for d in disks:
        print d
        for r in d.rings:
            print r


    # **************
    #  SCT Barrel
    # **************
    sctBarrel=layout['SSctBrlGeneral']
    nLayer = sctBarrel['NUMLAYERS']
    print 'nLayer',nLayer
    sensor.add('nsctlayer',nLayer)

    print "sct BARREL summary"
    for i in range(nLayer):
        l=SctLayer(tag='sctlayer[%d]'%i,layer_id=i,atlas=layout, stripDoubleLength=stripDoubleLength)
        print l
        sensor.add(l)

    # **************
    #  SCT Disks
    # **************

    sctDisks=layout['SSctFwdGeneral']
    nDisk = sctDisks['NUMWHEELS']
    print 'nDisk',nDisk
    sensor.add('nsctdisk',nDisk)

    disks=[]
    for i in range(nDisk):
        d=SctDisk(tag='sctdisk[%d]'%i,disk_id=i,atlas=layout)
        sensor.add(d )
        disks.append(d)

    print "DISKS summary"
    #for d in disks:
    for r in disks[0].rings:
        print r

    return sensor

