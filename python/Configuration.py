import optparse, glob, os

usage = """%prog [options]

'InDetUpgradePerformanceAnalysis

One must either specify an input file directly via "--infile '<filename>'", where <filename> can be a wildcard string, or as a text file containing
a list of input files with "--fileList <flist,txt>", or alternatively
one can use a text file containing a list of single particle samples, along with the pileup and pdgId for each sample. An example of such a
textfile is in share/samples.txt, which points to samples on the EOS group area. To use the text file, one must specify 
"--samplesFile <samplesTextFile> --pdgId <pdgId> --pileup <pileup>". If specifying the input file directly, the pdgId flag is still used
to determine which particles to produce the resolution/efficiency plots for, and the pileup flag may be used to label the output file.

Example plot generation, specifying input file directly:
'%prog --infile="/afs/cern.ch/user/n/nedwards/work/data/sim/user.silviam.group.det-slhc.mc11_14TeV.4907443.multi_muons.reco.ESD.
LowPtMinbiasOnly_pileup_23.val2v.110923140607_vder1317307621/*root" --outfilelabel=slhc01 --pdgId 13 --maxEvents 1000'
Example plot generation, using samples text file:
'%prog --samplesFile=../share/samples.txt --outfilelabel=slhc01 --pdgId 13 --pileup 23 --maxEvents 1000'

"""

optP = optparse.OptionParser(usage=usage,conflict_handler="resolve")

optP.add_option('--infile', action='store', dest='infile',  default='',
                type='string',    help='Input file. Wildcards accepted')
optP.add_option('--outfilelabel', action='store', dest='outfilelabel',
                default='', type='string', help='output file name label - will have the pdgID and .root automatically attached to it  (defaut:output)')
optP.add_option('--maxEvents', action='store', dest='maxEvents',  default=-1,
                type='int',    help='limit analysis to a given number of events')
optP.add_option('--pdgId', action='store', dest='pdgId',  default=0,
                type='int',    help='pdgId of particles to make plots for')
optP.add_option('--minProb', action='store', dest='minProb',  default=0.5,
                type='string',    help='Minimum probability for matching truth particle to reco particle.')
optP.add_option('--dbfile', action='store', dest='dbfile',  default='/afs/cern.ch/atlas/project/database/DBREL/packaging/DBRelease/current/geomDB/geomDB_sqlite',
                type='string',    help='DB file')
optP.add_option('--pileup', action='store', dest='pileup',  default=0,
                type='int',    help='For reading from text file - select pileup level')
optP.add_option('--samplesFile', action='store', dest='samplesFile',  default='',
                type='string',    help='Samples text file')
optP.add_option('--fileList', action='store', dest='fileList',  default='',
                type='string',    help='List of files in text file')
optP.add_option('--treename', action='store', dest='treename',  default='InDetTrackTree',
                type='string',    help='Tree name')
optP.add_option('--layout', action='store', dest='layout',  default='loi',
                type='string',    help='Layout. Currently only used when applying variable eta cuts')

def dbfile():

    # parse options
    options,args = optP.parse_args()

    return options.dbfile

def GetOptions(occupancies=False):

    # parse options
    options,args = optP.parse_args()

    if occupancies:
        options.outfile = "occupancies"
        if options.outfilelabel: options.outfile += "_" + options.outfilelabel
        options.outfile += ".root"
    else:
        # Set output file name
        options.outfile = "InDetUpPerfPlots"
        if options.outfilelabel: options.outfile += "_" + options.outfilelabel
        if options.pdgId: options.outfile +="_pdgId" + str(options.pdgId)
        if options.pileup: options.outfile +="_pileup" + str(options.pileup)
        if options.layout: options.outfile += "_"+str(options.layout)
        options.outfile += ".root"

    # Check compatible options set
    if options.infile and options.samplesFile:
        optP.print_help()
        raise RuntimeError("Can not specify --infile and --samplesFile: please specify only one")

    # Check compatible options set
    if options.infile and options.fileList:
        optP.print_help()
        raise RuntimeError("Can not specify --infile and --fileList: please specify only one")

    # Check compatible options set
    if options.samplesFile and options.fileList:
        optP.print_help()
        raise RuntimeError("Can not specify --samplesFile and --fileList: please specify only one")

    # MAke list of inoput files
    if options.infile:
        print "Globbing for input files using "+options.infile
        options.inputfiles = glob.glob(os.path.expanduser(options.infile))
        print "Found files: "
        for name in options.inputfiles:
            print name
    elif options.samplesFile:
        if not options.pdgId:
            optP.print_help()
            raise RuntimeError("Need to specify --pdgId if using --samplesFile")
        if not options.pileup:
            optP.print_help()
            raise RuntimeError("Need to specify --pileup if using --samplesFile")
        import ParseSamples
        samples = ParseSamples.ParseSamples(options.samplesFile)
        options.inputfiles = samples.getFiles(options.pdgId, options.pileup)
    elif options.fileList:
        fileList = file(options.fileList)
        if not fileList:
            raise RunTimeError("Cannot open file "+options.fileList)
        options.inputfiles = [ line.strip() for line in fileList.readlines() ]
    else:
        optP.print_help()
        raise RuntimeError("Must specify either --samplesFile or --infile")

    if not len(options.inputfiles):
        raise RuntimeError("No input files found!")

    pdgId_labels = {11: "Electron", 13: "Muon", 211: "Pion", 0:""}
    if options.pdgId in pdgId_labels.keys():
        options.pdgId_label = pdgId_labels[options.pdgId]
    else:
        options.pdgId_label = ""

    return options
