import ROOT as R
from InDetUpgradePerformanceAnalysis import AllCuts, MinCut, RangeCut, MaxCut, AbsSelectorWithIndex, RangeCutWithIndex, AbsSelector, RangeDependentMinCut
from InDetUpgradePerformanceAnalysis import ObservableGroup, CustomBinning, Efficiency, TrackRatio, Binning, FakeRate, DiffObservable, PullObservable, SingleObservable
from InDetUpgradePerformanceAnalysis import vector_float, CONTROL


# Define the basic track cuts. These are extended by TrackResolutionCuts
class BasicTrackCutsVariableEtaHits(AllCuts):
    def __init__(self, set="loi"):
        AllCuts.__init__(self)
        self.Add(MinCut("trk_pt", 1000))  # MeV
        self.Add(RangeCut("trk_d0_wrtBS",-2.0,2.0)) # mm
        self.Add(RangeCut("trk_z0_wrtBS",-250.0,250.0)) # mm
        #self.Add(MinimumHitsCut(11,"trk_nPixHits","trk_nSCTHits"))
        self.Add(RangeCut("trk_eta",-2.7,2.7))
        #self.Add(MinCut("trk_nAllHits",11))
        self.Add(MaxCut("trk_nPixHoles",2))
        eta_bins = [ 0, 1.1, 1.3, 1.4, 1.7, 2.2, 2.4]
        if set == "loi":
            cuts = [11, 11, 12, 12, 14, 12, 10]
        elif set == "new":
            cuts = [10, 10, 11, 12, 14, 12, 10]
        elif set == "veto":
            cuts = [8, 9, 10, 11, 14, 12, 10]
        else:
            raise RuntimeError("Did not understand set=="+set)
        hits_cut = RangeDependentMinCut(float, float)(
            "trk_nAllHits", "trk_eta", 
            vector_float(eta_bins), vector_float(cuts)
        )
        print "Using layout "+set+" for variable eta cuts"
        print cuts
        self.Add(hits_cut)

class TrackCutsForHitsPlots(AllCuts):
    def __init__(self, prob=0.5, set="loi"):
        AllCuts.__init__(self)
        self.Add(MinCut("trk_pt", 1000))  # MeV
        self.Add(RangeCut("trk_d0_wrtBS",-2.0,2.0)) # mm
        self.Add(RangeCut("trk_z0_wrtBS",-250.0,250.0)) # mm
        self.Add(RangeCut("trk_eta",-2.7,2.7))
        # Must be matched to a charged MC truth particle 
        self.Add(MinCut("trk_mc_index",0))
        # Cut on track match probability
        self.Add(MinCut("trk_mc_probability", prob))

        
# Cuts applied to truth particles considered for tracking efficiency plots for ttbar samples
class EfficiencyTruthParticleCutsTtbar(AllCuts):
    def __init__(self):
        AllCuts.__init__(self)
        self.Add(MinCut("mc_gen_pt", 1000))
        self.Add(RangeCut("mc_gen_barcode",0,200000))
        self.Add(RangeCut("mc_gen_eta",-2.7,2.7))
        self.Add(RangeCut("mc_perigee_d0",-2.0,2.0)) # mm
        self.Add(RangeCut("mc_perigee_z0",-250.0,250.0)) # mm
        self.Add(AbsSelector("mc_charge",1)) 

# Cuts applied to tracks entering the resolution plots
class TrackResolutionCuts(AllCuts):
    def __init__(self,  prob, trackPtCut=True):
        AllCuts.__init__(self)
        # Apply all the basic track cuts 
        self.Add(BasicTrackCutsVariableEtaHits())
        # Must be matched to an MC truth particle 
        self.Add(MinCut("trk_mc_index",0))
        # Cut on track match probability
        self.Add(MinCut("trk_mc_probability", prob))

# Definition of a set of efficiency observables
class TtbarInclusiveFakeRateObservables(TrackRatio):
    def __init__(self, title, extended_eta=False, layout="loi"):
        TrackRatio.__init__(self, EfficiencyTruthParticleCutsTtbar(), BasicTrackCutsVariableEtaHits(layout))
        #, BasicTrackCutsVariableEtaHits())
        #self.SetPath(path)

# Definition of a set of fake rate plots
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class TtbarExclusiveFakeRateObservables(ObservableGroup):
    def __init__(self, title, extended_eta=False, layout="loi"):
        ObservableGroup.__init__(self,title)
        self.AddStepVeto(CONTROL)
        #self.SetPath("ttbar")
        self.SetCuts(BasicTrackCutsVariableEtaHits(layout))
        binning = Binning("trk_eta", 27,0,2.7, "eta", "#eta", True)
        fr = FakeRate(binning)
        self.Add(fr)

# Definition of a set of efficiency observables
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class TtbarEfficiencyObservables(ObservableGroup):
    def __init__(self, title, extended_eta=False, layout="loi"):
        ObservableGroup.__init__(self,title)
        self.AddStepVeto(CONTROL)
        #self.SetPath(ttbar)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCutsTtbar())
        binning = Binning("mc_gen_eta", 27,0,2.7, "eta", "#eta", True)
        eff=Efficiency( binning )
        #eff=Efficiency( binning )
        # Only count tracks passing quality cuts: d0/z0/pt/nHits   
        eff.SetCuts(BasicTrackCutsVariableEtaHits(layout))
        self.Add(eff)

# Hit "resolution" obeservables - used for calculating the means and widths of the nHits distributions in bins
# Needs to be sepearate as dont apply track hits cuts
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class TtbarHitsResolutionObservables(ObservableGroup):
    def __init__(self, title, prob, charge=None, extended_eta=False):
        ObservableGroup.__init__(self,title)
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinning()
        self.SetCuts(TrackCutsForHitsPlots(prob))
        ## Hits
        self.Add(SingleObservable( "trk_nAllHits", binning))
        self.Add(SingleObservable( "trk_nAllHoles", binning))
        self.Add(SingleObservable( "trk_nSCTHits", binning))
        self.Add(SingleObservable( "trk_nSCTHoles", binning))
        self.Add(SingleObservable( "trk_nPixHits", binning))
        self.Add(SingleObservable( "trk_nPixHoles", binning))

# Definition of a set of resolution observables, but with Gaussian fitting
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class TtbarResolutionObservables(ObservableGroup):
    def __init__(self, title, prob, charge=None,fit=True, extended_eta=False):
        ObservableGroup.__init__(self,title)
        # Use either a gaussian fit or the RMS to get the resolutions
        if fit:
            DiffObs = DiffObservableFitted
            PullObs = PullObservableFitted
        else:
            DiffObs = DiffObservable
            PullObs = PullObservable
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinning()
        # Set cuts
        # Apply the TrackResolutionCuts defined above
        self.SetCuts(TrackResolutionCuts(prob))
        # Resolutions
        self.Add(DiffObs( "trk_d0","mc_perigee_d0", binning))
        self.Add(DiffObs( "trk_z0","mc_perigee_z0", binning))
        self.Add(DiffObs( "trk_phi","mc_perigee_phi", binning))
        self.Add(DiffObs( "trk_pt","mc_gen_pt", binning))
        self.Add(DiffObs( "trk_qoverp","mc_perigee_qoverp", binning))
        #self.Add(DiffObs( "trk_qoverpt","mc_qoverpt", binning))
        self.Add(DiffObs( "trk_theta","mc_perigee_theta", binning))
        #self.Add(DiffObs( "trk_z0SinTheta","mc_z0SinTheta", binning))
        ## Pulls
        self.Add(PullObs( "trk_d0","trk_d0_err", "mc_perigee_d0", binning))
        self.Add(PullObs( "trk_z0","trk_z0_err", "mc_perigee_z0", binning))
        self.Add(PullObs( "trk_theta","trk_theta_err", "mc_perigee_theta", binning))
        self.Add(PullObs( "trk_phi","trk_phi_err", "mc_perigee_phi", binning))
