import ROOT

_RAnalysis=ROOT.InDetUpgradeAnalysis

rana=None

class InDetUpgradeAnalysis(_RAnalysis):
    def __init__(self):
        global rana
        _RAnalysis.__init__(self)
        print 'set analysis'
        rana=self

def _check_branch_type(cut_class, iArg=0):
    def ret_fun(*args):
            
        global rana
        t=rana.GetChain()
        if iArg != -1:
            var=args[iArg]
        else:
            for i in range(len(args)):
                if type(args[i]) == type("s"):
                    var=args[i]
                    break

        b=t.GetBranch(var)
        #print '_check_branch_type', var,b

        if b == None:
            #print 'checking friend'
            f=t.GetFriend('friend')
            b=f.GetBranch(var)
            #print '_check_branch_type',var,b
        if b == None:
            raise RuntimeError( 'ERROR did not manage to derive type from ',args,'assuming float')
            cls= float
        else:
            cls_name=b.GetClassName()
            if cls_name=='vector<float>':
                cls=float
            elif cls_name=='vector<int>':
                cls=int
            elif cls_name=='vector<double>':
                cls=ROOT.Double
            else:
                print "ERROR: Do not understand branch type: ", cls_name, " for branch ", var
                cls = float
        return cut_class(cls)(*args)
    return ret_fun
    
def vector_float( l ):
    vec = ROOT.std.vector(float)()
    [ vec.push_back(f) for f in l ]
    return vec

Selector=_check_branch_type(ROOT.AbsSelector)
AbsSelector=_check_branch_type(ROOT.AbsSelector)
MinCut=_check_branch_type(ROOT.MinCut)
MaxCut=_check_branch_type(ROOT.MaxCut)
RangeCut=_check_branch_type(ROOT.RangeCut)
RangeProductCut=_check_branch_type(ROOT.RangeProductCut)

Distribution=_check_branch_type(ROOT.Distribution)
DiffObservable=_check_branch_type(ROOT.DiffObservable)
PullObservable=_check_branch_type(ROOT.PullObservable)
SingleObservable=_check_branch_type(ROOT.SingleObservable)
Z0SinThetaObs=_check_branch_type(ROOT.Z0SinThetaObs)
QOverPtObs=_check_branch_type(ROOT.QOverPtObs)
Addition=_check_branch_type(ROOT.Addition,1)
DerivedBranch=ROOT.DerivedBranch(float)
#DerivedBranch=_check_branch_type(ROOT.DerivedBranch,1)

