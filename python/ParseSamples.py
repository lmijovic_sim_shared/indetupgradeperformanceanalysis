
class ParseSamples:
    def __init__(self, fname = 'samples.txt'):
        self.fname=fname
        #self.eoscmd='/afs/cern.ch/project/eos/installation/0.1.0-22d/bin/eos.select'
        # NE - Point to latest ATLAS release of EOS
        self.eoscmd='/afs/cern.ch/project/eos/installation/atlas/bin/eos.select'
        self.eospref='root://eosatlas/'
        self.eospath='/eos/atlas/atlasgroupdisk/det-slhc/'
        self.castorcmd='nsls'
        self.castorpref='root://castoratlas/'
        self.castorpath='/castor/cern.ch/grid/atlas/atlasgroupdisk/det-slhc/'
        self.pattern='D3PD'
        self.dlist=[]
        self.parse()

    def parse(self):
        f = file(self.fname)
        for l in f:
            #print l,
            if l.startswith('#') or len(l)==0:
                continue
            parts=l.split()
            if len(parts)>=3:
                pdg = int(parts[0])
                pileup = int(parts[1])
                dataset = parts[2]
                self.dlist.append( (pdg,pileup,dataset) )

    def getFiles(self,pdgCode,pileupLevel):
        res=[]
        for pdg,pileup,dataset in self.dlist:
            if pdgCode==pdg and pileupLevel==pileup:
                print dataset
                res+=self.readDir(dataset)
        #for l in res:
        #    print l
        print 'found',len(res),'files'
        return res

    def readLocal(self,path):
        print path

        from subprocess import Popen,PIPE
        # check for 'eos' command (macro)
        p=Popen("ls "+path,shell=True,stdout=PIPE)
        infiles=p.stdout.readlines()
        p.wait()
        if p.returncode!=0:
            print 'ERROR reading directory',path

        res=[]
        for l in infiles:
            if self.pattern!=None and l.find(self.pattern)>=0:
                res.append(path+'/'+l.strip())
        print 'found',len(res),'files'
        return res
            

    def readDir(self,dset):
        pref=self.eospref
        path=self.eospath+'/'.join(dset.split('.',3)[:3])+'/'+dset
        print path
        
        from subprocess import Popen,PIPE
        # check for 'eos' command (macro)
        p=Popen(self.eoscmd+" ls "+path,shell=True,stdout=PIPE)
        infiles=p.stdout.readlines()
        p.wait()
        if p.returncode!=0:
            print 'failed eos access try castor'
            pref=self.castorpref
            path=self.castorpath+'/'.join(dset.split('.',3)[:3])+'/'+dset
            print path
            p=Popen(self.castorcmd+" "+path,shell=True,stdout=PIPE)
            infiles=p.stdout.readlines()
            p.wait()
            if p.returncode!=0:
                path='/tmp/nstyles/'+dset
                pref=''
                print path
                p=Popen("ls "+path,shell=True,stdout=PIPE)
                infiles=p.stdout.readlines()
            print "return code",p.returncode
        
        res=[]
        for l in infiles:
            if self.pattern!=None and l.find(self.pattern)>=0:
                res.append(pref+path+'/'+l.strip())
        print 'found',len(res),'files'
        return res

        
    
if __name__ == "__main__":
    parser = ParseSamples()
    parser.getFiles(11,161)
