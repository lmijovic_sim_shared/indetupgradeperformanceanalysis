#!/usr/bin/env python

from InDetUpgradePerformanceAnalysis.PlotMaker import *
import ROOT as R
R.gROOT.SetBatch()

import optparse, glob, os
usage = "Script for making plot for the IDR"
optP = optparse.OptionParser(usage=usage,conflict_handler="resolve")
#optP.add_option('--infilelabel', action='store', dest='infilelabel',
#default='InDetUpPerfPlots', type='string', help='input file name label - should be the same as specified in --outfilelabel in upg_idr_analyze.py')
#optP.add_option('--layout', action='store', dest='layout',
#default='ITk LoI', type='string', help='Name of the layout - used on plot label')
optP.add_option('--extendedEta', action='store_true', dest='extended_eta',
default=False, help='Use extended eta range upt to |eta|<4 - for VF layouts')
optP.add_option('--outlabel', action='store', dest='outlabel',
                default='', type='string', help='')
optP.add_option('--preliminary', action='store_true', dest='preliminary',
                default=False, help='Add Preliminary label (rather than internal)')
(options, args) = optP.parse_args()

print options

pdgId_labels = {11: "Electron", 13: "Muon", 211: "Pion"}
#pdgId_style = {11: 20, 13: 21, 211: 22}
#pdgId_color = {11: R.kGreen, 13: R.kBlue, 211: R.kRed}
pdgId_yrange = {11: (0.7, 1.05), 13: (0.8, 1.05), 211: (0.7, 1.05) }

#mu_style = {0: 20, 80: 21, 140: 22, 200:23}
#mu_color = {0: R.kBlack, 80:R.kGreen, 140: R.kBlue, 200: R.kRed}

def main():
    # define layouts
    layouts = []
    if options.extended_eta: layouts.append( Layout( "LoI-VF",  "LOIVF.vJuly21_v3.pilup140.010914_tweakFitStartParams", R.kBlue, 23, 32, extended_eta=True) )
    layouts.append( Layout( "LoI",  "LOI.v0.pilup140.250814_tripleGuass", R.kBlack, 20, 24) )
    layouts.append( Layout( "livEC02",  "livEC02.v3.pilup140.250814_tripleGuass", R.kRed, 22, 26) )
    
    # Make plots
    make_comparison_plots( layouts, 140, 13)
    #make_comparison_plots( layouts, 140, 11)


def make_comparison_plots( layouts, mu, pdgId ):

    pm = PlotMaker()

    for layout in layouts:
        infile = get_filename(layout.tag, pdgId)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        path_5 = get_dir(5)
        path_100 =get_dir(100)
        if layout.extended_eta:
            path_5 += "_extendedEta"
            path_100 +=  "_extendedEta"
        pm.AddPath(infile, path_5,  legend= get_legend( layout.name, 5, pdgId, mu),  linecolor=layout.color, markerstyle=layout.marker2, xerr=False, linestyle=R.kSolid)
        pm.AddPath(infile, path_100, legend= get_legend( layout.name, 100, pdgId, mu),  linecolor=layout.color, markerstyle=layout.marker, xerr=False, linestyle=R.kDashed, )

    add_graphs( pm, pdgId)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel(preliminary=options.preliminary)
    pm.MakeParticleLabel("%ss, <#mu>=%i" % ( pdgId_labels[pdgId], mu))

    # save plots into files
    tag = options.outlabel
    directory = 'plots/layout_comparison/%s/%s' % (tag, pdgId_labels[pdgId])
    pm.SavePlots(directory)

class Layout:
    def __init__( self, name, tag, color, marker, marker2=1, extended_eta=False ):
        self.name = name
        self.tag = tag
        self.color = color
        self.marker = marker
        self.marker2 = marker2
        self.extended_eta = extended_eta

def get_dir(pt):
    dir = "pt%i" % (pt,)
    #if options.extended_eta: 
        #dir += "_extendedEta"
    return dir

def get_filename(tag, pdgId):
    filename = "%s_pdgId%i.root" % (tag, pdgId)
    return filename

def get_legend(layout, pt, pdgId, mu):
    return '%s layout, p_{T}=%i GeV' % (layout, pt, )
#return '%s layout, p_{T}=%i GeV %ss, <#mu>=%i' % (layout, pt, pdgId_labels[pdgId], mu)

def add_graphs(pm, pdgId):

    binning = "etaSingleSided"
    binning_eff = "etaSingleSidedMC"

    # Resolutions
    pm.AddGraph('trk_d0_mc_perigee_d0_res_'+binning)
    pm.AddGraph('trk_z0_mc_perigee_z0_res_'+binning, )
    pm.AddGraph('trk_theta_mc_perigee_theta_res_'+binning, legendloc='topright')
    pm.AddGraph('trk_phi_mc_perigee_phi_res_'+binning, )
    pm.AddGraph('trk_qoverpt_mc_qoverpt_res_'+binning, ytitle='p_{T} #times #sigma q/p_{T}', legendloc='topleft', scale=True)

    # Hits n Holes
    #pm.AddGraph('trk_nAllHits_bias_'+binning, legendloc='topleft', ytitle='<No. Track Hits>')
    #pm.AddGraph('trk_nAllHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Track Holes>')
    #pm.AddGraph('trk_nPixHits_bias_'+binning, legendloc='topleft', ytitle='<No. Pixel Hits>')
    #pm.AddGraph('trk_nPixHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Pixel Holes>')
    #pm.AddGraph('trk_nSCTHits_bias_'+binning, legendloc='topleft', ytitle='<No. SCT Hits>')
    #pm.AddGraph('trk_nSCTHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. SCT Holes>')

    # Efficiencies
    (ymin, ymax) = pdgId_yrange[pdgId]
    pm.AddGraph('efficiencyPlot_'+binning_eff, ytitle="Efficiency", legendloc='bottomleft', ymin =ymin, ymax=ymax)

if __name__ == "__main__":
    main()
