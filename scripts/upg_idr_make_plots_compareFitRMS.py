#!/usr/bin/env python

from InDetUpgradePerformanceAnalysis.PlotMaker import *
import ROOT as R
R.gROOT.SetBatch()

import optparse, glob, os
usage = "Script for making plot for the IDR"
optP = optparse.OptionParser(usage=usage,conflict_handler="resolve")
optP.add_option('--infilelabel', action='store', dest='infilelabel',
                default='InDetUpPerfPlots', type='string', help='input file name label - should be the same as specified in --outfilelabel in upg_idr_analyze.py')
optP.add_option('--outlabel', action='store', dest='outlabel',
                default='', type='string', help='')
optP.add_option('--layout', action='store', dest='layout',
                default='ITk LoI', type='string', help='Name of the layout - used on plot label')
optP.add_option('--extendedEta', action='store_true', dest='extended_eta',
                default=False, help='Use extended eta range upt to |eta|<4 - for VF layouts')
(options, args) = optP.parse_args()

print options

pdgId_labels = {11: "Electron", 13: "Muon", 211: "Pion"}
pdgId_style = {11: 20, 13: 21, 211: 22}
pdgId_color = {11: R.kGreen, 13: R.kBlue, 211: R.kRed}
pdgId_yrange = {11: (0.7, 1.05), 13: (0.8, 1.06), 211: (0.7, 1.05) }

def get_dir(pt, rms=False):
    dir = "pt%i" % (pt,)
    if options.extended_eta: 
        dir += "_extendedEta"
    if rms: 
        dir += "_rms"
    return dir

def get_filename(pdgId):
    filename = "%s_pdgId%i.root" % (options.infilelabel, pdgId)
    return filename

def get_legend(pt, pdgId, mu):
    return 'p_{T}=%i GeV %ss' % (pt, pdgId_labels[pdgId], )
    #return 'p_{T}=%i GeV %ss, <#mu>=%i' % (pt, pdgId_labels[pdgId], mu)

def make_plots(pdgId, mu, ):

    pm = PlotMaker()

    infile = get_filename(pdgId)

    if not os.path.exists(infile):
        print ""
        print "WARNING: Cannot find file: "+infile
        print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
        print ""
        return

    pm.AddPath(infile, "pt5",  legend="pt=5 Fitted",  linecolor=R.kBlue, markerstyle=23, xerr=False, pt=5e3)
    pm.AddPath(infile, "pt5_rms",  legend="pt=5 RMS",  linecolor=R.kBlue, markerstyle=23, xerr=False, pt=5e3, linestyle=R.kDashed)
    pm.AddPath(infile, get_dir(15), legend= get_legend( 15, pdgId, mu),  linecolor=R.kRed, markerstyle=22, xerr=False, pt=15e3)
    pm.AddPath(infile, get_dir(15,rms=True),  linecolor=R.kRed, markerstyle=22, xerr=False, pt=15e3, linestyle=R.kDashed)
    pm.AddPath(infile, get_dir(50), legend= get_legend( 50, pdgId, mu),  linecolor=R.kGreen, markerstyle=21, xerr=False, pt=50e3)
    pm.AddPath(infile, get_dir(50, rms=True),   linecolor=R.kGreen, markerstyle=21, xerr=False, linestyle=R.kDashed, pt=50e3)
    pm.AddPath(infile, get_dir(100),legend= get_legend(100, pdgId, mu),  linecolor=R.kBlack, markerstyle=20, xerr=False, pt=100e3)
    pm.AddPath(infile, get_dir(100,rms=True),   linecolor=R.kBlack, markerstyle=20, xerr=False,  linestyle=R.kDashed, pt=100e3)

    add_graphs(pm, pdgId)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel()
    pm.MakeLayoutLabel( options.layout )

    # save plots into files
    tag =options.infilelabel
    if options.outlabel: tag += "_" + options.outlabel
    directory = 'plots/%s/%s' % (tag, pdgId_labels[pdgId])
    os.system("mkdir -p %s" % directory)
    pm.SavePlots(directory)

def add_graphs(pm, pdgId):

    binning = "etaSingleSided"
    binning_eff = "etaSingleSidedMC"

    # Resolutions
    pm.AddGraph('trk_d0_mc_perigee_d0_res_'+binning)
    pm.AddGraph('trk_z0_mc_perigee_z0_res_'+binning, )
    pm.AddGraph('trk_theta_mc_perigee_theta_res_'+binning, legendloc='topright')
    pm.AddGraph('trk_phi_mc_perigee_phi_res_'+binning, )
    pm.AddGraph('trk_qoverpt_mc_qoverpt_res_'+binning, ytitle='p_{T} #times #sigma q/p_{T}', legendloc='topleft',)
    #pm.AddGraph('trk_qoverpt_mc_qoverpt_res_'+binning, ytitle='p_{T} #times #sigma q/p_{T}', legendloc='topleft', scale=True)

    # Hits n Holes
    pm.AddGraph('trk_nAllHits_bias_'+binning, legendloc='topleft', ytitle='<No. Track Hits>')
    pm.AddGraph('trk_nAllHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Track Holes>')
    pm.AddGraph('trk_nPixHits_bias_'+binning, legendloc='topleft', ytitle='<No. Pixel Hits>')
    pm.AddGraph('trk_nPixHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Pixel Holes>')
    pm.AddGraph('trk_nSCTHits_bias_'+binning, legendloc='topleft', ytitle='<No. SCT Hits>')
    pm.AddGraph('trk_nSCTHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. SCT Holes>')

    # Efficiencies
    (ymin, ymax) = pdgId_yrange[pdgId]
    #pm.AddGraph('efficiencyPlot_'+binning_eff, ytitle="Efficiency", legendloc='bottomleft', ymin =ymin, ymax=ymax)

def make_comparison_plots( mu, ):

    pm = PlotMaker()

    for pdgId in [11, 13, 211]:
        infile = get_filename(pdgId)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        pm.AddPath(infile, get_dir(5),  legend= get_legend(  5, pdgId, mu),  linecolor=pdgId_color[pdgId], markerstyle=pdgId_style[pdgId], xerr=False, linestyle=R.kSolid, pt=5)
        pm.AddPath(infile, get_dir(100),legend= get_legend(100, pdgId, mu),  linecolor=pdgId_color[pdgId], markerstyle=pdgId_style[pdgId], xerr=False, linestyle=R.kDashed, pt=100)

    add_graphs( pm, 11)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel()
    pm.MakeLayoutLabel( options.layout )

    # save plots into files
    directory = 'plotsTest/%s/%s' % (options.infilelabel, "comparison")
    os.system("mkdir -p %s" % directory)
    pm.SavePlots(directory)

def main():
    make_plots(11, 140)
    make_plots(13, 140)
    make_plots(211, 140)
    #make_comparison_plots( 140)

if __name__ == "__main__":
    main()
