#!/usr/bin/env python
import ROOT as R
import SetPalette
import AtlasStyle
import AtlasUtil
from sys import argv
import os

R.gROOT.SetBatch()

if len(argv)>=2:
    infile=argv[1]
else:
    print "Usage: upg_make_occ_plots.py <infile> "
    os.sys.exit()

rf = R.TFile(infile)

#plots = [ "occupancy_maps/id_occ_map2d", ]
plots = [
 "pixel_occ_map2d_detail",
 "pixel_flu_map2d_detail",
 "id_occ_map2d",
 "id_flu_map2d",
]

p = SetPalette.SetPalette()
p.set_palette(ncontours=20)

for plotname in plots:
    plot = rf.Get("occupancy_maps/"+plotname)

    c = R.TCanvas()
    c.SetRightMargin(0.18)
    c.SetLeftMargin(0.15)
    plot.Draw("COLZ")

    y_label = 0.88
    AtlasUtil.DrawText(0.18,y_label, "ITk LoI Layout, #mu=200");
    AtlasUtil.AtlasLabel(0.52,y_label, simulation=True)

    t = R.TLatex()
    t.SetTextAngle(90)
    t.SetNDC()
    if "occ" in plotname:
        t.DrawLatex(0.97, 0.4, "Sensor Hit Occupancy %")
    elif "flu" in plotname:
        t.DrawLatex(0.97, 0.4, "Hits /mm^{2}")
    elif "clus" in plotname:
        t.DrawLatex(0.97, 0.4, "Clusters (summed over phi)")
    R.gPad.Update()
    R.gPad.Modified()

    c.SaveAs("plots/"+plotname+".eps")
    c.SaveAs("plots/"+plotname+".png")

    if "detail" in plotname or "pixel" in plotname:
        continue

    plot.GetYaxis().SetRangeUser(350, 1100)

    plot.Draw("COLZ")


    y_label = 0.87
    AtlasUtil.DrawText(0.18,y_label, "ITk LoI Layout, #mu=200");
    AtlasUtil.AtlasLabel(0.52,y_label, simulation=True)

    t = R.TLatex()
    t.SetTextAngle(90)
    t.SetNDC()
    if "occ" in plotname:
        t.DrawLatex(0.97, 0.4, "Sensor Hit Occupancy %")
    elif "flu" in plotname:
        t.DrawLatex(0.97, 0.4, "Hits /mm^{2}")
    elif "clus" in plotname:
        t.DrawLatex(0.97, 0.4, "Clusters (summed over phi)")

    R.gPad.Update()
    R.gPad.Modified()

    c.SaveAs("plots/"+plotname.replace("id_","strip_")+".eps")
    c.SaveAs("plots/"+plotname.replace("id_","strip_")+".png")

    #print plotname
    #for iBin in range( plot.GetNbinsX()*plot.GetNbinsY() ):
    #    content = plot.GetBinContent(iBin)
    #    if content:
    #        print "%5i : %.5f" % (iBin, content)
