#!/usr/bin/env python

import sys, glob, os

usage = """Script for making  plots for the scoping documen with flat in eta hits cuts\n
    upg_scoping_plot_flatCuts.py <tag> [<public=True>]
where <tag> should match the outfilelabel argument passed to upg_scoping_flatCuts.py"""

# Option parsing
if len(sys.argv)<2:
    print usage
    sys.exit()
tag = sys.argv[1]
if len(sys.argv)>2: public=bool(int(sys.argv[2]))
else: public=False

print "public=",public

from InDetUpgradePerformanceAnalysis.PlotMaker import *
from InDetUpgradePerformanceAnalysis.ScopingPlotting import *
import ROOT as R
R.gROOT.SetBatch()


def make_hits_comparison_plots( layout, nHoles=1 ):

    pm = PlotMaker()

    infile = get_filename(layout, tag)
    for nHits in [ 7, 8, 9, 10, 11, 12]:
        #for nHits in [ 7, 8, 9, 10, 11]:
        if layout=="VF_Gold" and nHits<9: continue

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            #print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        path = "Zmumu_"+str(nHits)+"hits_"
        if nHoles>-1: holes_cut = "{0}holes".format(nHoles)
        else: holes_cut = "noHolesCut"
        path += holes_cut
        #if "VF" in layout: path+= "_extendedEta"
        print path
        xmax = 2.7
        if "VF_Silver" in layout: xmax = 3.2
        elif "VF_Gold" in layout: xmax = 4.0
        xmax=4.0
        pm.AddPath(infile, path,  legend= "#geq "+str(nHits)+" Hits" ,
                   linecolor=hits_color[nHits], linestyle=hits_linestyle[nHits], markerstyle=hits_style[nHits], 
                   kill_beyond=xmax)

    ymin, ymax = layout_ranges[layout]
    if "Bronze" in layout:
        loc_eff = "bottomright3"
        loc_fr = "right2c"
    elif "VF_Silver_m10" in layout:
        loc_eff = "bottomleft"
        loc_fr = "bottommid"
    elif "VF_Gold_m10" in layout:
        loc_eff = "bottomleft"
        loc_fr = "right2c"
    elif "VF_Gold" in layout:
        loc_eff = "bottomleft"
        loc_fr = "right2c"
    elif "Silver" in layout:
        loc_eff = "bottomright3"
        loc_fr = "right2c"
    else:
        loc_eff = "bottomright"
        loc_fr = "right2c"
    if "Bronze" in layout:
        xrange = (0,2.7)
        kill_below=0
    elif "Silver" in layout:
        xrange = (0,3.2)
        kill_below=0
    else:
        xrange = (0,4.0)
        kill_below=0
    # Hack for markus
    xrange = (0,4.0)
    kill_below=0
    pm.AddGraph('efficiencyPlot_etaSingleSidedMC', ytitle="Efficiency", legendloc=loc_eff, ymin=ymin, ymax=ymax, xrange=xrange, kill_below=kill_below, xerr=False)
    pm.AddGraph('efficiencyPlot_varEtaSingleSidedMC', ytitle="Efficiency", legendloc=loc_eff, ymin=ymin, ymax=ymax, xrange=xrange, kill_below=kill_below, xerr=True, nolines=False)
    pm.AddGraph('frPlot_etaSingleSided', ytitle="Fake-Rate", legendloc=loc_fr, ymin =1e-5, ymax=5, log=True, xrange=xrange, xerr=False)
    pm.AddGraph('frPlot_varEtaSingleSided', ytitle="Fake-Rate", legendloc=loc_fr, ymin =1e-5, ymax=5, log=True, xrange=xrange, xerr=True, nolines=False)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel(preliminary=False, public=public)
    #pm.MakeLayoutLabel( get_legend(layout) + ", Prompt Muons" )
    pm.MakeLayoutLabel( get_legend(layout) +", Z#rightarrow#mu#mu, Primary #mu p_{T}>4 GeV, <#mu>=190-210" )
    #pm.MakeLayoutLabel( "Z#mu#mu, Primary #mu p_{T}>4 GeV, <#mu>=190-210" )

    # save plots into files
    #tag = "Zmumu_optHits"
    directory = 'plots/%s_%s_%s' % (tag, holes_cut, layout)
    if public: directory+= "_public"
    pm.SavePlots(directory)


def make_material_comparison( path, label ):

    pmEff = PlotMaker()
    pmFr = PlotMaker()

    for layout in [ "VF_Gold", "Gold", ]:
        infile = get_filename(layout, tag)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            #print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        _path = path
        if "VF" in layout: _path += "_extendedEta"

        if layout=="VF_Gold":
            leg = "Reference (no services)"
        else:
            leg = "Reference (with services)"
        xmax = 2.5
        pmEff.AddPath(infile, _path,  legend= leg,
                linecolor=pdgId_color[layout], linestyle=pdgId_linestyle[layout], markerstyle=pdgId_style[layout], 
                xerr=False, kill_beyond=xmax )
        pmEff.AddRatio("Services/No Services")
        pmFr.AddPath(infile, _path,  legend= leg,
                linecolor=pdgId_color[layout], linestyle=pdgId_linestyle[layout], markerstyle=pdgId_style[layout], 
                xerr=False, kill_beyond=xmax )
        pmFr.AddRatio("Services/No Services")

   #xrange=(0,2.5), 
    pmEff.AddGraph('efficiencyPlot_varEtaSingleSidedMC', ytitle="Efficiency", legendloc='ratiomid', ymin =0.8, ymax=1.1, )
    pmFr.AddGraph('frPlot_varEtaSingleSided', ytitle="Mis-reconstructed track fraction", legendloc='ratiomid', ymin =0.00005, ymax=0.05, log=True, xrange=(0,2.5), kill_beyond=2.5)
    pmFr.AddGraph('genTracks_etaSingleSidedMC', ytitle="No, truth muons", legendloc='ratiomid', xrange=(0,2.5), kill_beyond=2.5)
    ##add_graphs( pm )
    binning = "etaSingleSided"

    directory = 'plots/%s/%s' % (tag+"_"+path, "material_comparison")
    if public: directory+= "_public"

    # create plots
    for pm in [pmEff, pmFr]:
        pm.MakePlots( )
        pm.MakeLegend()
        pm.MakeAtlasLabel(preliminary=preliminary)
        if pm==pmFr:
            pm.MakeLayoutLabel( "<#mu>=190-210"+label )
        else:
            pm.MakeLayoutLabel( "Z#mu#mu, Primary #mu p_{T}>4 GeV, <#mu>=190-210"+label )
        pm.SavePlots(directory)

def main():
    for nHits in [1]:
        make_hits_comparison_plots( "VF_Gold", nHits)
        make_hits_comparison_plots( "VF_Gold_m10", nHits )
        #make_hits_comparison_plots( "VF_Silver", nHits )
        #make_hits_comparison_plots( "VF_Silver_m10", nHits )
        make_hits_comparison_plots( "Silver", nHits )
        make_hits_comparison_plots( "Silver_m10", nHits )
        make_hits_comparison_plots( "Bronze", nHits)
        make_hits_comparison_plots( "Bronze_m10", nHits)
        
    #make_material_comparison( "Zmumu_11hits_1holes", "" )


if __name__ == "__main__":
    main()
