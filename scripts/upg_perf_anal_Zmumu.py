#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, Addition, DerivedBranch

# Import cut definitions
# see python/StandardCuts.py
from InDetUpgradePerformanceAnalysis.StandardCuts import *

# Import custom binning definitions
# see python/CustomBinning.py
# By default, all resoltuion/efficiency plots are binned as a function
# of |eta| with 10 bins from 0->2.5
# Alternative options are defined in python/CustomBinning.py
import InDetUpgradePerformanceAnalysis.CustomBinning

# Import observable definitions - define the things we want to plot
# (reolsutions, efficiencies etc)
# see python/StandardObservableDefinitions.py
from InDetUpgradePerformanceAnalysis.StandardObservableDefinitions import *

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranches

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

pdgId=13
folder = "Zmumu"
label = "Zmumu; prompt muons only"
outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

# Set up a "SingleAnalysis" job. Each SingleAnalysis job corresponds
# to one run of the event loop. We add spepcific instances of the observable classes
# defined in python/StandardObservableDefinitions.py e..g with different pT cuts
sa_EfficiencyFakesHits=SingleAnalysis("EfficienciesFakesHits",TreeHouse.Current())
# "SingleAnalysis" job for the resolutions observables. For the resolution
# observables it is necessary to run the event loop twice, the first time to determine the RMS
# of the distributions so that in a second pass outliers can be excluded.
# This means we need to make a new SingleAnalysis, and add an extra "step" called CONTROL
sa_Resolutions=SingleAnalysis("Resolutions",TreeHouse.Current())
sa_Resolutions.AddStep(InDetUpgradePerformanceAnalysis.CONTROL)
# Add derived branches to calculate e.g. the nAllHoles / nAllHits variables
# These are used in cuts so have to add them before anything else
sa_EfficiencyFakesHits.Add(DerivedBranches())


# Resolution observables using RMS to get resolution 
sa_Resolutions.Add(ResolutionObservables(pdgId, None, minProb, folder, label, fit=False, minPt=4e3))
sa_Resolutions.Add(MomentumResolutionObservables(pdgId,None, minProb, folder, label, fit=False, minPt=4e3))
sa_Resolutions.Add(HitsResolutionObservables(pdgId,None, minProb, folder, label, minPt=4e3))
sa_Resolutions.Add(ResolutionObservables(pdgId, None, minProb, folder+"_extendedEta", label, fit=False, extended_eta=True, minPt=4e3))
sa_Resolutions.Add(MomentumResolutionObservables(pdgId,None, minProb, folder+"_extendedEta", label, fit=False, extended_eta=True, minPt=4e3))
sa_Resolutions.Add(HitsResolutionObservables(pdgId,None, minProb, folder+"_extendedEta", label, extended_eta=True, minPt=4e3))
# Resolution observables using gaussian fit to get resolution
#sa_Resolutions.Add(ResolutionObservables(pdgId, None, minProb, folder+"_fit", label, fit=True))
#sa_Resolutions.Add(MomentumResolutionObservables(pdgId,None, minProb, folder+"_fit", label, fit=True))
#sa_Resolutions.Add(HitsResolutionObservables(pdgId,None, minProb, folder+"_fit", label))
# Efficiencies
sa_EfficiencyFakesHits.Add(EfficiencyObservables(pdgId, None, folder, label, minPt=4e3) )
sa_EfficiencyFakesHits.Add(EfficiencyObservables(pdgId, None, folder+"_extendedEta", label, extended_eta=True, minPt=4e3))
# Fakes
sa_EfficiencyFakesHits.Add(FakeRateObservables(pdgId,None, folder,  label))
# Hits
sa_EfficiencyFakesHits.Add(TrackHitsObservables(pdgId,None, minProb, folder,  label))

# Add to the main analysis
ana.Add(sa_EfficiencyFakesHits)
ana.Add(sa_Resolutions)

# And finally.... run the job!
# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
