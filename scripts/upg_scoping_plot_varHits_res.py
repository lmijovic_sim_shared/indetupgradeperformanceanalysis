#!/usr/bin/env python

import sys, glob, os

usage = """Script for making efficiency plots for the scoping document\n
    upg_scoping_plot_varHits_res.py <tag> [<preliminary=True>]
where <tag> should match the outfilelabel argument passed to upg_scoping_varHits_eff.py"""

# Option parsing
if len(sys.argv)<2:
    print usage
    sys.exit()
tag = sys.argv[1]
if len(sys.argv)>2: public=bool(int(sys.argv[2]))
else: public=False

print "public=",public

from InDetUpgradePerformanceAnalysis.PlotMaker import *
from InDetUpgradePerformanceAnalysis.ScopingPlotting import *
import ROOT as R
R.gROOT.SetBatch()

def make_comparison_plots_res( path, label ):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ## Resolution as a function of eta
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    pmRes = PlotMaker()

    for layout in [ "VF_Gold", "VF_Gold_m10", "Silver", "Silver_m10", "Bronze", "Bronze_m10"]:
        infile = get_filename(layout, tag)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            #print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        xmax = 2.7
        if "VF_Gold" in layout: xmax = 4.0
        #kill_beyond=xmax 
        pmRes.AddPath(infile, path,  legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True, kill_beyond=xmax)

    pmRes.AddGraph('trk_phi_mc_perigee_phi_res_etaSingleSided', )
    pmRes.AddGraph('trk_d0_mc_perigee_d0_res_etaSingleSided')
    pmRes.AddGraph('trk_z0_mc_perigee_z0_res_etaSingleSided', )
    pmRes.AddGraph('trk_theta_mc_perigee_theta_res_etaSingleSided', )
    pmRes.AddGraph('trk_pt_mc_gen_pt_res_etaSingleSided', )
    pmRes.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_etaSingleSided', scale=True)

    xrange_eta = (0,4.0)
    pmRes.AddGraph('trk_phi_mc_perigee_phi_res_varEtaSingleSided', xrange=xrange_eta, ymin=0)
    pmRes.AddGraph('trk_d0_mc_perigee_d0_res_varEtaSingleSided', xrange=xrange_eta, ymin=0)
    pmRes.AddGraph('trk_z0_mc_perigee_z0_res_varEtaSingleSided', xrange=xrange_eta, ymin=0)
    pmRes.AddGraph('trk_theta_mc_perigee_theta_res_varEtaSingleSided', xrange=xrange_eta, ymin=0)
    pmRes.AddGraph('trk_pt_mc_gen_pt_res_varEtaSingleSided', xrange=xrange_eta, ymin=0)
    pmRes.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_varEtaSingleSided', scale=True, xrange=xrange_eta, ymin=0)

    pmRes.AddRatio("Ratio to Reference")
    pmRes.MakePlots( )
    pmRes.MakeLegend()
    pmRes.MakeAtlasLabel(preliminary=False, public=public)
    pmRes.MakeLayoutLabel( label+", p_{T}>4 GeV, <#mu>=190-210")
    # save plots into files
    directory = 'plots/%s/%s' % (tag+"_"+path, "resolution_ratio")
    if public: directory+= "_public"
    pmRes.SavePlots(directory)
    del pmRes 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ## Resolution as a function of pT
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if "pion" in path: varPt = "varTrkPtPion"
    else: varPt = "varTrkPt"
    pmResPt = PlotMaker()
    for layout in [ "VF_Gold", "VF_Gold_m10", "Silver", "Silver_m10", "Bronze", "Bronze_m10"]:
        infile = get_filename(layout, tag)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            #print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        xmax = 2.7
        if "VF_Gold" in layout: xmax = 4.0
        #kill_beyond=xmax 
        pmResPt.AddPath(infile, path+"_pt",  legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True, )

    if "pion" in path: varPt = "varTrkPtPion"
    else: varPt = "varTrkPt"

    if 'pion' in path:
        xmin=1
        xmax=30
        ymax_pt = 0.06
        ymax_qoverpt = 0.06
    else:
        xmin=5
        xmax=100
        ymax_pt = 0.065
        ymax_qoverpt = 0.065

    pmResPt.AddGraph('trk_phi_mc_perigee_phi_res_'+varPt, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, )
    pmResPt.AddGraph('trk_d0_mc_perigee_d0_res_'+varPt, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, )
    pmResPt.AddGraph('trk_z0_mc_perigee_z0_res_'+varPt, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, )
    pmResPt.AddGraph('trk_theta_mc_perigee_theta_res_'+varPt, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, )
    pmResPt.AddGraph('trk_pt_mc_gen_pt_res_'+varPt, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, ymax=ymax_pt, )
    pmResPt.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_'+varPt, scale=True, scale_x=0.001, xrange=(xmin,xmax), kill_beyond=xmax*1e3, kill_before=xmin*1e3, ymax=ymax_qoverpt, )
    pmResPt.AddHisto("trk_pt", xtitle="p_{T} [MeV]", ytitle="Unit Norm",  unitNorm=True, log=True, legendloc='topfarright', ymax=2, rebin=5, )

    pmResPt.AddRatio("Ratio to Reference")
    pmResPt.MakePlots( )
    pmResPt.MakeLegend()
    pmResPt.MakeAtlasLabel(preliminary=False, public=public)
    pmResPt.MakeLayoutLabel( label+", |#eta|<2.7, <#mu>=190-210")
    # save plots into files
    directory = 'plots/%s/%s' % (tag+"_"+path, "resolution_ratio")
    if public: directory+= "_public"
    pmResPt.SavePlots(directory)

def main():
    make_comparison_plots_res( "Zmumu_max1Holes_pion", "Pion" )
    make_comparison_plots_res( "Zmumu_max1Holes_muon", "Muon")

if __name__ == "__main__":
    main()
