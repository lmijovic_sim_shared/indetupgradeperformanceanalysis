#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, CONTROL

# Import cut and observable definitions for the scoping analysis
from InDetUpgradePerformanceAnalysis.ScopingDefinitions import *

# Import custom binning definitions
# Alternative options are defined in python/CustomBinning.py
import InDetUpgradePerformanceAnalysis.CustomBinning

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranchesScoping

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
print ana
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

sa=SingleAnalysis("Zmumu",TreeHouse.Current())
sa.Add(DerivedBranchesScoping())
# Can add "0" to the list here to run a selection with zero-holes requirement
for maxHoles in [1,]:
    folder = "Zmumu_max{0}Holes_incl".format(maxHoles)
    folder_mu = "Zmumu_max{0}Holes_muon".format(maxHoles)
    folder_pi = "Zmumu_max{0}Holes_pion".format(maxHoles)
    # Efficiency plots vs eta/nVtx
    sa.Add(EfficiencyObservables(13, None, folder_mu, "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles), minPt=4e3))
    # Efficiency plots vs eta/nVtx without IP cuts (can safely re-enable)
    #sa.Add(EfficiencyObservables(pdgId, None, folder_mu+"_noIP", "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles, noIP=True), minPt=prompt_mu_pt))
    # Efficiency plots vs pT (lower pT cut of 1 GeV to match lowest pT bin low-edge in the efficiency plot)
    sa.Add(EfficiencyObservablesPt(13, None, folder_mu, "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles), [CustomBinning.VariableTruthPtBinning(),], minPt=1))
    # As above but without IP cuts (can safely re-enable)
    #sa.Add(EfficiencyObservablesPt(pdgId, None, folder_mu+"_noIP", "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles, noIP=True), minPt=1))
    # Misreconstructed track fraction
    sa.Add(FakeRateObservables(folder,  "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles)))
    # Misreconstructed track fraction without IP cuts (can safely re-enable
    #sa.Add(FakeRateObservables(folder+"_noIP",  "Efficiency_maxHoles"+str(maxHoles), VariableEtaTrackCuts(maxHoles, noIP=True)))
    # Inclusive fakes
    sa.Add(InclusiveFakeRateObservables(folder,  VariableEtaTrackCuts(maxHoles)))
    # Pion efficiency
    sa.Add(EfficiencyObservables(211, None, folder_pi, folder_pi+"_Efficiency", VariableEtaTrackCuts(maxHoles),  minPt=4e3 ))
    # Pion efficiency plots vs pT (lower pT cut of 1 GeV to match lowest pT bin low-edge in the efficiency plot)
    sa.Add(EfficiencyObservablesPt(211, None, folder_pi, folder_pi+"_Efficiency", VariableEtaTrackCuts(maxHoles), [CustomBinning.VariableTruthPtBinningPion(),], minPt=1e3) )
ana.Add(sa)

# And finally.... run the job!
# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
