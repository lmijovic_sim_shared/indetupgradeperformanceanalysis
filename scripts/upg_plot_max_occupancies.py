import ROOT as R
import PyROOTUtils
import AtlasStyle
import os

R.gROOT.SetBatch()

#tag = "all_27May"
tag = "all_3Jun"

if not os.path.exists("plots/"+tag):
    os.mkdir("plots/"+tag)
confs = [
    #"s#lhc_piplus_mu0_sigZ50",
    #"slhc_piplus_mu0_sigZ75",
    #"slhc_piplus_mu130to150_sigZ75",
    #"slhc_jetjet_JZ2W_mu0_sigZ75",  
    #"slhc_jetjet_JZ4W_mu0_sigZ75",
    #"slhc_jetjet_JZ7W_mu0_sigZ75",
    #"slhc_ttbar_mu140_sigZ75",
    #"slhc_ttbar_mu80_sigZ50",
    "slhc_minbias_sigZ50",
]

class MaxOccResults:

    def __init__(self, rfname, plot_dir, dir, nlayers=6, save_stats = True):
        self.rf = R.TFile( rfname )
        self.plot_dir = plot_dir
        self.dir = dir
        self.nlayers = nlayers
        if not os.path.exists(self.plot_dir):
            os.mkdir( self.plot_dir )
        if save_stats:
            self.rf_stats = R.TFile(self.plot_dir+"/stats.root", "RECREATE")
        if "reticule" in self.dir:
            self.chip_str = "Reticle"
        else:
            self.chip_str = "Chip"

    def get_stats( self, hist ):
        stats = {}
        stats[ "mean" ] = hist.GetMean()
        stats[ "mean_err" ] = hist.GetMeanError()
        stats[ "max" ] = self.get_y_max( hist )
        stats[ "90pc" ] = self.get_90pc( hist )
        return stats

    def get_90pc( self, hist ):
        bins = range( hist.GetNbinsX() )
        sum = 0
        integral = hist.Integral()
        for bin in bins:
            if (sum+hist.GetBinContent(bin+1)) > 0.9*integral:
                return hist.GetBinCenter(bin+1)
            sum += hist.GetBinContent(bin+1)
        return hist.GetXaxis().GetXmax()

    def get_y_max( self, hist ):
        bins = range( hist.GetNbinsX() )
        bins.reverse()
        for bin in bins:
            if hist.GetBinContent(bin+1):
                return hist.GetBinCenter(bin+1)
        return hist.GetXaxis().GetXmin()

    def draw_stats_graph(self, stats, name, label):
        #g_max = PyROOTUtils.Graph( range(self.nlayers), y = [ s["max"] for s in stats ])
        #print stats[0][ "mean_err" ]
        g_90pc = PyROOTUtils.Graph( range(self.nlayers), y = [ s["90pc"] for s in stats ])
        g_mean = PyROOTUtils.Graph( range(self.nlayers), y = [ s["mean"] for s in stats ], yerr = [ s["mean_err"] for s in stats ] )
        leg = PyROOTUtils.Legend(0.6,0.9)
        leg.AddEntry(g_90pc, "90% of distribution", "lp")
        #leg.AddEntry(g_max, "Maximum", "lp")
        leg.AddEntry(g_mean, "Mean", "lep")
        c = R.TCanvas()
        #g_max.Draw("ALP")
        g_90pc.Draw("ALP")
        #g_max.SetMinimum( g_mean.argminY() * 0.5)
        #g_max.SetMaximum( g_max.argmaxY() * 1.3 )
        #PyROOTUtils.format_hist( g_max, ymin=g_mean.argminY() * 0.5, ymax=g_max.argmaxY() * 1.3, ytitle = label, xtitle = "Layer", line_style=R.kDashed, marker_style=22, xmin=-0.5, xmax=5.5)
        PyROOTUtils.format_hist( g_90pc, ymin=g_mean.argminY() * 0.5, ymax=g_90pc.argmaxY() * 1.3, ytitle = label, xtitle = "Layer", line_style=R.kDashed, marker_style=22, xmin=-0.5, xmax=5.5)
        g_mean.Draw("same,ELP")
        leg.Draw()
        c.SaveAs( self.plot_dir + "/" + name + "_stats.eps")
        #c.SaveAs( self.plot_dir + "/" + name + "_stats.pdf")
        if self.rf_stats:
            self.rf_stats.cd()
            g_mean.SetName(name+"_mean")
            g_mean.Write()
            g_90pc.SetName(name+"_90pc")
            g_90pc.Write()


    def make_hists(self, hname, **kwargs):
        hists = []
        leg = PyROOTUtils.Legend(0.7,0.9)
        stats = []
        maxHistMax=0
        maxHistLayer=0
        for iLayer in range(self.nlayers):
            full_hname = self.dir + "/layer" + str(iLayer) + "_" + hname
            hist = self.rf.Get( full_hname )
            if not hist:
                raise RuntimeError("hist not found "+ full_hname )
            # Important - do this before changing axis range as stats will be recomputed using binned quantities
            layer_stats = self.get_stats( hist )
            if "logy" in kwargs.keys(): 
                scale = 4.
            else:
                scale = 1.1
            PyROOTUtils.format_hist(hist, ymax_scale=scale, line_colour = PyROOTUtils.pretty_colour(iLayer), marker_style=20, line_style = 1, line_width=2, **kwargs)
            hists.append( hist )
            leg.AddEntry( hist, "Layer "+str(iLayer), "l")
            stats.append( layer_stats )
            if hist.GetMaximum() > maxHistMax:
                maxHistLayer=iLayer
                maxHistMax=hist.GetMaximum()
        c = R.TCanvas()
        if "logy" in kwargs.keys():
            hists[iLayer].SetMinimum(1)
            for hist in hists: hist.SetMinimum(1)
            if kwargs["logy"]: c.SetLogy()
        hists[maxHistLayer].Draw("hist")
        for hist in hists:
            hist.Draw("hist,same")
        leg.Draw()
        c.SaveAs( self.plot_dir + "/" + hname + ".eps")
        #c.SaveAs( self.plot_dir + "/" + hname + ".pdf")
        self.draw_stats_graph( stats, hname, kwargs['xtitle'] )

    def cum_int_hist(self, hist, backwards):
        hist_cum = hist.Clone( hist.GetName() + "_cum_int" )
        hist_int = hist.Clone( hist.GetName() + "_cum_int" )
        hist_cum.SetDirectory(0)
        hist_int.SetDirectory(0)
        sum = 0.
        if not backwards:
            bins = range(0, hist.GetNbinsX()+1 )
        else:
            bins = range(hist.GetNbinsX()+1,0,-1)
        for bin in bins:
            sum += hist.GetBinContent( bin )
            hist_cum.SetBinContent( bin, sum )
        for bin in range(0, hist.GetNbinsX()+1 ):
            hist_int.SetBinContent( bin, sum )
        hist_cum.Divide(hist_cum, hist_int, 1, 1, "B")
        #print hist.GetName()
        #for bin in range(1, 20):
            #print bin, hist_cum.GetBinContent(bin)
        return hist_cum


    def graph_from_hist(self, hist, min=0):
        y = []
        x = []
        firstPast=True
        for bin in range(1, hist.GetNbinsX()+1):
            cont = hist.GetBinContent(bin) 
            if cont > min or firstPast:
                x.append( hist.GetBinCenter(bin) )
                y.append( cont )
                if cont < min: firstPast=False
        gr = PyROOTUtils.Graph(x, y)
        return gr

    def make_percent_lt_hist(self, hname, backwards=False, ymin=0.4, **kwargs):
        graphs = []
        hists = []
        leg = PyROOTUtils.Legend(0.7,0.45)
        maxHistMax=0
        maxHistLayer=0
        maxX=0
        for iLayer in range(self.nlayers):
            full_hname = self.dir + "/layer" + str(iLayer) + "_" + hname
            hist = self.rf.Get( full_hname )
            if not hist:
                raise RuntimeError("hist not found "+ full_hname )
            int_hist = self.cum_int_hist(hist, backwards)
            hists.append(int_hist)
            gr_int = self.graph_from_hist( int_hist,  ymin )
            PyROOTUtils.format_hist(gr_int, line_colour = PyROOTUtils.pretty_colour(iLayer), marker_colour = PyROOTUtils.pretty_colour(iLayer), marker_style=8, marker_size=0.7, line_style = 1, line_width=2, **kwargs )
            graphs.append( gr_int )
            leg.AddEntry( gr_int, "Layer "+str(iLayer), "lp")
            if gr_int.argmaxY() > maxHistMax:
                maxHistLayer=iLayer
                maxHistMax=hist.GetMaximum()
            if not backwards:
                this_maxX = gr_int.getFirstIntersectionsWithValue(0.999, xRange=[0.,750.], xCenter=0.)
                #print this_maxX
                if this_maxX[1] > maxX:
                    maxX = this_maxX[1]
        c = R.TCanvas()
        if "logy" in kwargs.keys():
            for hist in graphs: hist.SetMinimum(0.1)
            if kwargs["logy"]: c.SetLogy()

        # Draw frame
        hframe = hists[maxHistLayer].Clone(hists[maxHistLayer].GetName()+"_frame")
        PyROOTUtils.format_hist(hframe, **kwargs )
        hframe.Reset()
        if backwards:
            hframe.GetXaxis().SetRangeUser(0, graphs[maxHistLayer].maxX()*1.4)
        else:
            #print "maxX", maxX
            hframe.GetXaxis().SetRangeUser(0, maxX*1.1)
        hframe.SetMinimum(ymin)
        hframe.SetMaximum(1.05)
        hframe.Draw()

        # Draw inset
        if backwards:
            pad = R.TPad("inset", "", 0.55,0.6,0.94,0.92)
        else:
            pad = R.TPad("inset", "", 0.55,0.45,0.94,0.75)
        pad.SetBottomMargin(0.1)
        pad.SetLeftMargin(0.13)
        pad.SetRightMargin(0.05)
        pad.SetTopMargin(0.05)
        pad.Draw()
        pad.cd()
        hists_inset = []
        iLayer=0
        for hist in hists:
            hists_inset.append( self.graph_from_hist(hist, 0.9))
            hists_inset[-1].SetMarkerSize(0.3)
            PyROOTUtils.format_hist(hists_inset[-1], line_colour = PyROOTUtils.pretty_colour(iLayer), marker_colour = PyROOTUtils.pretty_colour(iLayer), marker_style=8, marker_size=0.4, line_style = 1, line_width=2, **kwargs )
            iLayer += 1
        hframe_inset = hists[maxHistLayer].Clone(hists_inset[maxHistLayer].GetName()+"_inset")
        hframe_inset.Reset()
        hframe_inset.GetXaxis().SetTitle("")
        hframe_inset.GetYaxis().SetTitle("")
        hframe_inset.GetXaxis().SetLabelSize(0.09)
        hframe_inset.GetYaxis().SetLabelSize(0.09)
        if backwards:
            hframe_inset.GetXaxis().SetRangeUser(0, hists_inset[maxHistLayer].maxX())
        else:
            hframe_inset.GetXaxis().SetRangeUser(0,maxX*1.05)
        hframe_inset.SetMinimum(0.9)
        hframe_inset.SetMaximum(1.01)
        #hists_inset[-1].GetXaxis().SetTitle("")
        #hists_inset[-1].GetYaxis().SetTitle("")
        pad.cd()
        hframe_inset.Draw()
        for hist in hists_inset:
            hist.Draw("pl,same")

        # draw main hist
        c.cd()
        for hist in graphs:
            hist.Draw("pl")
        leg.Draw()
        plotname = hname
        if "tag" in kwargs.keys():
            plotname += "_"+kwargs["tag"]
        if backwards:
            plotname += "_backwards"
        c.cd()
        c.SaveAs( self.plot_dir + "/" + plotname + "_int.eps")
        if self.rf_stats:
            self.rf_stats.cd()
            for hist in hists: hist.Write()

    def make_hists_pix_sp(self):
        self.make_hists("sensor_max_hits", xtitle="Max Hits Per Sensor Per Event", ytitle="Events", xmax=50, rebin=1, logy=True)
        self.make_hists("sensor_max_clus", xtitle="Max Clusters Per Sensor Per Event", ytitle="Events", xmax=20, logy=True)
        self.make_hists("sensor_max_clus_hits", xtitle="Number of Hits on Sensor With max Clusters", ytitle="Events", rebin=4, xmax=400)
        self.make_hists("sensor_max_occ", xtitle="Max Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_max_flu", xtitle="Max Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_max_flu_clus", xtitle="Max Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Events", xmax=50, rebin=1, logy=True)
        self.make_hists("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("sensor_mean_hits", xtitle="Mean Hits Per Sensor Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("sensor_mean_clus", xtitle="Mean Clusters Per Sensor Per Event", ytitle="Events")
        self.make_hists("sensor_mean_occ", xtitle="Mean Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_mean_flu", xtitle="Mean Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_max_hits", xtitle="Max Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=50, rebin=2, logy=True)
        self.make_hists("chip_max_clus", xtitle="Max Clusters Per "+self.chip_str+" Per Event", ytitle="Events", xmax=20, logy=True)
        self.make_hists("chip_max_clus_hits", xtitle="Number of Hits on "+self.chip_str+" With max Clusters", ytitle="Events", rebin=1, xmax=400)
        self.make_hists("chip_max_occ", xtitle="Max Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_max_flu", xtitle="Max Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_max_flu_clus", xtitle="Max Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_all_hits", xtitle="Hits Per "+self.chip_str+"", ytitle="Events", xmax=50, rebin=1, logy=True)
        self.make_hists("chip_all_clus", xtitle="Clusters Per "+self.chip_str+"", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("cluster_all_hits", xtitle="Hits Per Cluster", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("chip_mean_hits", xtitle="Mean Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("chip_mean_clus", xtitle="Mean Clusters Per "+self.chip_str+" Per Event", ytitle="Events")
        self.make_hists("chip_mean_occ", xtitle="Mean Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_mean_flu", xtitle="Mean Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_percent_lt_hist("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=25, ymin=0.2, ymax=1.07)
        self.make_percent_lt_hist("sensor_all_hits", backwards=True, xtitle="Hits Per Sensor", ytitle="1-Cumulative Integral", xmax=25, ymin=0.2, ymax=1.07)

        self.make_percent_lt_hist("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Cumulative Integral", xmax=10, logy=False)
        self.make_percent_lt_hist("sensor_all_clus", backwards=True, xtitle="Clusters Per Sensor", ytitle="1-Cumulative Integral", xmax=10, logy=False)

        self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=50, ymin=0.4, ymax=1.07)
        self.make_percent_lt_hist("chip_all_hits", backwards=True, xtitle="Hits Per "+self.chip_str, ytitle="1-Cumulative Integral", xmax=50, ymin=0.4, ymax=1.07)

        self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)
        self.make_percent_lt_hist("chip_all_clus", backwards=True, xtitle="Clusters Per "+self.chip_str, ytitle="1-Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)

    def make_hists_pix(self):
        self.make_hists("sensor_max_hits", xtitle="Max Hits Per Sensor Per Event", ytitle="Events", xmax=700, rebin=6)
        self.make_hists("sensor_max_clus", xtitle="Max Clusters Per Sensor Per Event", ytitle="Events", xmax=150)
        self.make_hists("sensor_max_clus_hits", xtitle="Number of Hits on Sensor With max Clusters", ytitle="Events", rebin=4, xmax=400)
        self.make_hists("sensor_max_occ", xtitle="Max Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_max_flu", xtitle="Max Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_max_flu_clus", xtitle="Max Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Events", xmax=500, rebin=2, logy=True)
        self.make_hists("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Events", xmax=150, rebin=1, logy=True)

        self.make_hists("sensor_mean_hits", xtitle="Mean Hits Per Sensor Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("sensor_mean_clus", xtitle="Mean Clusters Per Sensor Per Event", ytitle="Events")
        self.make_hists("sensor_mean_occ", xtitle="Mean Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_mean_flu", xtitle="Mean Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_max_hits", xtitle="Max Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=700, rebin=6)
        self.make_hists("chip_max_clus", xtitle="Max Clusters Per "+self.chip_str+" Per Event", ytitle="Events", xmax=100)
        self.make_hists("chip_max_clus_hits", xtitle="Number of Hits on "+self.chip_str+" With max Clusters", ytitle="Events", rebin=1, xmax=400)
        self.make_hists("chip_max_occ", xtitle="Max Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_max_flu", xtitle="Max Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_max_flu_clus", xtitle="Max Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_all_hits", xtitle="Hits Per "+self.chip_str+"", ytitle="Events", xmax=250, rebin=2, logy=True)
        self.make_hists("chip_all_clus", xtitle="Clusters Per "+self.chip_str+"", ytitle="Events", xmax=150, rebin=1, logy=True)

        self.make_hists("cluster_all_hits", xtitle="Hits Per Cluster", ytitle="Events", xmax=250, rebin=2, logy=True)

        self.make_hists("chip_mean_hits", xtitle="Mean Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("chip_mean_clus", xtitle="Mean Clusters Per "+self.chip_str+" Per Event", ytitle="Events")
        self.make_hists("chip_mean_occ", xtitle="Mean Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_mean_flu", xtitle="Mean Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_percent_lt_hist("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=400, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=400, ymin=0.2, ymax=1.07, tag="log", logy=True)
        #self.make_percent_lt_hist("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Cumulative Integral", xmax=150, logy=True, tag="log")
        self.make_percent_lt_hist("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Cumulative Integral", xmax=150, logy=False)
        self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=200, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=200, ymin=0.2, ymax=1.07, tag="log", logy=True)
        self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=70, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=70, ymin=0.2, ymax=1.07, tag="log", logy=True)

        self.make_percent_lt_hist("sensor_all_hits", backwards=True, xtitle="Hits Per Sensor", ytitle="1-Cumulative Integral", xmax=400, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("sensor_all_hits", backwards=True, xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=400, ymin=0.2, ymax=1.07, tag="log", logy=True)
        #self.make_percent_lt_hist("sensor_all_clus", backwards=True, xtitle="Clusters Per Sensor", ytitle="Cumulative Integral", xmax=150, logy=True, tag="log")
        self.make_percent_lt_hist("sensor_all_clus", backwards=True, xtitle="Clusters Per Sensor", ytitle="1-Cumulative Integral", xmax=150, logy=False)
        self.make_percent_lt_hist("chip_all_hits", backwards=True, xtitle="Hits Per "+self.chip_str, ytitle="1-Cumulative Integral", xmax=200, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_hits", backwards=True, xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=200, ymin=0.2, ymax=1.07, tag="log", logy=True)
        self.make_percent_lt_hist("chip_all_clus", backwards=True, xtitle="Clusters Per "+self.chip_str, ytitle="1-Cumulative Integral", xmax=70, ymin=0.2, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_clus", backwards=True, xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=70, ymin=0.2, ymax=1.07, tag="log", logy=True)

    def make_hists_sct_sp(self):
        self.make_hists("sensor_max_hits", xtitle="Max Hits Per Sensor Per Event", ytitle="Events", xmax=50, rebin=1, logy=True)
        self.make_hists("sensor_max_clus", xtitle="Max Clusters Per Sensor Per Event", ytitle="Events", xmax=20, logy=True)
        self.make_hists("sensor_max_clus_hits", xtitle="Number of Hits on Sensor With max Clusters", ytitle="Events", rebin=4, xmax=400)
        self.make_hists("sensor_max_occ", xtitle="Max Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_max_flu", xtitle="Max Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_max_flu_clus", xtitle="Max Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Events", xmax=20, rebin=1, logy=True)
        self.make_hists("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("sensor_mean_hits", xtitle="Mean Hits Per Sensor Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("sensor_mean_clus", xtitle="Mean Clusters Per Sensor Per Event", ytitle="Events")
        self.make_hists("sensor_mean_occ", xtitle="Mean Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_mean_flu", xtitle="Mean Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_max_hits", xtitle="Max Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=20, rebin=1, logy=True)
        self.make_hists("chip_max_clus", xtitle="Max Clusters Per "+self.chip_str+" Per Event", ytitle="Events", xmax=20, logy=True)
        self.make_hists("chip_max_clus_hits", xtitle="Number of Hits on "+self.chip_str+" With max Clusters", ytitle="Events", rebin=1, xmax=400)
        self.make_hists("chip_max_occ", xtitle="Max Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_max_flu", xtitle="Max Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_max_flu_clus", xtitle="Max Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_all_hits", xtitle="Hits Per "+self.chip_str+"", ytitle="Events", xmax=20, rebin=1, logy=True)
        self.make_hists("chip_all_clus", xtitle="Clusters Per "+self.chip_str+"", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("cluster_all_hits", xtitle="Hits Per Cluster", ytitle="Events", xmax=20, rebin=1, logy=True)

        self.make_hists("chip_mean_hits", xtitle="Mean Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("chip_mean_clus", xtitle="Mean Clusters Per "+self.chip_str+" Per Event", ytitle="Events")
        self.make_hists("chip_mean_occ", xtitle="Mean Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_mean_flu", xtitle="Mean Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        #self.make_percent_lt_hist("sensor_all_clus", xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=10, logy=True, tag="log")
        self.make_percent_lt_hist("sensor_all_clus", xtitle="Hits Per Sensor", ytitle="Cumulative Integral", xmax=10, logy=False)
        self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07, tag="log", logy=True)
        self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07, tag="log", logy=True)

    def make_hists_sct(self):
        self.make_hists("sensor_max_hits", xtitle="Max Hits Per Sensor Per Event", ytitle="Events", xmax=20, rebin=6)
        self.make_hists("sensor_max_clus", xtitle="Max Clusters Per Sensor Per Event", ytitle="Events", xmax=100)
        self.make_hists("sensor_max_clus_hits", xtitle="Number of Hits on Sensor With max Clusters", ytitle="Events", rebin=4, xmax=400)
        self.make_hists("sensor_max_occ", xtitle="Max Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_max_flu", xtitle="Max Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_max_flu_clus", xtitle="Max Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("sensor_all_hits", xtitle="Hits Per Sensor", ytitle="Events", xmax=500, rebin=1, logy=True)
        self.make_hists("sensor_all_clus", xtitle="Clusters Per Sensor", ytitle="Events", xmax=100, rebin=1, logy=True)

        self.make_hists("sensor_mean_hits", xtitle="Mean Hits Per Sensor Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("sensor_mean_clus", xtitle="Mean Clusters Per Sensor Per Event", ytitle="Events")
        self.make_hists("sensor_mean_occ", xtitle="Mean Occupancy Per Sensor Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("sensor_mean_flu", xtitle="Mean Hits/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("sensor_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per Sensor Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_max_hits", xtitle="Max Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=128, rebin=1, logy=True)
        self.make_hists("chip_max_clus", xtitle="Max Clusters Per "+self.chip_str+" Per Event", ytitle="Events", xmax=100)
        self.make_hists("chip_max_clus_hits", xtitle="Number of Hits on "+self.chip_str+" With max Clusters", ytitle="Events", rebin=1, xmax=400)
        self.make_hists("chip_max_occ", xtitle="Max Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_max_flu", xtitle="Max Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_max_flu_clus", xtitle="Max Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_hists("chip_all_hits", xtitle="Hits Per "+self.chip_str+"", ytitle="Events", xmax=128, rebin=1, logy=True)
        self.make_hists("chip_all_clus", xtitle="Clusters Per "+self.chip_str+"", ytitle="Events", xmax=50, rebin=1, logy=True)

        self.make_hists("cluster_all_hits", xtitle="Hits Per Cluster", ytitle="Events", xmax=250, rebin=1, logy=True)

        self.make_hists("chip_mean_hits", xtitle="Mean Hits Per "+self.chip_str+" Per Event", ytitle="Events", xmax=700, rebin=1)
        self.make_hists("chip_mean_clus", xtitle="Mean Clusters Per "+self.chip_str+" Per Event", ytitle="Events")
        self.make_hists("chip_mean_occ", xtitle="Mean Occupancy Per "+self.chip_str+" Per Event (%)", ytitle="Events", xmax=60, rebin=4)
        self.make_hists("chip_mean_flu", xtitle="Mean Hits/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.12, rebin=2)
        self.make_hists("chip_mean_flu_clus", xtitle="Mean Clusters/mm^{2} Per "+self.chip_str+" Per Event", ytitle="Events", xmax=0.02, rebin=2)

        self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_hits", xtitle="Hits Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07, tag="log", logy=True)
        self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07)
        #self.make_percent_lt_hist("chip_all_clus", xtitle="Clusters Per "+self.chip_str, ytitle="Cumulative Integral", xmax=25, ymin=0.4, ymax=1.07, tag="log", logy=True)


        if self.rf_stats:
            self.rf_stats.Close()


for conf in confs:
    ma = MaxOccResults( "output/occupancies_"+conf+"_"+tag+".root", "plots/"+tag+"/"+conf+"_pixel_800um", "pixel_max_occupancy_800um", nlayers=4)
    if "piplus_mu0" in conf:
        ma.make_hists_pix_sp()
    else:
        ma.make_hists_pix()
    ma = MaxOccResults( "output/occupancies_"+conf+"_"+tag+".root", "plots/"+tag+"/"+conf+"_pixel_400um", "pixel_max_occupancy_400um", nlayers=4)
    if "piplus_mu0" in conf:
        ma.make_hists_pix_sp()
    else:
        ma.make_hists_pix()
    ma = MaxOccResults( "output/occupancies_"+conf+"_"+tag+".root", "plots/"+tag+"/"+conf+"_pixel", "pixel_max_occupancy_chip", nlayers=4)
    if "piplus_mu0" in conf:
        ma.make_hists_pix_sp()
    else:
        ma.make_hists_pix()
    ma = MaxOccResults( "output/occupancies_"+conf+"_"+tag+".root", "plots/"+tag+"/"+conf+"_sct", "sct_max_occupancy_chip")
    if "piplus_mu0" in conf:
        ma.make_hists_sct_sp()
    else:
        ma.make_hists_sct()
    ma = MaxOccResults( "output/occupancies_"+conf+"_"+tag+".root", "plots/"+tag+"/"+conf+"_sct_reticule", "sct_max_occupancy_reticule")
    if "piplus_mu0" in conf:
        ma.make_hists_sct_sp()
    else:
        ma.make_hists_sct()
