#!/usr/bin/env python

from InDetUpgradePerformanceAnalysis.PlotMaker import *
import ROOT as R
from sys import argv


R.gROOT.SetBatch()

style=MyRootStyle()
pm = PlotMaker()
directory = 'plots'
infile = 'OUT.root'

if len(argv)>=3:
    infile=argv[1]
    directory=argv[2]

pref=''

addFittedRes=False

print infile

pm.AddPath(infile,'pt5',legend='p_{T}=5 GeV'+pref,linecolor=R.kBlue)
pm.AddPath(infile,'pt15',legend='p_{T}=15 GeV'+pref,linecolor=R.kRed)
pm.AddPath(infile,'pt50',legend='p_{T}=50 GeV'+pref,linecolor=R.kGreen)
pm.AddPath(infile,'pt100',legend='p_{T}=100 GeV'+pref,linecolor=R.kBlack)

binnings = ["etaSingleSided",]
#binnings = ["etaSingleSided","etaDoubleSided", "phi", "genVtx", "nHits"]

if addFittedRes:
    pref = " Gauss. Fit "
    pm.AddPath(infile,'pt5_fit',legend='', linecolor=R.kBlue, linestyle=R.kDashed)
    pm.AddPath(infile,'pt15_fit',legend='',linecolor=R.kRed, linestyle=R.kDashed)
    pm.AddPath(infile,'pt50_fit',legend='',linecolor=R.kGreen, linestyle=R.kDashed)
    pm.AddPath(infile,'pt100_fit',legend='',linecolor=R.kBlack, linestyle=R.kDashed)

for binning in binnings:
    pm.AddGraph('trk_d0_mc_perigee_d0_bias_'+binning, ytitle='<d_{0}(rec) - d_{0}(true)>')
    pm.AddGraph('trk_d0_mc_perigee_d0_outlierFrac_'+binning, ytitle='Outlier Fraction')
    pm.AddGraph('trk_d0_mc_perigee_d0_outlierSum_'+binning, ytitle='Number of Outliers')
    pm.AddGraph('trk_nAllHits_bias_'+binning, legendloc='topleft', ytitle='<No. Track Hits>')
    pm.AddGraph('trk_nAllHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Track Holes>')
    pm.AddGraph('trk_d0_mc_perigee_d0_res_'+binning)
    pm.AddGraph('trk_z0_mc_perigee_z0_res_'+binning, ltitle=["", "Width"])
    pm.AddGraph('trk_theta_mc_perigee_theta_res_'+binning, ltitle=["", "Width"])
    pm.AddGraph('trk_phi_mc_perigee_phi_res_'+binning, ltitle=["", "Width"])
    #pm.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_'+binning, ltitle=["", ""], ytitle='p_{T} #times #sigma q/p_{T}')
    pm.AddGraph('trk_qoverpt_mc_qoverpt_res_'+binning, ltitle=["", ""], ytitle='p_{T} #times #sigma q/p_{T}')

    # Pull biases
    pm.AddGraph('trk_d0_mc_perigee_d0_pull_bias_'+binning, ytitle='<Pull(d_{0})>')
    pm.AddGraph('trk_z0_mc_perigee_z0_pull_bias_'+binning, ytitle='<Pull(z_{0})>')
    pm.AddGraph('trk_theta_mc_perigee_theta_pull_bias_'+binning, ytitle='<Pull(#theta)>')
    pm.AddGraph('trk_phi_mc_perigee_phi_pull_bias_'+binning, ytitle='<Pull(#phi)>')
    #pm.AddGraph('trk_qoverpt_mc_qoverpt_pull_bias_'+binning, ytitle='<Pull(q/p_{T})>')
    pm.AddGraph('trk_qoverp_mc_perigee_qoverp_pull_bias_'+binning, ytitle='<Pull(q/p)>')
    # Pull width
    pm.AddGraph('trk_d0_mc_perigee_d0_pull_res_'+binning, ytitle='#sigma(Pull(d_{0}))')
    pm.AddGraph('trk_z0_mc_perigee_z0_pull_res_'+binning, ytitle='#sigma(Pull(z_{0}))')
    pm.AddGraph('trk_theta_mc_perigee_theta_pull_res_'+binning, ytitle='#sigma(Pull(#theta))')
    pm.AddGraph('trk_phi_mc_perigee_phi_pull_res_'+binning, ytitle='#sigma(Pull(#phi))')
    ##pm.AddGraph('trk_qoverpt_mc_qoverpt_pull_res_'+binning, ytitle='#sigma(Pull(q/p_{T}))')
    pm.AddGraph('trk_qoverp_mc_perigee_qoverp_pull_res_'+binning, ytitle='#sigma(Pull(q/p))')

pm.AddHisto('trk_nAllHits', xtitle="No. Hits on Track", ytitle="Events", legendloc='topleft')
pm.AddHisto('trk_nAllHoles', xtitle="No. Holes on Track", ytitle="Events", legendloc='topright', xrange=[0,5])

# create plots
pm.MakePlots()
pm.MakeLegend()

if addFittedRes:
    gr_normal = pm.path_graphs["pt5"][0]
    gr_fit = pm.path_graphs["pt5_fit"][0]
    for gr in pm.graphs:
        legs = pm.GetLegend(gr[0])
        leg = legs[1]
        leg.AddEntry(gr_normal, "RMS", "l")
        leg.AddEntry(gr_fit, "Gaussian", "l")


# save plots into files
pm.SavePlots(directory)

