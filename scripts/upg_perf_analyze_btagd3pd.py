#!/usr/bin/env python

import InDetUpgradePerformanceAnalysis as UpgPerfAna
from InDetUpgradePerformanceAnalysis import *


class MyStandardCuts(AllCuts):
    def __init__(self):
        AllCuts.__init__(self)
        self.Add(MinCut(float)("trk_pt",1000))  # MeV
        self.Add(RangeCut(float)("trk_d0_wrtPV",-1.0,1.0)) # mm
        self.Add(RangeCut(float)("trk_z0_wrtPV",-150,150)) # mm
        self.Add(MinimumHitsCut(9,"trk_nPixHits","trk_nSCTHits"))
        self.Add(MaxCut(int)("trk_nPixHoles",1))
#        self.Add(RangeCut(int)("trk_mc_barcode",1,200000)) # to be moved !!



class MyStandardRecoCuts(AllCuts):
    def __init__(self,pt):
        AllCuts.__init__(self)
        self.Add(RangeCut(float)("trk_pt",pt-1000.,pt+1000.))
        self.Add(RangeCut(float)("trk_d0_wrtPV",-1.0,1.0)) # mm
        self.Add(RangeCut(float)("trk_z0_wrtPV",-150,150)) # mm
        self.Add(MinimumHitsCut(9,"trk_nPixHits","trk_nSCTHits"))
        self.Add(MinCut(int)("trk_nPixHoles",0))
        

class GeneratorCuts(AllCuts):
    def __init__(self, pdg, pt, prob):
        AllCuts.__init__(self)
        self.Add(MinCut(int)("trk_mcpart_index",0))
        self.Add(AbsSelectorWithIndex("mcpart_type","trk_mcpart_index",pdg))
        self.Add(RangeCutWithIndex("mcpart_pt","trk_mcpart_index",pt-1000.,pt+1000.))
        self.Add(MinCut(float)("trk_mcpart_probability", prob))
        self.Add(MyStandardRecoCuts(pt))

class SignalSelection(AllCuts):
    def __init__(self,pt):
        AllCuts.__init__(self)
        self.Add(MinCut(int)("mcpart_truthtracks_index",0))
        self.Add(MinCut(float)("mcpart_pt",pt))
        self.Add(RangeCut(int)("mcpart_barcode",0,200000))
        self.Add(RangeCut(float)("mcpart_eta",-1.0,1.0))
        self.Add(RangeCutWithIndex("truthtrack_d0","mcpart_truthtracks_index",-1.0,1.0)) # mm
        self.Add(RangeCutWithIndex("truthtrack_z0","mcpart_truthtracks_index",-150.0,150.0)) # mm


class SignalTrackStandardSelection(AllCuts):
    def __init__(self, pdg, pt):
        AllCuts.__init__(self)
        #self.Add(MinCut(int)("trk_mcpart_index",0))
        #self.Add(AbsSelectorWithIndex("mcpart_type","trk_mcpart_index",pdg))
        #self.Add(RangeCutWithIndex("mcpart_pt","trk_mcpart_index",pt-1000.,pt+1000.))
        self.Add(MyStandardCuts())

class ResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(GeneratorCuts(pdg,pt,prob))
        #self.SetCuts(MyStandardRecoCuts(pt))
        self.Add(DiffObservable("trk_eta", "trk_mcpart_index","trk_d0","truthtrack_d0"))
        self.Add(DiffObservable("trk_eta", "trk_mcpart_index","trk_z0","truthtrack_z0"))
        self.Add(DiffObservable("trk_eta", "trk_mcpart_index","trk_phi","truthtrack_phi"))
        self.Add(DiffObservable("trk_eta", "trk_mcpart_index","trk_theta","truthtrack_theta"))
        self.Add(DiffObservable("trk_eta", "trk_mcpart_index","trk_pt","mcpart_pt"))
        self.Add(Z0SinThetaObs("trk_eta", "trk_mcpart_index","trk_z0", "truthtrack_z0", "truthtrack_theta"))
        self.Add(QOverPtObs("trk_eta", "trk_mcpart_index","trk_qoverp", "truthtrack_qoverp", "truthtrack_theta"))

class EfficiencyObservables(ObservableGroup):
    def __init__(self,xvar, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(SignalSelection(pt))
        eff=Efficiency("mcpart_eta","mcpart_pt","trk_mcpart_index","trk_mcpart_probability","trk_eta",xvar,800000)
        eff.SetCuts(MyStandardCuts())
        self.Add(eff)

                     
class FakeRateObservables(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(SignalTrackStandardSelection(pdg,pt))
        self.Add(FakeRate("trk_eta","trk_mcpart_probability"))


conf=Configuration()

# TODO: make command line option
pdgCode=conf.options.pdgCode
outfile=conf.outputfile()
pileup=conf.options.pileup
minProb=0.5

parser=ParseSamples()
files=parser.getFiles(pdgCode,pileup)

ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
##ana.SetChain('InDetTrackTree')
ana.SetChain('btagd3pd')
#ana.SetMaxEvents(19900)

for f in files:
    print 'adding',f
    ana.AddFile(f)
#ana.AddFile('utopia_muon_23.txt')
#ana.AddFile('testfiles.txt')
###ana.Test(None)

## sa=SingleAnalysis("Resolutions",TreeHouse(ana.GetChain()))
## sa.AddStep(UpgPerfAna.CONTROL)
## sa.Add(ResolutionObservables(pdgCode,5000.,minProb,"pt5","muon pt=5GeV"))
## sa.Add(ResolutionObservables(pdgCode,15000.,minProb,"pt15","muon pt=15GeV"))
## sa.Add(ResolutionObservables(pdgCode,50000.,minProb,"pt50","muon pt=50GeV"))
## sa.Add(ResolutionObservables(pdgCode,100000.,minProb,"pt100","muon pt=100GeV"))
## ana.Add(sa)

sa=SingleAnalysis("EfficienciesFakes",TreeHouse(ana.GetChain()))
sa.Add(EfficiencyObservables(3,1000,"pt5","muon pt=5GeV"))
sa.Add(EfficiencyObservables(3,15000,"pt15","muon pt=15GeV"))
sa.Add(EfficiencyObservables(2,1000,"pt5_1","muon pt=5GeV"))
sa.Add(EfficiencyObservables(2,15000,"pt15_1","muon pt=15GeV"))
ana.Add(sa)

ana.Initialize()
ana.Execute()
ana.Finalize()
