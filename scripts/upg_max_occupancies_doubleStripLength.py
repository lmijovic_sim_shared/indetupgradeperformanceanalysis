#!/usr/bin/env python

import InDetUpgradePerformanceAnalysis as UpgPerfAna
from InDetUpgradePerformanceAnalysis import *

from InDetUpgradePerformanceAnalysis.geometry import AtlasDetector
from InDetUpgradePerformanceAnalysis.sensorinfo import getSensorInfo

from InDetUpgradePerformanceAnalysis.occupancy import SCTDisks, SCTBarrel

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions(True)
        
class SCTMaximumOccupancyObservables(ObservableGroup):
    def __init__(self,path,title,nChips=10):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.Add(SCTBarrelPerEventOccupancy(nChips))

class PixelMaximumOccupancyObservables(ObservableGroup):
    def __init__(self,path,title, chipLength=0):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.Add(PixelBarrelPerEventOccupancy(chipLength))
        
        

#utopia=AtlasDetector('ATLAS-SLHC-01-00-00')
#sensor=getSensorInfo(utopia)

#cartigny=AtlasDetector('ATLAS-SLHC-01-02-01')
#sensor=getSensorInfo(cartigny)

itkLoi=AtlasDetector('ATLAS-SLHC-01-03-00')
sensor=getSensorInfo(itkLoi,True)

ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(options.outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(options.maxEvents)
for f in options.inputfiles:
    ana.AddFile(f)
ana.AddSensor(sensor)

print "Chain entries", ana.GetChain().GetEntries()
print "maxEvents", options.maxEvents



#c=ana.GetChain()
#c.Print()
sa=SingleAnalysis("OccupancyAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(options.maxEvents)
#sa.Add(DerivedBranch("pixClus_r","sqrt(pixClus_x*pixClus_x+pixClus_y*pixClus_y)"))
#sa.Add(PixelOccupancyObservables('pixel_occupancy','PixelOccupancyObservables'))
sa.Add(SCTMaximumOccupancyObservables('sct_max_occupancy_chip','SCTMaxOccupancyObservables', nChips=10))
sa.Add(SCTMaximumOccupancyObservables('sct_max_occupancy_reticule','SCTMaxOccupancyObservables', nChips=5))
sa.Add(PixelMaximumOccupancyObservables('pixel_max_occupancy_chip','PixelMaxOccupancyObservables'))
sa.Add(PixelMaximumOccupancyObservables('pixel_max_occupancy_400um','PixelMaxOccupancyObservables', chipLength=0.4))
sa.Add(PixelMaximumOccupancyObservables('pixel_max_occupancy_800um','PixelMaxOccupancyObservables', chipLength=0.8))
sa.Add(DumpSensorInfo())
#sa.Add(Distribution("mcVx_z", 500, -250, 250))
#sa.Add(Distribution("pixClus_z", 500, -250, 250))
# Define the basic track cuts for hits plots
#cluster_cuts_pos = AllCuts()
#cluster_cuts_pos.Add(MinCut("pixClus_z", 800))
#cluster_cuts_neg = AllCuts()
#cluster_cuts_neg.Add(MaxCut("pixClus_z", -800))

# Haave to define these AFTER making the derived branch
# A neater way than this is to define them as classes
# which only get initiated when they're being added
# see upg_perf_analyze.py
#og = ObservableGroup("pixClusters_negZ")
#og.SetCuts(cluster_cuts_neg)
#og.Add(Distribution("pixClus_x", 350, 0, 350))
#og.Add(Distribution("pixClus_r", 350, 0, 350))
#og.Add(Distribution("pixClus_y", 350, 0, 350))
#og.Add(Distribution("pixClus_z", 180*4, -1800, 1800))
#og.SetPath("pixHits_negZ")
#sa.Add(og)
#
#og = ObservableGroup("pixClusters_posZ")
#og.SetCuts(cluster_cuts_pos)
#og.Add(Distribution("pixClus_x", 350, 0, 350))
#og.Add(Distribution("pixClus_r", 350, 0, 350))
#og.Add(Distribution("pixClus_y", 350, 0, 350))
#og.Add(Distribution("pixClus_z", 180*4, -1800, 1800))
#og.SetPath("pixHits_posZ")
#sa.Add(og)
#
sa.AddStep(UpgPerfAna.EVENT)
ana.Add(sa)
#
#sa=SingleAnalysis("SecondaryAnalysis",TreeHouse(ana.GetChain()))
#sa.SetMaxEvents(5000)  # less statistics needed
#sa.SetPath("occupancy_maps")
#sa.Add(PixelOccupancy2D())
#ana.Add(sa)
#
#sa=SingleAnalysis("SecondaryAnalysis",TreeHouse(ana.GetChain()))
#sa.SetMaxEvents(5000)  # less statistics needed
#sa.SetPath("occupancy_maps")
#sa.Add(Occupancy2D())
#ana.Add(sa)

ana.Initialize()
ana.Execute()
ana.Finalize()
