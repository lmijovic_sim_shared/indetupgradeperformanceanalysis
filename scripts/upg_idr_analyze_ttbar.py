#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, Addition, DerivedBranch, CONTROL

# Import observable definitions - define the things we want to plot
# (reolsutions, efficiencies etc)
# see python/StandardObservableDefinitions.py
from InDetUpgradePerformanceAnalysis.TtbarObservableDefinitions import *

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranchesRecoOnly

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

pdgId=options.pdgId
outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

# Set up a "SingleAnalysis" job. Each SingleAnalysis job corresponds
# to one run of the event loop. We add spepcific instances of the observable classes
# defined in python/StandardObservableDefinitions.py e..g with different pT cuts
sa=SingleAnalysis("EfficienciesFakesHits",TreeHouse.Current())
sa.AddStep(CONTROL)
# Add derived branches to calculate e.g. the nAllHoles / nAllHits variables
# These are used in cuts so have to add them before anything else
sa.Add(DerivedBranchesRecoOnly())
#Add observables as defined above for different choices of pT
sa.Add(TtbarEfficiencyObservables("Efficiency", extended_eta=False, layout=options.layout))
sa.Add(TtbarResolutionObservables("Resolution", minProb, fit=False))
sa.Add(TtbarHitsResolutionObservables("HitsResolution", minProb))
sa.Add(TtbarInclusiveFakeRateObservables("InclusiveFakeRate", extended_eta=False, layout=options.layout))
sa.Add(TtbarExclusiveFakeRateObservables("ExclusiveFakeRate", extended_eta=False, layout=options.layout))
ana.Add(sa)

# And finally.... run the job!
# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
