#!/usr/bin/env python

import sys

import ROOT
import os
import re

from ROOT import TFile, TCanvas, TMultiGraph, TLegend, TGaxis

#from upg_make_plots import MyRootStyle, PlotMaker, RootFileParser
from InDetUpgradePerformanceAnalysis.PlotMaker import *

                
                

if __name__ == '__main__':
    style=MyRootStyle()
    pm = PlotMaker()
    directory = 'plots'
    #infile = 'occupancies.root'
    from sys import argv
    if len(argv)>=2:
        infile=argv[1]
    else:
        print "Usage: upg_make_occ_plots.py <infile> [<output_dir>]"
        os.sys.exit()
    if len(argv)>=3:
        directory=argv[2]
    else:
        directory = "plots"

    rfp=RootFileParser(infile)

    pm.AddPath(infile,'',legend="Pixel Occupancies")
    """
    for i in range(4):
        pm.AddGraph("pixel_occupancy/occ_pixlayer_%d"%i)
    """

    #
    #  Pixel Layer results
    #
    pm.AddGraph("pixel_occupancy/occ_pixlayer_mg",legendloc=(0.67,0.45,0.9,0.7),
                xtitle="moduleID",ytitle="Occupancy in percent",
                title="Pixel Barrel Occupancy (200 pileup)",
                ltitle="Pixel Barrel",
                ymin=0.
               )

    pm.AddGraph("pixel_occupancy/occ_pixlayer_mgz",legendloc='right2',
                xtitle="z [mm]",ytitle="Occupancy in percent",
                title="Pixel Barrel Occupancy (200 pileup)",
                ltitle="Pixel Barrel",
                ymin=0.)

    pm.AddGraph("pixel_occupancy/clus_pixlayer_mg",legendloc='right2',
                xtitle="moduleID",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/clus_pixlayer_mgz",legendloc='right2',
                xtitle="z [mm]",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/clus_pixlayer_sides_mgz",legendloc=['right2',],
                xtitle="z [mm]",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/hit_pixlayer_mg",legendloc='right2',
                xtitle="moduleID",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/hit_pixlayer_mgz",legendloc='right2',
                xtitle="z [mm]",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/flu_pixlayer_mg",legendloc='right2',
                xtitle="moduleID",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/flu_pixlayer_mgz",legendloc='right2',
                xtitle="z [mm]",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/size_pixlayer_mg",legendloc='topcenter2',
                xtitle="moduleID",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle="Pixel Barrel")

    pm.AddGraph("pixel_occupancy/size_pixlayer_mgz",legendloc='topcenter2',
                xtitle="z [mm]",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle="Pixel Barrel")

    # Disks
     
    pm.AddGraph("pixel_occupancy/occ_pixdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Occupancy in percent",
                title="Pixel Disk Occupancy (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/occ_pixdisk_mgr",legendloc=['topright2b','topright2c'],
                xtitle="r [mm]",ytitle="Occupancy in percent",
                title="Pixel Disk Occupancy (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/clus_pixdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/clus_pixdisk_mgr",legendloc=['topleft2a','topleft2b'],
                xtitle="r [mm]",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/hit_pixdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/hit_pixdisk_mgr",legendloc=['topleft2a','topleft2b'],
                xtitle="r [mm]",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/flu_pixdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])

    pm.AddGraph("pixel_occupancy/flu_pixdisk_mgr",legendloc=['topright2b','topright2c'],
                xtitle="r [mm]",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle=["Pixel Disk (left)","  (right)"])
    """
    pm.AddGraph("pixel_occupancy/size_pixdisk_mg",legendloc='topcenter2',
                xtitle="moduleID",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle="Pixel Disk")

    """
    pm.AddGraph("pixel_occupancy/size_pixdisk_mgr",legendloc=['topleft2a', 'topleft2b'],
                xtitle="r [mm]",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle=["Pixel Disk (left)","  (right)"])
    #
    #  SCT Layer results
    #
    pm.AddGraph("sct_occupancy/occ_sctlayer_mg",legendloc=(0.72,0.12,0.95,0.37),
                xtitle="moduleID",ytitle="Occupancy in percent",
                title="SCT Barrel Occupancy (200 pileup)",
                ltitle="SCT Barrel",
                ymin=0.)

    pm.AddGraph("sct_occupancy/occ_sctlayer_mgz",legendloc=(0.72,0.12,0.95,0.37),
                xtitle="z [mm]",ytitle="Occupancy in percent",
                title="SCT Barrel Occupancy (200 pileup)",
                ltitle="SCT Barrel",
                ymin=0.)

    pm.AddGraph("sct_occupancy/clus_sctlayer_mg",legendloc=(0.72,0.62,0.95,0.87),
                xtitle="moduleID",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle="SCT Barrel")

    pm.AddGraph("sct_occupancy/clus_sctlayer_mgz",legendloc=(0.72,0.62,0.95,0.87),
                xtitle="z [mm]",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle="SCT Barrel")

    pm.AddGraph("sct_occupancy/hit_sctlayer_mg",legendloc=(0.72,0.62,0.95,0.87),
                xtitle="moduleID",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle="SCT Barrel")

    pm.AddGraph("sct_occupancy/hit_sctlayer_mgz",legendloc='topcenter2',
                xtitle="z [mm]",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle=["SCT Barrel"])

    pm.AddGraph("sct_occupancy/flu_sctlayer_mg",legendloc=(0.72,0.12,0.95,0.37),
                xtitle="moduleID",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle="SCT Barrel")

    pm.AddGraph("sct_occupancy/flu_sctlayer_mgz",legendloc=['right2a','right2b'],
                xtitle="z [mm]",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle=["SCT Barrel","_"])

    pm.AddGraph("sct_occupancy/size_sctlayer_mg",legendloc='topright2',
                xtitle="moduleID",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle="SCT Barrel")

    pm.AddGraph("sct_occupancy/size_sctlayer_mgz",legendloc='topcenter2',
                xtitle="z [mm]",ytitle="Cluster size",
                title="Avg. cluster size",
                ltitle="SCT Barrel")

    # Disks
     
    pm.AddGraph("sct_occupancy/occ_sctdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Occupancy in percent",
                title="Sct Disk Occupancy (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/occ_sctdisk_mgr",legendloc=['topleft2a','topleft2b'],
                xtitle="r [mm]",ytitle="Occupancy in percent",
                title="Sct Disk Occupancy (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/clus_sctdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/clus_sctdisk_mgr",legendloc=['topleft2a','topleft2b'],
                xtitle="r [mm]",ytitle="Cluster per etaModuleID",
                title="Cluster distribution (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/hit_sctdisk_mg",legendloc=['topleft2a','topleft2b'],
                xtitle="moduleID",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/hit_sctdisk_mgr",legendloc=['topleft2a','topleft2b'],
                xtitle="r [mm]",ytitle="Hits per etaModuleID",
                title="Hit distribution (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/flu_sctdisk_mg",legendloc=['topright2b','topright2c'],
                xtitle="moduleID",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/flu_sctdisk_mgr",legendloc=['topright2b','topright2c'],
                xtitle="r [mm]",ytitle="Hits per mm^{2}",
                title="Hit density (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    pm.AddGraph("sct_occupancy/size_sctdisk_mgr",legendloc=['topleft2a','topleft2c'],
                xtitle="r [mm]",ytitle="Cluster size",
                title="Hit density (200 pileup)",
                ltitle=["Sct Disk (left)","  (right)"])

    # ** add remaining graphs **
    for n in rfp.graphs.keys():
        m=re.search('.*_[0-9]+$',n)
        if m is not None:
            # but skipping (many) single graphs keeping the multigraphs only
            #print 'skipping',n
            continue
            
        if not pm.graphnames.has_key(n):
            
            pm.AddGraph(n)
            pass

            

    # add histos
    for n in rfp.histos.keys():
        m=re.search('.*_[0-9]+$',n)
        if m is not None:
            #print 'skipping',n
            continue
        if not pm.histonames.has_key(n):
            pm.AddHisto(n)
            pass
    
    # create plots
    pm.MakePlots()
    pm.MakeLegend()

    # fine tuning
    #f=pm.GetFrame('pixel_occupancy/clus_pixlayer_mg')
    #f.SetTitle("Pixel Barrel Occupancy (200 pileup)")
    #f.SetMinimum(0.)
    
    # save pts into files
    pm.SavePlots(directory)
