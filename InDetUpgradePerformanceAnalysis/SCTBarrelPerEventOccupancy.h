#ifndef SCTBarrelPerEventOccupancy_h
#define SCTBarrelPerEventOccupancy_h

#include "PerEventOccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

#include <vector>

class TString;

class SCTBarrelPerEventOccupancy : public PerEventOccupancyObservable {

public:

  double stripWidth;
  int nStrips;
  const int nChips;
  const double chipWidth;

  SCTBarrelPerEventOccupancy(int numChips=10);

  void Analyze(UpgPerfAna::AnalysisStep, int);
  int GetNTrks();  ///< loop SCT clusters

  void setDoubleStripLength(bool val) { doubleStripLength=val; }
  
 private:

  void Initialize();
  void fillHists( std::vector< PerElementCounters > &counters, ElemType elem );
  int getChip( float locX );
  void debugLargeClusters(PerEventCounter *counter, long counterId);
  void defineHists();

  VectorBranch<float> sctClus_x, sctClus_y, sctClus_z; // vector<float>
  VectorBranch<int> sctClus_size;
  VectorBranch<int> sctClus_layer, sctClus_bec, sctClus_eta_module, sctClus_phi_module;
  VectorBranch<ULong64_t> sctClus_detElementId;
  VectorBranch<int> sctClus_side;
  VectorBranch<float> sctClus_locX;

  bool doubleStripLength;
  
};



// Local Variables:
// mode: c++
// End:
#endif
