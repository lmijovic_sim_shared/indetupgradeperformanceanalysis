#ifndef FakeRate_H
#define FakeRate_H

#include <iostream>
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "VectorBranch.h"
#include "Observable.h"
#include "Binning.h"

class Cuts;

class FakeRate : public Observable{

 public:

  FakeRate(Binning binning = Binning() );
  FakeRate(std::string etastring, std::string probstring, Binning binning = Binning()); 

  void Initialize();
  void AnalyzeEvent(UpgPerfAna::AnalysisStep);
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  void StorePlots();
  int GetNTrks();
  Binning* GetBinning() { return &m_binning; };

 private:

  //Cuts * cutSet;
  Binning m_binning;

  double probCut;

  std::vector<int> nNum;
  std::vector<int> nDen;

  int Ntrks;
  //bool passProb;

  VectorBranch<float> probability;
  VectorBranch<float> eta;

  TGraphErrors *frPlot;
  TGraphErrors *frPlot_num;
  TGraphErrors *frPlot_den;
};

#endif

