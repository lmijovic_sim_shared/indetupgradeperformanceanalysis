#ifndef MinMaxCut_H
#define MinMaxCut_H

#include "Cuts.h"
#include "VectorBranch.h"
#include <math.h>
#include <vector>

template <class T>
class MinCut : public Cuts {
public:
  MinCut(const std::string & varname, T value);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  T minvar;
};

template <class T>
class AbsMinCut : public Cuts {
public:
  AbsMinCut(const std::string & varname, T value);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  T minvar;
};

template <class T, class U>
class RangeDependentMinCut : public Cuts {
public:
  RangeDependentMinCut(const std::string & varname, const std::string & range_varname, std::vector<U> & ranges, std::vector<T> & values, bool absrange=true) ;
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  VectorBranch<U> range_var;
  std::vector<T> minvars;
  std::vector<U> ranges;
  bool absrange;
};


template <class T>
class MaxCut : public Cuts {
public:
  MaxCut(const std::string & varname, T value);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  T maxvar;
};

template <class T>
class RangeCut: public Cuts {
public:
  RangeCut(const std::string & varname, T vmin, T vmax);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  T minvar;
  T maxvar;
};

template <class T>
class RangeProductCut: public Cuts {
public:
  RangeProductCut(const std::string & varname,const std::string & varname2, T vmin, T vmax);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  VectorBranch<T> var2;
  T minvar;
  T maxvar;
};

#endif // MinMaxCut_H
