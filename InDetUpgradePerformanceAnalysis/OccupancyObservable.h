#ifndef OccupancyObservable_H
#define OccupancyObservable_H

#include <vector>
#include <string>
#include <set>
#include <map>
#include <cmath>

#include "Observable.h"
#include "VectorBranch.h"

// Need to include some root to get ULong64_t defined
// Maybe a cleaner include than TH1, but we need it anyway...
#include "TH1.h"

class TGraph;
class TGraphErrors;
class TMultiGraph;

class OccupancyObservable : public Observable {
public:
  typedef std::set<ULong64_t> ElementList;
  class Counters {
    long sum_evnt;
    float sum_x,sum_y,sum_z,sum_r;
    float f_area;
    long sum_clus,sum_clussize,sum_clussize2;
    long tot_pix;
  public:
    Counters();
    ElementList detElementIds;
    void setTotalPixel(int total) { tot_pix=total; }
    //int totalPixel() { return tot_pix; }
    void increaseEvent() { ++sum_evnt; }
    void add(float x, float y, float z, int clussize, ULong64_t detElementId) {
      sum_x+=x; sum_y+=y; sum_z+=z;
      sum_r+=sqrt(x*x+y*y);
      sum_clus+=1; sum_clussize+=clussize; sum_clussize2+=clussize*clussize;
      detElementIds.insert(detElementId);
    }
    float area() const { return f_area; }
    void setArea(float a) { f_area=a; }
    float occupancy() const;
    float occupancyError() const;
    float hits() const;
    float hitsError() const;
    float cluster() const;
    float clusterError() const;
    float r() const { return sum_r/float(sum_clus); }
    float z() const { return sum_z/float(sum_clus); }
    friend class Occupancy2D;
    friend class PixelOccupancy2D;
  };

  typedef std::pair<int,int> LMPair;
  typedef std::map< LMPair, OccupancyObservable::Counters > LayerIdCounters;
  
  enum OHisto {
    CLUS_MAP=0,
    HIT_MAP=1,
    OCC_MAP=2,
    FLU_MAP=3,
    CLUS_MAP_DETAIL=4,
    HIT_MAP_DETAIL=5,
    OCC_MAP_DETAIL=6,
    FLU_MAP_DETAIL=7,
    CLUS_SIZE_ALL,
    CLUS_SIZE_PRIM,
    CLUS_SIZE_SEC,
    CLUS_SIZE_UNMATCHED,
    END_HISTOS
  };
protected:

  enum ClusterTruthType {
    ClusterTruthType_UNMATCHED,
    ClusterTruthType_PRIMARY,
    ClusterTruthType_SECONDARY
  };

  LayerIdCounters counters;
  std::string moduleType;

  bool EventInit(UpgPerfAna::AnalysisStep step, int k);
  TGraphErrors* CreateGraph(int npoints, const char * fmt, const std::string & n, int i);
  TMultiGraph * CreateMultiGraph(const char * fmt, const std::string & n);
  void NormalizeOccupancyMap(OHisto clus_id, OHisto occ_id, float factor);
public:
  OccupancyObservable(const std::string & name, const std::string & mType);
  void Initialize();
  void AnalyzeEvent(UpgPerfAna::AnalysisStep step);
  void StorePlots();
  ~OccupancyObservable();

  TH1* GetHistogram(size_t i) { return m_histos[i]; }
 protected:
  int ndisk;
  std::vector<TH1*> m_histos;
  std::vector<TGraph*> m_graphs;
  std::vector<TMultiGraph*> m_multigraphs;

  // other classes allowed to access
  friend class Occupancy2D;
  friend class PixelOccupancy2D;

};

// Local Variables:
// mode: c++
// End:
#endif
