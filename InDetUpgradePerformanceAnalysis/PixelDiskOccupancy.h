#ifndef PixelDiskOccupancy_h
#define PixelDiskOccupancy_h

#include "DiskOccupancy.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

class PixelDiskOccupancy : public DiskOccupancy {
public:

  PixelDiskOccupancy();
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  //   void Finalize();
  int GetNTrks();  ///< loop pixel clusters


 private:
 
  //  int ndisk;
  // Branch<int> sctClus_n; // int

  VectorBranch<float> pixClus_x, pixClus_y, pixClus_z; // vector<float>
  VectorBranch<int> pixClus_size;
  VectorBranch<char> pixClus_layer, pixClus_bec, pixClus_eta_module; 
  VectorBranch<ULong64_t> pixClus_detElementId;
};

// Local Variables:
// mode: c++
// End:
#endif
