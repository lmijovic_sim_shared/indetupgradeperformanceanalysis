#ifndef Helpers_H
#define Helpers_H

#include <sstream>

inline std::string stri(int value) {
  std::string s;
  std::stringstream sstr;
  sstr<<value;
  sstr>>s;
  return s;
}

#endif
