#ifndef TreeHouse_H
#define TreeHouse_H

#include "TChain.h"
#include <map>
#include <vector>
#include <iostream>

class TFile;

//class TChain;
template <class T> class VectorBranch;
template <class T> class ScalarBranch;

class TreeHouse {

public:
  TreeHouse(TChain* =0);
  ~TreeHouse();
  void SetChain(TChain* c) { chain=c;}
  TChain * GetChain() { return chain;}
  void* GetBranch(std::string);
  size_t GetBranchId(std::string);

  TTree * AddFriend();
  TTree * GetFriend() { return m_friend; }
  template <class V>
  size_t AddBranch(const std::string,V &); 
  void Fill();

  
  void SelectBranches();

  static TreeHouse * Current() { return current; }
  void cd() { current=this; }
  size_t size() { 
    if (nmax!=-1 && nmax< chain->GetEntries()) return nmax;
    return chain->GetEntries();
  }
  Int_t GetEntry(Long64_t entry = 0, Int_t getall = 0) {
    return chain->GetEntry(entry, getall);
  }

  void SetMaxEvents(int n) { nmax=n;}
  void AddFile(const TString &);

private:
  
  typedef std::map<std::string, size_t> BMap;
  typedef std::vector<void*> BranchList;
  typedef std::vector<TBranch*> TBranchList;

  // index list
  BMap bmap;
  // pointers to leafs
  BranchList branches;
  // reference list with TBranches
  TBranchList metabranches;

  static TreeHouse * current;  

  TChain * chain;
  Long64_t nmax;

  TTree  * m_friend;
  TFile  * m_friend_file;

  template <class T> friend class VectorBranch;
  template <class T> friend class ScalarBranch;
  friend std::ostream & operator<<(std::ostream &, const TreeHouse &);
};

#endif // TreeHouse
