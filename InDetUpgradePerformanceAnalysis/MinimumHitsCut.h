#ifndef MinimumHitsCut_H
#define MinimumHitsCut_H

#include "Cuts.h"
#include "VectorBranch.h"


class MinimumHitsCut : public Cuts {
public:
  MinimumHitsCut(int value, const std::string & pixHitsbranch="trk_nPixHits", const std::string & sctHitsbranch="trk_nSCTHits");
  bool AcceptTrack(int);
private:
  VectorBranch<int> nPixHits;
  VectorBranch<int> nSCTHits;
  int minHits;
};

#endif
