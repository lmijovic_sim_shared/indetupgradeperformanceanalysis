#ifndef DumpSensorInfo_H
#define DumpSensorInfo_H

#include "Observable.h"
#include <string>

class DumpSensorInfo : public Observable {
public:
  DumpSensorInfo();
  DumpSensorInfo(const std::string &);
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void StorePlots();
};

// Local Variables:
// mode: c++
// End:
#endif
