#ifndef Selector_H
#define Selector_H

#include "Cuts.h"
#include "VectorBranch.h"

template <class T>
class Selector : public Cuts {
public:
  Selector(const std::string & varname, int value);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  int sel;
};

template <class T>
class AbsSelector : public Cuts {
public:
  AbsSelector(const std::string & varname, int value);
  bool AcceptTrack(int);
private:
  VectorBranch<T> var;
  int sel;
};

#endif // Selector_H
