#ifndef Analysis_H
#define Analysis_H

#include "SingleAnalysis.h"

class TGraphErrors;
class Binning;

void SafeWrite(TObject* obj);
TGraphErrors* MakeGraphErrors(std::string label, int nPoint, Double_t *xvars, Double_t *yvars, Double_t *xerrs, Double_t *yerrs, Binning& binning, std::string ylabel="");

enum RatioErrorType {
  BINOMIAL,
  SIMPLE,
  NUM_ONLY,
  INVERSE_BINOMIAL
};

class InDetUpgradeAnalysis : public SingleAnalysis {
 public:

  InDetUpgradeAnalysis();
  ~InDetUpgradeAnalysis();

  void Test(TChain *);

  //void DoIt(UpgPerfAna::AnalysisStep);

  /// Perform loops over Observables in m_obs, calling Execute() on each
  UpgPerfAna::AnalysisStep Execute(); 

  void Finalize();

  void SetOutputFileName(const TString & f) { m_outfile=f; }

  void Add(Observable*);
  void SetCuts(Cuts *);
private:
  TString m_outfile;

  double minProb;

};

#endif

