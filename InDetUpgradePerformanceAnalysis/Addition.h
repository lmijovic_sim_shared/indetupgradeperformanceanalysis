#ifndef Addition_H
#define Addition_H

#include "InDetUpgradePerformanceAnalysis/Calculator.h"

template < class observableT >
class Addition : public Calculator<observableT> {

 public:
  Addition(const std::string & out,
		const std::string & in,
		const std::string & in2);

private:
  void Calc(UpgPerfAna::AnalysisStep, int i); 
  // virtual void Calc(UpgPerfAna::AnalysisStep);

};

#endif
