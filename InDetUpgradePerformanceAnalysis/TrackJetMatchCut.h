#ifndef TrackJetMatchCut_H
#define TrackJetMatchCut_H

#include "Cuts.h"
#include "VectorBranch.h"

class TrackJetMatchCut : public Cuts{

 public:

  TrackJetMatchCut(const std::string & etaTrkBranch, const std::string & phiTrkBranch, const std::string & etaJetBranch, const std::string & phiJetBranch, const std::string & jetPtBranch, const std::string & jetBBranch, double value, double jetPtCut_value, bool bjet);
  bool AcceptTrack(int i);
  int AcceptAndMatchTrack(int i);
  double DeltaR(int track, int jet);

 private: 

  VectorBranch<float> etaTrk;
  VectorBranch<float> phiTrk;
  VectorBranch<float> etaJet;
  VectorBranch<float> phiJet;
  VectorBranch<float> ptJet;
  VectorBranch<int> bJet;
  double matchCut;
  double jetPtCut;
  bool match_bjet;
};

#endif
