
#ifndef Vector_H
#define Vector_H

#include <vector>
#include <string>
#include <iostream>

class TreeHouse;

template <class T>
class VectorBranch {
 public:
  typedef std::vector<T> VecT;

  VectorBranch(const std::string & name);
  VectorBranch(const std::string & name, VecT & var);
  const VecT & Get();
  // mimic vector
  T operator[](size_t i);
  size_t size();
  void SetAnalysis(TreeHouse *); // needed ?
  inline const std::string & Name() const { return name; }
  void Fill();  // only for calc records
 private:
  std::string name;
  size_t id;
  TreeHouse * m_treehouse;
};

#endif
