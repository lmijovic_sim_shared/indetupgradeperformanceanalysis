#ifndef AllCuts_H
#define AllCuts_H

#include "Cuts.h"

#include <vector>

class AllCuts : public Cuts {
public:
  AllCuts();
  bool AcceptEvent();
  bool AcceptTrack(int i);
  void Add(Cuts*);
  ~AllCuts();
private:
  std::vector<Cuts*> allcuts;
  friend std::ostream & operator<<(std::ostream & s, const Cuts*);
};

#endif // AllCuts_H
/*
 Local Variables:
 mode:c++
 End:
*/
