#ifndef PixelBarrelPerEventOccupancy_h
#define PixelBarrelPerEventOccupancy_h

#include "PerEventOccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

#include <vector>

class TString;

class PixelBarrelPerEventOccupancy : public PerEventOccupancyObservable {

  public:

    // chipLength can be used to force square chips of length*length (in mm)
    // length will be adjusted such that an integer number of chips fit in each module
    PixelBarrelPerEventOccupancy(float chipLength=0);

    void Initialize( );
    void Analyze(UpgPerfAna::AnalysisStep, int);
    int GetNTrks();  ///< loop SCT clusters

  private:

    void fillHists( std::vector< PerElementCounters > &counters, ElemType elem );
    int getChip( float locX, float locY, int layer);
    void debugLargeClusters(PerEventCounter *counter, long counterId);
    void defineHists();

    float userChipLength;

    std::vector<double> chipLenX;
    std::vector<double> chipLenY;
    std::vector< int> nChipsX;
    std::vector< int> nChipsY;
    std::vector< int> chipColsX;
    std::vector< int> chipColsY;

    //std::vector<int> nStripsPerChip_layers;
    //std::vector<double> m_pixelWidth_layers;
    //std::vector<int> nClus_layer;
    // Branch<int> pixClus_n; // int
    //
    //std::vector< std::vector< TH1* > > m_histos;
    //std::vector< std::vector< TH1* > > m_histos_chip;

    VectorBranch<float> pixClus_x, pixClus_y, pixClus_z; // vector<float>
    //  VectorBranch<float> pixClus_size; // Utopia 1.0
    VectorBranch<int> pixClus_size;
    VectorBranch<char> pixClus_layer, pixClus_bec, pixClus_eta_module, pixClus_phi_module;
    VectorBranch<ULong64_t> pixClus_detElementId;
    //VectorBranch<int> pixClus_side;
    VectorBranch<float> pixClus_locX;
    VectorBranch<float> pixClus_locY;

    //std::vector<TH1*> extra_hists;
    //std::map<long, TH1*> m_hmap;
};



// Local Variables:
// mode: c++
// End:
#endif
