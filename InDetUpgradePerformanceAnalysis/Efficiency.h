#ifndef Efficiency_H
#define Efficiency_H

#include <iostream>
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "VectorBranch.h"
#include "ObservableGroup.h"
#include "TrackJetMatchCut.h"
#include "Binning.h"

class Cuts;

class Efficiency : public ObservableGroup{

 public:

  Efficiency(Binning binning = Binning("mc_gen_eta", 15, 0, 2.7), std::string mc_eta_name="mc_gen_eta", std::string mc_pt_name = "mc_gen_pt", std::string mc_index_name = "trk_mc_index", std::string mc_prob_name="trk_mc_probability", std::string eta_name = "trk_eta");
  Efficiency(std::string mceta, std::string mcpt, std::string mcindex, std::string mcprob, std::string etabranch, int x_var, double jetPtCut);
  void Initialize();
  void AnalyzeEvent(UpgPerfAna::AnalysisStep);
  void Analyze(UpgPerfAna::AnalysisStep,int);
  void Finalize();
  void StorePlots();
  void setmcEta(VectorBranch<float> branch);
  void setmcPt(VectorBranch<float> branch);
  void setmc_index(VectorBranch<int> branch);
  void setprobability(VectorBranch<float> branch);
  void seteta(VectorBranch<float> branch);
  int GetNTrks();  ///< Note: returns mc size -- needs renaming in Observable.h
  Binning* GetBinning() { return &m_binning; };

 protected:

  Binning m_binning;

  //int nEtaBins;
  //double etaMax;
  //double etaMin;
  //double binLength;
  //double halfBin;
  int x_variable; ///< 1=Eta,2=deltaR,3=JetPt
  int jet_index;

  std::vector<int> nNum;
  std::vector<int> nDen;

  TGraphErrors *effPlot;
  TGraphErrors *genTracks;
  TGraphErrors *recoTracks;

  VectorBranch<float> mcEta;
  VectorBranch<float> mcPt;
  VectorBranch<int> mc_index;
  VectorBranch<float> probability;
  VectorBranch<float> eta;
  //VectorBranch<float> jet_pt;

  TrackJetMatchCut *trackJetAssoc;

  };

#endif
