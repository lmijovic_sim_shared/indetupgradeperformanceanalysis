#ifndef PixelOccupancy2D_h
#define PixelOccupancy2D_h

#include "OccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

class PixelOccupancy2D : public OccupancyObservable {
public:
  PixelOccupancy2D();
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  int GetNTrks();  ///< loop pixel clusters

 private:
  VectorBranch<float> pixClus_x, pixClus_y, pixClus_z; // vector<float>
  VectorBranch<ULong64_t> pixClus_detElementId;

  typedef std::map<ULong64_t,Counters> OccupanciesMap;
  OccupanciesMap omap;
};

// Local Variables:
// mode: c++
// End:
#endif
