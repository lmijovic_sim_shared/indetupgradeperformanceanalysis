#ifndef PixelDiskPerEventOccupancy_h
#define PixelDiskPerEventOccupancy_h

#include "PerEventOccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

#include <vector>

class TString;

class PixelDiskPerEventOccupancy : public PerEventOccupancyObservable {

  public:

    // chipLength can be used to force square chips of length*length (in mm)
    // length will be adjusted such that an integer number of chips fit in each module
    PixelDiskPerEventOccupancy(/*float chipLength=0*/);

    void Initialize( );
    void Analyze(UpgPerfAna::AnalysisStep, int);
    int GetNTrks();  ///< loop SCT clusters

  private:

    void fillHists( std::vector< PerElementCounters > &counters, ElemType elem );
    void defineHists();
    void getRingCoords(const int ring, int& side, int& disk, int& loc_ring);
    void defineHistLayers(ElemType elem, PlotVariable var, TString name, int nbins, float min, float max);
    int getRingId( const int bec, const int disk, const int ring) { return (ring<<6) + ((bec+2)/4<<3) + disk; }
    int getRingIndex( const int bec, const int disk, const int ring);

    int n_rings;
    int n_disks;

    std::map<int,int> ring_indices;
    std::map<int,int> ring_ids;


    VectorBranch<float> pixClus_x, pixClus_y, pixClus_z; 
    VectorBranch<int> pixClus_size;
    VectorBranch<char> pixClus_layer, pixClus_bec, pixClus_eta_module, pixClus_phi_module;
    VectorBranch<ULong64_t> pixClus_detElementId;
    VectorBranch<float> pixClus_locX;
    VectorBranch<float> pixClus_locY;

};

#endif
