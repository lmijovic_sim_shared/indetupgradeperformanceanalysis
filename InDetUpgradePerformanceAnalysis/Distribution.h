#ifndef Distribution_H
#define Distribution_H

#include <vector>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include <string>

#include "Observable.h"
#include "VectorBranch.h"


template <class T> 
class Distribution : public Observable {

 public:
  Distribution(const std::string & name1,int nbin, float xmin, float xmax);
  Distribution(const std::string & name1,int nbinx, float xmin, float xmax, 
	       const std::string & name2,int nbiny, float ymin, float ymax);
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  int GetNTrks();
  void Finalize();
  void StorePlots();
  ~Distribution();

 protected:

  int m_nbinx;
  float m_xmin, m_xmax;
  int m_nbiny;
  float m_ymin, m_ymax;

  std::vector<TH1*> m_histos;

  // used branches
  VectorBranch<T> xvar;
  VectorBranch<T> yvar;

  bool twoD;
};


#endif
