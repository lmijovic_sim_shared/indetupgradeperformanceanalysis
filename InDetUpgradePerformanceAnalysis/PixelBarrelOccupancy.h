#ifndef PixelBarrelOccupancy_h
#define PixelBarrelOccupancy_h

#include "OccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

class PixelBarrelOccupancy : public OccupancyObservable {
public:

  PixelBarrelOccupancy(bool is_xaod=false);
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  int GetNTrks();  ///< loop pixel clusters

 private:

  bool is_xaod;

  std::string pixClus_x_varname, pixClus_y_varname, pixClus_z_varname;
  std::string pixClus_size_varname;
  std::string pixClus_layer_varname, pixClus_bec_varname, pixClus_eta_module_varname;
  std::string pixClus_detElementId_varname;
  
  int nlayer;
  // Branch<int> pixClus_n; // int

  VectorBranch<float> pixClus_x, pixClus_y, pixClus_z; // vector<float>
  VectorBranch<int> pixClus_size;
  VectorBranch<char> pixClus_layer, pixClus_bec, pixClus_eta_module; 
  VectorBranch<ULong64_t> pixClus_detElementId;

};

// Local Variables:
// mode: c++
// End:
#endif
