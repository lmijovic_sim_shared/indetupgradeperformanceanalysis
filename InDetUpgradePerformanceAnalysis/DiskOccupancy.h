#ifndef DiskOccupancy_h
#define DiskOccupancy_h

#include "OccupancyObservable.h"

class DiskOccupancy : public OccupancyObservable {
public:

  DiskOccupancy(const std::string & name, const std::string & mType);
  //void Initialize();
  //  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  //  int GetNTrks();  // loop clusters

  void setDoubleStripLength(bool val) { doubleStripLength=val; }

 protected:

  bool doubleStripLength;

  // int ndisk;
  };

// Local Variables:
// mode: c++
// End:
#endif
