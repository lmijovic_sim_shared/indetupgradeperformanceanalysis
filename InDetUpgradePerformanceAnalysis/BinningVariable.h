#ifndef BINNINGVARIABLE_H
#define BINNINGVARIABLE_H

class BinningVariable {

    public:
        virtual float Value(int index) = 0;
        virtual ~BinningVariable() {};

};

#endif
