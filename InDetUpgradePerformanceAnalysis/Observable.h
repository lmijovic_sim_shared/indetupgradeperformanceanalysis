#ifndef Observable_H
#define Observable_H

#include <string>

namespace UpgPerfAna 
{
  enum AnalysisStep {
    INITIAL=1,
    CONTROL=2,
    RERUN=3,
    FINAL=4,
    EVENT=5,
    DONE};
}

//class ObservableGroup;
class Cuts;
class SensorInfoBase;
class SingleAnalysis;

/***
  Pure virtual class
  Provides base for 
    - Observable (e.g impact parameter resolution)
    - ObservableGroup (collection of observables and cuts)
    - SingleAnalysis (performs main loop over a tree)
    - InDetUpgradeAnalysis (performs loop over analysis steps)

   As these classes constitute an inheritance chain. Not all level have to be present in a single run.
 */
class Observable{

 public:
  Observable(const std::string & name);
  virtual void AddSensor(SensorInfoBase*);

  /// might create plots via "DefinePlots"
  virtual void Initialize()=0;

  /// Execute analysis, i.e. loop over tree. Cannot not be called directly
  virtual UpgPerfAna::AnalysisStep Execute();

  /// Execute single step of analysis. Cannot not be called directly
  virtual UpgPerfAna::AnalysisStep ExecuteStep(UpgPerfAna::AnalysisStep);

  /// Extra hook guaranteed to be called at start of every event
  virtual void InitializeEvent(UpgPerfAna::AnalysisStep) {}; 

  /// Loops over tracks, and calls Analyze(step, iTrack) for each 
  virtual void AnalyzeEvent(UpgPerfAna::AnalysisStep); 

  /// Perform the actual analysis for a specific track. Pure virtual; cannot not be called directly
  virtual void Analyze(UpgPerfAna::AnalysisStep,int);

  /// Does nothing
  virtual void Finalize();

  /// Pure virtual
  virtual void StorePlots()=0; 

  virtual ~Observable();

  void SetPath(const std::string & p) { path=p; }
  virtual void SetAnalysis(SingleAnalysis * ana);
  virtual Observable * GetObservable(const std::string & name);
  const std::string & Name() const { return nameobs; }
protected:
  // little helpers
  static void cd(const std::string path);
  

  // *AS* remove or relocate to ResolutionObservable
  virtual void DefinePlots();
  virtual void FillPlots(UpgPerfAna::AnalysisStep, int); 

  virtual int GetNTrks(); /* deprecated */

  friend class ObservableGroup;
  friend std::ostream & operator<<(std::ostream & s, const Observable &);
  friend std::ostream & operator<<(std::ostream & s, const Observable *);
  friend std::ostream & operator<<(std::ostream & s, const Cuts*);
  static int output_level;
  
protected:
  std::string nameobs;
  std::string path;
  SensorInfoBase * sensor;
  SingleAnalysis * analysis;
};

inline std::string Spaces(int l) {
  std::string s;
  for (int i=0;i<2*l;++i) s+=" ";
  return s;
}


// Local Variables:
// mode: c++
// End:
#endif
