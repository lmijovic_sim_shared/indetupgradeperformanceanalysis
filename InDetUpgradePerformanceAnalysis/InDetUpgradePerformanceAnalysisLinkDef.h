#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/DumpSensorInfo.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "InDetUpgradePerformanceAnalysis/SingleObservable.h"
#include "InDetUpgradePerformanceAnalysis/DiffObservable.h"
#include "InDetUpgradePerformanceAnalysis/PullObservable.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h"
#include "InDetUpgradePerformanceAnalysis/QOverPtObs.h"

#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "InDetUpgradePerformanceAnalysis/AllCuts.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCut.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/MinimumHitsCut.h"

#include "InDetUpgradePerformanceAnalysis/Selector.h"
#include "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include "InDetUpgradePerformanceAnalysis/FakeRate.h"
#include "InDetUpgradePerformanceAnalysis/TrackRatio.h"

#include "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/Occupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/Distribution.h"
#include "InDetUpgradePerformanceAnalysis/Binning.h"

#include "InDetUpgradePerformanceAnalysis/Calculator.h"
#include "InDetUpgradePerformanceAnalysis/Addition.h"
#include "InDetUpgradePerformanceAnalysis/DerivedBranch.h"
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class InDetUpgradeAnalysis;
#pragma link C++ class SingleAnalysis;
#pragma link C++ class ObservableGroup;
#pragma link C++ class Observable;
#pragma link C++ class DumpSensorInfo;
#pragma link C++ class SensorInfoBase;

#pragma link C++ namespace UpgPerfAna;
#pragma link C++ enum UpgPerfAna::AnalysisStep;
#pragma link C++ class TreeHouse;

#pragma link C++ class Cuts;
#pragma link C++ class AllCuts;
#pragma link C++ class StandardCuts;

#pragma link C++ class RangeDependentMinCut<float,float>;
#pragma link C++ class RangeDependentMinCut<int,float>;
#pragma link C++ class MinCut<int>;
#pragma link C++ class MinCut<float>;
#pragma link C++ class MaxCut<float>;
#pragma link C++ class MaxCut<int>;
#pragma link C++ class RangeCut<int>;
#pragma link C++ class RangeCut<float>;
#pragma link C++ class RangeProductCut<float>;

#pragma link C++ class Selector<int>;
#pragma link C++ class Selector<float>;
#pragma link C++ class AbsSelector<int>;
#pragma link C++ class AbsSelector<float>;
#pragma link C++ class MinimumHitsCut;
#pragma link C++ class SelectorWithIndex;
#pragma link C++ class AbsSelectorWithIndex;
#pragma link C++ class RangeCutWithIndex;
#pragma link C++ class TrackJetMatchCut;

#pragma link C++ class ResolutionObservable<float>;
#pragma link C++ class ResolutionObservable<int>;
#pragma link C++ class DiffObservable<float>;
#pragma link C++ class PullObservable<float>;
#pragma link C++ class SingleObservable<float>;
#pragma link C++ class SingleObservable<int>;
#pragma link C++ class Z0SinThetaObs<float>;
#pragma link C++ class QOverPtObs<float>;
#pragma link C++ class Efficiency;
#pragma link C++ class FakeRate;
#pragma link C++ class TrackRatio;

#pragma link C++ class OccupancyObservable;
#pragma link C++ class DiskOccupancy;
#pragma link C++ class Distribution<float>;
#pragma link C++ class Distribution<int>;
#pragma link C++ class SCTDiskOccupancy;
#pragma link C++ class SCTBarrelOccupancy;
#pragma link C++ class SCTBarrelPerEventOccupancy;
#pragma link C++ class PixelBarrelPerEventOccupancy;
#pragma link C++ class PixelDiskPerEventOccupancy;
#pragma link C++ class PixelDiskOccupancy;
#pragma link C++ class PixelBarrelOccupancy;
#pragma link C++ class Occupancy2D;
#pragma link C++ class PixelOccupancy2D;
#pragma link C++ class Binning;

#pragma link C++ class Calculator<int>;
#pragma link C++ class Calculator<float>;
#pragma link C++ class Addition<int>;
#pragma link C++ class Addition<float>;
//#pragma link C++ class DerivedBranch<int>;
#pragma link C++ class DerivedBranch<float>;
//#pragma link C++ class DerivedBranch<double>;


#pragma link C++ class std::vector<std::vector<ULong64_t> >;
#pragma link C++ class std::vector<std::vector<int> >;
#pragma link C++ class std::vector<std::vector<float> >;

#pragma link C++ class std::pair<std::string,std::string>;

#endif
