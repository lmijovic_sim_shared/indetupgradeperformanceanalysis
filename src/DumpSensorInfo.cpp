#include "InDetUpgradePerformanceAnalysis/DumpSensorInfo.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include <iostream>

DumpSensorInfo::DumpSensorInfo() : Observable("SensorInfo") {}
DumpSensorInfo::DumpSensorInfo(const std::string & n) : Observable(n) {}

void DumpSensorInfo::Initialize() 
{
  if (sensor!=0) {
    std::cout<<"DumpSensorInfo::Initialize() "<<std::endl;
    SensorInfoBase::SensorStore::const_iterator it=sensor->sensors.begin();
    for (;it!=sensor->sensors.end();++it) {
      std::cout<<it->first<<" ("<<it->second.size()<<")"<<std::endl;
    }
  }
  else {
    std::cout<<"no sensor data found"<<std::endl;
  }
}


void DumpSensorInfo::Analyze(UpgPerfAna::AnalysisStep, int) {}

void DumpSensorInfo::StorePlots() 
{
}
