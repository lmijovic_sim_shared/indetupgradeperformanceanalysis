#include "TChain.h"
#include "TTree.h"
#include "TString.h"
#include "TStopwatch.h"
#include "TGraph.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <vector>

#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Cuts.h"
#include "InDetUpgradePerformanceAnalysis/Observable.h"
#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"

#include "TROOT.h"

using namespace std;

//void RunAnalysis(TChain *chain, char *outputFile, int);

int main(int argc, char **argv){

  gROOT->ProcessLine("#include <vector>");


  cout<<" argc="<<argc<<endl;
  if (argc<=2) {
    cerr<<" not enough arguments given"<<endl;
    return 1;
  } 

  InDetUpgradeAnalysis* ana = new InDetUpgradeAnalysis();

  char *inputFileName = argv[1];
  TString inputName = inputFileName;
  char* outputFile = argv[2];
  ana->SetOutputFileName(outputFile);
  cout << "--> opened file " << inputName << endl;

  //  ana->SetTreeHouse(new TreeHouse);
  //  ana->SetChain(new TChain("InDetTrackTree"));
  //  ana->SetTreeHouse(new TreeHouse);
  ana->SetChain("InDetTrackTree");
  if (argc>=4) {
    int nmax;
    std::stringstream str;
    str<<argv[3];
    str>>nmax;
    ana->SetMaxEvents(nmax);
  }
  ana->AddFile(inputName);
  ana->Test(0); // initialize some variables
  ana->Initialize();
  /*
  if(inputName.Contains("root", TString::kIgnoreCase)){
    cout<<inputName<<endl;
    chain->Add(inputName);
  }
  else {
    // part to parse txt file
    ifstream inputFile(inputName);
    while (!inputFile.eof()) {
      std::string line;
      inputFile>>line;
      cout<<line<<endl;
      chain->Add(line.c_str());
      if (nmax!=-1 && chain->GetEntries() > nmax) break;
    }
  } 
  */

  // *AS* how to access user (real) time ?
  TStopwatch cputime;
  cputime.Start(true);
  //  RunAnalysis(chain, outputFile,nmax);
  ana->Execute();
  cputime.Stop();
  cout << "This job ran for " << cputime.CpuTime() << "s (cpu time)" << endl;
  ana->Finalize();
  /*
  TFile * file = TFile::Open(outputFileName, "recreate");
  ana->StorePlots();
  eff->WritePlot();
  file->Close();
  */

  delete ana;
  return 0;
}

/*
void RunAnalysis(TChain *chain, char *outputFileName, int nmax){

  InDetUpgradeAnalysis* ana = new InDetUpgradeAnalysis();
  Efficiency* eff = new Efficiency();
  ana->Init(chain);
  int n_events = chain->GetEntries();
  cout << "Total number of events to be analysed: " << n_events << endl;
  if (nmax!=-1 && nmax<n_events) {
    n_events=nmax;
  }

  //  Observable* obs = new Observable();
  //  obs->DefinePlots();
  //  Cuts* instance = new Cuts();


  for(int i=0; i<n_events; i++){
    if(i%100 == 0){
      cout << "...processing event number " << i << endl;
    }
    chain->GetEvent(i);
    ana->DoIt(UpgPerfAna::INITIAL);
  }

  for(int i=0; i<n_events; i++){
    if(i%100 == 0){
      cout << "...processing event number " << i << endl;
    }
    chain->GetEvent(i);
    ana->DoIt(UpgPerfAna::CONTROL);
    eff->CalcEff();
  }


  ana->FillProfiles();
  eff->PlotEff();

  TFile * file = TFile::Open(outputFileName, "recreate");
  ana->StorePlots();
  eff->WritePlot();
  file->Close();

  delete ana;
  delete eff;
}
*/
