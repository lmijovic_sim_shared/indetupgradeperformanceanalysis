#include "InDetUpgradePerformanceAnalysis/PullObservable.h"
#include "TMath.h"
#include <iostream>

class Binning;

// Standard constructor
// @param varname Name of reco variable
// @param mctruth Name of truth (MC) variable
// @param binning Binning definition. Defaults to eta single sided in 25 bins
// @param fractional If true, calculate (reco-truth)/truth rather than (reco-truth). Default: false.
// @param indexstring Variable in the reco collection giving index of the associated truth particle. Default trk_mc_index
template < class observableT >
PullObservable< observableT >::PullObservable(const std::string & var_reco, const std::string & var_err_reco, const std::string & var_truth, Binning binning, std::string indexstring) :
  ResolutionObservable<observableT>(var_reco, 0, var_truth, indexstring, binning), 
  m_doubleIndexed(false), 
  m_var_err_reco(var_err_reco)
{
  this->nameobs=var_reco+"_"+var_truth+"_pull";
}

// Constructor for btag d3pd (?). Needs fixed.
// template < class observableT >
// PullObservable< observableT >::PullObservable(Binning binning, std::string indexstring,std::string indexstring_2, const std::string & varname, const std::string & mctruth, double pt) :
//     ResolutionObservable<observableT>(varname, pt, mctruth, indexstring, binning, "",  indexstring_2)
// {
//   std::cout<<"3rd"<<std::endl;
//   m_doubleIndexed = true;
//   this->nameobs=varname+"_"+mctruth;
// }

template < class observableT >
float PullObservable< observableT >::EvaluateVariable(int i/*referred to the iteration we are in*/, const std::string & ){ 

  // Get reco and MC valu
  float reco_val = this->rec_var[i];  
  int mc_index = (m_doubleIndexed) ? this->mc_index_2[this->mc_index[i]] : this->mc_index[i];
  float mc_val = this->MC_var[mc_index];
  float resolution = reco_val - mc_val;
  // Deal with discontinutities at +/- pi in phi variable
  if (this->varname=="trk_phi") resolution = wrap_pi(resolution);
  float pull = resolution / m_var_err_reco[i];

  return pull;
}
template class PullObservable<float>;


