#include <sstream>
#include <math.h>

#include "InDetUpgradePerformanceAnalysis/TrackJetMatchCut.h"

std::string strf(double value) {
  std::string s;
  std::stringstream sstr;
  sstr<<value;
  sstr>>s;
  return s;
}


//NB Jet Cone size can be explicitly set, but if a negative value is used, the default pt-dependent jet cone size (from b-tagging recommendations) will be used 

TrackJetMatchCut::TrackJetMatchCut(const std::string & etaTrkBranch, const std::string & phiTrkBranch, const std::string & etaJetBranch, const std::string & phiJetBranch, const std::string & jetPtBranch, const std::string & jetBBranch, double value, double jetPtCut_value, bool bjet)
  : Cuts("sqrt("+etaTrkBranch+"-"+etaJetBranch+")^2+("+phiTrkBranch+"-"+phiJetBranch+")^2)<"+strf(value)), etaTrk(etaTrkBranch), phiTrk(phiTrkBranch), etaJet(etaJetBranch), phiJet(phiJetBranch), ptJet(jetPtBranch),bJet(jetBBranch),matchCut(value),jetPtCut(jetPtCut_value),match_bjet(bjet)
{
}

bool TrackJetMatchCut::AcceptTrack(int i){

  bool passCut = false;


  int bJetType=999;


  //loop on jets to check track matching
  for(size_t j=0; j<phiJet.size(); j++){
 
    float closest_jet_pt = ptJet[j];
     if(match_bjet) bJetType = bJet[j];

    if (bJetType>0 && closest_jet_pt>jetPtCut){
    
      float param1 = 0.239;
      float param2 = -1.22;
      float param3 = -0.0000164;
      
      float jet_cone = (param1 + exp(param2 + param3*closest_jet_pt));  
      
      double matchCone = 0;
      
      if(matchCut<0) matchCone = jet_cone;
      else matchCone = matchCut;
      
      float deltaEta = fabs(etaTrk[i] - etaJet[j]);
      float deltaPhi = fabs(phiTrk[i] - phiJet[j]);
      float deltaR = sqrt(deltaEta*deltaEta+deltaPhi*deltaPhi);
      if(deltaR<matchCone){
	passCut = true;
	break;
      }
    }
  }
  
  return passCut;
}

int TrackJetMatchCut::AcceptAndMatchTrack(int i){

  //bool passCut = false;

  //loop on jets to check track matching

  int jet_index = -1;

  int bJetType=999;

  float deltaR_closest = 999;

  for(size_t j=0; j<phiJet.size(); j++){

    if(match_bjet) bJetType = bJet[j];
    double closest_jet_pt =  ptJet[j];

    if (bJetType>0 && closest_jet_pt>jetPtCut){
    
      double param1 = 0.239;
      double param2 = -1.22;
      double param3 = -0.0000164;
      
      double jet_cone = (param1 + exp(param2 + param3*closest_jet_pt));
      
      double matchCone = 0;
      
      if(matchCut<0) matchCone = jet_cone;
      else matchCone = matchCut;
      
      float deltaEta = fabs(etaTrk[i] - etaJet[j]);
      float deltaPhi = fabs(phiTrk[i] - phiJet[j]);
      float deltaR = sqrt(deltaEta*deltaEta+deltaPhi*deltaPhi);
      if(deltaR<matchCone && deltaR<deltaR_closest){
	//passCut = true; 
	jet_index=j;
	deltaR_closest = deltaR;
	//break;
      }
    }
    
  }
    
    return jet_index;
}


double TrackJetMatchCut::DeltaR(int track, int jet){
  
  float deltaEta = fabs(etaTrk[track] - etaJet[jet]);
  float deltaPhi = fabs(phiTrk[track] - phiJet[jet]);
  double deltaR = sqrt(deltaEta*deltaEta+deltaPhi*deltaPhi);
  
  return deltaR;
  
}

