#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TGraphErrors.h"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>

#include "InDetUpgradePerformanceAnalysis/SingleObservable.h"
#include "InDetUpgradePerformanceAnalysis/DiffObservable.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h"
#include "InDetUpgradePerformanceAnalysis/QOverPtObs.h"

#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "InDetUpgradePerformanceAnalysis/AllCuts.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCut.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/Selector.h"
#include "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include "InDetUpgradePerformanceAnalysis/FakeRate.h"

using namespace std;

InDetUpgradeAnalysis::InDetUpgradeAnalysis()
  : SingleAnalysis("InDetUpgradeAnalysis",0)
{}

InDetUpgradeAnalysis::~InDetUpgradeAnalysis()
{
}

/* for tests only */
void InDetUpgradeAnalysis::Test(TChain*){
  std::cout<<" InDetUpgradeAnalysis::Test()"<<std::endl;

  minProb = 0.5;

  AllCuts * cuts=0;
  ObservableGroup *obs=0;
  SingleAnalysis *sa=0;

  //  int pdgCode = 211; ///11 electrons, 13 muons, 211 pions
  int npoints =4;
  //  float pts[] = { 5000, 15000, 50000, 100000,1000 };
  std::string titles[] = { "muons pt=5GeV", "muons pt=15GeV", "muons pt=50GeV", "muons pt=100GeV", "all"};
  std::string paths[] = { "pt5", "pt15", "pt50", "pt100","all" };

  // *** setup resolution observables ***
    Add(sa=new SingleAnalysis("Resolutions",new TreeHouse(m_treeHouse->GetChain())));
  //Add(sa=new SingleAnalysis("Resolutions",m_treeHouse));
  sa->AddStep(UpgPerfAna::CONTROL);
  for (int i=0; i<npoints;++i) {
    sa->Add(obs=new ObservableGroup(titles[i]));  // loops over all tracks
    obs->SetPath(paths[i]);
    obs->SetCuts(cuts=new AllCuts());
    //  cuts->Add(new MinCut<int>("trk_mc_index",0));
    // cuts->Add(new AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdgCode));
    //if (paths[i] == "all") cuts->Add(new RangeCutWithIndex("mc_gen_pt","trk_mc_index",pts[i]-1000.,pts[i]+100000.));
    //else cuts->Add(new RangeCutWithIndex("mc_gen_pt","trk_mc_index",pts[i]-1000.,pts[i]+1000.));
    //cuts->Add(new MinCut<float>("trk_mc_probability", minProb));
    //cuts->Add(new RangeCut<int>("trk_mc_barcode", 1, 200000));
    //cuts->Add(new StandardCuts()); // standard track cuts
    //obs->Add(new DiffObservable("trk_d0","mc_perigee_d0"));
    //obs->Add(new DiffObservable("trk_z0","mc_perigee_z0"));
    //obs->Add(new DiffObservable("trk_phi","mc_perigee_phi"));
    //obs->Add(new DiffObservable("trk_theta","mc_perigee_theta"));
    //obs->Add(new DiffObservable("trk_pt","mc_gen_pt"));
    //obs->Add(new Z0SinThetaObs("trk_z0", "mc_perigee_z0", "mc_perigee_theta"));
    //obs->Add(new QOverPtObs("trk_qoverp", "mc_perigee_qoverp", "mc_perigee_theta"));
  } 


  Add(sa=new SingleAnalysis("SingleAnalysis",new TreeHouse(m_treeHouse->GetChain())));
  for (int i=0; i<npoints;++i) {
    sa->Add(obs=new ObservableGroup(titles[i]));  // loops over all gen particles
    obs->SetPath(paths[i]);
    obs->SetCuts(cuts=new AllCuts());
    ///      cuts->Add(new AbsSelector("mc_gen_type",pdgCode));
    ///    cuts->Add(new RangeCut<float>("mc_gen_pt",pts[i]-1000.,pts[i]+1000.));
    //  cuts->Add(new MinCut<float>("mc_gen_pt", 1000));
    //  cuts->Add(new RangeCut<int>("trk_mc_barcode", 1, 200000));
    obs->Add(new Efficiency() );
  }

  Add(sa=new SingleAnalysis("SingleAnalysis",new TreeHouse(m_treeHouse->GetChain())));
  for (int i=0; i<npoints;++i) {
    sa->Add(obs=new ObservableGroup(titles[i]));  // loops over all tracks 
    obs->SetPath(paths[i]);
    obs->SetCuts(cuts=new AllCuts());
    ///     cuts->Add(new MinCut<int>("trk_mc_index",0));
    ///     cuts->Add(new AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdgCode));
    ///     cuts->Add(new RangeCutWithIndex("mc_gen_pt","trk_mc_index",pts[i]-1000.,pts[i]+1000.));
    //cuts->Add(new StandardCuts()); // standard track cuts
    obs->Add(new FakeRate());
  }
}

void InDetUpgradeAnalysis::Add(Observable * obs)
{
  SingleAnalysis* sa = dynamic_cast<SingleAnalysis*>(obs);
  if (sa) {
    ObservableGroup::Add(sa);
  }
  else {
    // create a single analysis if needed 
    if (m_obs.size()==0) {
      std::cout<<"creating SingleAnalysis"<<std::endl;
      sa = new SingleAnalysis("SingleAnalysis",m_treeHouse);
      ObservableGroup::Add(sa);
    }
    // add to last entry
    sa= dynamic_cast<SingleAnalysis*>(m_obs.back());
    sa->Add(obs);
  }
}

void InDetUpgradeAnalysis::SetCuts(Cuts * cuts)
{
  // create a new single analysis if needed 
  if (m_obs.size()==0) {
    std::cout<<"creating SingleAnalysis"<<std::endl;
    SingleAnalysis* sa = new SingleAnalysis("SingleAnalysis",m_treeHouse);
    ObservableGroup::Add(sa);
  }
  // add to last entry
  SingleAnalysis * sa= dynamic_cast<SingleAnalysis*>(m_obs.back());
  sa->SetCuts(cuts);
}

// sould perform loop over analysis steps and sub-analyses
UpgPerfAna::AnalysisStep InDetUpgradeAnalysis::Execute()
{
  std::cout<<"InDetUpgradeAnalysis::Execute()"<<std::endl;
  for (size_t i=0; i<m_obs.size(); i++) {
    m_obs[i]->Execute(); 
  }
  return UpgPerfAna::DONE;
} 

/*
void InDetUpgradeAnalysis::DoIt(UpgPerfAna::AnalysisStep flag=UpgPerfAna::INITIAL) {
  ObservableGroup::AnalyzeEvent(flag);  // check FillPlots is replaced with Analyze
}
*/

void InDetUpgradeAnalysis::Finalize() {
  std::cout<<"InDetUpgradeAnalysis::Finalize()"<<std::endl;
  SingleAnalysis::Finalize();
  TFile * file = TFile::Open(m_outfile, "recreate");
  StorePlots();
  //eff->WritePlot();
  file->Close();
  std::cout<<this;
  delete m_treeHouse;
}

/*
void InDetUpgradeAnalysis::FillProfiles() {
  for (size_t i=0; i<obs.size(); i++) {
       obs[i]->FillProfile();
  }

}
*/
/*
void InDetUpgradeAnalysis::StorePlots() {
  for (size_t i=0; i<obs.size(); i++) {
    obs[i]->StorePlots(); 
  }

}
*/

void SafeWrite(TObject* obj) {
    // Check an object of teh same name doesn't already exist. If so, increment name
    TString newname = obj->GetName();

    if (gDirectory->GetListOfKeys()->Contains(obj->GetName())) {
        TString oldname(obj->GetName());
        TString lastchar( oldname(oldname.Length()-1,1));
        if (lastchar.IsDigit()) {
          int num = lastchar.Atoi();
            num++;
            newname.Replace(newname.Length()-1, 1, Form("%i", num));
        } else {
            newname += "_1";
        }
        obj->Write(newname);
    }

    obj->Write();
}

TGraphErrors* MakeGraphErrors(std::string label, int nPoint, Double_t *xvars, Double_t *yvars, Double_t *xerrs, Double_t *yerrs, Binning& binning, std::string ylabel) {

    std::string title = label;
    if (binning.name() != "") {
    title += "_" +binning.name();
    }

    // Number of outliers
    TGraphErrors *graph = new TGraphErrors(nPoint, xvars, yvars, xerrs, yerrs);
    graph->SetNameTitle(title.c_str(), title.c_str());
    graph->GetXaxis()->SetTitle(binning.label().c_str());

    if (ylabel == "") ylabel = label;
    graph->GetYaxis()->SetTitle(ylabel.c_str());

    return graph;
}
