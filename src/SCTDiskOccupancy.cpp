#include "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TH2D.h"

#include <iostream>

SCTDiskOccupancy::SCTDiskOccupancy() :
  DiskOccupancy("SCTDiskOccupancy","sctdisk"),
  sctClus_x("sctClus_x"), sctClus_y("sctClus_y"), sctClus_z("sctClus_z"),
  sctClus_size("sctClus_size"), sctClus_layer("sctClus_layer"), 
  sctClus_bec("sctClus_bec"), sctClus_eta_module("sctClus_eta_module"),
  sctClus_detElementId("sctClus_detElementId")
 {}

void SCTDiskOccupancy::Initialize() {
  // create standard histograms
  OccupancyObservable::Initialize();

  // prepare counters for each etaID and layer
  if (sensor != 0 ) {
    ndisk = sensor->getValue("nsctdisk");
    for (int i=0; i<ndisk; ++i) {

      const SensorInfoBase* disk = sensor->get(moduleType,i);
      int nring = disk->getValue("nring");
      for (int j=0; j<nring; ++j) {
	
	int module=j;
	if (doubleStripLength) {
	  if (j<8) {
	    if (j%2) continue; // merge first 8 rings into 4
	    else module/=2;
	  }
	  else module-=4;
	}
	const SensorInfoBase* ring = disk->get("ring",j);
        const SensorInfoBase* ringnext = 0;
	if (doubleStripLength && j<8) ringnext = disk->get("ring",j+1);
	
	Counters c=Counters();

	int total = ring->getValue("total");
	//if (doubleStripLength && j<8) { total += ringnext->getValue("total"); }
	c.setTotalPixel(total);
	
	float area = ring->getFloat("area")*ring->getValue("nmodule")*2;
	if (doubleStripLength && j<8) { 
          area += ringnext->getFloat("area")*ringnext->getValue("nmodule")*2; 
        } 
	c.setArea(area);   // TODO: remove hardcoded 2

	counters[ std::make_pair(i,module) ] = c;
	counters[ std::make_pair(i+ndisk,module) ] = c;
      }
    }
  }
}

void SCTDiskOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event counters during "EVENT" step
  if (OccupancyObservable::EventInit(step,k)) return;
  
  // enter occupancys in counters
  if (step==UpgPerfAna::INITIAL && sctClus_bec[k]!=0){  // discs only
    int module=sctClus_eta_module[k];    
    if (doubleStripLength) {
      if (module<8) { // first 8 rings are merged into 4
	if (module%2) module=(module-1)/2;
	else module/=2;
      }
      else module-=4;      
    }
    int layer=sctClus_layer[k];
    LMPair layer_module = std::make_pair(layer,module);
    if (sctClus_bec[k]==-2) {
      layer_module=std::make_pair(layer+ndisk,module);
    }
    float x=sctClus_x[k];
    float y=sctClus_y[k];
    float z=sctClus_z[k];
    float s=sctClus_size[k];
    float r=sqrt(x*x+y*y);
    // if (layer==0 && module==1) {
    //   std::cout<<"cs = "<<s<<std::endl;
    // }
    counters[layer_module].add(x,y,z,s,sctClus_detElementId[k]);
    // fill cluster and hit maps
    m_histos[CLUS_MAP]->Fill(z,r);
    static_cast<TH2*>(m_histos[HIT_MAP])->Fill(z,r,s);
  }
}


int SCTDiskOccupancy::GetNTrks() {
  //  std::cout<<"sctClus_n="<<sctClus_x.size()<<std::endl;
  return sctClus_x.size();
}
