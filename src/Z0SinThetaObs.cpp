#include "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h"
#include "TMath.h"

template < class observableT >
Z0SinThetaObs< observableT >::Z0SinThetaObs(const std::string & varname, const std::string & mctruth, const std::string & theta, Binning binning, std::string indexstring, double pt) :
    ResolutionObservable<observableT>(varname, pt, mctruth, indexstring, binning, theta), eta_var("trk_eta")
{
  this->nameobs=varname+"_"+mctruth+"_"+theta;
}

template < class observableT >
float Z0SinThetaObs< observableT >::EvaluateVariable(int i/*referred to the iteration we are in*/, const std::string & /*varname*/){ 
  
  float variable = (this->rec_var[i]/TMath::CosH(eta_var[i])) - (this->MC_var[this->mc_index[i]]*TMath::Sin(this->add_var[this->mc_index[i]]));
  return variable;
}
template class Z0SinThetaObs<float>;
