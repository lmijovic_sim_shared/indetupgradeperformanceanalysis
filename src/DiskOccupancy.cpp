#include "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TMultiGraph.h"

DiskOccupancy::DiskOccupancy(const std::string & name, const std::string & mType ) :
  OccupancyObservable(name,mType),
  doubleStripLength(false)
{}

void DiskOccupancy::Finalize() {
  float pileup = 200.;
  float factor = pileup*100.; 

  std::cout<<Name()<<"::Finalize()"<<std::endl;

  //  create occupancy graphs
  TMultiGraph *mg = CreateMultiGraph("occ_%s_mg",moduleType);
  TMultiGraph *mgr = CreateMultiGraph("occ_%s_mgr",moduleType);
  TMultiGraph *hits_mg = CreateMultiGraph("hit_%s_mg",moduleType);
  TMultiGraph *hits_mgr = CreateMultiGraph("hit_%s_mgr",moduleType);
  TMultiGraph *clus_mg = CreateMultiGraph("clus_%s_mg",moduleType);
  TMultiGraph *clus_mgr = CreateMultiGraph("clus_%s_mgr",moduleType);
  TMultiGraph *size_mg = CreateMultiGraph("size_%s_mg",moduleType);
  TMultiGraph *size_mgr = CreateMultiGraph("size_%s_mgr",moduleType);
  TMultiGraph *flu_mg = 0;
  TMultiGraph *flu_mgr = 0;

  if ( counters[std::make_pair(0,0)].area() >0.) {
    flu_mg = CreateMultiGraph("flu_%s_mg",moduleType);
    flu_mgr = CreateMultiGraph("flu_%s_mgr",moduleType);
  }

  for (int i=0; i<ndisk*2; ++i ) {
    const SensorInfoBase* disk = sensor->get(moduleType,i%ndisk);
    int nring = disk->getValue("nring");
    if (doubleStripLength) nring-=4;

    TGraphErrors * g = CreateGraph(nring,"occ_%s_%d",moduleType,i);
    TGraphErrors * gr = CreateGraph(nring,"occ_r_%s_%d",moduleType,i);
    TGraphErrors * hits = CreateGraph(nring,"hit_%s_%d",moduleType,i);
    TGraphErrors * hits_r = CreateGraph(nring,"hit_r_%s_%d",moduleType,i);
    TGraphErrors * clus = CreateGraph(nring,"clus_%s_%d",moduleType,i);
    TGraphErrors * clus_r = CreateGraph(nring,"clus_r_%s_%d",moduleType,i);
    TGraphErrors * size = CreateGraph(nring,"size_%s_%d",moduleType,i);
    TGraphErrors * size_r = CreateGraph(nring,"size_r_%s_%d",moduleType,i);
    TGraphErrors * flu = 0;
    TGraphErrors * flu_r = 0;
    if ( counters[std::make_pair(0,0)].area() >0.) {
      flu = CreateGraph(nring,"flu_%s_%d",moduleType,i);
      flu_r = CreateGraph(nring,"flu_r_%s_%d",moduleType,i);
    }

    for (int k=0; k<nring;++k) {
      Counters & c=counters.find(std::make_pair(i,k))->second;
      float r=c.r();
      float o=factor*c.occupancy();
      float oerr=factor*c.occupancyError();
      float h=pileup*c.hits();
      float herr=pileup*c.hitsError();
      float cl=pileup*c.cluster();
      float clerr=pileup*c.clusterError();
      float a=c.area();

      g->SetPoint(k,k,o);
      g->SetPointError(k,0,oerr);
      gr->SetPoint(k,r,o);
      gr->SetPointError(k,0,oerr);
      hits->SetPoint(k,k,h);
      hits->SetPointError(k,0,herr);
      hits_r->SetPoint(k,r,h);
      hits_r->SetPointError(k,0,herr);
      clus->SetPoint(k,k,cl);
      clus->SetPointError(k,0,clerr);
      clus_r->SetPoint(k,r,cl);
      clus_r->SetPointError(k,0,clerr);
      size->SetPoint(k,k,h/cl);
      size->SetPointError(k,0,herr/cl);
      size_r->SetPoint(k,r,h/cl);
      size_r->SetPointError(k,0,herr/cl);

      if (a>0.) {
	flu->SetPoint(k,k,h/a);
	flu_r->SetPoint(k,r,h/a);
      }
    }
    mg->Add(g,"lp");
    mgr->Add(gr,"lp");
    hits_mg->Add(hits,"lp");
    hits_mgr->Add(hits_r,"lp");
    clus_mg->Add(clus,"lp");
    clus_mgr->Add(clus_r,"lp");
    size_mg->Add(size,"lp");
    size_mgr->Add(size_r,"lp");
    if ( counters[std::make_pair(0,0)].area() >0.) {
      flu_mg->Add(flu,"lp");
      flu_mgr->Add(flu_r,"lp");

    }
  }
}

