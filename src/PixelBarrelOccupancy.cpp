#include "InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TH2D.h"

PixelBarrelOccupancy::PixelBarrelOccupancy(bool is_xaod) :
  OccupancyObservable("PixelBarrelOccupancy","pixlayer"),
  pixClus_x_varname( (is_xaod) ? "PixelClustersAux.globalX" : "pixClus_x"),
  pixClus_y_varname( (is_xaod) ? "PixelClustersAux.globalY" : "pixClus_y"),
  pixClus_z_varname( (is_xaod) ? "PixelClustersAux.globalZ" : "pixClus_z"),
  pixClus_size_varname( (is_xaod) ? "PixelClustersAux." : "pixClus_size"),
  pixClus_layer_varname( (is_xaod) ? "PixelClustersAux." : "pixClus_layer"), 
  pixClus_bec_varname( (is_xaod) ? "PixelClustersAux.global" : "pixClus_bec"),
  pixClus_eta_module_varname((is_xaod) ? "PixelClustersAux." : "pixClus_eta_module"),
  pixClus_detElementId_varname((is_xaod) ? "PixelClustersAux." : "pixClus_detElementId"),
  nlayer(0),
  pixClus_x(pixClus_x_varname), pixClus_y(pixClus_y_varname), pixClus_z(pixClus_z_varname),
  pixClus_size(pixClus_size_varname), pixClus_layer(pixClus_layer_varname), 
  pixClus_bec(pixClus_bec_varname), pixClus_eta_module(pixClus_eta_module_varname),
  pixClus_detElementId(pixClus_detElementId_varname)
 {

 }

void PixelBarrelOccupancy::Initialize() {
  // create standard histograms
  OccupancyObservable::Initialize(); 

  // prepare counters for each etaID and layer
  if (sensor != 0 ) {
    nlayer = sensor->getValue("npixlayer");
    for (int i=0; i<nlayer; ++i) {
      const SensorInfoBase* layer = sensor->get(moduleType,i);
      int nmodule = layer->getValue("nmodule");
      // area is area of chip
      float area  = layer->getFloat("area");
      int nmax = nmodule/2;
      int nmin = -nmax;
      bool hasZero = (nmodule%2!=0);
      //      std::cout<<"layer="<<i<<" nmodule="<<nmodule<<std::endl;
      for (int j=nmin; j<=nmax; ++j) {
        if (j==0 && !hasZero ) continue;
        Counters c=Counters();
        c.setArea(layer->getValue("sector")*area);
        c.setTotalPixel(layer->getValue("perModuleEta"));
        counters[ std::make_pair(i,j) ] = c;
      }
    }
  }
}

void PixelBarrelOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event counters during "EVENT" step
  if (OccupancyObservable::EventInit(step,k)) return;
  // enter occupancys in counters    
  if (pixClus_bec[k]==0){  // barrel only
    int module=pixClus_eta_module[k];
    int layer=pixClus_layer[k];
    LMPair layer_module = std::make_pair(layer,module);
    float x=pixClus_x[k];
    float y=pixClus_y[k];
    float z=pixClus_z[k];
    float s=pixClus_size[k];
    float r=sqrt(x*x+y*y);
    //printf("PixelBarrelOccupancy %llu %.3f %.3f %.3f %i\n", pixClus_detElementId[k], x, y, z, s);
    counters[layer_module].add(x,y,z,s,pixClus_detElementId[k]);
    // fill cluster and hit maps
    m_histos[CLUS_MAP]->Fill(z,r);
    m_histos[CLUS_MAP_DETAIL]->Fill(z,r);
    static_cast<TH2*>(m_histos[HIT_MAP])->Fill(z,r,s);
    static_cast<TH2*>(m_histos[HIT_MAP_DETAIL])->Fill(z,r,s);
  }
}

void PixelBarrelOccupancy::Finalize() {
  float pileup = 200.;
  float factor = pileup*100.; 

  std::cout<<"PixelBarrelOccupancy::Finalize()"<<std::endl;

  //  create occupancy graphs
  TMultiGraph *mg = CreateMultiGraph("occ_%s_mg",moduleType);
  TMultiGraph *mgz = CreateMultiGraph("occ_%s_mgz",moduleType);
  TMultiGraph *hits_mg = CreateMultiGraph("hit_%s_mg",moduleType);
  TMultiGraph *hits_mgz = CreateMultiGraph("hit_%s_mgz",moduleType);
  TMultiGraph *clus_mg = CreateMultiGraph("clus_%s_mg",moduleType);
  TMultiGraph *clus_mgz = CreateMultiGraph("clus_%s_mgz",moduleType);
  TMultiGraph *clus_mgz_sides = CreateMultiGraph("clus_%s_sides_mgz",moduleType);
  TMultiGraph *size_mg = CreateMultiGraph("size_%s_mg",moduleType);
  TMultiGraph *size_mgz = CreateMultiGraph("size_%s_mgz",moduleType);
  TMultiGraph *flu_mg = 0;
  TMultiGraph *flu_mgz = 0;

  if ( counters[std::make_pair(0,1)].area() >0.) {
    flu_mg = CreateMultiGraph("flu_%s_mg",moduleType);
    flu_mgz = CreateMultiGraph("flu_%s_mgz",moduleType);
  }

  for (int i=0; i<nlayer; ++i ) {
    const SensorInfoBase* layer = sensor->get(moduleType,i);

    int nmodule = layer->getValue("nmodule");
    int nmax=nmodule/2;
    int nmin=-nmax;
    bool hasZero=(nmodule%2!=0);

    TGraphErrors * g = CreateGraph(nmodule,"occ_%s_%d",moduleType,i);
    TGraphErrors * gz = CreateGraph(nmodule,"occ_z_%s_%d",moduleType,i);
    TGraphErrors * hits = CreateGraph(nmodule,"hit_%s_%d",moduleType,i);
    TGraphErrors * hits_z = CreateGraph(nmodule,"hit_z_%s_%d",moduleType,i);
    TGraphErrors * clus = CreateGraph(nmodule,"clus_%s_%d",moduleType,i);
    TGraphErrors * clus_z = CreateGraph(nmodule,"clus_z_%s_%d",moduleType,i);
    TGraphErrors * clus_z_neg = CreateGraph(nmodule/2,"clus_z_%s_%d_neg",moduleType,i);
    TGraphErrors * clus_z_pos = CreateGraph(nmodule/2,"clus_z_%s_%d_pos",moduleType,i);
    TGraphErrors * size = CreateGraph(nmodule,"size_%s_%d",moduleType,i);
    TGraphErrors * size_z = CreateGraph(nmodule,"size_z_%s_%d",moduleType,i);
    TGraphErrors * flu = 0;
    TGraphErrors * flu_z = 0;

    clus_z_pos->SetLineStyle(2);
    clus_z_pos->SetMarkerStyle(21);
    clus_z_pos->SetTitle( TString(clus_z_pos->GetTitle()) + " (z>0)");
    clus_z_neg->SetTitle( TString(clus_z_neg->GetTitle()) + " (z<0)");

    if ( counters[std::make_pair(i,1)].area() >0.) {
      flu = CreateGraph(nmodule,"flu_%s_%d",moduleType,i);
      flu_z = CreateGraph(nmodule,"flu_z_%s_%d",moduleType,i);
    }

    for (int k=nmin,j=0,j_neg=0,j_pos=0; k<=nmax; ++k) {
      if (k==0 && !hasZero ) continue;
      Counters & c=counters.find(std::make_pair(i,k))->second;
      float z=c.z();
      float o=factor*c.occupancy();
      float oerr=factor*c.occupancyError();
      float h=pileup*c.hits();
      float herr=pileup*c.hitsError();
      float cl=pileup*c.cluster();
      float clerr=pileup*c.clusterError();
      float a=c.area();

      g->SetPoint(j,k,o);
      g->SetPointError(j,0,oerr);
      gz->SetPoint(j,z,o);
      gz->SetPointError(j,0,oerr);
      hits->SetPoint(j,k,h);
      hits->SetPointError(j,0,herr);
      hits_z->SetPoint(j,z,h);
      hits_z->SetPointError(j,0,herr);
      clus->SetPoint(j,k,cl);
      clus->SetPointError(j,0,clerr);
      clus_z->SetPoint(j,z,cl);
      clus_z->SetPointError(j,0,clerr);
      if (z<=0) {
        clus_z_neg->SetPoint(j_neg, fabs(z), cl);
        clus_z_neg->SetPointError(j_neg,0,clerr);
        j_neg++;
      } else {
        clus_z_pos->SetPoint(j_pos,z,cl);
        clus_z_pos->SetPointError(j_pos,0,clerr);
        j_pos++;
      }
      size->SetPoint(j,k,h/cl);
      size->SetPointError(j,0,herr/cl);
      size_z->SetPoint(j,z,h/cl);
      size_z->SetPointError(j,0,herr/cl);
      if (a>0.) {
        flu->SetPoint(j,k,h/a);
        flu_z->SetPoint(j,z,h/a);
      }
      ++j;
    }
    mg->Add(g,"lp");
    mgz->Add(gz,"lp");
    hits_mg->Add(hits,"lp");
    hits_mgz->Add(hits_z,"lp");
    clus_mg->Add(clus,"lp");
    clus_mgz->Add(clus_z,"lp");
    clus_mgz_sides->Add(clus_z_pos,"lp");
    clus_mgz_sides->Add(clus_z_neg,"lp");
    size_mg->Add(size,"lp");
    size_mgz->Add(size_z,"lp");
    if ( counters[std::make_pair(i,1)].area() >0.) {
      flu_mg->Add(flu,"lp");
      flu_mgz->Add(flu_z,"lp");
    }
  }
}

int PixelBarrelOccupancy::GetNTrks() {
  //std::cout<<"pixClus_n="<<pixClus_x.size()<<std::endl;
  return pixClus_x.size();
}
