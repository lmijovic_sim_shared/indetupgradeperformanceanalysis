#include "InDetUpgradePerformanceAnalysis/TrackRatio.h"
#include <iostream>
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/TrackJetMatchCut.h"
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TCanvas.h"

using namespace std;

//
// Track ratio - plots nRecoTracks and nRecoTracks/nTruthTracks as a function of the binning variable
// A common choice of binning would be mu
// Should be added in an ObservableGroup  in a loop over tracks

// Constructor
//
TrackRatio::TrackRatio( Cuts* truth_cuts, Cuts* reco_cuts, Binning binning ) :
    Observable("TrackRatio"), 
    m_binning(binning),
    m_track_binning( Binning("dummy", 100, 0, 2000, "nRecoTrack") ),
    m_truth_binning( Binning("dummy", 100, 0, 2000, "nTruthTrack") ),
    mc_n("mc_n"),
    trk_n("trk_n"),
    trk_mc_index("trk_mc_index"),
    trk_mc_probability("trk_mc_probability"),
    m_reco_cuts(reco_cuts),
    m_truth_cuts(truth_cuts)
{

  //cout << reco_cuts << endl;


}

void TrackRatio::Initialize()
{
  //std::cout << "TrackRatio Binning " << GetBinning()->GetNBins() << " " << GetBinning()->GetMinimum() <<  " " << GetBinning()->GetMaximum() << std::endl;
    nTrack_puBins.resize(GetBinning()->GetNBins());
    nTruth_puBins.resize(GetBinning()->GetNBins());
    nTruthMatched_puBins.resize(GetBinning()->GetNBins());
    nEv_puBins.resize(GetBinning()->GetNBins());

    for(int l=0; l < GetBinning()->GetNBins(); l++){
        nTrack_puBins[l] = 0;
        nTruth_puBins[l] = 0;
        nTruthMatched_puBins[l] = 0;
        nEv_puBins[l] = 0;
    }

    nTrack_trackBins.resize( m_track_binning.GetNBins() );
    nTruth_trackBins.resize( m_track_binning.GetNBins() );
    nTruthMatched_trackBins.resize( m_track_binning.GetNBins() );
    nEv_trackBins.resize( m_track_binning.GetNBins() );

    for(int l=0; l < m_track_binning.GetNBins(); l++){
        nTrack_trackBins[l] = 0;
        nTruth_trackBins[l] = 0;
        nTruthMatched_trackBins[l] = 0;
        nEv_trackBins[l] = 0;
    }

    nTrack_truthBins.resize( m_truth_binning.GetNBins() );
    nTruth_truthBins.resize( m_truth_binning.GetNBins() );
    nTruthMatched_truthBins.resize( m_truth_binning.GetNBins() );
    nEv_truthBins.resize( m_truth_binning.GetNBins() );

    for(int l=0; l < m_truth_binning.GetNBins(); l++){
        nTrack_truthBins[l] = 0;
        nTruth_truthBins[l] = 0;
        nTruthMatched_truthBins[l] = 0;
        nEv_truthBins[l] = 0;
    }

}

void TrackRatio::InitializeEvent(UpgPerfAna::AnalysisStep step)
{
  //std::cout << "TrackRatio Binning " << GetBinning()->GetNBins() << " " << GetBinning()->GetMinimum() <<  " " << GetBinning()->GetMaximum() << std::endl;
  cout << "TrackRatio::InitializeEvent called - handing to TrackRatio::AnalyzeEvent" << endl;

  AnalyzeEvent(step);
}

/** AnalyzeEvent
 *  All the processing done here
 *  Is a bit of a hack, but this class doesn't fit the normal model of doing one loop over the objects
 */
void TrackRatio::AnalyzeEvent(UpgPerfAna::AnalysisStep) {

    //cout << "TrackRatio::AnalyzeEvent" << endl;
  
  // Reset counters
  ev_nTruth = ev_nTrack = ev_nTruthMatched = 0;

  // Get vector of indiced of selected tracks
  std::vector<int> selected_tracks;
  SelectTracks( selected_tracks );
  ev_nTrack = selected_tracks.size();
  // Count the number of selected truth particles and the number that have a matching track
  CountTruth(selected_tracks);

  FillCounters();

}


void TrackRatio::SelectTracks( std::vector<int> & selected_tracks ) {
  for (int iTrack=0; iTrack < trk_n.Get(); iTrack++) {
    if (m_reco_cuts->AcceptTrack(iTrack)) selected_tracks.push_back(iTrack);
  }
  //cout << "trk_n " << trk_n.Get() << " selected_tracks " << selected_tracks.size() << endl;
}

// Fill the counters
void TrackRatio::FillCounters(  ) {
  // Pilup binned
  int puBin = GetBinning()->GetBinIndex(0);
  nTrack_puBins[puBin] += ev_nTrack;
  nTruth_puBins[puBin] += ev_nTruth;
  nTruthMatched_puBins[puBin] += ev_nTruthMatched;
  nEv_puBins[puBin] += 1;
  // Track binned
  int trackBin = m_track_binning.GetBinIndexValue(ev_nTrack);
  nTrack_trackBins[trackBin] += ev_nTrack;
  nTruth_trackBins[trackBin] += ev_nTruth;
  nTruthMatched_trackBins[trackBin] += ev_nTruthMatched;
  nEv_trackBins[trackBin] += 1;
  // Truth binned
  int truthBin = m_truth_binning.GetBinIndexValue(ev_nTruth);
  nTrack_truthBins[truthBin] += ev_nTrack;
  nTruth_truthBins[truthBin] += ev_nTruth;
  nTruthMatched_truthBins[trackBin] += ev_nTruthMatched;
  nEv_truthBins[trackBin] += 1;
}

void TrackRatio::CountTruth(  const std::vector<int> & selected_tracks ) {
  for (int iTruth=0; iTruth < mc_n.Get(); iTruth++) {
    if (!m_truth_cuts->AcceptTrack(iTruth)) continue;
    ev_nTruth++;
    std::vector<int>::const_iterator itTrack = selected_tracks.begin();
    for (; itTrack != selected_tracks.end(); itTrack++) {
      if (trk_mc_index[*itTrack]==iTruth) {
        if (trk_mc_probability[*itTrack]>0.5) {
            ev_nTruthMatched+=1;
            break;
        }
      }
    }
  }
  //cout << "mc_n " << mc_n.Get() << " selected_truth " << ev_nTruth++ << endl;
}

void TrackRatio::Analyze(UpgPerfAna::AnalysisStep, int ) {
    std::cerr << "ERROR - must add TrackRatio to an ObservableGroup with RunLoop(false)" << std::endl;
}


void TrackRatio::Finalize(){
  m_graphs.push_back( MakeRatioGraph(*GetBinning(), nTrack_puBins, nTruth_puBins, "recoToTruthTrackRatio", "nTracksReco/nTracksTruth", SIMPLE) );
  m_graphs.push_back( MakeRatioGraph(*GetBinning(), nTrack_puBins, nTruthMatched_puBins, "recoToTruthMatchedTrackRatio", "nTracksReco/nTracksTruth (normalised by efficiency)", INVERSE_BINOMIAL) );
  m_graphs.push_back( MakeRatioGraph(*GetBinning(), nTrack_puBins,  nEv_puBins, "recoTracks", "nTracksReco", NUM_ONLY ));

  m_graphs.push_back( MakeRatioGraph(m_track_binning, nTrack_trackBins, nTruth_trackBins, "recoToTruthTrackRatio", "nTracksReco/nTracksTruth", SIMPLE) );
  m_graphs.push_back( MakeRatioGraph(m_track_binning, nTrack_trackBins, nTruthMatched_trackBins, "recoToTruthMatchedTrackRatio", "nTracksReco/nTracksTruth (normalised by efficiency)", INVERSE_BINOMIAL) );
  //m_graphs.push_back( MakeRatioGraph(m_track_binning, nTrack_trackBins,  nEv_trackBins, "recoTracks", "nTracksReco") );
  //
  m_graphs.push_back( MakeRatioGraph(m_truth_binning, nTrack_truthBins, nTruth_truthBins, "recoToTruthTrackRatio", "nTruthsReco/nTracksTruth", SIMPLE) );
  m_graphs.push_back( MakeRatioGraph(m_truth_binning, nTrack_truthBins, nTruthMatched_truthBins, "recoToTruthMatchedTrackRatio", "nTracksReco/nTracksTruth (normalised by efficiency)", INVERSE_BINOMIAL) );
  m_graphs.push_back( MakeRatioGraph(m_truth_binning, nTrack_truthBins,  nEv_truthBins, "recoTracks", "nTracksReco") );

  cout << "TrackRatio Cutflow" << endl;
  cout << m_reco_cuts;
  cout << m_truth_cuts;
}

TGraphErrors* TrackRatio::MakeGraph( Binning &binning, std::vector<int> &num, string name, string ylabel) {

    int nbins = binning.GetNBins();
    double* x = new double[nbins];
    double* xerr = new double[nbins];
    double* y = new double[nbins];
    double* yerr = new double[nbins];

    for (int i=0; i< nbins; i++) {
        x[i] = binning.GetBinCenter(i);
        xerr[i] = binning.GetBinWidth(i)/2;
        y[i] = (double)(num[i]);
        yerr[i] = (num[i]>0) ? sqrt((double)num[i]) : 0.;
    }

    TGraphErrors* effPlot = MakeGraphErrors(name, nbins, x, y, xerr, yerr, binning, ylabel);

    delete[] x;
    delete[] xerr;
    delete[] y;
    delete[] yerr;
    return effPlot;
}

TGraphErrors* TrackRatio::MakeRatioGraph( Binning &binning, std::vector<int> &num, std::vector<int> & denom, string name, string ylabel, RatioErrorType error) {

    int nbins = binning.GetNBins();
    double* x = new double[nbins];
    double* xerr = new double[nbins];
    double* y = new double[nbins];
    double* yerr = new double[nbins];

    //cout << endl << name << " " << error << endl;
    for (int i=0; i< nbins; i++) {
        x[i] = binning.GetBinCenter(i);
        xerr[i] = binning.GetBinWidth(i)/2;
        if ((double)(denom[i])<1.0) y[i] = 0;
        else y[i] = (double)(num[i])/(double)(denom[i]);
        if (error==BINOMIAL) {
          yerr[i] = (denom[i]>0) ? sqrt((y[i] * (1-y[i])) / ((double)denom[i])) : 0;
        } else if (error==INVERSE_BINOMIAL) {
          double inv_err = (num[i]>0) ? sqrt(( (1.0/y[i]) * (1-(1.0/y[i]))) / ((double) num[i])) : 0;
          yerr[i] = y[i]*y[i] * inv_err; 
        } else if (error==NUM_ONLY) {
          yerr[i] = sqrt(y[i]) / denom[i];
        } else if (error==SIMPLE){
          yerr[i] = y[i] * sqrt((1./num[i]) + (1./denom[i]));
        }
        //cout << num[i] << "/" << denom[i] << " " << y[i] << "+/-" << yerr[i] << endl;
    }

    TGraphErrors* effPlot = MakeGraphErrors(name, nbins, x, y, xerr, yerr, binning, ylabel);

    delete[] x;
    delete[] xerr;
    delete[] y;
    delete[] yerr;
    return effPlot;
}

void TrackRatio::StorePlots()
{
  cd(path);
  std::vector<TGraphErrors*>::const_iterator grIter = m_graphs.begin();
  for (; grIter != m_graphs.end(); grIter++) (*grIter)->Write();
  cd("..");
}

int TrackRatio::GetNTrks()
{
    //return mcEta.size();
  return 0;
}

std::ostream & operator<<(std::ostream & s, const Observable* obs) 
{
    int & l=Observable::output_level;

    s<<Spaces(l)<<"Observable : "<<obs->nameobs<<std::endl;
    const ObservableGroup* og = dynamic_cast<const ObservableGroup*>(obs);
    if (og) {
        s<<og->m_cuts;
        l++;
        for (size_t i=0; i<og->m_obs.size(); i++) 
            s<<og->m_obs[i];
        l--;
    }
    return s;
}
