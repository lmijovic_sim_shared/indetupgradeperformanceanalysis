#include "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h"

#include "InDetUpgradePerformanceAnalysis/Helpers.h"
#include <stdio.h>

SelectorWithIndex::SelectorWithIndex(const std::string & varname, const std::string & indexname, int value)
  : Cuts(varname+":"+indexname+"=="+stri(value)), var(varname), index(indexname), sel(value)
{}

bool SelectorWithIndex::AcceptTrack(int i)
{
  int j = index[i];
  return var[j]==sel;
}

AbsSelectorWithIndex::AbsSelectorWithIndex(const std::string & varname, const std::string & indexname, int value)
  : Cuts("abs("+varname+":"+indexname+")=="+stri(value)), var(varname),index(indexname), sel(value)
{}

bool AbsSelectorWithIndex::AcceptTrack(int i)
{
  int j = index[i];
  if (j<0) {
     printf(" ERROR: j<0 in cut %s for i=%i j=%i\n", Name().c_str(), i, j);
    return false;
  } 
  return (var[j]==sel) ||  (var[j]==-sel) ;
}
