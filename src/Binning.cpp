#include "InDetUpgradePerformanceAnalysis/Binning.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "InDetUpgradePerformanceAnalysis/VectorBinningVariable.h"
#include "InDetUpgradePerformanceAnalysis/ScalarBinningVariable.h"
#include <math.h>
#include <TLeaf.h>

Binning::Binning() : 
    m_varname("trk_eta"), m_name("etaSingleSided"), 
    m_label("#eta"), 
    m_binning_variable(0),
    //m_var(new VectorBranch<float>("trk_eta")), m_var_int(0), m_var_scalar(0), 
    m_isAbs(true)  {
  SetBinning(20, 0, 2.5 );
  InitialiseBinningVariable();
}

Binning::Binning(const Binning &other) {
  //this->m_var = (other.m_var) ? new VectorBranch<float>(*other.m_var) : 0;
  //this->m_var_int = (other.m_var_int) ? new VectorBranch<int>(*other.m_var_int) : 0;
  //this->m_var_scalar = (other.m_var_scalar) ? new ScalarBranch<int>(*other.m_var_scalar) : 0;

  this->m_varname = other.m_varname;
  this->m_name = other.m_name;
  this->m_label = other.m_label;
  this->m_nBins = other.m_nBins;
  this->m_binMin = other.m_binMin;
  this->m_binMax = other.m_binMax;
  this->m_isAbs = other.m_isAbs;
  this->m_binWidth = other.m_binWidth;
  this->m_bins = other.m_bins;

  InitialiseBinningVariable();
}

Binning::Binning(std::string varname, int nBins, float binMin, float binMax, std::string name, std::string label , bool isAbs) :
  m_varname(varname), m_name(name), m_label(label), 
    //m_var(0), m_var_int(0), m_var_scalar(0), 
    m_binning_variable(0)
{
  SetBinning(nBins, binMin, binMax, isAbs, 0 );

  InitialiseBinningVariable();

}

Binning::Binning(std::string varname, std::vector<float> bins, std::string name, std::string label , bool isAbs) :
  m_varname(varname), m_name(name), m_label(label), 
    //m_var(0), m_var_int(0), m_var_scalar(0), 
    m_binning_variable(0)
{
  SetBinning(0, 0, 0, isAbs, &bins );

  InitialiseBinningVariable();

}

void Binning::InitialiseBinningVariable() {
  TLeaf* leaf =  TreeHouse::Current()->GetChain()->GetLeaf(m_varname.c_str());
  if (!leaf) return;
  const char* branch_type = leaf->GetTypeName();
  if (strcmp(branch_type, "vector<float>")==0) {
    m_binning_variable = dynamic_cast<BinningVariable*> ( new VectorBinningVariable<float>(m_varname) );
  } else if (strcmp(branch_type, "vector<int>")==0) {
    m_binning_variable = dynamic_cast<BinningVariable*> ( new VectorBinningVariable<int>(m_varname) );
  } else if (strcmp(branch_type, "Int_t")==0) {
    m_binning_variable = dynamic_cast<BinningVariable*> ( new ScalarBinningVariable<int>(m_varname) );
  } else if (strcmp(branch_type, "Float_t")==0) {
    m_binning_variable = dynamic_cast<BinningVariable*> ( new ScalarBinningVariable<float>(m_varname) );
  } else {
      std::cerr<< "Unknown branch type " << branch_type << std::endl;
  }
}


/** Set the binning */
void Binning::SetBinning(int nBins, float binMin, float binMax, bool isAbs, const std::vector<float> *bins) {
  if (bins==NULL and nBins!=0 and binMin!=binMax) {
    m_nBins = nBins; m_binMin = binMin; m_binMax = binMax;
    m_binWidth = (m_binMax - m_binMin) / m_nBins;
  } else if (bins!=NULL) {
    m_nBins = bins->size()-1; m_binMin = bins->front(); m_binMax = bins->back();
    m_binWidth = 0;
    m_bins = *bins;
  } else {
      std::cerr << "Must specify either nBins/binMin/binMax or variable bins" << std::endl;
  }
  m_isAbs = isAbs;
}

/** Get the bin index for object with d3pd_index  */
int Binning::GetBinIndex( int d3pd_index) {

  float value = m_binning_variable->Value(d3pd_index);

  return GetBinIndexValue(value);

}

/** Get the bin index for value  */
int Binning::GetBinIndexValue(float value) {

  // If absolute binning, take absolute value
  if ( m_isAbs ) value = fabs(value);

  int theBin=-1;
  // If outside of range, return -1
  if (value<m_binMin || value>=m_binMax) return -1;

  if (m_binWidth) {
      // Find bin
    theBin = int(((value-m_binMin)*m_nBins)/(m_binMax-m_binMin));
  } else {
    for (int iBin=0; iBin<m_nBins; iBin++) {
      if (value<m_bins[iBin+1]) {
        theBin=iBin; break;
      }
    }
  }
  //std::cout << " value " << value << " theBin " << theBin << std::endl;

  return theBin;

}

/** Get the central value of the bin with index 'index'  */
float Binning::GetBinCenter(int index) {
  float centre;
  if (index<0) { 
      centre= m_binMin;
  } else if (index>=m_nBins) { 
      centre = m_binMax;
  } else  if (m_binWidth>0) {
    centre = m_binMin + m_binWidth * ( index + 0.5);
  } else {
    centre = (m_bins[index]+m_bins[index+1])/2;
  }
  return centre;
}

float Binning::GetBinWidth(int index) {
  if (index<0) return 0;
  if (index>=m_nBins) return 0;
  if (m_binWidth>0) {
    return m_binWidth;
  } else {
    return (m_bins[index+1]-m_bins[index]);
  }
}

/** Destructor */
Binning::~Binning() {
    if (m_binning_variable) { delete m_binning_variable; }
}
