#include "InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TString.h"
#include "TMultiGraph.h"
#include "TH2D.h"
#include "math.h"


SCTBarrelPerEventOccupancy::SCTBarrelPerEventOccupancy(int numChips) :
  PerEventOccupancyObservable("SCTBarrelPerEventOccupancy","sctlayer"),
  stripWidth(0.0745), 
  nStrips(1280),
  nChips(numChips), 
  chipWidth( stripWidth * nStrips / nChips ),
  sctClus_x("sctClus_x"), sctClus_y("sctClus_y"), sctClus_z("sctClus_z"),
  sctClus_size("sctClus_size"), sctClus_layer("sctClus_layer"), 
  sctClus_bec("sctClus_bec"), sctClus_eta_module("sctClus_eta_module"),
  sctClus_phi_module("sctClus_phi_module"),
  sctClus_detElementId("sctClus_detElementId"),
  sctClus_side("sctClus_side"),
  sctClus_locX("sctClus_locX"),
  doubleStripLength(false)
{}

void SCTBarrelPerEventOccupancy::Initialize( ) {
  n_regions = sensor->getValue( "n"+moduleType );
  PerEventOccupancyObservable::Initialize();
}


// Called at the start of every event
// Calculate the min/max rates for the previous event and clear counters
void SCTBarrelPerEventOccupancy::fillHists( std::vector< PerElementCounters > &counters, ElemType elem ) {
  for (int layer=0; layer < n_regions; layer++) {
    // Skip if there are no sensor_counters (probably first event)
    if (counters[ layer ].size() ==0) continue;
    // These are floats because in normal occupancy mode they are averaged ver many events
    float maxClus=0; float maxHits=0; double maxFlu=0; float maxOcc=0; float maxClus_hits=0; float maxFluClus=0; float maxHits_clus=0;
    // Hist to stroe distribution to find mean.max
    const char* elem_str = (elem==CHIP) ? "chip" : "sensor";
    TH1D *h_clus = new TH1D(Form("ev%i_layer_%i_%s_clus", nEvents, layer, elem_str), "clusters in event", 100, 0.5, 100.5); 
    TH1D *h_hits = new TH1D(Form("ev%i_layer_%i_%s_hits", nEvents, layer, elem_str), "hits in event", 750, 0.5, 750.5);
    TH1D *h_occ = new TH1D(Form("ev%i_layer_%i_%s_occ", nEvents, layer, elem_str), "occupancy in event", 500, 0, 100);
    TH1D *h_flu = new TH1D(Form("ev%i_layer_%i_%s_flu", nEvents, layer, elem_str), "fluency in event", 200, 0, 2);
    TH1D *h_flu_clus = new TH1D(Form("ev%i_layer_%i_%s_flu_clu", nEvents, layer, elem_str), "cluster fluency in event", 50, 0, 0.1);
    h_clus->StatOverflows(kTRUE);
    h_hits->StatOverflows(kTRUE);
    h_occ->StatOverflows(kTRUE);
    h_flu->StatOverflows(kTRUE);
    h_flu_clus->StatOverflows(kTRUE); 
    // Loop over sensor_counters from last event to find maximum no. clusters in this event
    PerElementCounters::iterator counter_it = counters[ layer ].begin();
    PerElementCounters::iterator maxHits_counter;
    PerElementCounters::iterator maxClus_counter;
    //std::cout << " Start of new event, checking " << sensor_counters[ layer ].size() << " sensor_counters " << std::endl;
    for (;counter_it!=counters[ layer ].end();++counter_it) {
      // Calculate variables
      float hits = (*counter_it).second.hits();
      float occ = (*counter_it).second.occupancy();
      float clus = (*counter_it).second.cluster();
      float area = (*counter_it).second.area();
      double flu = hits / area;
      double fluClus = clus / area;
      // Check that counter doesn't have more than one detElementId
      // Ignore if counter_it does
      if ( (*counter_it).second.detElementIds.size() > 1 ) {
        printf("ERROR: counter has more than one detElementId!\n");
        printf( " Id %lli clusters: %.1f hits: %.1f detElementIds size: %i", (*counter_it).first, clus, hits, (int) (*counter_it).second.detElementIds.size());
        OccupancyObservable::ElementList::iterator el_it = (*counter_it).second.detElementIds.begin();
        for(;el_it!=(*counter_it).second.detElementIds.end(); ++el_it) printf(" %lli", (*el_it));
        printf("\n");
        continue;
      }
      // Fill hists
      h_clus->Fill(clus);
      h_hits->Fill(hits);
      h_occ->Fill(occ*100);
      h_flu->Fill(flu);
      h_flu_clus->Fill(fluClus);
      fillHist( getHistId(layer, elem, ALL_CLUS) , clus );
      fillHist( getHistId(layer, elem, ALL_HITS) , hits );
      fillHist( getHistId(layer, elem, ALL_FLU) ,  flu );
      fillHist( getHistId(layer, elem, ALL_FLU_CLUS) , fluClus );
      fillHist( getHistId(layer, elem, ALL_OCC) , occ*100 );
      // Get maxima
      if (clus > maxClus) {
        maxClus = clus;
        maxClus_hits = hits;
        maxClus_counter = counter_it;
      }
      if (hits > maxHits) {
        maxHits = hits;
        maxHits_clus = clus;
        maxHits_counter = counter_it;
      }
      if (occ > maxOcc) maxOcc = occ;
      if (flu > maxFlu) maxFlu = flu;
      if (fluClus > maxFluClus) maxFluClus = fluClus;

      if (hits > (*counter_it).second.totalPixel()) {
        //debugLargeClusters(&(*counter_it).second, (*counter_it).first, layer);
      }
      /* DEBUGGING
         printf( "Max hits counter clusters: %.1f size: %.1f detElementIds size: %i", (*maxHits_counter).second.cluster(), (*maxHits_counter).second.hits(), (*maxHits_counter).second.detElementIds.size());
         ElementList::iterator el_it = (*maxHits_counter).second.detElementIds.begin();
         for(;el_it!=(*maxHits_counter).second.detElementIds.end(); ++el_it) printf(" %lli", (*el_it));
         printf("\n");

         printf( "Max clus counter clusters: %.1f size: %.0f detElementIds size: %i", (*maxClus_counter).second.cluster(), (*maxClus_counter).second.hits(), (*maxClus_counter).second.detElementIds.size());
         for(el_it= (*maxClus_counter).second.detElementIds.begin(); el_it!=(*maxClus_counter).second.detElementIds.end(); el_it++) printf(" %lli", (*el_it));
         printf("\n");
         */
    }

    // Fill hists
    fillHist( getHistId(layer, elem, MAX_HITS) , maxHits );
    fillHist( getHistId(layer, elem, MAX_HITS_CLUS) , maxHits_clus );
    fillHist( getHistId(layer, elem, MAX_OCC) , maxOcc *100);
    fillHist( getHistId(layer, elem, MAX_CLUS) , maxClus );
    fillHist( getHistId(layer, elem, MAX_CLUS_HITS) , maxClus_hits );
    fillHist( getHistId(layer, elem, MAX_FLU) , maxFlu );
    fillHist( getHistId(layer, elem, MAX_FLU_CLUS) , maxFluClus );

    fillHist( getHistId(layer, elem, MEAN_HITS) ,       h_hits->GetMean());
    fillHist( getHistId(layer, elem, MEAN_OCC) ,        h_occ->GetMean());
    fillHist( getHistId(layer, elem, MEAN_CLUS) ,       h_clus->GetMean());
    fillHist( getHistId(layer, elem, MEAN_FLU) ,        h_flu->GetMean());
    fillHist( getHistId(layer, elem, MEAN_FLU_CLUS) ,   h_flu_clus->GetMean());

    fillHist( getHistId(layer, elem, NPC_HITS) ,       find90pc(h_hits));
    fillHist( getHistId(layer, elem, NPC_OCC) ,        find90pc(h_occ));
    fillHist( getHistId(layer, elem, NPC_CLUS) ,       find90pc(h_clus));
    fillHist( getHistId(layer, elem, NPC_FLU) ,        find90pc(h_flu));
    fillHist( getHistId(layer, elem, NPC_FLU_CLUS) ,   find90pc(h_flu_clus));

    // Delete all sensor_counters
    counters[ layer ].clear();

    // Delete temporaries
    bool save_all = false;
    if (save_all) {
      extra_hists.push_back(h_clus);
      extra_hists.push_back(h_hits);
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    } else {
      if (h_clus) delete h_clus;
      if (h_hits) delete h_hits;
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    }
  } 
}

void SCTBarrelPerEventOccupancy::debugLargeClusters(PerEventCounter *counter, long counterId) {
  // Oh nos. More hits than strips. How can this be?
  int nStrips = (*counter).totalPixel();
  TH1D* h_dbg_strips = new TH1D(Form("ev%i_%li_strips", nEvents, counterId ), "strips", nStrips, (*counter).elem_start, (*counter).elem_end);
  TH1D* h_dbg_clus = new TH1D(Form("ev%i_%li_clus_locX", nEvents, counterId ), "strips", nStrips, (*counter).elem_start, (*counter).elem_end);
  std::vector<float>::iterator locX_it = (*counter).clus_locXs.begin();
  std::vector<int>::iterator full_size_it = (*counter).clus_full_sizes.begin();
  std::vector<int>::iterator size_it = (*counter).clus_sizes.begin();
  // Loop over the clusters in this counter
  for (; locX_it < (*counter).clus_locXs.end(); locX_it++) {
    float locX = (*locX_it);
    // Full size is total number of hits in cluster
    int clus_full_size = (*full_size_it);
    // Clus size is number of those hits in this element
    int clus_size = (*size_it);
    if ((*locX_it) > h_dbg_clus->GetXaxis()->GetXmax()) {
      h_dbg_clus->Fill( h_dbg_clus->GetXaxis()->GetBinCenter( h_dbg_clus->GetNbinsX() ) );
    } else if ((*locX_it) < h_dbg_clus->GetXaxis()->GetXmin())  {
      h_dbg_clus->Fill( h_dbg_clus->GetXaxis()->GetBinCenter( 1 ) );
    } else {
      h_dbg_clus->Fill(*locX_it);
    }
    // start needs to be calculated with full cluster size
    double clus_pos_counter = locX - clus_full_size*stripWidth/2;
    // If start before this element then change to the start of the element
    if (clus_pos_counter <  (*counter).elem_start) {
      clus_pos_counter = (*counter).elem_start;
    }
    for (int iStrip=0; iStrip < clus_size; iStrip++) {
      //printf(" clus_pos_counter %.2f elem_start %.2f hist start %.2f\n", clus_pos_counter, (*counter).elem_start, h_dbg_strips->GetXaxis()->GetXmin());
      h_dbg_strips->Fill(clus_pos_counter);
      clus_pos_counter+=stripWidth;
    }
    size_it++;
    full_size_it++;
  } // end loop over clusters
  // Loop again to print stuff
  if ( h_dbg_strips->GetEntries() != (*counter).hits() ) {
    printf("ERROR h_dbg_strips->GetEntries() = %.2f (*counter).hits() = %.2f nClus %.0f\n", h_dbg_strips->GetEntries(), (*counter).hits(), counter->cluster());
    for (size_t iClus=0; iClus < (*counter).clus_sizes.size(); iClus++) {
      //printf(" cluster %i size %i full_size %i locX %.2f \n", iClus, (*counter).clus_sizes[iClus], (*counter).clus_full_sizes[iClus], (*counter).clus_locXs[iClus] );
    }
  }
  extra_hists.push_back( h_dbg_clus );
  extra_hists.push_back( h_dbg_strips );
}


int SCTBarrelPerEventOccupancy::getChip( float locX ) {
  // Make all positive
  int chip = int( locX / chipWidth );
  if (chip >= nChips) {
    return nChips-1;
  } else {
  return chip;
  }
}

void SCTBarrelPerEventOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event sensor_counters and find max occupancy for last event during "EVENT" step
  if (EventInit(step,k)) return;

  //std::cout << "sctClus_size.size() " << sctClus_size.size()  << std::endl;
  //std::cout << "sctClus_size[0] " << sctClus_size[0]  << std::endl;
  //std::cout << "sctClus_size[k] " << sctClus_size[k]  << std::endl;
  //sctClus_size.Get().Print();

  // enter occupancys in sensor_counters    
  if (step==UpgPerfAna::INITIAL && sctClus_bec[k]==0){  // barrel only
    int etaModule=sctClus_eta_module[k];
    int phiModule=sctClus_phi_module[k];
    int layer=sctClus_layer[k];
    int side=sctClus_side[k];

    nClus_layer[layer]++;

    const SensorInfoBase* layerInfo = sensor->get(moduleType,layer);
    ElementId sensorId = getElementId(layer, etaModule, phiModule, side);

    //printf(" layer %i etaModule %i phiModule %i side %i id %li locX %.1f chip %i chipId %li \n", layer, etaModule, phiModule, side, sensorId, locX, chip, chipId );

    //float x=sctClus_x[k];
    //float y=sctClus_y[k];
    //float z=sctClus_z[k];
    //float r=sqrt(x*x+y*y);
    int size=sctClus_size[k]; // size in number of strips
    float locX=sctClus_locX[k];
    locX += chipWidth*nChips/2;

    fillHist( getHistId( layer, CLUSTER, ALL_HITS) , size );

    // Find counter for sensor, and create if non-existant
    PerElementCounters::iterator counter = sensor_counters[layer].find( sensorId );
    if (counter == sensor_counters[layer].end()) {
      PerEventCounter c= PerEventCounter(0., 100.);
      c.setArea(sensor->get(moduleType,layer)->getFloat("area")); // Factor two removed because now treat side 0 and 1 separatly
      // Gives number of channels for this counter
      // Each counter corresponds to one "segment" of strips (ie 4 of these per module side for short strips, 2 for long)
      // perModule also corresponds to each segment
      // Separate sensor_counters for each side, so no factors of two here
      c.setTotalPixel(layerInfo->getValue("perModule") );
      //c.increaseEvent();
      sensor_counters[ layer ][ sensorId ] = c;
      counter = sensor_counters[ layer ].find( sensorId );
    }
    //printf("Recording cluster %.3f %.3f %.3f %i %llu\n", x,y,z,size,sctClus_detElementId[k]);
    (*counter).second.add(locX,size,sctClus_detElementId[k]);

    //int nStripsPerChip = m_nStripsPerChip_layers[layer];

    //printf("nStripsPerChip %i stripWidth %.4f\n", nStripsPerChip, stripWidth);
    //int chip = getChip( locX );
    double clus_start = locX - size*stripWidth/2;
    double clus_end = locX + size*stripWidth/2;

    //printf("Recording cluster locX %.4f size %i clus_start %.4f clus_end %.4f\n", locX, size, clus_start, clus_end);

    // for each chip, figure out how mancy chips hit
    int nHitsTotal=0; // sanity check
    double strip_start =  clus_start;
    //double strip_start =  clus_start + stripWidth/2;
    // count number of hits in each chip
    std::vector<int> chip_hits(nChips, 0);
    // Loop over number of strips in cluster and increment counter 
    // corresponding to the chip for each one
    for (int iStrip=0; iStrip < size; iStrip++) {
      chip_hits[ getChip(strip_start) ]++;
      strip_start += stripWidth;
    }
    // Loop over chips and record number of hits in each
    for (int iChip=0; iChip<nChips; iChip++) {
      int nHits = chip_hits[iChip];
      if ( nHits == 0) continue;
      //printf(" Adding %i hits to chip %i\n", nHits, iChip);
      ElementId chipId = getElementId(layer, etaModule, phiModule, side, iChip);
      // Find counter for chip, and create if non-existant
      counter = chip_counters[layer].find( chipId );
      if (counter == chip_counters[layer].end()) {
        double chip_start = iChip * chipWidth;
        double chip_end = (iChip+1) * chipWidth;
        PerEventCounter c= PerEventCounter(chip_start, chip_end);
        c.setArea(sensor->get(moduleType,layer)->getFloat("area")/nChips); // Factor two removed because now treat side 0 and 1 separatly
        // Gives number of channels for this counter
        // Each counter corresponds to one "segment" of strips (ie 4 of these per module side for short strips, 2 for long)
        // perModule also corresponds to each segment
        // Separate chip_counters for each side, so no factors of two here
        c.setTotalPixel(layerInfo->getValue("perModule")/nChips );
        chip_counters[ layer ][ chipId ] = c;
        counter = chip_counters[ layer ].find( chipId );
      }
      (*counter).second.add(locX,nHits,sctClus_detElementId[k], size);
      nHitsTotal += nHits;
    }

    if (nHitsTotal!=size) {
      printf("ERROR did not assign same number of hits to chip counters as size. size=%i nHitsTotal=%i clus_start=%.4f clu_end=%.4f locX %.4f\n", size, nHitsTotal, clus_start, clus_end, locX);
    }
  }
}



int SCTBarrelPerEventOccupancy::GetNTrks() {
  //std::cout<<"sctClus_n="<<sctClus_x.size()<<std::endl;
  return sctClus_x.size();
}


void SCTBarrelPerEventOccupancy::defineHists() {

  // Just for histogram binning
  int nChannelsPerChip=256;

  defineHistLayers( LAYER, ALL_CLUS,           "layer_n_clus",                       500,  0,    200e3     );

  defineHistLayers( SENSOR, MAX_CLUS,       "sensor_max_clus",                     100,  0.5,    100.5     );
  defineHistLayers( SENSOR, MAX_HITS,       "sensor_max_hits",                     750,  0.5,    750.5     );
  defineHistLayers( SENSOR, MAX_OCC,        "sensor_max_occ",                      500,  0,    100     );
  defineHistLayers( SENSOR, MAX_FLU,        "sensor_max_flu",                      200,  0,    0.2     );
  defineHistLayers( SENSOR, MAX_FLU_CLUS,   "sensor_max_flu_clus",                 200,  0,    0.05    );
  defineHistLayers( SENSOR, MAX_CLUS_HITS,  "sensor_max_clus_hits",                500,  0.5,    500.5     );
  defineHistLayers( SENSOR, MAX_HITS_CLUS,  "sensor_max_hits_clus",                100,  0.5,    100.5     );

  defineHistLayers( SENSOR, MEAN_CLUS,       "sensor_mean_clus",                    200,  0.5,    10.5      );
  defineHistLayers( SENSOR, MEAN_HITS,       "sensor_mean_hits",                    200,  0.5,    20.5      );
  defineHistLayers( SENSOR, MEAN_OCC,        "sensor_mean_occ",                     200,  0,    2       );
  defineHistLayers( SENSOR, MEAN_FLU,        "sensor_mean_flu",                     200,  0,    0.01     );
  defineHistLayers( SENSOR, MEAN_FLU_CLUS,   "sensor_mean_flu_clus",                200,  0,    0.005    );

  defineHistLayers( SENSOR, NPC_CLUS,       "sensor_90pc_clus",                    200,  0.5,    10.5       );
  defineHistLayers( SENSOR, NPC_HITS,       "sensor_90pc_hits",                    200,  0.5,    20.5      );
  defineHistLayers( SENSOR, NPC_OCC,        "sensor_90pc_occ",                     200,  0,    1      );
  defineHistLayers( SENSOR, NPC_FLU,        "sensor_90pc_flu",                     200,  0,    0.01     );
  defineHistLayers( SENSOR, NPC_FLU_CLUS,   "sensor_90pc_flu_clus",                200,  0,    0.005    );

  defineHistLayers( SENSOR, ALL_CLUS,       "sensor_all_clus",                    100,  0.5,    100.5     );
  defineHistLayers( SENSOR, ALL_HITS,       "sensor_all_hits",                    750,  0.5,    750.5     );
  defineHistLayers( SENSOR, ALL_OCC,        "sensor_all_occ",                     500,  0,    100     );
  defineHistLayers( SENSOR, ALL_FLU,        "sensor_all_flu",                     200,  0,    0.2     );
  defineHistLayers( SENSOR, ALL_FLU_CLUS,   "sensor_all_flu_clus",                200,  0,    0.05    );

  defineHistLayers( CLUSTER, ALL_HITS,      "cluster_all_hits",                    500,  0.5,    500.5     );

  defineHistLayers( CHIP, MAX_CLUS,       "chip_max_clus",                       100,  0.5,    100.5     );
  defineHistLayers( CHIP, MAX_HITS,       "chip_max_hits",                       nChannelsPerChip+5,  0.5,    nChannelsPerChip+5.5     );
  defineHistLayers( CHIP, MAX_OCC,        "chip_max_occ",                        500,  0,    100     );
  defineHistLayers( CHIP, MAX_FLU,        "chip_max_flu",                        200,  0,    2       );
  defineHistLayers( CHIP, MAX_FLU_CLUS,   "chip_max_flu_clus",                   50,   0,    0.1     );
  defineHistLayers( CHIP, MAX_CLUS_HITS,   "chip_max_clus_hits",                  nChannelsPerChip+5,  nChannelsPerChip+5.5,    140.5     );
  defineHistLayers( CHIP, MAX_HITS_CLUS,   "chip_max_hits_clus",                  100,  0.5,    100.5     );

  defineHistLayers( CHIP, MEAN_CLUS,       "chip_mean_clus",                      200,  0.5,    2.5       );
  defineHistLayers( CHIP, MEAN_HITS,       "chip_mean_hits",                      200,  0.5,    5.5       );
  defineHistLayers( CHIP, MEAN_OCC,        "chip_mean_occ",                       200,  0,    10      );
  defineHistLayers( CHIP, MEAN_FLU,        "chip_mean_flu",                       200,  0,    0.04   );
  defineHistLayers( CHIP, MEAN_FLU_CLUS,   "chip_mean_flu_clus",                  200,   0,    0.01     );

  defineHistLayers( CHIP, NPC_CLUS,       "chip_90pc_clus",                      200,  0.5,    5.5        );
  defineHistLayers( CHIP, NPC_HITS,       "chip_90pc_hits",                      200,  0.5,    10.5       );
  defineHistLayers( CHIP, NPC_OCC,        "chip_90pc_occ",                       200,  0,    10        );
  defineHistLayers( CHIP, NPC_FLU,        "chip_90pc_flu",                       200,  0,    0.05     );
  defineHistLayers( CHIP, NPC_FLU_CLUS,   "chip_90pc_flu_clus",                  200,  0,    0.02     );

  defineHistLayers( CHIP, ALL_CLUS,       "chip_all_clus",                      100,  0.5,    100.5     );
  defineHistLayers( CHIP, ALL_HITS,       "chip_all_hits",                      nChannelsPerChip+5,  0.5,    nChannelsPerChip+5.5     );
  defineHistLayers( CHIP, ALL_OCC,        "chip_all_occ",                       125,  0,    100     );
  defineHistLayers( CHIP, ALL_FLU,        "chip_all_flu",                       200,  0,    2       );
  defineHistLayers( CHIP, ALL_FLU_CLUS,   "chip_all_flu_clus",                  50,   0,    0.1     );
}


