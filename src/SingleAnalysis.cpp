#include "InDetUpgradePerformanceAnalysis/SingleAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Cuts.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"

#include <iostream>
using namespace std;

SingleAnalysis::SingleAnalysis(const std::string & name, TreeHouse * treeHouse) 
  : ObservableGroup(name), m_treeHouse(treeHouse)
{
  Observable::SetAnalysis(this);
  m_steps.insert(UpgPerfAna::INITIAL);
  m_steps.insert(UpgPerfAna::DONE);

}

void SingleAnalysis::Initialize()
{
  //ObservableGroup::Initialize();
}

UpgPerfAna::AnalysisStep SingleAnalysis::Execute() {
    std::cout<<"void SingleAnalysis::Execute()"<<std::endl;
  ObservableGroup::Initialize();
    std::cout<<"Calling m_treeHouse->SelectBranches() "<<std::endl;
  if (m_treeHouse) m_treeHouse->SelectBranches();
  UpgPerfAna::AnalysisStep step = UpgPerfAna::INITIAL;
  std::cout<<" *InDetUpgradeAnalysis* SingleAnalysis::Execute() call step " <<step<<std::endl;
  while ((step=ExecuteStep(step))!=UpgPerfAna::DONE) {
    std::cout<<" *InDetUpgradeAnalysis* SingleAnalysis::Execute() call step " <<step<<std::endl;
  }
  return UpgPerfAna::DONE;
}


// sould perform loop over events (i.e. tree)
UpgPerfAna::AnalysisStep SingleAnalysis::ExecuteStep(UpgPerfAna::AnalysisStep step) {
  std::cout<<"void SingleAnalysis::ExecuteStep("<<step<<")"<<std::endl;
  StepList::iterator it = m_steps.find(step);
  if (it==m_steps.end()) {
    std::cout<<"WARNING: step not expected!"<<std::endl;
    return UpgPerfAna::DONE;
  }



  size_t nevents = m_treeHouse->size();
  std::cout<<"*SingleAnalysis* ExecuteStep"<<std::endl;
  std::cout<<" Treehous size() == "<<nevents<<std::endl;
  std::cout<<*m_treeHouse;

  //for (size_t j=0; j<m_obs.size(); j++) {
  //    m_obs[j]->Initialize(step);
  //  }          

  // loop over events
  std::cout << "Getting friend" << std::endl;
  TTree * tFriend = m_treeHouse->GetFriend();
  std::cout << "Got friend" << std::endl;
  for(size_t i=0; i<nevents; i++){
    if (step==UpgPerfAna::INITIAL) {
      // add event init, if neccessary
      if (m_steps.find(UpgPerfAna::EVENT)!=m_steps.end()) {
        m_treeHouse->GetEntry(i);
        for (size_t j=0; j<m_obs.size(); j++) {
          m_obs[j]->Analyze(UpgPerfAna::EVENT,0);
        }
      }
    }
    if(i%100 == 0){
      std::cout << "...processing event number " << i << std::endl;
    }

    m_treeHouse->GetEntry(i);
    bool passEvent = true;
    if (m_cuts) {
      passEvent = m_cuts->AcceptEvent();
    }
    if (passEvent) {
      for (size_t j=0; j<m_obs.size(); j++) {
	      m_obs[j]->AnalyzeEvent(step);
      }    
    }
    if (tFriend) {
      // store new values
      tFriend->Fill();
      //      cout <<"SingleAnalysis  tFriend->Fill() "<<endl;

    }
  }
  ++it;
  if ( (*it)==UpgPerfAna::EVENT) ++it;

  return (*it);
}


TChain * SingleAnalysis::GetChain() {
  return m_treeHouse->GetChain();
}


void SingleAnalysis::SetChain(const TString & treename)
{ 
  if (!m_treeHouse) m_treeHouse=new TreeHouse();
  m_treeHouse->SetChain(new TChain(treename));
}

void SingleAnalysis::SetChain(TChain *c)
{ m_treeHouse->SetChain(c);}

void SingleAnalysis::SetMaxEvents(int n)
{ 
    // The call to TreeHouse controls how many files
    // are loaded in to chain, and is returend by TreeHouse::size()
    std::cout << "Setting max events to " << n << std::endl;
    m_treeHouse->SetMaxEvents(n);
}

void SingleAnalysis::AddFile(const TString & fname)
{ m_treeHouse->AddFile(fname); }
