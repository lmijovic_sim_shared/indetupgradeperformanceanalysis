#include "InDetUpgradePerformanceAnalysis/Occupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/SingleAnalysis.h"

#include <iostream>
#include "TH2D.h"

Occupancy2D::Occupancy2D( double pileup )
: OccupancyObservable("Occupancy2D","id"),
  //trk_n(0),
  m_pileup(pileup),
  trk_SCT_hit_n("trk_SCT_hit_n"), trk_SCT_hit_x("trk_SCT_hit_x"), 
  trk_SCT_hit_y("trk_SCT_hit_y"), trk_SCT_hit_z("trk_SCT_hit_z"),
  trk_SCT_hit_detElementId("trk_SCT_hit_detElementId"),
  trk_Pixel_hit_n("trk_Pixel_hit_n"), trk_Pixel_hit_x("trk_Pixel_hit_x"), 
  trk_Pixel_hit_y("trk_Pixel_hit_y"), trk_Pixel_hit_z("trk_Pixel_hit_z"),
  trk_Pixel_hit_detElementId("trk_Pixel_hit_detElementId")
{}

void Occupancy2D::Initialize() {
  // setup 2d histograms in z-r
  OccupancyObservable::Initialize();
  m_histos[CLUS_MAP]->SetTitle("clusters distribution");
  m_histos[OCC_MAP]->SetTitle(Form("occupancies in percent ( %.0f pileup)", m_pileup));
  m_histos[CLUS_MAP_DETAIL]->SetTitle("clusters distribution");
  m_histos[OCC_MAP_DETAIL]->SetTitle(Form("occupancies in percent ( %.0f pileup)", m_pileup));
  m_histos.push_back(new TH2D("sct_trkclus_map2d","clusters distribution",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D("sct_occ_map2d","occupancies in percent (200 pileup)",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D("pix_trkclus_map2d","clusters distribution",120,-1750.,1750.,80,0.,350.));
  m_histos.push_back(new TH2D("pix_occ_map2d","occupancies in percent (200 pileup)",120,-1750.,1750.,80,0.,350.));

  if (analysis) {
    // fetch "daughter" observable and occupancies of sct and pixel detector elements
    std::vector<std::string> obsnames;
    obsnames.push_back("PixelBarrelOccupancy");
    obsnames.push_back("PixelDiskOccupancy");
    obsnames.push_back("SCTDiskOccupancy");
    obsnames.push_back("SCTBarrelOccupancy");

    std::cout<<"looking for sct and pixel occupancy information"<<std::endl;
    for (size_t i=0; i<obsnames.size(); ++i) {
      OccupancyObservable * obs = 
        dynamic_cast<OccupancyObservable*>(analysis->GetObservable(obsnames[i]));
      if (obs) {
        std::cout<<"found "<<obsnames[i]<<std::endl;
        LayerIdCounters::const_iterator it=obs->counters.begin();
        for (;it!=obs->counters.end();++it) {
          const Counters & c = it->second;
          //float occ = c.occupancy();
          // create map of element ids.
          ElementList::const_iterator el=c.detElementIds.begin();
          for (;el!=c.detElementIds.end();++el) {
            omap[*el]=c;
          }
        }
      }
    }
  }
  else {
    std::cout<<"ERROR: analysis not found in  Occupancy2D::Initialize()"<<std::endl;
  }
}

void Occupancy2D::Analyze(UpgPerfAna::AnalysisStep step, int k) {  // loop over track ids
  if (step==UpgPerfAna::INITIAL) {
    TH2D * h_clus = static_cast<TH2D*>(m_histos[CLUS_MAP]);
    TH2D * h_occ = static_cast<TH2D*>(m_histos[OCC_MAP]);
    TH2D * h_flu = static_cast<TH2D*>(m_histos[FLU_MAP]);

    int nscthit = trk_SCT_hit_n[k];
    //  loop over clusters on track, in order to get x,y,z positions of modules.
    for (int i=0; i<nscthit;++i) { // sct hit loop
      float x=trk_SCT_hit_x[k][i];
      float y=trk_SCT_hit_y[k][i];
      float z=trk_SCT_hit_z[k][i];
      float r=sqrt(x*x+y*y);
      h_clus->Fill(z,r);
      ULong64_t id = trk_SCT_hit_detElementId[k][i];
      const Counters & c = omap.find(id)->second;
      //printf("Occ %u %.3f %.3f %.3f Occupancy %.8f\n", int(id), x, y, z, c.occupancy());
      h_occ->Fill(z,r,c.occupancy());
      float area=c.area();
      if (area>0.) {
        h_flu->Fill(z,r,c.hits()/area);
      }
    }

    TH2D * h_pix_clus = static_cast<TH2D*>(m_histos[CLUS_MAP_DETAIL]);
    TH2D * h_pix_occ = static_cast<TH2D*>(m_histos[OCC_MAP_DETAIL]);
    TH2D * h_pix_flu = static_cast<TH2D*>(m_histos[FLU_MAP_DETAIL]);
    int npixhit = trk_Pixel_hit_n[k];
    for (int i=0; i<npixhit;++i) { // pixel hit loop
      float x=trk_Pixel_hit_x[k][i];
      float y=trk_Pixel_hit_y[k][i];
      float z=trk_Pixel_hit_z[k][i];
      float r=sqrt(x*x+y*y);
      h_clus->Fill(z,r);
      h_pix_clus->Fill(z,r);
      ULong64_t id = trk_Pixel_hit_detElementId[k][i];
      const Counters & c = omap.find(id)->second;
      h_occ->Fill(z,r,c.occupancy());
      h_pix_occ->Fill(z,r,c.occupancy());
      float area=c.area();
      if (area>0.) {
        h_flu->Fill(z,r,c.hits()/area);
        h_pix_flu->Fill(z,r,c.hits()/area);
      }
    }
  }
}

void Occupancy2D::Finalize() {
  TH2D * h_pix_occ_big=0;
  TH2D * h_pix_flu_big=0;
  OccupancyObservable * obs = 
    dynamic_cast<OccupancyObservable*>(analysis->GetObservable("PixelOccupancy2D"));
  if (obs) {
    h_pix_occ_big=dynamic_cast<TH2D*>(obs->GetHistogram(OCC_MAP));
    h_pix_flu_big=dynamic_cast<TH2D*>(obs->GetHistogram(FLU_MAP));
  }

  double factor=m_pileup*100.; // pileup * percent (before hist stored occupany as fraction, want it in percent)

  // normalize occupancies to entries
  TH2D * h_clus = dynamic_cast<TH2D*>(m_histos[CLUS_MAP]);
  TH2D * h_occ = dynamic_cast<TH2D*>(m_histos[OCC_MAP]);
  TH2D * h_flu = dynamic_cast<TH2D*>(m_histos[FLU_MAP]);
  TAxis *xaxis = h_clus->GetXaxis();
  TAxis *yaxis = h_clus->GetYaxis();
  // TODO: check binning consistency
  for (int i=1; i<=xaxis->GetNbins();++i) {
    for (int j=1; j<=yaxis->GetNbins();++j) {
      double o=factor*h_occ->GetBinContent(i,j);
      double f=m_pileup*h_flu->GetBinContent(i,j);
      if (o>0.) {
        double c=h_clus->GetBinContent(i,j);
        if (c==0) {
          printf("ERROR - c<1. Should not happen! c=%e, coords: i j %i %i Histograms: %s %s\n", c, i, j, h_occ->GetName(), h_clus->GetName());
          printf("        h_occ bins %i %i h_clus bins %i %i\n", h_occ->GetXaxis()->GetNbins(), h_occ->GetYaxis()->GetNbins(), h_clus->GetXaxis()->GetNbins(), h_clus->GetYaxis()->GetNbins());
        }
        h_occ->SetBinContent(i,j,o/c);
        //printf("Norm-Occ %i %i Occupancy %.4f Clusters %.1f NewOccupancy %.4f\n", i, j, o, c, o/c);
        h_flu->SetBinContent(i,j,f/c);
      }
      // Over-rides the occupancy and fluency maps
      // with ones from the PixelOccupancy2D class, if present.
      // This is becuase this class loops over the trk_SCT_hit_* collection
      // and so might not pick up all pixel locations
      // Means that have to run PixelOccupancy2D first so that these plots are normalised correctly
      // This is pretty uggly...
      if (h_pix_occ_big && h_pix_flu_big) {
        o=h_pix_occ_big->GetBinContent(i,j);
        if (o>0.) {
          h_occ->SetBinContent(i,j,o);
          printf("Norm-Occ %i %i Changing Occupancy to %.4f \n", i, j, o );
        }
        f=h_pix_flu_big->GetBinContent(i,j);
        if (f>0.) {
          h_flu->SetBinContent(i,j,f);
        }
      }
    }
  }

  // normalize occupancies to entries
  NormalizeOccupancyMap(CLUS_MAP_DETAIL,OCC_MAP_DETAIL,factor);
  NormalizeOccupancyMap(CLUS_MAP_DETAIL,FLU_MAP_DETAIL,m_pileup);

}

int Occupancy2D::GetNTrks() {
  return trk_SCT_hit_n.size(); // loop over tracks
}
