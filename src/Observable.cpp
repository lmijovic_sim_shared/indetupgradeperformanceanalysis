#include "InDetUpgradePerformanceAnalysis/Observable.h"

#include <iostream>

#include "TDirectory.h"

using namespace std;

int Observable::output_level=0;

Observable::Observable(const std::string & name) :
  sensor(0),analysis(0)
{
  nameobs = name;
}

void Observable::SetAnalysis(SingleAnalysis * ana) { analysis=ana; }

Observable * Observable::GetObservable(const std::string & name) {
  if (name==nameobs) {
    return this;
  }
  return 0;
}


void Observable::AddSensor(SensorInfoBase* s) {
  sensor =s;
}


// sould perform loop over analysis steps
UpgPerfAna::AnalysisStep Observable::Execute() {
  std::cerr<<"interface UpgPerfAna::AnalysisStep Observable::Execute() should not be called directly"<<std::endl;
  return UpgPerfAna::DONE;
}

// sould perform loop over events (i.e. tree)
UpgPerfAna::AnalysisStep Observable::ExecuteStep(UpgPerfAna::AnalysisStep ) {
  std::cerr<<"interface UpgPerfAna::AnalysisStep Observable::Execute(UpgPerfAna::AnalysisStep step) should not be called directly"<<std::endl;
  return UpgPerfAna::DONE;
}

// should perform loop over e.g. tracks (if needed)
void Observable::AnalyzeEvent(UpgPerfAna::AnalysisStep step=UpgPerfAna::INITIAL) {
    InitializeEvent(step);
  int n_trks = GetNTrks();
  for(int j=0; j<n_trks; j++){
    Analyze(step, j);
  }
}

void Observable::Analyze(UpgPerfAna::AnalysisStep,int) 
{
  std::cerr<<"interface void Observable::Analyze(UpgPerfAna::AnalysisStep, int) should not be called directly"<<std::endl;
}


void Observable::Finalize() 
{}

void Observable::cd(const std::string path)
{
  if (path!="") {
    if (path!=".." && !gDirectory->Get(path.c_str()))
      gDirectory->mkdir(path.c_str());
    gDirectory->cd(path.c_str());
  }
}

int Observable::GetNTrks(){
  return 0;
}



void Observable::DefinePlots(){
}

void Observable::FillPlots(UpgPerfAna::AnalysisStep , int ){
}


Observable::~Observable() {
}

std::ostream & operator<<(std::ostream & s, const Observable & obs)  {
  s<<"Observable"<<std::endl;
  s<<obs.nameobs<<"\n";
  return s;
}
