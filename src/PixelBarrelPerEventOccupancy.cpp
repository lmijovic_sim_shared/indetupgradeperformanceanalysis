#include "InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TString.h"
#include "TMultiGraph.h"
#include "TH2D.h"
#include "math.h"


PixelBarrelPerEventOccupancy::PixelBarrelPerEventOccupancy(float chipLength) :
  PerEventOccupancyObservable("PixelBarrelPerEventOccupancy","pixlayer"),
  userChipLength(chipLength),
  pixClus_x("pixClus_x"), pixClus_y("pixClus_y"), pixClus_z("pixClus_z"),
  pixClus_size("pixClus_size"), pixClus_layer("pixClus_layer"), 
  pixClus_bec("pixClus_bec"), pixClus_eta_module("pixClus_eta_module"),
  pixClus_phi_module("pixClus_phi_module"),
  pixClus_detElementId("pixClus_detElementId"),
  //pixClus_side("pixClus_side"),
  pixClus_locX("pixClus_locX"),
  pixClus_locY("pixClus_locY")
{}


void PixelBarrelPerEventOccupancy::Initialize( ) {

  n_regions = sensor->getValue( "n"+moduleType );

  PerEventOccupancyObservable::Initialize();

  for (int iLayer=0; iLayer < n_regions; iLayer++) {
    const SensorInfoBase* layerInfo = sensor->get(moduleType,iLayer);
    //std::cout << "CHIPLEN " << layerInfo->getFloat("chiplength") << " " << layerInfo->getFloat("chipwidth") << std::endl;
    //std::cout << "phi eta " << layerInfo->getValue("phi") << " " << layerInfo->getValue("eta") << std::endl;
    if (userChipLength>0) {
      // Set N chips and chip lengths to user specified value
      int nChipX = round( layerInfo->getFloat("chipwidth") / userChipLength );
      int nChipY = round( layerInfo->getFloat("chiplength") / userChipLength );
      float lenX = layerInfo->getFloat("chipwidth") / nChipX; 
      float lenY = layerInfo->getFloat("chiplength") / nChipY; 
      // How many chips in X/Y
      nChipsX.push_back( nChipX );
      nChipsY.push_back( nChipY );
      chipLenX.push_back( lenX );
      chipLenY.push_back( lenY );
      printf("Setting chipLenX/Y to %.5f %.5f for layer %i for userChipLength %.5f, nChipX/Y %i %i\n", lenX, lenY, iLayer, userChipLength, nChipX, nChipY);
    } else {
      // Use values from database
      // How many chips in X/Y
      nChipsX.push_back( layerInfo->getValue("phi") );
      nChipsY.push_back( layerInfo->getValue("eta") );
      // chipwidth and chiplength gives the lengtha nd width of *module* not readout chips
      chipLenX.push_back( layerInfo->getFloat("chipwidth") / layerInfo->getValue("phi") );
      chipLenY.push_back( layerInfo->getFloat("chiplength") / layerInfo->getValue("eta"));
    }
    // How many rows / cols of pixels in chip (really is per chip as comes from pixRO object
    //std::cout << "row col " << layerInfo->getValue("row") << " " << layerInfo->getValue("col") << std::endl;
    //chipColsX.push_back( layerInfo->getValue("row") );
    //chipColsY.push_back( layerInfo->getValue("col") );
  }

}

void PixelBarrelPerEventOccupancy::fillHists( std::vector< PerElementCounters > &counters, ElemType elem ) {
  for (int layer=0; layer < n_regions; layer++) {
    // Skip if there are no sensor_counters (probably first event)
    if (counters[ layer ].size() ==0) continue;
    // These are floats because in normal occupancy mode they are averaged ver many events
    float maxClus=0; float maxHits=0; double maxFlu=0; float maxOcc=0; float maxClus_hits=0; float maxFluClus=0; float maxHits_clus=0;
    // Hist to stroe distribution to find mean.max
    const char* elem_str = (elem==CHIP) ? "chip" : "sensor";
    TH1D *h_clus = new TH1D(Form("ev%i_layer_%i_%s_clus", nEvents, layer, elem_str), "clusters in event", 100, 0.5, 100.5); 
    TH1D *h_hits = new TH1D(Form("ev%i_layer_%i_%s_hits", nEvents, layer, elem_str), "hits in event", 750, 0.5, 750.5);
    TH1D *h_occ = new TH1D(Form("ev%i_layer_%i_%s_occ", nEvents, layer, elem_str), "occupancy in event", 500, 0, 100);
    TH1D *h_flu = new TH1D(Form("ev%i_layer_%i_%s_flu", nEvents, layer, elem_str), "fluency in event", 200, 0, 2);
    TH1D *h_flu_clus = new TH1D(Form("ev%i_layer_%i_%s_flu_clu", nEvents, layer, elem_str), "cluster fluency in event", 50, 0, 0.1);
    h_clus->StatOverflows(kTRUE);
    h_hits->StatOverflows(kTRUE);
    h_occ->StatOverflows(kTRUE);
    h_flu->StatOverflows(kTRUE);
    h_flu_clus->StatOverflows(kTRUE); 
    // Loop over sensor_counters from last event to find maximum no. clusters in this event
    PerElementCounters::iterator counter_it = counters[ layer ].begin();
    PerElementCounters::iterator maxHits_counter;
    PerElementCounters::iterator maxClus_counter;
    //std::cout << " Start of new event, checking " << sensor_counters[ layer ].size() << " sensor_counters " << std::endl;
    for (;counter_it!=counters[ layer ].end();++counter_it) {
      // Calculate variables
      float hits = (*counter_it).second.hits();
      float occ = (*counter_it).second.occupancy();
      float clus = (*counter_it).second.cluster();
      float area = (*counter_it).second.area();
      double flu = hits / area;
      double fluClus = clus / area;
      // Check that counter doesn't have more than one detElementId
      // Ignore if counter_it does
      if ( (*counter_it).second.detElementIds.size() > 1 ) {
        printf("ERROR: counter has more than one detElementId!\n");
        printf( " Id %lli clusters: %.1f hits: %.1f detElementIds size: %i", (*counter_it).first, clus, hits, (int) (*counter_it).second.detElementIds.size());
        OccupancyObservable::ElementList::iterator el_it = (*counter_it).second.detElementIds.begin();
        for(;el_it!=(*counter_it).second.detElementIds.end(); ++el_it) printf(" %lli", (*el_it));
        printf("\n");
        continue;
      }
      // Fill hists
      h_clus->Fill(clus);
      h_hits->Fill(hits);
      h_occ->Fill(occ*100);
      h_flu->Fill(flu);
      h_flu_clus->Fill(fluClus);
      fillHist( getHistId(layer, elem, ALL_CLUS) , clus );
      fillHist( getHistId(layer, elem, ALL_HITS) , hits );
      fillHist( getHistId(layer, elem, ALL_FLU) ,  flu );
      fillHist( getHistId(layer, elem, ALL_FLU_CLUS) , fluClus );
      fillHist( getHistId(layer, elem, ALL_OCC) , occ*100 );
      // Get maxima
      if (clus > maxClus) {
        maxClus = clus;
        maxClus_hits = hits;
        maxClus_counter = counter_it;
      }
      if (hits > maxHits) {
        maxHits = hits;
        maxHits_clus = clus;
        maxHits_counter = counter_it;
      }
      if (occ > maxOcc) maxOcc = occ;
      if (flu > maxFlu) maxFlu = flu;
      if (fluClus > maxFluClus) maxFluClus = fluClus;

      if (hits > (*counter_it).second.totalPixel()) {
        //debugLargeClusters(&(*counter_it).second, (*counter_it).first, layer);
      }
      /* DEBUGGING
         printf( "Max hits counter clusters: %.1f size: %.1f detElementIds size: %i", (*maxHits_counter).second.cluster(), (*maxHits_counter).second.hits(), (*maxHits_counter).second.detElementIds.size());
         ElementList::iterator el_it = (*maxHits_counter).second.detElementIds.begin();
         for(;el_it!=(*maxHits_counter).second.detElementIds.end(); ++el_it) printf(" %lli", (*el_it));
         printf("\n");

         printf( "Max clus counter clusters: %.1f size: %.0f detElementIds size: %i", (*maxClus_counter).second.cluster(), (*maxClus_counter).second.hits(), (*maxClus_counter).second.detElementIds.size());
         for(el_it= (*maxClus_counter).second.detElementIds.begin(); el_it!=(*maxClus_counter).second.detElementIds.end(); el_it++) printf(" %lli", (*el_it));
         printf("\n");
         */
    }

    // Fill hists
    fillHist( getHistId(layer, elem, MAX_HITS) , maxHits );
    fillHist( getHistId(layer, elem, MAX_HITS_CLUS) , maxHits_clus );
    fillHist( getHistId(layer, elem, MAX_OCC) , maxOcc *100);
    fillHist( getHistId(layer, elem, MAX_CLUS) , maxClus );
    fillHist( getHistId(layer, elem, MAX_CLUS_HITS) , maxClus_hits );
    fillHist( getHistId(layer, elem, MAX_FLU) , maxFlu );
    fillHist( getHistId(layer, elem, MAX_FLU_CLUS) , maxFluClus );

    fillHist( getHistId(layer, elem, MEAN_HITS) ,       h_hits->GetMean());
    fillHist( getHistId(layer, elem, MEAN_OCC) ,        h_occ->GetMean());
    fillHist( getHistId(layer, elem, MEAN_CLUS) ,       h_clus->GetMean());
    fillHist( getHistId(layer, elem, MEAN_FLU) ,        h_flu->GetMean());
    fillHist( getHistId(layer, elem, MEAN_FLU_CLUS) ,   h_flu_clus->GetMean());

    fillHist( getHistId(layer, elem, NPC_HITS) ,       find90pc(h_hits));
    fillHist( getHistId(layer, elem, NPC_OCC) ,        find90pc(h_occ));
    fillHist( getHistId(layer, elem, NPC_CLUS) ,       find90pc(h_clus));
    fillHist( getHistId(layer, elem, NPC_FLU) ,        find90pc(h_flu));
    fillHist( getHistId(layer, elem, NPC_FLU_CLUS) ,   find90pc(h_flu_clus));

    // Delete all sensor_counters
    counters[ layer ].clear();

    // Delete temporaries
    bool save_all = false;
    if (save_all) {
      extra_hists.push_back(h_clus);
      extra_hists.push_back(h_hits);
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    } else {
      if (h_clus) delete h_clus;
      if (h_hits) delete h_hits;
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    }
  } 
}


int PixelBarrelPerEventOccupancy::getChip( float locX, float locY, int layer) {
  // Make all positive
  float locY_orig = locY;
  locX += (float(nChipsX[layer]) /2)* chipLenX[layer] ;
  locY += (float(nChipsY[layer]) /2)* chipLenY[layer] ;
  if (locX<0) {
    std::cerr << "ERROR - locX<0 " << locX << std::endl;
    printf(" nChipsX[layer] %i chipLenX[layer] %.4f locX %.4f\n", nChipsX[layer] , chipLenX[layer] ,locX );
  }
  if (locY<0) {
    std::cerr << "ERROR - locY<0 " << locY << std::endl;
    printf(" nChipsY[layer] %i chipLenY[layer] %.4f locY %.4f\n", nChipsY[layer] , chipLenY[layer] ,locY_orig );
  }
  int row = int( locX / chipLenX[layer] );
  int col = int( locY / chipLenY[layer] );
  if ( row >= nChipsX[layer] ) {
    std::cerr << "ERROR row >= numChipsX[layer] " << row << " " << nChipsX[layer] << std::endl;
  }
  if ( col >= nChipsY[layer] ) {
    std::cerr << "ERROR col >= numChipsY[layer] " << col << " " << nChipsY[layer] << std::endl;
  }

  return (col*nChipsX[layer]) + row;
}

void PixelBarrelPerEventOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event sensor_counters and find max occupancy for last event during "EVENT" step
  if (EventInit(step,k)) return;

  //std::cout << "pixClus_size.size() " << pixClus_size.size()  << std::endl;
  //std::cout << "pixClus_size[0] " << pixClus_size[0]  << std::endl;
  //std::cout << "pixClus_size[k] " << pixClus_size[k]  << std::endl;
  //pixClus_size.Get().Print();

  // enter occupancys in sensor_counters    
  if (step!=UpgPerfAna::INITIAL || pixClus_bec[k]!=0) {
    return;
  }

  int etaModule=pixClus_eta_module[k];
  int phiModule=pixClus_phi_module[k];
  int layer=pixClus_layer[k];
  //int side=pixClus_side[k];

  //std::cout << "  k " << k << " " << pixClus_layer[k] << " pixClus_layer.size() " << pixClus_layer.size() ;
  //std::cout << " GetNTrack() " << GetNTrks() << " pixClus_eta_module.size() " << pixClus_eta_module.size() << " pixClus_eta_module[k] " << pixClus_eta_module[k] << std::endl;
  //" layer " << layer << " nClus_layer.size() " << nClus_layer.size() << std::endl;
  nClus_layer[layer]++;

  ElementId sensorId = getElementId(layer, etaModule, phiModule, 0);

  //printf(" layer %i etaModule %i phiModule %i side %i id %li locX %.1f chip %i chipId %li \n", layer, etaModule, phiModule, side, sensorId, locX, chip, chipId );

  int size=pixClus_size[k]; // size in number of strips
  float locX=pixClus_locX[k];
  float locY=pixClus_locY[k];

  fillHist( getHistId( layer, CLUSTER, ALL_HITS) , size );

  // Find counter for sensor, and create if non-existant
  PerElementCounters::iterator counter = sensor_counters[layer].find( sensorId );
  if (counter == sensor_counters[layer].end()) {
    const SensorInfoBase* layerInfo = sensor->get(moduleType,layer);
    PerEventCounter c= PerEventCounter(0., 100.);
    c.setArea(sensor->get(moduleType,layer)->getFloat("area")); // Factor two removed because now treat side 0 and 1 separatly
    // Gives number of channels for this counter
    // Each counter corresponds to one "segment" of strips (ie 4 of these per module side for short strips, 2 for long)
    // perModule also corresponds to each segment
    // Separate sensor_counters for each side, so no factors of two here
    c.setTotalPixel(layerInfo->getValue("perModule") );
    //c.increaseEvent();
    sensor_counters[ layer ][ sensorId ] = c;
    counter = sensor_counters[ layer ].find( sensorId );
  }
  //printf("Recording cluster %.3f %.3f %.3f %i %llu\n", x,y,z,size,pixClus_detElementId[k]);
  (*counter).second.add(locX,size,pixClus_detElementId[k]);

  // Just assume all hits accociated with this cluster fall into one chip
  // given by locX, locYj
  int nChips = nChipsX[layer] * nChipsY[layer];
  int chip = getChip( locX, locY, layer);
  ElementId chipId = getElementId(layer, etaModule, phiModule, 0, chip);
  counter = chip_counters[layer].find( chipId );
  if (counter == chip_counters[layer].end()) {
    double chip_start = -1;
    double chip_end = -1;
    PerEventCounter c= PerEventCounter(chip_start, chip_end);
    c.setArea(sensor->get(moduleType,layer)->getFloat("area")/nChips); // Factor two removed because now treat side 0 and 1 separatly
    const SensorInfoBase* layerInfo = sensor->get(moduleType,layer);
    c.setTotalPixel(layerInfo->getValue("perModule")/nChips );
    chip_counters[ layer ][ chipId ] = c;
    counter = chip_counters[ layer ].find( chipId );
  }
  (*counter).second.add(locX, size ,pixClus_detElementId[k], pixClus_size[k]);

}



int PixelBarrelPerEventOccupancy::GetNTrks() {
  //std::cout<<"pixClus_n="<<pixClus_x.size()<<std::endl;
  return pixClus_x.size();
}


void PixelBarrelPerEventOccupancy::defineHists() {

  // Just for histogram binning
  int nChannelsPerChip;
  if (moduleType == "pixlayer") {
    nChannelsPerChip=88704;
  } else {
    nChannelsPerChip=256;
  }

  defineHistLayers( LAYER, ALL_CLUS,           "layer_n_clus",                       500,  0,    200e3     );

  defineHistLayers( SENSOR, MAX_CLUS,       "sensor_max_clus",                     300,  0.5,    300.5     );
  defineHistLayers( SENSOR, MAX_HITS,       "sensor_max_hits",                     400,  0.5,    800.5     );
  defineHistLayers( SENSOR, MAX_OCC,        "sensor_max_occ",                      250,  0,    5     );
  defineHistLayers( SENSOR, MAX_FLU,        "sensor_max_flu",                      300,  0,    1.5     );
  defineHistLayers( SENSOR, MAX_FLU_CLUS,   "sensor_max_flu_clus",                 500,  0,    0.5    );
  defineHistLayers( SENSOR, MAX_CLUS_HITS,  "sensor_max_clus_hits",                500,  0.5,    500.5     );
  defineHistLayers( SENSOR, MAX_HITS_CLUS,  "sensor_max_hits_clus",                150,  0.5,    150.5     );

  defineHistLayers( SENSOR, MEAN_CLUS,       "sensor_mean_clus",                    100,  0.5,    100.5      );
  defineHistLayers( SENSOR, MEAN_HITS,       "sensor_mean_hits",                    300,  0.5,    300.5      );
  defineHistLayers( SENSOR, MEAN_OCC,        "sensor_mean_occ",                     200,  0,    1       );
  defineHistLayers( SENSOR, MEAN_FLU,        "sensor_mean_flu",                     200,  0,    0.5     );
  defineHistLayers( SENSOR, MEAN_FLU_CLUS,   "sensor_mean_flu_clus",                200,  0,    0.2);

  defineHistLayers( SENSOR, NPC_CLUS,       "sensor_90pc_clus",                    200,  0.5,    10.5       );
  defineHistLayers( SENSOR, NPC_HITS,       "sensor_90pc_hits",                    200,  0.5,    20.5      );
  defineHistLayers( SENSOR, NPC_OCC,        "sensor_90pc_occ",                     200,  0,    1      );
  defineHistLayers( SENSOR, NPC_FLU,        "sensor_90pc_flu",                     200,  0,    0.01     );
  defineHistLayers( SENSOR, NPC_FLU_CLUS,   "sensor_90pc_flu_clus",                200,  0,    0.005    );

  defineHistLayers( SENSOR, ALL_CLUS,       "sensor_all_clus",                    200,  0.5,    200.5     );
  defineHistLayers( SENSOR, ALL_HITS,       "sensor_all_hits",                    750,  0.5,    750.5     );
  defineHistLayers( SENSOR, ALL_OCC,        "sensor_all_occ",                     100,  0,    5     );
  defineHistLayers( SENSOR, ALL_FLU,        "sensor_all_flu",                     500,  0,    1.0     );
  defineHistLayers( SENSOR, ALL_FLU_CLUS,   "sensor_all_flu_clus",                300,  0,    0.3    );

  defineHistLayers( CLUSTER, ALL_HITS,      "cluster_all_hits",                    400,  0.5,    400.5     );

  float sf_area = (userChipLength>0) ? pow(userChipLength,2) / (20.75*8.4) : 1.0;

  defineHistLayers( CHIP, MAX_CLUS,       "chip_max_clus",                       200,  0.5,    200.5      );
  defineHistLayers( CHIP, MAX_HITS,       "chip_max_hits",                       600,  0.5,   600.5  );
  defineHistLayers( CHIP, MAX_OCC,        "chip_max_occ",                        100,  0,    5 / sf_area     );
  defineHistLayers( CHIP, MAX_FLU,        "chip_max_flu",                        250,  0,    2.5  / sf_area      );
  defineHistLayers( CHIP, MAX_FLU_CLUS,   "chip_max_flu_clus",                   250,   0,    0.5  / sf_area    );
  defineHistLayers( CHIP, MAX_CLUS_HITS,   "chip_max_clus_hits",                  100,  0.5,    100.5  );
  defineHistLayers( CHIP, MAX_HITS_CLUS,   "chip_max_hits_clus",                  100,  0.5,    100.5      );

  defineHistLayers( CHIP, MEAN_CLUS,       "chip_mean_clus",                      100,  0.5,    50.5        );
  defineHistLayers( CHIP, MEAN_HITS,       "chip_mean_hits",                      150,  0.5,    150.5        );
  defineHistLayers( CHIP, MEAN_OCC,        "chip_mean_occ",                       200,  0,    0.5  / sf_area     );
  defineHistLayers( CHIP, MEAN_FLU,        "chip_mean_flu",                       500,  0,    0.5  / sf_area  );
  defineHistLayers( CHIP, MEAN_FLU_CLUS,   "chip_mean_flu_clus",                  200,   0,    0.2  / sf_area    );

  defineHistLayers( CHIP, NPC_CLUS,       "chip_90pc_clus",                      200,  40.5,    5.5         );
  defineHistLayers( CHIP, NPC_HITS,       "chip_90pc_hits",                      200,  0.5,    10.5        );
  defineHistLayers( CHIP, NPC_OCC,        "chip_90pc_occ",                       200,  0,    10 / sf_area        );
  defineHistLayers( CHIP, NPC_FLU,        "chip_90pc_flu",                       200,  0,    0.05 / sf_area     );
  defineHistLayers( CHIP, NPC_FLU_CLUS,   "chip_90pc_flu_clus",                  200,  0,    0.02 / sf_area     );

  defineHistLayers( CHIP, ALL_CLUS,       "chip_all_clus",                      150,  0.5,    150.5      );
  defineHistLayers( CHIP, ALL_HITS,       "chip_all_hits",                      400,  0.5,   400.5      );
  defineHistLayers( CHIP, ALL_OCC,        "chip_all_occ",                       200,  0,    2 / sf_area     );
  defineHistLayers( CHIP, ALL_FLU,        "chip_all_flu",                       200,  0,    2 / sf_area       );
  defineHistLayers( CHIP, ALL_FLU_CLUS,   "chip_all_flu_clus",                  150,   0,    0.3 / sf_area     );
}
