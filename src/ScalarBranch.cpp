#include "InDetUpgradePerformanceAnalysis/ScalarBranch.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"

#include <iostream>

// ======================================================================

template <class T>
ScalarBranch<T>::ScalarBranch(const std::string & _name) 
  : name(_name), id(99999) {
  m_treehouse=TreeHouse::Current();
  if (name!="") {
    id=m_treehouse->GetBranchId(name);
  }
}


template <class T>
ScalarBranch<T>::ScalarBranch(const std::string & _name, T & var) 
  : name(_name), id(99999) {
  m_treehouse=TreeHouse::Current();
  if (name!="") {
    id=m_treehouse->AddBranch(name,var);
    //std::cout<<" register "<<name<<std::endl;
  }
}

template <class T>
const T & ScalarBranch<T>::Get() {
  return * ( (T*) (&m_treehouse->branches[id]) );
}

template <class T>
void ScalarBranch<T>::Fill() {
    //std::cout << "Filling friend tree branch, #entries " << m_treehouse->GetChain()->GetEntries() << std::endl;
  m_treehouse->metabranches[id]->Fill();
}

template <class T>
T ScalarBranch<T>::operator()() {
  return  Get();
}

// explicit instantiation
template class ScalarBranch<int>;
template class ScalarBranch<float>;
