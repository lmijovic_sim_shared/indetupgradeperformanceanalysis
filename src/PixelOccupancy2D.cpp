#include "InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TH2D.h"
#include "InDetUpgradePerformanceAnalysis/SingleAnalysis.h"
#include <iostream>

PixelOccupancy2D::PixelOccupancy2D() :
  OccupancyObservable("PixelOccupancy2D","pixel"),
  pixClus_x("pixClus_x"), pixClus_y("pixClus_y"), pixClus_z("pixClus_z"),
  pixClus_detElementId("pixClus_detElementId")
 {}

void PixelOccupancy2D::Initialize() {
  // setup 2d histograms in z-r
  // create standard histograms
  OccupancyObservable::Initialize();

  if (analysis) {
    // fetch "daughter" observable and occupancies pixel detector elements
    std::vector<std::string> obsnames;
    obsnames.push_back("PixelBarrelOccupancy");
    obsnames.push_back("PixelDiskOccupancy");

    std::cout<<"looking for pixel occupancy information"<<std::endl;
    for (size_t i=0; i<obsnames.size(); ++i) {
      OccupancyObservable * obs = 
        dynamic_cast<OccupancyObservable*>(analysis->GetObservable(obsnames[i]));
      if (obs) {
        std::cout<<"found "<<obsnames[i]<< " " << obs->counters.size() << std::endl;
        LayerIdCounters::const_iterator it=obs->counters.begin();
        for (;it!=obs->counters.end();++it) {
          const Counters & c = it->second;
          //float occ = c.occupancy();
          // create map of element ids.
          ElementList::const_iterator el=c.detElementIds.begin();
          for (;el!=c.detElementIds.end();++el) {
            //printf(" Adding %llu \n", *el);
            omap[*el]=c;
          }
        }
      }
    }
  }
  else {
    std::cout<<"ERROR: analysis not found in  Occupancy2D::Initialize()"<<std::endl;
  }
}

void PixelOccupancy2D::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // enter occupancys in counters
  if (step==UpgPerfAna::INITIAL) {
    //  loop over clusters on track, in order to get x,y,z positions of modules.
    float x=pixClus_x[k];
    float y=pixClus_y[k];
    float z=pixClus_z[k];
    float r=sqrt(x*x+y*y);
    m_histos[CLUS_MAP]->Fill(z,r);
    m_histos[CLUS_MAP_DETAIL]->Fill(z,r);
    ULong64_t id = pixClus_detElementId[k];
    OccupanciesMap::const_iterator occIter = omap.find(id);
    if ( occIter == omap.end()) {
      printf("ERROR: Cannot find element ID in OccupanciesMap for element %llu\n", id);
    }
    const Counters & c  = occIter->second; 
    //printf("PixOcc %llu %.3f %.3f %.3f Occupancy %.8f\n", id, x, y, z, c.occupancy());
    static_cast<TH2D*>(m_histos[OCC_MAP])->Fill(z,r,c.occupancy());
    static_cast<TH2D*>(m_histos[OCC_MAP_DETAIL])->Fill(z,r,c.occupancy());
    float a=c.area();
    if (a>0.) {
      static_cast<TH2D*>(m_histos[FLU_MAP])->Fill(z,r,c.hits()/a);
      static_cast<TH2D*>(m_histos[FLU_MAP_DETAIL])->Fill(z,r,c.hits()/a);
    }
  }
}

void PixelOccupancy2D::Finalize() {
  double pileup=200.;
  double factor=pileup*100.; // pileup * percent

  // normalize occupancies to entries
  // For each bin, multply by factor then divide by number of clusters in that bin
  // We divide by number of clusters because the occupancies are calculated properly in OccupancyObservable
  // but then in order to fill the occupancy map we loop over clusters to dicsover where the modules are
  // Of course this leads to double counting so have to divide by ocupancy
  // "factor" multiplies by 200 to get the desired pileup (assume we've run on 1 MB event) and by 100 to convert occupancy to percent

  //printf("PixelOccupancy2D - Calling NormalizeOccupancyMap for OCC_MAP\n");
  NormalizeOccupancyMap(CLUS_MAP,OCC_MAP,factor);
  //printf("PixelOccupancy2D - Calling NormalizeOccupancyMap for OCC_MAP_DETAIL\n");
  NormalizeOccupancyMap(CLUS_MAP_DETAIL,OCC_MAP_DETAIL,factor);
  // normalize hit denstity to entries
  NormalizeOccupancyMap(CLUS_MAP,FLU_MAP,pileup);
  NormalizeOccupancyMap(CLUS_MAP_DETAIL,FLU_MAP_DETAIL,pileup);
}

int PixelOccupancy2D::GetNTrks() {
  //std::cout<<"pixClus_n="<<pixClus_x.size()<<std::endl;
  return pixClus_x.size();
}
