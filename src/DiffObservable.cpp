#include "InDetUpgradePerformanceAnalysis/DiffObservable.h"
#include "TMath.h"
#include <iostream>

class Binning;

// Standard constructor
// @param varname Name of reco variable
// @param mctruth Name of truth (MC) variable
// @param binning Binning definition. Defaults to eta single sided in 25 bins
// @param fractional If true, calculate (reco-truth)/truth rather than (reco-truth). Default: false.
// @param indexstring Variable in the reco collection giving index of the associated truth particle. Default trk_mc_index
template < class observableT >
DiffObservable< observableT >::DiffObservable(const std::string & varname, const std::string & mctruth, Binning binning, bool fractional, std::string indexstring) :
  ResolutionObservable<observableT>(varname, 0, mctruth, indexstring, binning), 
  m_doubleIndexed(false), m_fractional(fractional)
{
  this->nameobs=varname+"_"+mctruth;
}

// Constructor for btag d3pd (?). Needs fixed.
// template < class observableT >
// DiffObservable< observableT >::DiffObservable(Binning binning, std::string indexstring,std::string indexstring_2, const std::string & varname, const std::string & mctruth, double pt) :
//     ResolutionObservable<observableT>(varname, pt, mctruth, indexstring, binning, "",  indexstring_2)
// {
//   std::cout<<"3rd"<<std::endl;
//   m_doubleIndexed = true;
//   this->nameobs=varname+"_"+mctruth;
// }

template < class observableT >
float DiffObservable< observableT >::EvaluateVariable(int i/*referred to the iteration we are in*/, const std::string & varname){ 

  // Get reco and MC valu
  float reco_val = this->rec_var[i];  
  int mc_index = (m_doubleIndexed) ? this->mc_index_2[this->mc_index[i]] : this->mc_index[i];
  //std::cout << this->mc_index.Name() << " " << mc_index;
  float mc_val = this->MC_var[mc_index];

  float resolution = reco_val - mc_val;

  // Deal with discontinutities at +/- pi in phi variable
  if (varname=="trk_phi") resolution = wrap_pi(resolution);

  // If a "fractional" resolution (e.g. q/pt) divide by mc value
  // NE 12/8/14 - Use fabs(mc_val) here are otherwise the residual distribution tends to 
  // be very one sided.
  if (m_fractional) resolution = resolution / fabs(mc_val);

  return resolution;
}
template class DiffObservable<float>;


