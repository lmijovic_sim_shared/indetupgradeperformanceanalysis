#include "InDetUpgradePerformanceAnalysis/Addition.h"

#include <iostream>

using namespace std;


template < class observableT >
Addition<observableT>::Addition(const std::string & out,
					  const std::string & in,
					  const std::string & in2)
  : Calculator<observableT>("sum",in,out,in2)
{
}

template < class observableT >
void Addition<observableT>::Calc(UpgPerfAna::AnalysisStep /*step*/, int i) 
{

  size_t index = i;

  if (index==0) {
  // the if condition should go away, as we have to call these even if there are no tracks!
    //this->outVec.clear();
    this->outVec.resize(this->in_var.size());
    //cout << " Addition::Calc " << this->in_var.size() << endl;
  }


  // here the calculation takes place
  this->outVec[index]=this->in_var[index]+this->in2_var[index];
}


template class Addition<float>;
template class Addition<int>;
