#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include <vector>
#include "TChain.h"
#include "TTree.h"
#include <string>
#include <cmath>
#include <iostream>

using namespace std;

StandardCuts::StandardCuts() :
  Cuts("StandardCuts"),
  eta("trk_eta"),
  pt("trk_pt"),
  probability("trk_mc_probability"),
  rec_d0("trk_d0_wrtPV"),
  rec_z0("trk_z0_wrtPV"),
  barcode("trk_mc_barcode"),
  nPixHits("trk_nPixHits"),
  nSCTHits("trk_nSCTHits"),
  nTRTHits("trk_nTRTHits"),
  genPt("mc_gen_pt"),
  motherType("mc_gen_type") // *AS* why "mothertype" !!!
{

  ptcut = 900;
  barcodeMax = 200000;
  barcodeMin = 0;
  ///  probcut = 0.5;
  d0cut = 1.0;
  z0cut = 150.0;
  pixSCThits = 7;
  etaTRTcut = 2.0;
  TRThitscut = 1;
  // *AS* TODO: ommit pt and pdgCode cut here !
  genPtMin = 14000;
  genPtMax = 16000; ///this and the previous are useful to identify the correct set of particles in multiparticle samples
  motherID = 13; ///11 electrons, 13 muons, 211 pions
}

bool StandardCuts::AcceptEvent(){
  return true;
}

bool StandardCuts::AcceptTrack(int i){
  //  cout << "StandardCuts::AcceptTrack("<<i<<")=" << endl;
  bool goodTrack = true;
  // return true;
  if ( pt[i] <= ptcut) goodTrack = false;
  //  cout <<" pt "<<pt[i]<< " "<<ptcut<<" "<<goodTrack<< endl;
  // *AS*  if ( (barcode[i] >= barcodeMax) || (barcode[i] == barcodeMin)) goodTrack = false; 
  // cout <<" barcode "<<barcode[i]<< " "<<barcodeMax<<" "<<goodTrack<< endl;
  // cout <<" barcode "<<barcode[i]<< " "<<barcodeMin<<" "<<goodTrack<< endl;
  ///  if ( probability[i] <= probcut) goodTrack = false;
  // cout <<" probability "<<probability[i]<< " "<<probcut<<" "<<goodTrack<< endl;
  if ( fabsf(rec_d0[i]) >= d0cut) goodTrack = false;
  // cout <<" rec_d0 "<<rec_d0[i]<< " "<<d0cut<<" "<<goodTrack<< endl;
  if ( fabsf(rec_z0[i]) >= z0cut) goodTrack = false;
  // cout <<" rec_z0 "<<rec_z0[i]<< " "<<z0cut<<" "<<goodTrack<< endl;

  int nPixSCTHits = nPixHits[i]+nSCTHits[i];
  if (nPixSCTHits < pixSCThits) goodTrack = false;

  // *AS* check this cut - does not look right !!!!
  //  if (( eta[i] < etaTRTcut) && ( nTRTHits[i] < TRThitscut)) goodTrack = false;
   ///Chiara:eliminated for Utopia studies  if (( fabsf(eta[i]) < etaTRTcut) && ( nTRTHits[i] < TRThitscut)) goodTrack = false;
  // cout <<" "<<goodTrack<< endl;

  /*
  if((genPt[i] <= genPtMin) || (genPt[i] >= genPtMax)) return goodTrack = false;

  if(motherType[i] != motherID && motherType[i] != -motherID) return goodTrack = false;
  */
  return goodTrack;
}
