#include "InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TVector3.h"
#include "TH2D.h"


SCTBarrelOccupancy::SCTBarrelOccupancy() :
  OccupancyObservable("SCTBarrelOccupancy","sctlayer"),nlayer(0),
  sctClus_x("sctClus_x"), sctClus_y("sctClus_y"), sctClus_z("sctClus_z"),
  sctClus_size("sctClus_size"), sctClus_layer("sctClus_layer"), 
  sctClus_bec("sctClus_bec"), sctClus_eta_module("sctClus_eta_module"),
  sctClus_detElementId("sctClus_detElementId"),
  mc_gen_type("mc_gen_type"), 
  mc_gen_eta("mc_gen_eta"), mc_gen_phi("mc_gen_phi"),
  mc_gen_barcode("mc_gen_barcode"), mc_charge("mc_charge"),
  doubleStripLength(false)
 {}

void SCTBarrelOccupancy::Initialize() {

  // create standard histograms
  OccupancyObservable::Initialize();

  // prepare counters for each etaID and layer
  if (sensor != 0 ) {
    nlayer = sensor->getValue("nsctlayer");
    for (int i=0; i<nlayer; ++i) {

      std::cout<<"\n\nlayer i="<<i<<std::endl;
      
      const SensorInfoBase* layer = sensor->get(moduleType,i);
      int nmodule = layer->getValue("nmodule") * layer->getValue("nsegment");
      float area  = layer->getFloat("area");
      std::cout<<"nmodule: "<<nmodule<<std::endl;
      std::cout<<"area: "<<area<<std::endl;
      
      if (doubleStripLength && i<3) {
	nmodule /= 2;
	area *= 2;
      }
      
      int nmax=nmodule/2;
      int nmin=-nmax;
      bool hasZero=(nmodule%2!=0);

      std::cout<<"nmin="<<nmin<<", nmax="<<nmax<<std::endl;
      
      // Loop over number of sensors along Z (=nnodule*nsegment)
      for (int j=nmin; j<=nmax; ++j) {
        if (j==0 && !hasZero ) continue;
	std::cout<<"\ncreating new counter"<<std::endl;
	
        Counters c=Counters();
        // Set area of each sensor, summed around phi and accounting for both sides. 
        // nski gives the number of modules around phi. There are two sides.
        c.setArea(layer->getValue("nski")*area*2); // TODO: remove hardcoded 2
	std::cout<<"setting area to "<<layer->getValue("nski")*area*2<<std::endl;
	
        // Set the number of strips per sensor.
        // perModuleEta gives number of strips per sensor summed over phi and accounting for both sides
        //std::cout << __FUNCTION__ << " perModuleEta = " << layer->getValue("perModuleEta") << std::endl;
	int totalpixel=layer->getValue("perModuleEta");
	std::cout<<"totalpixel="<<totalpixel<<std::endl;
	
        c.setTotalPixel(totalpixel);
        counters[ std::make_pair(i,j) ] = c;
      }
    }
  }
}

void SCTBarrelOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event counters during "EVENT" step
  if (OccupancyObservable::EventInit(step,k)) return;
  // enter occupancys in counters    
  if (step==UpgPerfAna::INITIAL && sctClus_bec[k]==0){  // barrel only
    int module=sctClus_eta_module[k];    
    int layer=sctClus_layer[k];
    if (doubleStripLength && layer<3) {
      if (module>0) module = (module+1)/2;
      else module = (module-1)/2;
    }

    LMPair layer_module = std::make_pair(layer,module);
    float x=sctClus_x[k];
    float y=sctClus_y[k];
    float z=sctClus_z[k];
    float s=sctClus_size[k];
    float r=sqrt(x*x+y*y);

    counters[layer_module].add(x,y,z,s,sctClus_detElementId[k]);
    // fill cluster and hit maps
    m_histos[CLUS_MAP]->Fill(z,r);
    static_cast<TH2*>(m_histos[HIT_MAP])->Fill(z,r,s);
    // Cluster size study
    ClusterTruthType type = clusterTruthType(x, y, z);
    m_histos[CLUS_SIZE_ALL]->Fill(s);
    if (type==ClusterTruthType_PRIMARY) {
      m_histos[CLUS_SIZE_PRIM]->Fill(s);
    } else if (type==ClusterTruthType_SECONDARY) {
      m_histos[CLUS_SIZE_SEC]->Fill(s);
    } else {
      m_histos[CLUS_SIZE_UNMATCHED]->Fill(s);
    }
  }
}

OccupancyObservable::ClusterTruthType SCTBarrelOccupancy::clusterTruthType(float x, float y, float z) {
  TVector3 v_clus(x,y,z);
  float minDr=100;
  float minIndex=-1;
  // Loop over gen particles and look for one closest in dR
  for (size_t iMc=0; iMc<mc_gen_eta.size(); iMc++) {
    if (mc_charge[iMc]==0) continue;
    float eta_mc = mc_gen_eta[iMc];
    float phi_mc = mc_gen_phi[iMc];
    TVector3 v_mc; v_mc.SetPtEtaPhi(1, eta_mc, phi_mc);
    float delta_r = v_mc.DeltaR(v_clus);
    if (delta_r<minDr) {
      minIndex=iMc;
      minDr=delta_r;
    }
  }
  // Apply cut to minDr
  //std::cout << "minDr " << minDr << " phi " << phi << " theta " << theta << " eta " << eta << " index " << minIndex  << std::endl;
  if (  (minIndex==-1)) return ClusterTruthType_UNMATCHED;
  int barcode = mc_gen_barcode[minIndex];
  //std::cout << "minDr " << minDr << " index " << minIndex << " barcode " << barcode << std::endl;
  //if ( (fabs(minDr)>0.5) || (minIndex==-1)) return ClusterTruthType_UNMATCHED;
  if ( (fabs(minDr)>0.5) ) return ClusterTruthType_UNMATCHED;
  if (barcode<200e3) return ClusterTruthType_PRIMARY;
  else return ClusterTruthType_SECONDARY;

}

void SCTBarrelOccupancy::Finalize() {
  float pileup=200.;
  float factor=pileup*100.; 

  //std::cout<<"SCTBarrelOccupancy::Finalize()"<<std::endl;

  //  create occupancy graphs
  TMultiGraph *mg = CreateMultiGraph("occ_%s_mg",moduleType);
  TMultiGraph *mgz = CreateMultiGraph("occ_%s_mgz",moduleType);
  TMultiGraph *hits_mg = CreateMultiGraph("hit_%s_mg",moduleType);
  TMultiGraph *hits_mgz = CreateMultiGraph("hit_%s_mgz",moduleType);
  TMultiGraph *clus_mg = CreateMultiGraph("clus_%s_mg",moduleType);
  TMultiGraph *clus_mgz = CreateMultiGraph("clus_%s_mgz",moduleType);
  TMultiGraph *size_mg = CreateMultiGraph("size_%s_mg",moduleType);
  TMultiGraph *size_mgz = CreateMultiGraph("size_%s_mgz",moduleType);
  TMultiGraph *flu_mg = 0;
  TMultiGraph *flu_mgz = 0;

  if ( counters[std::make_pair(0,1)].area() >0.) {
    flu_mg = CreateMultiGraph("flu_%s_mg",moduleType);
    flu_mgz = CreateMultiGraph("flu_%s_mgz",moduleType);
  }

  for (int i=0; i<nlayer; ++i ) {
    const SensorInfoBase* layer = sensor->get(moduleType,i);

    int nmodule = layer->getValue("nmodule") * layer->getValue("nsegment");
    if (doubleStripLength && i<3) nmodule /= 2;
    
    int nmax=nmodule/2;
    int nmin=-nmax;
    bool hasZero=(nmodule%2!=0);

    TGraphErrors * g = CreateGraph(nmodule,"occ_%s_%d",moduleType,i);
    TGraphErrors * gz = CreateGraph(nmodule,"occ_z_%s_%d",moduleType,i);
    TGraphErrors * hits = CreateGraph(nmodule,"hit_%s_%d",moduleType,i);
    TGraphErrors * hits_z = CreateGraph(nmodule,"hit_z_%s_%d",moduleType,i);
    TGraphErrors * clus = CreateGraph(nmodule,"clus_%s_%d",moduleType,i);
    TGraphErrors * clus_z = CreateGraph(nmodule,"clus_z_%s_%d",moduleType,i);
    TGraphErrors * size = CreateGraph(nmodule,"size_%s_%d",moduleType,i);
    TGraphErrors * size_z = CreateGraph(nmodule,"size_z_%s_%d",moduleType,i);
    TGraphErrors * flu = 0;
    TGraphErrors * flu_z = 0;
    if ( counters[std::make_pair(i,1)].area() >0.) {
      flu = CreateGraph(nmodule,"flu_%s_%d",moduleType,i);
      flu_z = CreateGraph(nmodule,"flu_z_%s_%d",moduleType,i);
    }

    // k index is "etaModule"
    for (int k=nmin,j=0; k<=nmax; ++k) {
      if (k==0 && !hasZero ) continue;
      Counters & c=counters.find(std::make_pair(i,k))->second;
      float z=c.z();
      float o=factor*c.occupancy();
        //std::cout << __FUNCTION__ << " occupancy = " << c.occupancy() << std::endl;
        //std::cout << __FUNCTION__ << " hits = " << c.hits() << std::endl;
        //std::cout << __FUNCTION__ << " total_pux = " << c.totalPixel() << std::endl;
      float oerr=factor*c.occupancyError();
      float h=pileup*c.hits();
      float herr=pileup*c.hitsError();
      float cl=pileup*c.cluster();
      float clerr=pileup*c.clusterError();
      float a=c.area();

      g->SetPoint(j,k,o);
      g->SetPointError(j,0,oerr);
      gz->SetPoint(j,z,o);
      gz->SetPointError(j,0,oerr);
      hits->SetPoint(j,k,h);
      hits->SetPointError(j,0,herr);
      hits_z->SetPoint(j,z,h);
      hits_z->SetPointError(j,0,herr);
      clus->SetPoint(j,k,cl);
      clus->SetPointError(j,0,clerr);
      clus_z->SetPoint(j,z,cl);
      clus_z->SetPointError(j,0,clerr);
      size->SetPoint(j,k,h/cl);
      size->SetPointError(j,0,herr/cl);
      size_z->SetPoint(j,z,h/cl);
      size_z->SetPointError(j,0,herr/cl);
      if (a>0.) {
        flu->SetPoint(j,k,h/a);
        flu_z->SetPoint(j,z,h/a);
      }
      ++j;
    }
    mg->Add(g,"lp");
    mgz->Add(gz,"lp");
    hits_mg->Add(hits,"lp");
    hits_mgz->Add(hits_z,"lp");
    clus_mg->Add(clus,"lp");
    clus_mgz->Add(clus_z,"lp");
    size_mg->Add(size,"lp");
    size_mgz->Add(size_z,"lp");
    if ( counters[std::make_pair(i,1)].area() >0.) {
      flu_mg->Add(flu,"lp");
      flu_mgz->Add(flu_z,"lp");
    }
  }
}

int SCTBarrelOccupancy::GetNTrks() {
  //std::cout<<"sctClus_n="<<sctClus_x.size()<<std::endl;
  return sctClus_x.size();
}
