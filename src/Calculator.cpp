#include "InDetUpgradePerformanceAnalysis/Calculator.h"

#include <iostream>

using namespace std;


template < class observableT >
Calculator<observableT>::Calculator(const std::string & op,
				    const std::string & in,
				    const std::string & out,
				    const std::string & in2)
  : Observable(op+"_"+out),
    in_var(in),out_var(out,outVec),in2_var(in2)
{
}

template < class observableT >
int Calculator<observableT>::GetNTrks(){
    return in_var.size();
}


template < class observableT >
void Calculator<observableT>::Initialize()
{}

template < class observableT >
void Calculator<observableT>::Analyze(UpgPerfAna::AnalysisStep step,int i) 
{
  Calc(step,i);
}

template < class observableT >
void Calculator<observableT>::StorePlots()
{}



template class Calculator<float>;
template class Calculator<int>;
