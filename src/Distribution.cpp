#include "InDetUpgradePerformanceAnalysis/Distribution.h"
#include <iostream>

using namespace std;

template <class T> Distribution<T>::Distribution(const std::string & name1,int nbin, float xmin, float xmax)
    : Observable(name1),
    m_nbinx(nbin), m_xmin(xmin), m_xmax(xmax),
    m_nbiny(0), m_ymin(0.), m_ymax(1.),
    xvar(name1),yvar(""),
    twoD(false)
{
}
template <class T> Distribution<T>::Distribution(const std::string & name1,int nbinx, float xmin, float xmax, 
        const std::string & name2,int nbiny, float ymin, float ymax)
: Observable(name1+"_"+name2),
    m_nbinx(nbinx), m_xmin(xmin), m_xmax(xmax),
    m_nbiny(nbiny), m_ymin(ymin), m_ymax(ymax),
    xvar(name1),yvar(name2),
    twoD(true)
{
}

template<class T> void Distribution<T>::Initialize()
{
    m_histos.push_back(new TH1D(xvar.Name().c_str(),xvar.Name().c_str(),m_nbinx,m_xmin,m_xmax));
}

template<class T> void Distribution<T>::Analyze(UpgPerfAna::AnalysisStep step, int j)
{
    if (step==UpgPerfAna::EVENT) return;
    /*
       if (j==0 && xvar.Name()=="trk_nAllHits") {
       cout << "insert "<<xvar.size()<< endl;
       cout << " " << &(xvar.Get()) <<endl;
       cout << xvar[j] << endl;
       }
       */
    m_histos[0]->Fill(xvar[j]);
    // *AS* TODO : add support for 2D distribution possibly add second 'int' template argument to class
}

template<class T> int Distribution<T>::GetNTrks()
{
    return xvar.size();
}

template<class T> void Distribution<T>::Finalize()
{
}

template<class T> void Distribution<T>::StorePlots()
{
    for (size_t i=0; i<m_histos.size(); i++) {
        m_histos[i]->Write();
    }
}

template<class T> Distribution<T>::~Distribution()
{
    for (size_t i=0; i<m_histos.size(); i++) {
        delete m_histos[i];
    }
    m_histos.clear();
}

template class Distribution<int>;
template class Distribution<float>;
