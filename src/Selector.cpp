#include "InDetUpgradePerformanceAnalysis/Selector.h"

#include <sstream>

std::string stri(int value) {
  std::string s;
  std::stringstream sstr;
  sstr<<value;
  sstr>>s;
  return s;
}


template <class T>
Selector<T>::Selector(const std::string & varname, int value)
  : Cuts(varname+"=="+stri(value)), var(varname), sel(value)
{}

template <class T>
bool Selector<T>::AcceptTrack(int i)
{
  return var[i]==sel;
}

template <class T>
AbsSelector<T>::AbsSelector(const std::string & varname, int value)
  : Cuts("abs("+varname+")=="+stri(value)), var(varname), sel(value)
{}

template <class T>
bool AbsSelector<T>::AcceptTrack(int i)
{
  return ( (var[i]==sel) ||  (var[i]==-1.0*sel) );
}

template class Selector<int>;
template class Selector<float>;
template class AbsSelector<int>;
template class AbsSelector<float>;
