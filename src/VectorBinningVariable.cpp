#include "InDetUpgradePerformanceAnalysis/VectorBinningVariable.h"

template <class T> VectorBinningVariable<T>::VectorBinningVariable(std::string branch_name) :
    m_var(branch_name) {
    }

template <class T> float VectorBinningVariable<T>::Value(int index) {
    return m_var[index];
}

template class VectorBinningVariable<int>;
template class VectorBinningVariable<float>;
