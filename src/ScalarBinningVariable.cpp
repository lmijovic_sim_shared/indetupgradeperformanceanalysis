#include "InDetUpgradePerformanceAnalysis/ScalarBinningVariable.h"

template <class T> ScalarBinningVariable<T>::ScalarBinningVariable(std::string branch_name) :
    m_var(branch_name) {
    }

template <class T> float ScalarBinningVariable<T>::Value(int ) {
    return m_var();
}

template class ScalarBinningVariable<int>;
template class ScalarBinningVariable<float>;
