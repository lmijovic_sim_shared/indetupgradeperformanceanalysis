#!/usr/bin/env python

import InDetUpgradePerformanceAnalysis as UpgPerfAna
from InDetUpgradePerformanceAnalysis import *

class MyStandardCuts(AllCuts):
    def __init__(self):
        AllCuts.__init__(self)
        self.Add(MinCut(float)("trk_pt",900))  # MeV
        self.Add(RangeCut(float)("trk_d0_wrtPV",-1.0,1.0)) # mm
        self.Add(RangeCut(float)("trk_z0_wrtPV",-150.0,150.0)) # mm
        self.Add(MinimumHitsCut(9,"trk_nPixHits","trk_nSCTHits"))
#        self.Add(RangeCut(int)("trk_mc_barcode",1,200000)) # to be moved !!
        

class GeneratorCuts(AllCuts):
    def __init__(self, pdg, pt, prob):
        AllCuts.__init__(self)
        self.Add(MinCut(int)("trk_mc_index",0))
        self.Add(AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdg))
        self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",pt-1000.,pt+1000.))
        self.Add(MinCut(float)("trk_mc_probability", prob))
        self.Add(MyStandardCuts())

class SignalSelection(AllCuts):
    def __init__(self, pdg, pt):
        AllCuts.__init__(self)
        self.Add(AbsSelector("mc_gen_type",pdg))
        self.Add(RangeCut(float)("mc_gen_pt",pt-1000.,pt+1000.))
        self.Add(RangeCut(int)("mc_gen_barcode",1,200000))
        self.Add(RangeCut(float)("mc_gen_eta",-2.5,2.5))
        self.Add(RangeCut(float)("mc_perigee_d0",-1.0,1.0)) # mm
        self.Add(RangeCut(float)("mc_perigee_z0",-150.0,150.0)) # mm
        

class SignalTrackStandardSelection(AllCuts):
    def __init__(self, pdg, pt):
        AllCuts.__init__(self)
        self.Add(MinCut(int)("trk_mc_index",0))
        self.Add(AbsSelectorWithIndex("mc_gen_type","trk_mc_index",pdg))
        self.Add(RangeCutWithIndex("mc_gen_pt","trk_mc_index",pt-1000.,pt+1000.))
        self.Add(MyStandardCuts())


class ResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(GeneratorCuts(pdg,pt,prob))
        self.Add(DiffObservable("trk_d0","mc_perigee_d0"))
        self.Add(DiffObservable("trk_z0","mc_perigee_z0"))
        self.Add(DiffObservable("trk_phi","mc_perigee_phi"))
        self.Add(DiffObservable("trk_theta","mc_perigee_theta"))
        self.Add(DiffObservable("trk_pt","mc_gen_pt"))
        self.Add(Z0SinThetaObs("trk_z0", "mc_perigee_z0", "mc_perigee_theta"))
        self.Add(QOverPtObs("trk_qoverp", "mc_perigee_qoverp", "mc_perigee_theta"))

class EfficiencyObservables(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(SignalSelection(pdg,pt))
        eff=Efficiency()
        eff.SetCuts(MyStandardCuts())
        self.Add(eff)

                     
class FakeRateObservables(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(SignalTrackStandardSelection(pdg,pt))
        self.Add(FakeRate())

# TODO: make command line option
pdgCode=13
minProb=0.5
outfile="res.root"

ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
ana.SetChain('InDetTrackTree')
ana.SetMaxEvents(300)
#ana.AddFile('utopia_muon_23.txt')
ana.AddFile('testfiles.txt')
###ana.Test(None)

sa=SingleAnalysis("Resolutions",TreeHouse(ana.GetChain()))
sa.AddStep(UpgPerfAna.CONTROL)
sa.Add(ResolutionObservables(pdgCode,5000.,minProb,"pt5","muon pt=5GeV"))
sa.Add(ResolutionObservables(pdgCode,15000.,minProb,"pt15","muon pt=15GeV"))
sa.Add(ResolutionObservables(pdgCode,50000.,minProb,"pt50","muon pt=50GeV"))
sa.Add(ResolutionObservables(pdgCode,100000.,minProb,"pt100","muon pt=100GeV"))
ana.Add(sa)

sa=SingleAnalysis("EfficienciesFakes",TreeHouse(ana.GetChain()))
sa.Add(EfficiencyObservables(pdgCode,5000.,"pt5","muon pt=5GeV"))
sa.Add(EfficiencyObservables(pdgCode,15000.,"pt15","muon pt=15GeV"))
sa.Add(EfficiencyObservables(pdgCode,50000.,"pt50","muon pt=50GeV"))
sa.Add(EfficiencyObservables(pdgCode,100000.,"pt100","muon pt=100GeV"))
sa.Add(FakeRateObservables(pdgCode,5000.,"pt5","muon pt=5GeV"))
sa.Add(FakeRateObservables(pdgCode,15000.,"pt15","muon pt=15GeV"))
sa.Add(FakeRateObservables(pdgCode,50000.,"pt50","muon pt=50GeV"))
sa.Add(FakeRateObservables(pdgCode,100000.,"pt100","muon pt=100GeV"))
ana.Add(sa)

ana.Initialize()
ana.Execute()
ana.Finalize()
