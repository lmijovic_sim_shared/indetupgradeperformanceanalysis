#!/usr/bin/env python

import os
import optparse
usage = """%prog [options]

'%prog --help' prints a summary of the options

Example plot generation
'%prog --path=/Users/dreas/atlas/python/dataset/ --outfile=out.root'

'%prog --path=/castor/cern.ch/grid/atlas/atlasgroupdisk/det-slhc/user/silviam/group/user.silviam.group.det-slhc.mc11_14TeV.4907443.multi_muons.reco.ESD.LowPtMinbiasOnly_pileup_23.val2.110923140607_der1317307621/'
--pattern=D3PD.root

'%prog --path=/eos/atlas/atlasgroupdisk/det-slhc/user/silviam/group/user.silviam.group.det-slhc.mc11_14TeV.4907443.multi_muons.reco.ESD.LowPtMinbiasOnly_pileup_23.val2.110923140607_der1317307621/ --pattern=D3PD.root  --outfile=utopia_muon_23.root --maxEvents=300'
"""

optP = optparse.OptionParser(usage=usage,conflict_handler="resolve")

optP.add_option('-n', action='store_const', const=True, dest='dryrun',  default=False,
                help='dry run')

optP.add_option('--path', action='store', dest='path',  default='./',
                type='string',    help='use path for looking for root files')
optP.add_option('--outfile', action='store', dest='outfile',
                default='out.root', type='string', help='output file name  (defaut:out.root)')
optP.add_option('--pattern', action='store', dest='pattern',  default='.root',
                type='string',    help='[not supported yet]')
optP.add_option('--maxEvents', action='store', dest='maxEvents',  default=-1,
                type='string',    help='limit analysis to a given number of events')

# parse options
options,args = optP.parse_args()

#path='/Users/dreas/atlas/python/dataset/'
#infiles=['group.det-slhc.26678_000701.EXT3._00001.TRK_D3PD.root']

isCastor=False
prefix=""
if (options.path.find('castor')>=0) :
    from subprocess import Popen,PIPE
    p=Popen("nsls "+options.path,shell=True,stdout=PIPE).stdout
    infiles=p.readlines()
    p.close()
    isCastor=True
    prefix="root://castoratlas/"
elif (options.path.find('/eos/')>=0) :
    print 'eos'
    from subprocess import Popen,PIPE
    # check for 'eos' command (macro)
    p=Popen("/afs/cern.ch/project/eos/installation/0.1.0-22d/bin/eos.select ls "+options.path,shell=True,stdout=PIPE).stdout
    infiles=p.readlines()
    p.close()
    isCastor=True
    prefix="root://eosatlas/"
else:
    infiles=os.listdir(options.path)

executable='UpgradePerformanceAnalysis.exe'

outfile='out.root'

tempfile=os.tmpnam()

f=file(tempfile,'w')
for ifn in infiles:
    if ifn.find(options.pattern)>0:
        f.write(prefix+options.path+ifn.strip()+'\n')
f.close()

cmd=executable+' '+tempfile+' '+options.outfile
if options.maxEvents!=-1:
    cmd+=' '+options.maxEvents
print cmd
if not options.dryrun:
    os.system(cmd)
