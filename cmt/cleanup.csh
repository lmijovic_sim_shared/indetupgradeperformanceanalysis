# echo "cleanup InDetUpgradePerformanceAnalysis InDetUpgradePerformanceAnalysis-r734676 in /afs/cern.ch/work/l/lmijovic/private/files/work/atlas/upgrade/occupancy_studies/InDetUpgradePerformanceAnalysis_Aug2016_athena/InnerDetector/InDetPerformance"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetUpgradePerformanceAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetUpgradePerformanceAnalysistempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetUpgradePerformanceAnalysis -version=InDetUpgradePerformanceAnalysis-r734676 -path=/afs/cern.ch/work/l/lmijovic/private/files/work/atlas/upgrade/occupancy_studies/InDetUpgradePerformanceAnalysis_Aug2016_athena/InnerDetector/InDetPerformance  $* >${cmtInDetUpgradePerformanceAnalysistempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetUpgradePerformanceAnalysis -version=InDetUpgradePerformanceAnalysis-r734676 -path=/afs/cern.ch/work/l/lmijovic/private/files/work/atlas/upgrade/occupancy_studies/InDetUpgradePerformanceAnalysis_Aug2016_athena/InnerDetector/InDetPerformance  $* >${cmtInDetUpgradePerformanceAnalysistempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetUpgradePerformanceAnalysistempfile}
  unset cmtInDetUpgradePerformanceAnalysistempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtInDetUpgradePerformanceAnalysistempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtInDetUpgradePerformanceAnalysistempfile}
unset cmtInDetUpgradePerformanceAnalysistempfile
exit $cmtcleanupstatus

